package com.fr.adaming.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(Exception.class) ;

	public ExceptionUtil() {

	}

	public static void log(Exception e) {
		LOGGER.error("Exception : ", e);
	}
}
