package com.fr.adaming.soap.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.fr.adaming.wsdl.GetClientsRequest;
import com.fr.adaming.wsdl.GetClientsResponse;
import com.fr.adaming.wsdl.GetHistoricRequest;
import com.fr.adaming.wsdl.GetHistoricResponse;
import com.fr.adaming.wsdl.GetInstitutionRequest;
import com.fr.adaming.wsdl.GetInstitutionResponse;
import com.fr.adaming.wsdl.GetOverviewRequest;
import com.fr.adaming.wsdl.GetOverviewResponse;

public class SoapWsClientConfig extends WebServiceGatewaySupport {
	
	
	public GetInstitutionResponse getInstitution() {

		GetInstitutionRequest request = new GetInstitutionRequest();

		GetInstitutionResponse response = (GetInstitutionResponse) getWebServiceTemplate().marshalSendAndReceive(
				SoapUtils.URLSoap + "/institution", request, new SoapActionCallback("http://soap.adaming.fr.com"));

		return response;
	}

	public GetClientsResponse getClients() {
		GetClientsRequest request = new GetClientsRequest();
		GetClientsResponse response = (GetClientsResponse) getWebServiceTemplate().marshalSendAndReceive(
				SoapUtils.URLSoap + "/clients", request, new SoapActionCallback("http://soap.adaming.fr.com"));
		return response;
	}

	public GetHistoricResponse getHistoric(String idProcess) {
		GetHistoricRequest request = new GetHistoricRequest();
		request.setProcessId(idProcess);
		GetHistoricResponse response = (GetHistoricResponse) getWebServiceTemplate().marshalSendAndReceive(
				SoapUtils.URLSoap + "/historic", request, new SoapActionCallback("http://soap.adaming.fr.com"));

		return response;
	}

	public GetOverviewResponse getOverview() {
		GetOverviewRequest request = new GetOverviewRequest();
		// request.setProcessId(idProcess)
		GetOverviewResponse response = (GetOverviewResponse) getWebServiceTemplate().marshalSendAndReceive(
				SoapUtils.URLSoap + "/overview", request, new SoapActionCallback("http://soap.adaming.fr.com"));

		return response;
	}

}
