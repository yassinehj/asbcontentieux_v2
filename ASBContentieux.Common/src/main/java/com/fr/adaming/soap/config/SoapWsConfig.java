package com.fr.adaming.soap.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;


@Configuration
public class SoapWsConfig {
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage>
		// specified in
		// pom.xml
		marshaller.setContextPath("com.fr.adaming.wsdl");
		return marshaller;
	}
	
	@Bean
	public SoapWsClientConfig SoapWsClientConfig(Jaxb2Marshaller marshaller) {
		SoapWsClientConfig client = new SoapWsClientConfig();
		client.setDefaultUri(SoapUtils.URLSoap);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	
}
