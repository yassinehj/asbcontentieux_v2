package com.fr.adaming.soap.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public interface SoapUtils {

	public static final String URLSoap = "http://163.172.101.130:8181/ASBRecouvrement.Web/ws";
	public static final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SoapWsConfig.class);
	public static final SoapWsClientConfig quoteClient = context.getBean(SoapWsClientConfig.class);
}
