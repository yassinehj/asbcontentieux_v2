package com.fr.adaming.utils;

import org.slf4j.Logger;


import org.slf4j.LoggerFactory;

public class LoggerUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggerUtil.class);
	public static void log(String e){
		LOGGER.info(e);
	}
}