package com.fr.adaming.utils;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.client.util.FileUtils;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;

/**
 * 
 * @author azaaboub
 *
 */

public class AlfrescoOpenCmis {
	
	private static Session session;
	private static Folder folder;
	
	public static Session getSession() {
		
		if (session == null) {
		
		// The default factory implementation
		SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
		
		Map<String,String> parameter = new HashMap<String,String>();
		
		// ECM user credentials
		parameter.put(SessionParameter.USER, "taissa");
		
		parameter.put(SessionParameter.PASSWORD,"TwNokx!g");
		
		// ECM connection settings
		parameter.put(SessionParameter.ATOMPUB_URL,"http://51.38.203.218:8080/alfresco/api/-default-/public/cmis/versions/1.0/atom");
		
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		
		// Get a list of repositories retrieved by the atompub CMIS endpoint (for Alfresco there is only one element)
		List<Repository> s = sessionFactory.getRepositories(parameter);
		
		session = s.get(0).createSession();
		
		
		// Get some repository info
		System.out.println("Repository Name: "+session.getRepositoryInfo().getName());
		System.out.println("Repository ID: "+session.getRepositoryInfo().getId());
		System.out.println("CMIS Version: "+session.getRepositoryInfo().getCmisVersion());
		
		}
		return session;
		
	}
	
	public static Folder createFolderIfNotExist() {
		if (folder == null) {
			String query = "SELECT * FROM cmis:folder WHERE cmis:name LIKE 'ASB_contentieux_V2'";
			ItemIterable<QueryResult> q = getSession().query(query, false);
			if (q != null) {
				for (QueryResult qr : q) {
					folder = FileUtils.getFolder(qr.getPropertyByQueryName("cmis:objectId").getFirstValue().toString(),
							getSession());
					break;
				}
			}

		}
		return folder;
	}
	
	

	public static Document createFolder(FileInputStream fileInputStream, String name, Long length,String mimeTypes) {
		Folder Folder = createFolderIfNotExist();

		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
		properties.put(PropertyIds.NAME, name);

		ContentStream contentStream = new ContentStreamImpl(name, BigInteger.valueOf(length), mimeTypes,
				fileInputStream);

		return Folder.createDocument(properties, contentStream, VersioningState.MAJOR);
	}

	
	
	

}
