package com.fr.adaming.utils;

import java.io.File;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EnvoiMailUtil {

	private static final String MAIL_TRANSPORT_PROTOCOL = "smtp";

	private static final String MAIL_HOST = "smtp.gmail.com";

	private static final String MAIL_SMTP_STARTTLS_ENABLE = "true";

	private static final String MAIL_SMTP_AUTH = "true";

	private static final String MAIL_DEBUG = "false";

	// private static final String USERNAME = "aymen.kazdaghli@esprit.tn";
	private static final String USERNAME = "adaming.demo@gmail.com";

	// private static final String PASSWORD = "AymenAymen101";
	private static final String PASSWORD = "Adaming2018";

	public static final void sendMail(String to, String titreMail, String titreContentMail, String bodyContentMail)
			throws MessagingException {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", MAIL_TRANSPORT_PROTOCOL);
		props.setProperty("mail.host", MAIL_HOST);
		props.put("mail.smtp.starttls.enable", MAIL_SMTP_STARTTLS_ENABLE);
		props.put("mail.smtp.auth", MAIL_SMTP_AUTH);
		props.put("mail.debug", MAIL_DEBUG);
		props.put("mail.smtp.ssl.trust", "*");

		Session mailSession = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USERNAME, PASSWORD);
			}
		});

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();

		String templateMail1 = "<div id=\"wrapper\" style=\"width:100%;float:left; background-color: #ECECEC\">"
				+ "<div id=\"wrapper-content\" style=\"width:60%;margin:auto\">"
				+ "<div id=\"header\" style=\"width:100%; background-color: #E30521; border-radius: 10px 10px 0 0px;float:left\">"
				+ "<div style=\"color:#E30521;margin: 10px 10px 10px\"> | </div>" +

				"</div>" + "<div id=\"content\" style=\"width:100%;margin-bottom: -20px;float:left\">"
				+ "<div id=\"logo\" style=\"background-color: #EEEEEE; width:100%;float:left\">"
				+ "<div style=\"width:80%;float:left;\">"
				+ "<div  style=\"width: 50%;margin: 50px 0px 20px 10px; font-size: 25px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;color:#E30521\">ASB RECOUVREMENT</div>"
				+ "</div>" + "<div style=\"width:15%;float:left\">"
				+ "<img id=\"customHeaderImage\" label=\"Header Image\" editable=\"true\" width=\"107\" src=\"cid:image\" class=\"w640\" border=\"0\" align=\"top\" style=\"display: inline;margin-top:2px; margin-bottom: 2px\"/>"
				+ "</div>" + "</div>"
				+ "<div id=\"title\" style=\"background-color: #FFFFFF; width:100%;overflow: auto;height: 200px;\">" +

				"<p align=\"left\" style=\"font-size: 18px; line-height:24px; color: #0a0a0a; font-weight:bold; margin-top:15px; margin-bottom:18px; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; margin-left: 15px\"> "
				+ titreContentMail + "</p>" + bodyContentMail;
		messageBodyPart.setContent(templateMail1, "text/html; charset=utf-8");
		multipart.addBodyPart(messageBodyPart);
		MimeMessage message = new MimeMessage(mailSession);
		message.setContent(multipart);
		message.setSubject(titreMail);

		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

		Transport transport = mailSession.getTransport();
		transport.connect();
		transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}

	public static final void sendMail(String to, String titreMail, String titreContentMail, String bodyContentMail,
			boolean isUrl) throws MessagingException {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", MAIL_TRANSPORT_PROTOCOL);
		props.setProperty("mail.host", MAIL_HOST);
		props.put("mail.smtp.starttls.enable", MAIL_SMTP_STARTTLS_ENABLE);
		props.put("mail.smtp.auth", MAIL_SMTP_AUTH);
		props.put("mail.debug", MAIL_DEBUG);
		props.put("mail.smtp.ssl.trust", "*");

		Session mailSession = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USERNAME, PASSWORD);
			}
		});

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();
		String messageReset= "<p style=\"font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif;margin-left:15px\">Nous avons reçu une demande de réinitialisation du mot de passe associé à cette adresse de messagerie.</p>\n" +
				" \n" +
				"<p style=\"font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif;margin-left:15px\">S'il vous plaît, cliquez sur le bouton ci-dessous pour créer un\n" +
				"nouveau mot de passe sur ASB Contentieux.</p>";

		String templateMail1 = "<div id=\"wrapper\" style=\"width:100%;float:left; background-color: #ECECEC\">"
				+ "<div id=\"wrapper-content\" style=\"width:60%;margin:auto\">"
				+ "<div id=\"header\" style=\"width:100%; background-color: #E30521; border-radius: 10px 10px 0 0px;float:left\">"
				+ "<div style=\"color:#E30521;margin: 10px 10px 10px\"> | </div>" +

				"</div>" + "<div id=\"content\" style=\"width:100%;margin-bottom: -20px;float:left\">"
				+ "<div id=\"logo\" style=\"background-color: #EEEEEE; width:100%;float:left\">"
				+ "<div style=\"width:80%;float:left;\">"
				+ "<div  style=\"width: 50%;margin: 50px 0px 20px 10px; font-size: 25px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;color:#E30521\">ASB CONTENTIEUX</div>"
				+ "</div>" + "<div style=\"width:15%;float:left\">"
				+ "<img id=\"customHeaderImage\" label=\"Header Image\" editable=\"true\" width=\"107\" src=\"cid:image\" class=\"w640\" border=\"0\" align=\"top\" style=\"display: inline;margin-top:2px; margin-bottom: 2px\"/>"
				+ "</div>" + "</div>"
				+ "<div id=\"title\" style=\"background-color: #FFFFFF; width:100%;overflow: auto;height: 200px;\">" +

				"<p align=\"left\" style=\"font-size: 18px; line-height:24px; color: #0a0a0a; font-weight:bold; margin-top:15px; margin-bottom:18px; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; margin-left: 15px\"> "
				+ titreContentMail + "</p> <br>"+messageReset;
		if (isUrl) {
			templateMail1 += "<a style=\" margin-left: 30%; \" href=\"" + bodyContentMail + "\" ><button style=\"background-color: #e32e2d; border: none; color: white;\n" +
					"  padding: 15px 32px; text-align: center; text-decoration: none; font-size: 16px;\n" +
					"  cursor: pointer;\"> Change Password </button></a>";
		} else {
			templateMail1 += bodyContentMail;
		}
		messageBodyPart.setContent(templateMail1, "text/html; charset=utf-8");
		multipart.addBodyPart(messageBodyPart);
		MimeMessage message = new MimeMessage(mailSession);
		message.setContent(multipart);
		message.setSubject(titreMail);

		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

		Transport transport = mailSession.getTransport();
		transport.connect();
		transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}

	public static final void sendMailWithPj(String to, String titreMail, String titreContentMail, String idDossier,
			String login) throws MessagingException {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", MAIL_TRANSPORT_PROTOCOL);
		props.setProperty("mail.host", MAIL_HOST);
		props.put("mail.smtp.starttls.enable", MAIL_SMTP_STARTTLS_ENABLE);
		props.put("mail.smtp.auth", MAIL_SMTP_AUTH);
		props.put("mail.debug", MAIL_DEBUG);
		props.put("mail.smtp.ssl.trust", "*");

		Session mailSession = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USERNAME, PASSWORD);
			}
		});

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();

		String templateMail1 = "<div id=\"wrapper\" style=\"width:100%;float:left; background-color: #ECECEC\">"
				+ "<div id=\"wrapper-content\" style=\"width:60%;margin:auto\">"
				+ "<div id=\"header\" style=\"width:100%; background-color: #E30521; border-radius: 10px 10px 0 0px;float:left\">"
				+ "<div style=\"color:#E30521;margin: 10px 10px 10px\"> | </div>" +

				"</div>" + "<div id=\"content\" style=\"width:100%;margin-bottom: -20px;float:left\">"
				+ "<div id=\"logo\" style=\"background-color: #EEEEEE; width:100%;float:left\">"
				+ "<div style=\"width:80%;float:left;\">"
				+ "<div  style=\"width: 50%;margin: 50px 0px 20px 10px; font-size: 25px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;color:#E30521\">ASB CONTENTIEUX</div>"
				+ "</div>" + "<div style=\"width:15%;float:left\">"
				+ "<img id=\"customHeaderImage\" label=\"Header Image\" editable=\"true\" width=\"107\" src=\"cid:image\" class=\"w640\" border=\"0\" align=\"top\" style=\"display: inline;margin-top:2px; margin-bottom: 2px\"/>"
				+ "</div>" + "</div>"
				+ "<div id=\"title\" style=\"background-color: #FFFFFF; width:100%;overflow: auto;height: 200px;\">" +

				"<p align=\"left\" style=\"font-size: 18px; line-height:24px; color: #0a0a0a; font-weight:bold; margin-top:15px; margin-bottom:18px; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; margin-left: 15px\"> "
				+ titreContentMail + "</p>";
		messageBodyPart.setContent(templateMail1, "text/html; charset=utf-8");
		multipart.addBodyPart(messageBodyPart);
		messageBodyPart = new MimeBodyPart();
		String realPath = File.separator + "recouvrement" + File.separator + "arrangement" + File.separator + login
				+ File.separator;
		DateFormat df = new SimpleDateFormat("yyyyMMddHH");
		DataSource source = new FileDataSource(
				realPath + File.separator + "arrangement" + idDossier + df.format(new Date()) + ".pdf");
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName("arrangement" + idDossier + ".pdf");
		multipart.addBodyPart(messageBodyPart);
		MimeMessage message = new MimeMessage(mailSession);
		message.setContent(multipart);
		message.setSubject(titreMail);

		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

		Transport transport = mailSession.getTransport();
		transport.connect();
		transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}

	public static final void sendMailToSpeakers(String to, String titreMail, String titreContentMail,
			String urlConfirmation, String urlAnnulation, String bodyMail, boolean isUrl) throws MessagingException {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", MAIL_TRANSPORT_PROTOCOL);
		props.setProperty("mail.host", MAIL_HOST);
		props.put("mail.smtp.starttls.enable", MAIL_SMTP_STARTTLS_ENABLE);
		props.put("mail.smtp.auth", MAIL_SMTP_AUTH);
		props.put("mail.debug", MAIL_DEBUG);
		props.put("mail.smtp.ssl.trust", "*");

		Session mailSession = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USERNAME, PASSWORD);
			}
		});

		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();

		String templateMail1 = "<div id=\"wrapper\" style=\"width:100%;float:left; background-color: #ECECEC\">"
				+ "<div id=\"wrapper-content\" style=\"width:60%;margin:auto\">"
				+ "<div id=\"header\" style=\"width:100%; background-color: #E30521; border-radius: 10px 10px 0 0px;float:left\">"
				+ "<div style=\"color:#E30521;margin: 10px 10px 10px\"> | </div>" +

				"</div>" + "<div id=\"content\" style=\"width:100%;margin-bottom: -20px;float:left\">"
				+ "<div id=\"logo\" style=\"background-color: #EEEEEE; width:100%;float:left\">"
				+ "<div style=\"width:80%;float:left;\">"
				+ "<div  style=\"width: 50%;margin: 50px 0px 20px 10px; font-size: 25px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;color:#E30521\">ASB RECOUVREMENT</div>"
				+ "</div>" + "<div style=\"width:15%;float:left\">"
				+ "<img id=\"customHeaderImage\" label=\"Header Image\" editable=\"true\" width=\"107\" src=\"cid:image\" class=\"w640\" border=\"0\" align=\"top\" style=\"display: inline;margin-top:2px; margin-bottom: 2px\"/>"
				+ "</div>" + "</div>"
				+ "<div id=\"title\" style=\"background-color: #FFFFFF; width:100%;overflow: auto;height: 200px;\">" +

				"<div align=\"left\" style=\"font-size: 18px; line-height:24px; color: #0a0a0a; font-weight:bold; margin-top:15px; margin-bottom:18px; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; margin-left: 15px\"> "
				+ titreContentMail ;
		if (isUrl) {
			templateMail1 += "<a href="+urlConfirmation+">" + " ICI" + "</a>";
		} else {
			templateMail1 += urlConfirmation;
		}
		templateMail1 += "<div align=\"left\" style=\"font-size: 18px; line-height:24px; color: #0a0a0a; font-weight:bold; margin-top:15px; margin-bottom:18px; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; margin-left: 15px\"> "
				+ bodyMail ;

		if (isUrl) {
			templateMail1 += "<a href="+urlAnnulation+">" + " ICI" + "</a>";
		} else {
			templateMail1 += urlAnnulation;
		}
		templateMail1 +=  "</div> </div>";
		messageBodyPart.setContent(templateMail1, "text/html; charset=utf-8");
		multipart.addBodyPart(messageBodyPart);
		MimeMessage message = new MimeMessage(mailSession);
		message.setContent(multipart);
		message.setSubject(titreMail);

		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

		Transport transport = mailSession.getTransport();
		transport.connect();
		transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}

}
