package com.fr.adaming.utils;

public interface IConstants {
	/*
	 * ASB Recouvrement URLs : Client et communication entre ASBRecouvrement et ASBContentiaux
	 */
	public static String ASBUrl = "http://163.172.101.130:8181/ASBRecouvrement.Web/api/";
	public static String ASBUrlWithoutApi = "http://163.172.101.130:8181/ASBRecouvrement.Web/";
	public interface Rib {
		int CODE_BANQUE_START_POS = 0;
		int CODE_BANQUE_LONG = 2;
		int CODE_AGENCE_START_POS = 2;
		int CODE_AGENCE_LONG = 3;
		int NUM_COMPTE_START_POS = 5;
		int NUM_COMPTE_LONG = 13;
		int CLE_RIB_START_POS = 18;
		int CLE_RIB_LONG = 2;
	}
}
