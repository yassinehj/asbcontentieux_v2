package com.fr.adaming.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.Key;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.crypto.Cipher;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fr.adaming.exceptions.ExceptionUtil;
import org.apache.commons.codec.binary.Base32;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


public class Utilitaire {

	private static final String DEFAULT_FORMAT = "dd/MM/yyyy";
	private static final String ALGORITHM = "AES";
	private static final byte[] keyValue = "ADBSJHJS12547896".getBytes();

	public Utilitaire() {

	}
	
	public static String addFileAlfresco(MultipartFile file,String dossier,String documentId) {
		FileInputStream fileInputStream = null;
		try {
			String fileName = StringUtils.cleanPath(file.getOriginalFilename());
			int num = 1;
			String fileNameWithoutExtension = fileName.replaceFirst("[.][^.]+$", "");
			String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
			Path path = null;
			if (dossier.equals("null")) {
				//path = Paths.get("/opt/apache-tomcat-9099/archive/" + documentId).toAbsolutePath().normalize();
				path = Paths.get("/opt/tomcat-8085/archive/" + documentId).toAbsolutePath().normalize();
			} else {
				//path = Paths.get("/opt/apache-tomcat-9099/archive/" + dossier).toAbsolutePath().normalize();
				path = Paths.get("/opt/tomcat-8085/archive/" + dossier).toAbsolutePath().normalize();
					
				Files.createDirectories(path);
			}
			Path targetLocation = path.resolve(fileName);
			while (Files.exists(targetLocation) && !Files.isDirectory(targetLocation)) {
				fileName = fileNameWithoutExtension + " (" + num + ")" + "." + extension;
				targetLocation = path.resolve(fileName);
				num++;
			}
			System.out.println("passed2");
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			String realPath = path.toString() + File.separator + fileName;
			fileInputStream = new FileInputStream(realPath);
			org.apache.chemistry.opencmis.client.api.Document cvAlfresco = AlfrescoOpenCmis
					.createFolder(fileInputStream, fileName, fileInputStream.getChannel().size(),extension);
			return cvAlfresco.getId();
		} catch (Exception e) {
			LoggerUtil.log(e.getMessage() + " Erreur lors de l'ajout d'un addFichier");
			return null;
		} finally {
			if (fileInputStream != null)
				try {
					fileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	
	public static String encrypt(String valueToEnc) throws Exception {

		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(Cipher.ENCRYPT_MODE, key);

		System.out.println("valueToEnc.getBytes().length "+valueToEnc.getBytes().length);
		byte[] encValue = c.doFinal(valueToEnc.getBytes());
		System.out.println("encValue length" + encValue.length);
		byte[] encryptedByteValue = new Base32().encode(encValue);
		String encryptedValue = new String(encryptedByteValue);
		System.out.println("encryptedValue " + encryptedValue);

		return encryptedValue;
	}

	public static String decrypt(String encryptedValue) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(Cipher.DECRYPT_MODE, key);




		byte[] decordedValue = new Base32().decode(encryptedValue.getBytes());
		byte[] enctVal = c.doFinal(decordedValue);
		System.out.println("enctVal length " + enctVal.length);
		return new String(enctVal);
	}

	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, ALGORITHM);
		return key;
	}


	public static String convertDateToString(Date date, String format) {
		String dateFormatee;
		SimpleDateFormat formatDateJour;
		if (date != null) {
			if (format == null) {
				formatDateJour = new SimpleDateFormat(DEFAULT_FORMAT);
			} else {
				formatDateJour = new SimpleDateFormat(format);
			}
			dateFormatee = formatDateJour.format(date);
		} else {
			dateFormatee = "";
		}
		return dateFormatee;
	}

	public static String convertLongToString(Long val) {
		String valConvert;
		if (val != null) {
			valConvert = Long.toString(val);
		} else {
			valConvert = "";
		}
		return valConvert;
	}

	public static boolean isNumeric(String input) {
		boolean result = false;
		if (!Utilitaire.isEmpty(input) && !Utilitaire.isChaineVide(input)) {
			try {
				Double.parseDouble(input);
				result = true;
			} catch (NumberFormatException e) {
				ExceptionUtil.log(e);
			}
		}
		return result;
	}

	public static Boolean isChaineVide(String chaine) {
		String chaineVide = "[ ]*";
		return chaine.matches(chaineVide);
	}

	public static boolean isEmpty(String str) {
		if (str == null || str.isEmpty()) {
			return true;
		}
		return false;
	}

	public static Integer getNumberDaysBetweenTwoDates(Date dateBeguin, Date dateEnd) {
		int nbre = 0;
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(dateBeguin);

		while (calendar.getTime().before(dateEnd)) {
			nbre++;
			calendar.add(Calendar.DATE, 1);
		}
		return nbre;
	}

	public static BigDecimal convertStringToBigDecimal(String montantStr, Integer nbreDecimal) {
		BigDecimal montantValeur = null;
		if (nbreDecimal == null) {
			nbreDecimal = 0;
		}
		if (!nbreDecimal.equals(0)) {
			montantStr = new StringBuilder(montantStr).insert(montantStr.length() - nbreDecimal, ".").toString();
		}
		montantValeur = new BigDecimal(montantStr);
		return montantValeur;
	}

	public static String generateRandomNumbers(int length) {
		String chars = "1234567890";
		StringBuilder pass = new StringBuilder();
		for (int x = 0; x < length; x++) {
			int i = (int) Math.floor(Math.random() * (chars.length() - 1));
			pass.append(chars.charAt(i));
		}
		return pass.toString();
	}

	public static Boolean areDatesEqualsDMY(Date date1, Date date2) {

		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		calendar1.setTime(date1);
		calendar2.setTime(date2);
		return calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)
				&& calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH)
				&& calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR);

	}

	public static Connection getConnectionFromMsAccessFile(String urlFichierTelecompdb) {
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver").newInstance();
			return DriverManager.getConnection("jdbc:ucanaccess://" + urlFichierTelecompdb
					+ ";memory=true;immediatelyReleaseResources=true;jackcessOpener=com.fr.adaming.utils.Encrypt");
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			ExceptionUtil.log(e);
			return null;
		}

	}

	public static java.util.Date convertSqlDateToUtillDate(java.sql.Date sqlDate) {
		java.util.Date utilDate = null;
		if (sqlDate != null) {
			utilDate = new java.util.Date(sqlDate.getTime());
		}
		return utilDate;
	}

	public static Date convertStringToDate(String dateInString, String format) {

		SimpleDateFormat formatter = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = formatter.parse(dateInString);
		} catch (ParseException e) {
			ExceptionUtil.log(e);
		}
		return date;
	}

	public static Long convertStringToLong(String valStr) {
		try {
			Long val = Long.parseLong(valStr);
			return val;
		} catch (NumberFormatException e) {
			ExceptionUtil.log(e);
		}
		return null;
	}

	public static Date convertStringToDate(String dateInString) {

		return convertStringToDate(dateInString, DEFAULT_FORMAT);
	}

	public static String extractCodeInstitutionFromRib(String rib) {
		return rib.substring(IConstants.Rib.CODE_AGENCE_START_POS,
				IConstants.Rib.CODE_AGENCE_LONG + IConstants.Rib.CODE_AGENCE_START_POS);
	}
	
	public static boolean isFirstDayofMonth(Date date){
		if(date == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
	    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
	    return dayOfMonth == 1;
	}
	
	  public static Date toDate(XMLGregorianCalendar calendar){
	        if(calendar == null) {
	            return null;
	        }
	        return calendar.toGregorianCalendar().getTime();
	    }
	    
	    public static XMLGregorianCalendar toXMLGregorianCalendar(Date date){
	        GregorianCalendar gCalendar = new GregorianCalendar();
	        gCalendar.setTime(date);
	        XMLGregorianCalendar xmlCalendar = null;
	        try {
	            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
	        } catch (DatatypeConfigurationException ex) {
	           
	        }
	        return xmlCalendar;
	    }
}
