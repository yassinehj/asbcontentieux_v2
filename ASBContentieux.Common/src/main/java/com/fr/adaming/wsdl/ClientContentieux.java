//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2019.01.25 à 05:14:10 PM CET 
//

package com.fr.adaming.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Classe Java pour clientContentieux complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="clientContentieux"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prenom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="numTel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="cin" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mail" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rib" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="adresseSoap" type="{http://soap.adaming.fr.com}adresseSoap"/&gt;
 *         &lt;element name="operationDebitSoap" type="{http://soap.adaming.fr.com}operationDebitSoap" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clientContentieux", propOrder = { "nom", "prenom", "numTel", "cin", "mail", "rib", "adresseSoap",
        "operationDebitSoap" })
public class ClientContentieux {

    @XmlElement(required = true)
    protected String nom;
    @XmlElement(required = true)
    protected String prenom;
    @XmlElement(required = true)
    protected String numTel;
    @XmlElement(required = true)
    protected String cin;
    @XmlElement(required = true)
    protected String mail;
    @XmlElement(required = true)
    protected String rib;
    @XmlElement(required = true)
    protected AdresseSoap adresseSoap;
    protected List<OperationDebitSoap> operationDebitSoap;

    /**
     * Obtient la valeur de la propriété nom.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNom() {
        return nom;
    }

    /**
     * Définit la valeur de la propriété nom.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setNom(String value) {
        this.nom = value;
    }

    /**
     * Obtient la valeur de la propriété prenom.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Définit la valeur de la propriété prenom.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setPrenom(String value) {
        this.prenom = value;
    }

    /**
     * Obtient la valeur de la propriété numTel.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNumTel() {
        return numTel;
    }

    /**
     * Définit la valeur de la propriété numTel.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setNumTel(String value) {
        this.numTel = value;
    }

    /**
     * Obtient la valeur de la propriété cin.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCin() {
        return cin;
    }

    /**
     * Définit la valeur de la propriété cin.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setCin(String value) {
        this.cin = value;
    }

    /**
     * Obtient la valeur de la propriété mail.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getMail() {
        return mail;
    }

    /**
     * Définit la valeur de la propriété mail.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setMail(String value) {
        this.mail = value;
    }

    /**
     * Obtient la valeur de la propriété rib.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRib() {
        return rib;
    }

    /**
     * Définit la valeur de la propriété rib.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setRib(String value) {
        this.rib = value;
    }

    /**
     * Obtient la valeur de la propriété adresseSoap.
     * 
     * @return possible object is {@link AdresseSoap }
     * 
     */
    public AdresseSoap getAdresseSoap() {
        return adresseSoap;
    }

    /**
     * Définit la valeur de la propriété adresseSoap.
     * 
     * @param value allowed object is {@link AdresseSoap }
     * 
     */
    public void setAdresseSoap(AdresseSoap value) {
        this.adresseSoap = value;
    }

    /**
     * Gets the value of the operationDebitSoap property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot.
     * Therefore any modification you make to the returned list will be present
     * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
     * for the operationDebitSoap property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getOperationDebitSoap().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OperationDebitSoap }
     * 
     * 
     */
    public List<OperationDebitSoap> getOperationDebitSoap() {
        if (operationDebitSoap == null) {
            operationDebitSoap = new ArrayList<OperationDebitSoap>();
        }
        return this.operationDebitSoap;
    }

    @Override
    public String toString() {
        return "ClientContentieux{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", numTel='" + numTel + '\'' +
                ", cin='" + cin + '\'' +
                ", mail='" + mail + '\'' +
                ", rib='" + rib + '\'' +
                ", adresseSoap=" + adresseSoap +
                '}';
    }
}
