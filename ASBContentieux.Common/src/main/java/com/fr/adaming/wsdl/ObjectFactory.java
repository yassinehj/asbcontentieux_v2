//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2019.01.25 à 05:14:10 PM CET 
//

package com.fr.adaming.wsdl;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.fr.adaming.wsdl package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema
     * derived classes for package: com.fr.adaming.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetClientsRequest }
     * 
     */
    public GetClientsRequest createGetClientsRequest() {
        return new GetClientsRequest();
    }

    /**
     * Create an instance of {@link GetClientsResponse }
     * 
     */
    public GetClientsResponse createGetClientsResponse() {
        return new GetClientsResponse();
    }

    /**
     * Create an instance of {@link ClientContentieux }
     * 
     */
    public ClientContentieux createClientContentieux() {
        return new ClientContentieux();
    }

    /**
     * Create an instance of {@link GetHistoricRequest }
     * 
     */
    public GetHistoricRequest createGetHistoricRequest() {
        return new GetHistoricRequest();
    }

    /**
     * Create an instance of {@link GetHistoricResponse }
     * 
     */
    public GetHistoricResponse createGetHistoricResponse() {
        return new GetHistoricResponse();
    }

    /**
     * Create an instance of {@link HistoricSoap }
     * 
     */
    public HistoricSoap createHistoricSoap() {
        return new HistoricSoap();
    }

    /**
     * Create an instance of {@link GetInstitutionRequest }
     * 
     */
    public GetInstitutionRequest createGetInstitutionRequest() {
        return new GetInstitutionRequest();
    }

    /**
     * Create an instance of {@link GetInstitutionResponse }
     * 
     */
    public GetInstitutionResponse createGetInstitutionResponse() {
        return new GetInstitutionResponse();
    }

    /**
     * Create an instance of {@link Institution }
     * 
     */
    public Institution createInstitution() {
        return new Institution();
    }

    /**
     * Create an instance of {@link GetOverviewRequest }
     * 
     */
    public GetOverviewRequest createGetOverviewRequest() {
        return new GetOverviewRequest();
    }

    /**
     * Create an instance of {@link GetOverviewResponse }
     * 
     */
    public GetOverviewResponse createGetOverviewResponse() {
        return new GetOverviewResponse();
    }

    /**
     * Create an instance of {@link Overview }
     * 
     */
    public Overview createOverview() {
        return new Overview();
    }

    /**
     * Create an instance of {@link OperationDebitSoap }
     * 
     */
    public OperationDebitSoap createOperationDebitSoap() {
        return new OperationDebitSoap();
    }

    /**
     * Create an instance of {@link AdresseSoap }
     * 
     */
    public AdresseSoap createAdresseSoap() {
        return new AdresseSoap();
    }

}
