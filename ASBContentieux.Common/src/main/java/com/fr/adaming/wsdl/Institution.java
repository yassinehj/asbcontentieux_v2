//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2019.01.25 à 05:14:10 PM CET 
//

package com.fr.adaming.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour institution complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="institution"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idInstitution" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="raisonSociale" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ville" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="adresse" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="codeInstitution" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "institution", propOrder = { "idInstitution", "raisonSociale", "ville", "adresse", "codeInstitution","idInstitutionSiege","idInstitutionRegionale" })
public class Institution {

    protected long idInstitution;
    @XmlElement(required = true)
    protected String raisonSociale;
    @XmlElement(required = true)
    protected String ville;
    @XmlElement(required = true)
    protected String adresse;
    @XmlElement(required = true)
    protected String codeInstitution;
    @XmlElement(required = true)
    protected String idInstitutionSiege;
    @XmlElement(required = true)
    protected String idInstitutionRegionale;


    /**
     * Obtient la valeur de la propriété idInstitution.
     * 
     */
    public long getIdInstitution() {
        return idInstitution;
    }

    /**
     * Définit la valeur de la propriété idInstitution.
     * 
     */
    public void setIdInstitution(long value) {
        this.idInstitution = value;
    }

    /**
     * Obtient la valeur de la propriété raisonSociale.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRaisonSociale() {
        return raisonSociale;
    }

    /**
     * Définit la valeur de la propriété raisonSociale.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setRaisonSociale(String value) {
        this.raisonSociale = value;
    }

    /**
     * Obtient la valeur de la propriété ville.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getVille() {
        return ville;
    }

    /**
     * Définit la valeur de la propriété ville.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setVille(String value) {
        this.ville = value;
    }

    /**
     * Obtient la valeur de la propriété adresse.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Définit la valeur de la propriété adresse.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setAdresse(String value) {
        this.adresse = value;
    }

    /**
     * Obtient la valeur de la propriété codeInstitution.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCodeInstitution() {
        return codeInstitution;
    }

    /**
     * Définit la valeur de la propriété codeInstitution.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setCodeInstitution(String value) {
        this.codeInstitution = value;
    }
    /**
     * Obtient la valeur de la propriété idInstitutionSiege.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdInstitutionSiege() {
        return idInstitutionSiege;
    }

    /**
     * Définit la valeur de la propriété idInstitutionSiege.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setIdInstitutionSiege(String value) {
        this.idInstitutionSiege = value;
    }
    /**
     * Obtient la valeur de la propriété idInstitutionRegionale.
     *
     * @return possible object is {@link String }
     *
     */
    public String getIdInstitutionRegionale() {
        return idInstitutionRegionale;
    }

    /**
     * Définit la valeur de la propriété idInstitutionRegionale.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setIdInstitutionRegionale(String value) {
        this.idInstitutionRegionale = value;
    }

}
