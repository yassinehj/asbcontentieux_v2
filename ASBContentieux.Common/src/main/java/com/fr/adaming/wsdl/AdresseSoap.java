//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2019.01.25 à 05:14:10 PM CET 
//

package com.fr.adaming.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour adresseSoap complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="adresseSoap"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="adresse" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="pays" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="gouvernorat" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ville" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="codePostal" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adresseSoap", propOrder = { "adresse", "pays", "gouvernorat", "ville", "codePostal" })
public class AdresseSoap {

    @XmlElement(required = true)
    protected String adresse;
    @XmlElement(required = true)
    protected String pays;
    @XmlElement(required = true)
    protected String gouvernorat;
    @XmlElement(required = true)
    protected String ville;
    @XmlElement(required = true)
    protected String codePostal;

    /**
     * Obtient la valeur de la propriété adresse.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Définit la valeur de la propriété adresse.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setAdresse(String value) {
        this.adresse = value;
    }

    /**
     * Obtient la valeur de la propriété pays.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPays() {
        return pays;
    }

    /**
     * Définit la valeur de la propriété pays.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setPays(String value) {
        this.pays = value;
    }

    /**
     * Obtient la valeur de la propriété gouvernorat.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getGouvernorat() {
        return gouvernorat;
    }

    /**
     * Définit la valeur de la propriété gouvernorat.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setGouvernorat(String value) {
        this.gouvernorat = value;
    }

    /**
     * Obtient la valeur de la propriété ville.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getVille() {
        return ville;
    }

    /**
     * Définit la valeur de la propriété ville.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setVille(String value) {
        this.ville = value;
    }

    /**
     * Obtient la valeur de la propriété codePostal.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * Définit la valeur de la propriété codePostal.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setCodePostal(String value) {
        this.codePostal = value;
    }

    @Override
    public String toString() {
        return "AdresseSoap{" +
                "adresse='" + adresse + '\'' +
                ", pays='" + pays + '\'' +
                ", gouvernorat='" + gouvernorat + '\'' +
                ", ville='" + ville + '\'' +
                ", codePostal='" + codePostal + '\'' +
                '}';
    }
}
