//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2019.01.25 à 05:14:10 PM CET 
//

package com.fr.adaming.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour operationDebitSoap complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="operationDebitSoap"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="dateInitiale" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idDossier" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="financement" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="premierEcheance" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="compteur" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="duree" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tranche" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="numTranche" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operationDebitSoap", propOrder = { "nature", "dateInitiale", "idDossier", "financement",
        "premierEcheance", "compteur", "duree", "tranche", "numTranche" })
public class OperationDebitSoap {

    @XmlElement(required = true)
    protected String nature;
    @XmlElement(required = true)
    protected String dateInitiale;
    @XmlElement(required = true)
    protected String idDossier;
    @XmlElement(required = true)
    protected String financement;
    @XmlElement(required = true)
    protected String premierEcheance;
    @XmlElement(required = true)
    protected String compteur;
    @XmlElement(required = true)
    protected String duree;
    @XmlElement(required = true)
    protected String tranche;
    @XmlElement(required = true)
    protected String numTranche;

    /**
     * Obtient la valeur de la propriété nature.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNature() {
        return nature;
    }

    /**
     * Définit la valeur de la propriété nature.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setNature(String value) {
        this.nature = value;
    }

    /**
     * Obtient la valeur de la propriété dateInitiale.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDateInitiale() {
        return dateInitiale;
    }

    /**
     * Définit la valeur de la propriété dateInitiale.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setDateInitiale(String value) {
        this.dateInitiale = value;
    }

    /**
     * Obtient la valeur de la propriété idDossier.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIdDossier() {
        return idDossier;
    }

    /**
     * Définit la valeur de la propriété idDossier.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setIdDossier(String value) {
        this.idDossier = value;
    }

    /**
     * Obtient la valeur de la propriété financement.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getFinancement() {
        return financement;
    }

    /**
     * Définit la valeur de la propriété financement.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setFinancement(String value) {
        this.financement = value;
    }

    /**
     * Obtient la valeur de la propriété premierEcheance.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPremierEcheance() {
        return premierEcheance;
    }

    /**
     * Définit la valeur de la propriété premierEcheance.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setPremierEcheance(String value) {
        this.premierEcheance = value;
    }

    /**
     * Obtient la valeur de la propriété compteur.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCompteur() {
        return compteur;
    }

    /**
     * Définit la valeur de la propriété compteur.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setCompteur(String value) {
        this.compteur = value;
    }

    /**
     * Obtient la valeur de la propriété duree.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDuree() {
        return duree;
    }

    /**
     * Définit la valeur de la propriété duree.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setDuree(String value) {
        this.duree = value;
    }

    /**
     * Obtient la valeur de la propriété tranche.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getTranche() {
        return tranche;
    }

    /**
     * Définit la valeur de la propriété tranche.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setTranche(String value) {
        this.tranche = value;
    }

    /**
     * Obtient la valeur de la propriété numTranche.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNumTranche() {
        return numTranche;
    }

    /**
     * Définit la valeur de la propriété numTranche.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setNumTranche(String value) {
        this.numTranche = value;
    }

}
