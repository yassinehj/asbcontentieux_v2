//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2019.01.25 à 05:14:10 PM CET 
//

package com.fr.adaming.wsdl;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Classe Java pour historicSoap complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="historicSoap"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="createTime" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="endTime" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="descriptionProlongation" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="durationProlongation" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="lettreSevereEffectue" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="lettreCommercialEffectue" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="appelEffectue" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="sommationEffectue" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="commiteEffectue" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="idTask" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idProcess" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idDossier" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="image" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="color" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="actionnaire" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idRapportHn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "historicSoap", propOrder = { "name", "createTime", "endTime", "descriptionProlongation",
        "durationProlongation", "lettreSevereEffectue", "lettreCommercialEffectue", "appelEffectue",
        "sommationEffectue", "commiteEffectue", "idTask", "idProcess", "idDossier", "image", "color", "actionnaire",
        "idRapportHn" })
public class HistoricSoap {

    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar createTime;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endTime;
    @XmlElement(required = true)
    protected String descriptionProlongation;
    protected Long durationProlongation;
    protected boolean lettreSevereEffectue;
    protected boolean lettreCommercialEffectue;
    protected boolean appelEffectue;
    protected boolean sommationEffectue;
    protected boolean commiteEffectue;
    @XmlElement(required = true)
    protected String idTask;
    @XmlElement(required = true)
    protected String idProcess;
    protected Long idDossier;
    @XmlElement(required = true)
    protected String image;
    @XmlElement(required = true)
    protected String color;
    @XmlElement(required = true)
    protected String actionnaire;
    @XmlElement(required = true)
    protected String idRapportHn;

    /**
     * Obtient la valeur de la propriété name.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Définit la valeur de la propriété name.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtient la valeur de la propriété createTime.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getCreateTime() {
        return createTime;
    }

    /**
     * Définit la valeur de la propriété createTime.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setCreateTime(XMLGregorianCalendar value) {
        this.createTime = value;
    }

    /**
     * Obtient la valeur de la propriété endTime.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getEndTime() {
        return endTime;
    }

    /**
     * Définit la valeur de la propriété endTime.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setEndTime(XMLGregorianCalendar value) {
        this.endTime = value;
    }

    /**
     * Obtient la valeur de la propriété descriptionProlongation.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDescriptionProlongation() {
        return descriptionProlongation;
    }

    /**
     * Définit la valeur de la propriété descriptionProlongation.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setDescriptionProlongation(String value) {
        this.descriptionProlongation = value;
    }

    /**
     * Obtient la valeur de la propriété durationProlongation.
     * 
     */
    public Long getDurationProlongation() {
        return durationProlongation;
    }

    /**
     * Définit la valeur de la propriété durationProlongation.
     * 
     */
    public void setDurationProlongation(Long value) {
        this.durationProlongation = value;
    }

    /**
     * Obtient la valeur de la propriété lettreSevereEffectue.
     * 
     */
    public boolean isLettreSevereEffectue() {
        return lettreSevereEffectue;
    }

    /**
     * Définit la valeur de la propriété lettreSevereEffectue.
     * 
     */
    public void setLettreSevereEffectue(boolean value) {
        this.lettreSevereEffectue = value;
    }

    /**
     * Obtient la valeur de la propriété lettreCommercialEffectue.
     * 
     */
    public boolean isLettreCommercialEffectue() {
        return lettreCommercialEffectue;
    }

    /**
     * Définit la valeur de la propriété lettreCommercialEffectue.
     * 
     */
    public void setLettreCommercialEffectue(boolean value) {
        this.lettreCommercialEffectue = value;
    }

    /**
     * Obtient la valeur de la propriété appelEffectue.
     * 
     */
    public boolean isAppelEffectue() {
        return appelEffectue;
    }

    /**
     * Définit la valeur de la propriété appelEffectue.
     * 
     */
    public void setAppelEffectue(boolean value) {
        this.appelEffectue = value;
    }

    /**
     * Obtient la valeur de la propriété sommationEffectue.
     * 
     */
    public boolean isSommationEffectue() {
        return sommationEffectue;
    }

    /**
     * Définit la valeur de la propriété sommationEffectue.
     * 
     */
    public void setSommationEffectue(boolean value) {
        this.sommationEffectue = value;
    }

    /**
     * Obtient la valeur de la propriété commiteEffectue.
     * 
     */
    public boolean isCommiteEffectue() {
        return commiteEffectue;
    }

    /**
     * Définit la valeur de la propriété commiteEffectue.
     * 
     */
    public void setCommiteEffectue(boolean value) {
        this.commiteEffectue = value;
    }

    /**
     * Obtient la valeur de la propriété idTask.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIdTask() {
        return idTask;
    }

    /**
     * Définit la valeur de la propriété idTask.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setIdTask(String value) {
        this.idTask = value;
    }

    /**
     * Obtient la valeur de la propriété idProcess.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIdProcess() {
        return idProcess;
    }

    /**
     * Définit la valeur de la propriété idProcess.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setIdProcess(String value) {
        this.idProcess = value;
    }

    /**
     * Obtient la valeur de la propriété idDossier.
     * 
     */
    public Long getIdDossier() {
        return idDossier;
    }

    /**
     * Définit la valeur de la propriété idDossier.
     * 
     */
    public void setIdDossier(Long value) {
        this.idDossier = value;
    }

    /**
     * Obtient la valeur de la propriété image.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getImage() {
        return image;
    }

    /**
     * Définit la valeur de la propriété image.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setImage(String value) {
        this.image = value;
    }

    /**
     * Obtient la valeur de la propriété color.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getColor() {
        return color;
    }

    /**
     * Définit la valeur de la propriété color.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Obtient la valeur de la propriété actionnaire.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getActionnaire() {
        return actionnaire;
    }

    /**
     * Définit la valeur de la propriété actionnaire.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setActionnaire(String value) {
        this.actionnaire = value;
    }

    /**
     * Obtient la valeur de la propriété idRapportHn.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIdRapportHn() {
        return idRapportHn;
    }

    /**
     * Définit la valeur de la propriété idRapportHn.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setIdRapportHn(String value) {
        this.idRapportHn = value;
    }

}
