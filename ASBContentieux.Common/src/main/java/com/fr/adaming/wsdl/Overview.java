//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2019.01.25 à 05:14:10 PM CET 
//

package com.fr.adaming.wsdl;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Classe Java pour overview complex type.
 * 
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType name="overview"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idDossier" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="creationTime" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="dueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="procInstanceId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idTitulaire" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="nomTitulaire" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="telTtitulaire" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rib" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="solde" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="isRegularise" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "overview", propOrder = { "idDossier", "creationTime", "dueDate", "procInstanceId", "idTitulaire",
        "nomTitulaire", "telTtitulaire", "rib", "solde", "isRegularise" })
public class Overview {

    protected long idDossier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar creationTime;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dueDate;
    @XmlElement(required = true)
    protected String procInstanceId;
    protected long idTitulaire;
    @XmlElement(required = true)
    protected String nomTitulaire;
    @XmlElement(required = true)
    protected String telTtitulaire;
    @XmlElement(required = true)
    protected String rib;
    protected double solde;
    protected boolean isRegularise;

    /**
     * Obtient la valeur de la propriété idDossier.
     * 
     */
    public long getIdDossier() {
        return idDossier;
    }

    /**
     * Définit la valeur de la propriété idDossier.
     * 
     */
    public void setIdDossier(long value) {
        this.idDossier = value;
    }

    /**
     * Obtient la valeur de la propriété creationTime.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getCreationTime() {
        return creationTime;
    }

    /**
     * Définit la valeur de la propriété creationTime.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setCreationTime(XMLGregorianCalendar value) {
        this.creationTime = value;
    }

    /**
     * Obtient la valeur de la propriété dueDate.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Définit la valeur de la propriété dueDate.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Obtient la valeur de la propriété procInstanceId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getProcInstanceId() {
        return procInstanceId;
    }

    /**
     * Définit la valeur de la propriété procInstanceId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setProcInstanceId(String value) {
        this.procInstanceId = value;
    }

    /**
     * Obtient la valeur de la propriété idTitulaire.
     * 
     */
    public long getIdTitulaire() {
        return idTitulaire;
    }

    /**
     * Définit la valeur de la propriété idTitulaire.
     * 
     */
    public void setIdTitulaire(long value) {
        this.idTitulaire = value;
    }

    /**
     * Obtient la valeur de la propriété nomTitulaire.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNomTitulaire() {
        return nomTitulaire;
    }

    /**
     * Définit la valeur de la propriété nomTitulaire.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setNomTitulaire(String value) {
        this.nomTitulaire = value;
    }

    /**
     * Obtient la valeur de la propriété telTtitulaire.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getTelTtitulaire() {
        return telTtitulaire;
    }

    /**
     * Définit la valeur de la propriété telTtitulaire.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setTelTtitulaire(String value) {
        this.telTtitulaire = value;
    }

    /**
     * Obtient la valeur de la propriété rib.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRib() {
        return rib;
    }

    /**
     * Définit la valeur de la propriété rib.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setRib(String value) {
        this.rib = value;
    }

    /**
     * Obtient la valeur de la propriété solde.
     * 
     */
    public double getSolde() {
        return solde;
    }

    /**
     * Définit la valeur de la propriété solde.
     * 
     */
    public void setSolde(double value) {
        this.solde = value;
    }

    /**
     * Obtient la valeur de la propriété isRegularise.
     * 
     */
    public boolean isIsRegularise() {
        return isRegularise;
    }

    /**
     * Définit la valeur de la propriété isRegularise.
     * 
     */
    public void setIsRegularise(boolean value) {
        this.isRegularise = value;
    }

    @Override
    public String toString() {
        return "Overview{" +
                "idDossier=" + idDossier +
                ", creationTime=" + creationTime +
                ", dueDate=" + dueDate +
                ", procInstanceId='" + procInstanceId + '\'' +
                ", idTitulaire=" + idTitulaire +
                ", nomTitulaire='" + nomTitulaire + '\'' +
                ", telTtitulaire='" + telTtitulaire + '\'' +
                ", rib='" + rib + '\'' +
                ", solde=" + solde +
                ", isRegularise=" + isRegularise +
                '}';
    }
}
