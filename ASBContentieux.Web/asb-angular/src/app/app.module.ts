import { ViewsModule } from 'src/app/views/views.module';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { TableModule } from 'primeng/table';


import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FuseModule } from '@fuse/fuse.module';
import { JwtInterceptor } from './core/helpers/JwtInterceptor';
import { ErrorInterceptor } from './core/helpers/ErrorInterceptor';
import { fuseConfig } from './shared/utils/fuse-config';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from './shared/shared.module';






@NgModule({
  declarations: [
    AppComponent,
   
    
    
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
    ViewsModule,
    SharedModule,
    FuseModule.forRoot(fuseConfig),
    ToastrModule.forRoot(),
    TableModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
