import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./core/auth/login/login.component";
import { ResetPasswordComponent } from "./core/auth/reset-password/reset-password.component";
import { InitResetPasswordComponent } from "./core/auth/reset-password/init-reset-password/init-reset-password.component";
import { AuthGuard } from "./core/guards/AuthGuard";
import { LayoutComponent } from "./core/layout/layout.component";
import { PieceAffaireComponent } from "./views/piece-affaire/piece-affaire.component";
import { TribunalComponent } from "./views/tribunal/tribunal.component";
import { ListTribunalComponent } from "./views/tribunal/list-tribunal/list-tribunal.component";
import { PaysComponent } from "./views/pays/pays.component";
import { ListPaysComponent } from "./views/pays/list-pays/list-pays.component";
import { GouvernoratComponent } from "./views/gouvernorat/gouvernorat.component";
import { VilleComponent } from "./views/ville/ville.component";

import { UtilisateurComponent } from "./views/habilitation/utilisateur/utilisateur.component";
import { UtilisateurFormComponent } from "./views/habilitation/utilisateur/utilisateur-form/utilisateur-form.component";
import { ListUtilisateurComponent } from "./views/habilitation/utilisateur/list-utilisateur/list-utilisateur.component";
import { ListTacheComponent } from "./views/tache/list-tache/list-tache.component";
import { DossierPrecComponent } from "./views/dossier-prec/dossier-prec.component";
import { DossierPrecListComponent } from "./views/dossier-prec/dossier-prec-list/dossier-prec-list.component";
import { AuxiliaireComponent } from './views/auxiliaire/auxiliaire.component';
import { ListAuxilaireComponent } from './views/auxiliaire/list-auxilaire/list-auxilaire.component';
import { FormAuxiliaireComponent } from './views/auxiliaire/form-auxiliaire/form-auxiliaire.component';
import { TribunalFormComponent } from './views/tribunal/tribunal-form/tribunal-form.component';
import { AgenceComponent } from './views/agence/agence.component';
import { AgenceListComponent } from './views/agence/agence-list/agence-list.component';
import { AgenceFormComponent } from './views/agence/agence-form/agence-form.component';
import { DebiteurComponent } from './views/debiteur/debiteur.component';
import { DebiteurListComponent } from './views/debiteur/debiteur-list/debiteur-list.component';
import { DebiteurFormComponent } from './views/debiteur/debiteur-form/debiteur-form.component';

import { TacheComponent } from "./views/tache/tache.component";
import { AjoutTacheComponent } from "./views/tache/ajout-tache/ajout-tache.component";
import { HistoriqueRecouvrementComponent } from "./views/dossier-prec/historiqueRecouvrement/historiqueRecouvrement.component";
import { DossierPrecEditComponent } from "./views/dossier-prec/dossier-prec-edit/dossier-prec-edit.component";
import { ConfigurationComponent } from './views/configuration/configuration.component';
import { TypeAuxiliaireComponent } from './views/configuration/type-auxiliaire/type-auxiliaire.component';
import { PieceAffaireListComponent } from './views/piece-affaire/piece-affaire-list/piece-affaire-list.component';
import { PieceAffaireDetailsComponent } from './views/piece-affaire/piece-affaire-details/piece-affaire-details.component';
import { ListAudienceComponent } from './views/audience/list-audience/list-audience.component';
import { AudienceFormComponent } from './views/audience/audience-form/audience-form.component';
import { AudienceComponent } from './views/audience/audience.component';
import { RoleComponent } from './views/habilitation/role/role.component';
import { RoleListComponent } from './views/habilitation/role/role-list/role-list.component';
const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
    pathMatch: "full"
  },
  /*   {
    path: 'home',
    component: HomeComponent
  }, */
  {
    path: "reset-init",
    component: ResetPasswordComponent
  },
  {
    path: "init-reset-password",
    component: InitResetPasswordComponent
  },
  {
    path: "",
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "piece-affaire",
        component: PieceAffaireComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: 'list', component: PieceAffaireListComponent },



        ]
      },
      {
        path: "tribunal",
        component: TribunalComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: ListTribunalComponent },
          { path: 'ajout', component: TribunalFormComponent },
          { path: 'edit/:idTribunal', component: TribunalFormComponent }
        ]
      },

      {
        path: "audience",
        component: AudienceComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: ListAudienceComponent },
          { path: 'ajout', component: AudienceFormComponent },
          { path: 'edit/:idAudience', component: AudienceFormComponent }
        ]
      },
      {
        path: "pays",
        component: PaysComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: ListPaysComponent }
        ]
      },

      {
        path: "utilisateur",
        component: UtilisateurComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: ListUtilisateurComponent },
          { path: "ajout", component: UtilisateurFormComponent },
          { path: "edit/:id", component: UtilisateurFormComponent }
        ]
      },
      {
        path: "role",
        component: RoleComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: RoleListComponent }
        ]
      },
      {
        path: "tache",
        component: TacheComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: ListTacheComponent },
          { path: "ajout", component: AjoutTacheComponent },
          { path: "edit/:id", component: AjoutTacheComponent }
        ]
      },
      {
        path: "auxiliaire",
        component: AuxiliaireComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: ListAuxilaireComponent },
          { path: 'ajout', component: FormAuxiliaireComponent },
          { path: 'edit/:id', component: FormAuxiliaireComponent }
        ]
      },

      {
        path: "pays",
        component: PaysComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: ListPaysComponent }
        ]
      },
      {
        path: "gouvernorat/:idPays",
        component: GouvernoratComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "ville/:idGouvernorat",
        component: VilleComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "dossier-prec",
        component: DossierPrecComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: DossierPrecListComponent },
          { path: "edit/:idDosPrec", component: DossierPrecEditComponent },
          {
            path: "historique/:idDosPrec",
            component: HistoriqueRecouvrementComponent
          }
        ]
      },
      {
        path: "agence",
        component: AgenceComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: AgenceListComponent },
          { path: 'edit/:id', component: AgenceFormComponent }
        ]
      },
      {
        path: "debiteur",
        component: DebiteurComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "list", component: DebiteurListComponent },
          { path: 'edit/:id', component: DebiteurFormComponent }

        ]
      },

      {
        path: "configuration",
        component: ConfigurationComponent,
        canActivate: [AuthGuard],
        children: [
          { path: "", redirectTo: "list", pathMatch: "full" },
          { path: "auxiliaire", component: TypeAuxiliaireComponent },
          { path: "pays", component: ListPaysComponent }
        ]
      }


    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
