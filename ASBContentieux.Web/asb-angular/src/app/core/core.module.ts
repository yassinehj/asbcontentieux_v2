import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPassword2Module } from './auth/reset-password/forgot-password-2/forgot-password-2.module';
import { ResetPassword2Module } from './auth/reset-password/reset-password-2/reset-password-2.module';
import { LoginModule } from './auth/login/login.module';
import { LayoutModule } from './layout/layout.module';
import { InitResetPasswordComponent } from './auth/reset-password/init-reset-password/init-reset-password.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    InitResetPasswordComponent,
    ResetPasswordComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ForgotPassword2Module,
    ResetPassword2Module,
    LoginModule,
    LayoutModule
  ]
})
export class CoreModule { }
