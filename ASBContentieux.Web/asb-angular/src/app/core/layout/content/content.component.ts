import { Router, NavigationEnd } from "@angular/router";
import { Component, ViewEncapsulation } from "@angular/core";
import { filter } from "rxjs/operators";
import { Location } from "@angular/common";

@Component({
  selector: "content",
  templateUrl: "./content.component.html",
  styleUrls: ["./content.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class ContentComponent {
  currentRoute: string[] = [];
  /**
   * Constructor
   */
  constructor(private _location: Location, private router: Router) {
    let myroute: string[];
    this.router.events
      .pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe(event => {
        this.currentRoute = [];
        myroute = event.urlAfterRedirects.split("/");
        myroute.forEach((el: any) => {
          if (myroute.indexOf(el) !== 0 && isNaN(el) && (!el.includes('?'))) {
            this.currentRoute.push(el);
          }
        });
      });
  }
  retour() {
    this._location.back();
  }
}
