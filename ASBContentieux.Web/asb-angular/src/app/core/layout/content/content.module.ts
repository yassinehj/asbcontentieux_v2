import { MatIconModule } from "@angular/material";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { FuseSharedModule } from "@fuse/shared.module";

import { ContentComponent } from "./content.component";
import { ButtonModule } from "primeng/button";

@NgModule({
  declarations: [ContentComponent],
  imports: [ButtonModule, RouterModule, FuseSharedModule, MatIconModule],
  exports: [ContentComponent]
})
export class ContentModule {}
