import { SharedService } from "./../../../shared/services/shared.service";
import { AuthService } from "@shared/services/auth.service";
import { Component, OnDestroy, OnInit, AfterViewInit } from "@angular/core";
import { Subject, Observable } from "rxjs";
import { takeUntil, last, map } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as _ from "lodash";

import { FuseConfigService } from "@fuse/services/config.service";
import { FuseSidebarService } from "@fuse/components/sidebar/sidebar.service";

import { navigation } from '../../../shared/utils/navigation/navigation';
import { Router } from '@angular/router';


@Component({
  selector: "toolbar",
  templateUrl: "./toolbar.component.html",
  styleUrls: ["./toolbar.component.scss"]
})
export class ToolbarComponent implements OnInit, OnDestroy, AfterViewInit {
  horizontalNavbar: boolean;
  rightNavbar: boolean;
  hiddenNavbar: boolean;
  currentUser: any;
  subscription: any;
  languages: any;
  navigation: any;
  toolbarPosition: any;
  navigationPosition: any;
  userPosition: any;
  separatorPosition: any;
  selectedLanguage: any;
  userStatusOptions: any[];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FuseSidebarService} _fuseSidebarService
   * @param {TranslateService} translate
   */
  constructor(
    private authService: AuthService,
    private router: Router,
    private _fuseConfigService: FuseConfigService,
    private _fuseSidebarService: FuseSidebarService,
    private translate: TranslateService,
    private sharedService: SharedService
  ) {
    // Set the defaults
    this.userStatusOptions = [
      {
        title: "Online",
        icon: "icon-checkbox-marked-circle",
        color: "#4CAF50"
      },
      {
        title: "Away",
        icon: "icon-clock",
        color: "#FFC107"
      },
      {
        title: "Do not Disturb",
        icon: "icon-minus-circle",
        color: "#F44336"
      },
      {
        title: "Invisible",
        icon: "icon-checkbox-blank-circle-outline",
        color: "#BDBDBD"
      },
      {
        title: "Offline",
        icon: "icon-checkbox-blank-circle-outline",
        color: "#616161"
      }
    ];

    this.languages = [
      {
        id: "fr",
        title: "Français",
        flag: "fr"
      },
      {
        id: "en",
        title: "English",
        flag: "us"
      },
      {
        id: "ar",
        title: "العربية",
        flag: "tn"
      }
    ];

    this.navigation = navigation;

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    // Subscribe to the config changes
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(settings => {
        this.horizontalNavbar = settings.layout.navbar.position === "top";
        this.rightNavbar = settings.layout.navbar.position === "right";
        this.hiddenNavbar = settings.layout.navbar.hidden === true;
      });
    const lang = localStorage.getItem("langue");
    // Set the selected language from default languages
    this.selectedLanguage = _.find(this.languages, {
      id: lang
    });
    this.getCurrentUser();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.getConfig(this.selectedLanguage);
    }, 20)

  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggle sidebar open
   *
   * @param key
   */
  toggleSidebarOpen(key): void {
    this._fuseSidebarService.getSidebar(key).toggleOpen();
  }

  /**
   * Get the current user firstName and lastName
   */
  getCurrentUser() {
    this.authService.currentUserSubject.subscribe((currentUser: any) => {
      this.currentUser = currentUser;
    });
  }

  /**
   * Search
   *
   * @param value searched value
   */
  search(value): void {
    // Do your search here...
    console.log(value);
  }
  /**
   * logout
   */
  seDisconnect() {
    this.authService.logout();
    // this.subscription.unsubscribe();
    this.router.navigate(["/login"]);
  }

  /**
   * Set the language
   *
   * @param lang selected language
   */
  setLanguage(lang): void {
    // Set the selected language for the toolbar
    this.selectedLanguage = lang;
    localStorage.setItem('langue', lang.id);
    this.getConfig(lang);    
    // Use the selected language for translations
  }
  /**
   *
   * @param lang get the current langugae
   */
  getConfig(lang) {
    this._fuseConfigService.getConfig().subscribe(settings => {
      this.toolbarConfiguration(settings, lang);
      this.translate.getTranslation(lang.id).subscribe(val => {
        this.translate.setDefaultLang(lang.id);
        this.translate.use(lang.id);
      });
    });
  }

  /**
   *
   * @param settings get the settings file
   * @param lang get the current language
   */
  toolbarConfiguration(settings, lang) {
    if (lang.id === "ar") {
      settings.layout.navbar.position = "right";
      this.toolbarPosition = {
        display: "block"
      };
      this.separatorPosition = {
        "margin-left": "210px"
      };
      this.userPosition = {
        position: "absolute"
      };
      this.navigationPosition = {
        "margin-left": "auto"
      };
    } else {
      settings.layout.navbar.position = "left";
      this.toolbarPosition = {
        display: "flex"
      };
      this.separatorPosition = {
        "margin-left": "0px"
      };
      this.userPosition = {
        position: "relative"
      };
      this.navigationPosition = {
        position: "absolute",
        "margin-left": "-78%"
      };
    }
  }
}
