import { MatIconModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSidebarModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { NavbarModule } from './navbar/navbar.module';
import { ToolbarModule } from './toolbar/toolbar.module';

import { LayoutComponent } from './layout.component';
import { TableModule } from 'primeng/table';
import { ContentModule } from 'src/app/core/layout/content/content.module';

@NgModule({
  declarations: [
    LayoutComponent
  ],
  imports: [
    RouterModule,
    FuseSharedModule,
    FuseSidebarModule,
    ContentModule,
    NavbarModule,
    ToolbarModule,
    TableModule,
    MatIconModule
  ],
  exports: [LayoutComponent]
})
export class LayoutModule { }
