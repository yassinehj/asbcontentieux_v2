import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule
} from "@angular/material";

import { FuseSharedModule } from "@fuse/shared.module";

import { ResetPassword2Component } from "./reset-password-2.component";
import { SharedModule } from "@shared/shared.module";

const routes = [
  {
    path: "reset-password/:email",
    component: ResetPassword2Component
  }
];

@NgModule({
  declarations: [ResetPassword2Component],
  imports: [
    RouterModule.forChild(routes),

    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,

    FuseSharedModule,
    SharedModule
  ]
})
export class ResetPassword2Module {}
