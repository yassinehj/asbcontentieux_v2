import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from "@angular/forms";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/internal/operators";

import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import { ActivatedRoute } from "@angular/router";
import { UtilisateurService } from "src/app/shared/services/utilisateur.service";

@Component({
  selector: "reset-password-2",
  templateUrl: "./reset-password-2.component.html",
  styleUrls: ["./reset-password-2.component.scss"],
  animations: fuseAnimations
})
export class ResetPassword2Component implements OnInit, OnDestroy {
  resetPasswordForm: FormGroup;

  // Private
  private _unsubscribeAll: Subject<any>;
  private email: string;

  constructor(
    private route: ActivatedRoute,
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private _utilisateurService: UtilisateurService
  ) {
    this.email = this.route.snapshot.paramMap.get("email");
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.resetPasswordForm = this._formBuilder.group({
      password: ["", Validators.required],
      passwordConfirm: ["", [Validators.required, confirmPasswordValidator]]
    });

    // Update the validity of the 'passwordConfirm' field
    // when the 'password' field changes
    this.resetPasswordForm
      .get("password")
      .valueChanges.pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        this.resetPasswordForm.get("passwordConfirm").updateValueAndValidity();
      });
  }
  changePassword() {
    if (this.resetPasswordForm.valid) {
      this._utilisateurService
        .resetPassword(this.email, this.resetPasswordForm.get("password").value)
        .subscribe((response: any) => {

        });
    }
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (
  control: AbstractControl
): ValidationErrors | null => {
  if (!control.parent || !control) {
    return null;
  }

  const password = control.parent.get("password");
  const passwordConfirm = control.parent.get("passwordConfirm");

  if (!password || !passwordConfirm) {
    return null;
  }

  if (passwordConfirm.value === "") {
    return null;
  }

  if (password.value === passwordConfirm.value) {
    return null;
  }

  return { passwordsNotMatching: true };
};
