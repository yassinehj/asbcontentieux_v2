import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-init-reset-password',
  templateUrl: './init-reset-password.component.html',
  styleUrls: ['./init-reset-password.component.css']
})
export class InitResetPasswordComponent implements OnInit {
  login: string = '';
  resetSuccess = false;
  isResetActivated = false;
  staticAlertClosed = false;
  submitted = false;
  alerteMessage: string;
  private bodyText: string;
  resetForm: FormGroup;
  constructor( private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      login: ['', Validators.required]

    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.resetForm.controls; }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.resetForm.invalid) {
      return;
    }

  }
}
