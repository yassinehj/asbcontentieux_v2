/* import { MatSnackBar } from "@angular/material"; */
import { UtilisateurService } from "src/app/shared/services/utilisateur.service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "forgot-password-2",
  templateUrl: "./forgot-password-2.component.html",
  styleUrls: ["./forgot-password-2.component.scss"],
  animations: fuseAnimations
})
export class ForgotPassword2Component implements OnInit {
  forgotPasswordForm: FormGroup;
  emailInexistante = false;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private _utilisateurService: UtilisateurService,
    /* private snackBar: MatSnackBar, */
    private toastr: ToastrService
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.forgotPasswordForm = this._formBuilder.group({
      email: ["", [Validators.required, Validators.email]]
    });
  }
  sendEmailUrlResetPassword() {
    if (this.forgotPasswordForm.valid) {
      const email = this.forgotPasswordForm.get("email").value;
      const link = window.location.host;
      this._utilisateurService
        .sendLinkResetPassword(link, email)
        .subscribe((responseSend: boolean) => {
          if (responseSend === true) {
            this.toastr.success("Nous avons envoyé un lien de confirmation");
          } else if (responseSend === false) {
            this.emailInexistante = true;
          } else {
            this.toastr.error("Erreur Technique! Contacter l'administrateur ");
          }
        });
    }
    /*} else {
        console.log("sorry");
      }
    });*/
  }
}
