import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { fuseAnimations } from "@fuse/animations";
import { Router } from "@angular/router";
import { AuthService } from "src/app/shared/services/auth.service";
import { UtilisateurService } from "src/app/shared/services/utilisateur.service";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  animations: fuseAnimations
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  loggedIn = true;
  user = { login: null, password: null };

  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private router: Router,
    private _formBuilder: FormBuilder,
    private authService: AuthService
  ) {}
  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      login: ["", [Validators.required]],
      password: ["", [Validators.required, Validators.minLength(4)]]
    });
  }

  get f() {
    return this.loginForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.login(this.loginForm.value).subscribe(
      response => {
              this.authService.loadToken(response.headers.get("Authorization"));
              this.authService.getByLogin(this.loginForm.value.login)
              .subscribe(currentUser => {
                localStorage.setItem("currentUser", JSON.stringify(currentUser));
                this.authService.currentUserSubject.next(currentUser);
                this.router.navigate(["/"]);
              });

            // store user details and jwt token in local storage to keep user logged in between page refreshes
          });
  }
  // reinitialiser ma mot de passe
  resetPassword() {
    this.router.navigate(["/forgot-password"]);
  }
}
