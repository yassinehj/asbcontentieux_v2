import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { Configuration } from '@shared/models/Configuration';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Subject } from 'rxjs';
import { ConfigurationService } from '@shared/services/configuration.service';
import { ToastService } from '@shared/services/toast.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-type-auxiliaire',
  templateUrl: './type-auxiliaire.component.html',
  styleUrls: ['./type-auxiliaire.component.scss']
})
export class TypeAuxiliaireComponent implements OnInit , OnDestroy{

    @ViewChild('table') table: Table;
    configuration: Configuration[] = [];
    configurationSlice: Configuration[];
    msgs: Message[] = [];
    first = 0;
    clonedConfiguration: { [s: string]: Configuration; } = {};
    _unsubscribeAll: Subject<any>;

    constructor(
        private configurationService: ConfigurationService,
        private sharedService: SharedService,
        private toastService: ToastService, private router: Router) {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
        this.getConfiguration();
    }

    addRow() {
        const configurationItem: Configuration = {
            idConfiguration: null,
            nom: null,
            nom_ar : null,
            type : null,  
            isEditable: true
        };
        this.configurationService.addFirstItem(configurationItem);
        this.table.initRowEdit(configurationItem);
        // this.first = this.configuration.length - 1;
    }

    getConfiguration() {
        this.configurationService.findAll()
            .subscribe((configuration: Configuration[]) => {
                 this.configuration = configuration;
            });
    }


   
    onRowEditInit(configuration: Configuration) {
        this.clonedConfiguration[configuration.idConfiguration] = { ...configuration };
        this.configuration.filter(row => row.isEditable).map(r => r.isEditable = false);
        configuration.isEditable = true;
    }

    onRowEditSave(configuration: Configuration) {
        if (configuration.idConfiguration != null) {
            this.updateConfiguration(configuration);
        } else {
            this.addConfiguration(configuration);
        }
    }

    onRowEditCancel(configuration: Configuration, index: number) {
        if (configuration.idConfiguration != null) {
            this.configuration[index] = this.clonedConfiguration[configuration.idConfiguration];
            delete this.clonedConfiguration[configuration.idConfiguration];
        } else {
            this.configuration.splice(index , 1);
        }

    }
    addConfiguration(configuration) {
        this.configurationSlice = this.configuration.slice(1 , this.configuration.length);
        if (!this.sharedService.findItemInList(this.configurationSlice , configuration , 'nom')) {
            configuration.isEditable = false;
            this.configurationService.save(configuration)
            .subscribe(configurationResponse => {
                    this.configurationService.updateItem(configurationResponse , configuration);
                    this.toastService.success('successAjout');
            });
        } else {
            this.table.initRowEdit(configuration);
            this.toastService.info('alreadyExists');
        }
    }

    updateConfiguration(configuration) {
        this.configurationSlice = this.configuration.slice(1 , this.configuration.length);
        if (!this.sharedService.findItemInList(this.configurationSlice , configuration , 'nom')) {
            configuration.isEditable = false;
            this.configurationService.update(configuration)
            .subscribe(configurationResponse => {
                    this.toastService.success('successUpdate');
            }, err => {
                console.log(err);
            });
        } else {
            this.table.initRowEdit(configuration);
            this.toastService.info('alreadyExists');
          }
    }

    deleteConfiguration(idConfiguration: number) {
        this.toastService.confirm('configuration', 'messageDelete', 'titleDelete')
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.configurationService.delete(idConfiguration)
                    .subscribe(data => {
                        this.getConfiguration();
                        this.toastService.success('succesSuppression');
                    }, err => {
                        console.log(err);
                    });
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

}
