import { Component, OnInit, ViewChild } from '@angular/core';
import { AgenceService } from '@shared/services/agence.service';
import { Table } from "primeng/table";
import { Agence } from '@shared/models/Agence';
import { Router } from '@angular/router';
import { ToastService } from '@shared/services/toast.service';


@Component({
  selector: 'app-agence-list',
  templateUrl: './agence-list.component.html',
  styleUrls: ['./agence-list.component.scss']
})
export class AgenceListComponent implements OnInit {
  @ViewChild("dt") private _table: Table;

  selectedAgence: Agence[];
  agences:Agence[];
  items: any[] = [];
  selected: any;




  constructor( private agenceService :AgenceService,
               private router: Router,
               private toastService: ToastService,
    
    ) { }

  ngOnInit() {

    this.getAllAgences();
  }


  getAllAgences(){

    this.agenceService.findAll().subscribe((listOfAgences: Agence[]) => {
      this.agences = listOfAgences;

      console.log(listOfAgences);
    });


  }

  EditAgence(){

    this.router.navigate(['/agence/edit/' , this.selectedAgence[0].id]);
    this.agenceService.editMode = true;

  }


  deleteAgence(){
    let selectedAgencesId = [];
 
    
    this.selectedAgence.forEach(item => {
      
      selectedAgencesId.push(item.id);
    });

    this.toastService.confirm('agence', 'messageDelete' , 'titleDelete')
    .subscribe(() => {
      this.agenceService.deleteAllAgences(selectedAgencesId)
      .subscribe(result => {
        console.log(selectedAgencesId)
        this.toastService.success("La suppression est effectué avec succés");
        this.agenceService.deleteAllItems(this.selectedAgence);
      });
    });



  }

}
