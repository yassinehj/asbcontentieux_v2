import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AgenceService } from '@shared/services/agence.service';
import { ToastService } from '@shared/services/toast.service';
import { Agence } from '@shared/models/Agence';

@Component({
  selector: 'app-agence-form',
  templateUrl: './agence-form.component.html',
  styleUrls: ['./agence-form.component.scss']
})
export class AgenceFormComponent implements OnInit {

  form: FormGroup;
  formErrors: any;
  editMode: boolean;
  id: any;


  /**
   * Inject the formBuilder
   * @param _formBuilder FormBuilder to create the FormGroup
   */
  constructor(private _formBuilder: FormBuilder,
    private agenceService :AgenceService,
    private router: Router,
    private route: ActivatedRoute,
    private toastService: ToastService
  ) {
    this.formErrors = {
      idInstitutionSiege: {},
      idInstitutionRegionale: {},
      codeInstitution: {},
      adresse: {},
    };
  }

  ngOnInit() {
    this.editMode = this.agenceService.editMode;
    this.initFormGroup();
    this.OnEditMode();

  }

  OnSubmit() {
    
      this.updateAgence();
    }
  
    initFormGroup() {
      this.form = this._formBuilder.group({
        id: [null],
        idInstitutionSiege: [''],
        idInstitutionRegionale: [''],
        codeInstitution: [''],
        adresse: ['']
      });
    }
  
    updateAgence(){

     
        this.toastService.confirm('agence', 'messageUpdate' , 'titleUpdate')
          .subscribe(() => {
            this.agenceService.update(this.form.value)
              .subscribe((updatedAgence: Agence) => {
                console.log(updatedAgence)
                if (updatedAgence) {
                  this.toastService.success('Agence is updated with success');
                  this.router.navigate(['/agence/list']);
                }
              });
          });
      }


      OnEditMode(){
       

            this.route.params
              .subscribe((params: Params) => {
                this.id = params.id;
                this.agenceService.findOne(this.id)
                  .subscribe((agence: Agence) => {
                    console.log(agence)
                    this.form.patchValue(agence);
                  });
              });
          


      }


    }










