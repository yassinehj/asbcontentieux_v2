import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GouvernoratService } from 'src/app/shared/services/gouvernorat.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Gouvernorat } from 'src/app/shared/models/gouvernorat';
import { Table } from 'primeng/table';
import { ToastService } from '@shared/services/toast.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-gouvernorat',
  templateUrl: './gouvernorat.component.html',
  styleUrls: ['./gouvernorat.component.scss']
})
export class GouvernoratComponent implements OnInit, OnDestroy {

  @ViewChild('table') table: Table;
  gouvernorat: Gouvernorat[];
  gouvernoratSlice: Gouvernorat[] = [];

  idPays: any;
  idGouvernorat: any;
  first = 0;
  isEditable: true;
  clonedGouvernorat: { [s: string]: Gouvernorat; } = {};
  _unsubscribeAll: Subject<any>;

  constructor(
    private gouvernoratService: GouvernoratService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sharedService: SharedService,
    private toastService: ToastService) {
      this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.idPays = this.activatedRoute.snapshot.params.idPays;
    this.getGouvernorat(this.idPays);
  }

  getGouvernorat(idPays: number) {
    this.gouvernoratService.findGouvernoratByPays(idPays).subscribe(data => {
      this.gouvernorat = data;
      
    });
  }

  onRowEditInit(gouvernorat: Gouvernorat) {
    this.clonedGouvernorat[gouvernorat.idGouvernorat] = { ...gouvernorat };
    this.gouvernorat.filter(row => row.isEditable).map(r => { r.isEditable = false; return r; });
    gouvernorat.isEditable = true;
  }

  onRowEditSave(gouvernorat: Gouvernorat) {
    if (gouvernorat.idGouvernorat != null) {
      this.updateGouvernorat(gouvernorat);
    } else {
      this.addGouvernorat(gouvernorat);
    }
  }

  onRowEditCancel(gouvernorat: Gouvernorat, index: number) {
    if (gouvernorat.idGouvernorat != null) {
    this.gouvernorat[index] = this.clonedGouvernorat[gouvernorat.idGouvernorat];
    delete this.clonedGouvernorat[gouvernorat.idGouvernorat];
    } else {
      this.gouvernorat.splice(index , 1);
  }

  }
  addRow() {
    const gouvernoratItem : Gouvernorat = {
      idGouvernorat: null,
      nom: null,
      isEditable: true
    };
    this.gouvernoratService.addFirstItem(gouvernoratItem);
    this.table.initRowEdit(gouvernoratItem);
   // this.first = this.gouvernorat.length - 1;
  }

  addGouvernorat(gouvernorat: Gouvernorat) {
    this.gouvernoratSlice = this.gouvernorat.slice(1 , this.gouvernorat.length);
    if (!this.sharedService.findItemInList(this.gouvernoratSlice , gouvernorat , 'nom'))  {
    gouvernorat.isEditable = false;
    this.gouvernoratService.save(gouvernorat , this.idPays)
    .subscribe((gouvernoratResponse) => {
        this.gouvernoratService.updateItem(gouvernoratResponse , gouvernorat);
        this.toastService.success('successAjout');
    });
    } else {
      this.table.initRowEdit(gouvernorat);
      this.toastService.info('alreadyExists');
    }
  }

  updateGouvernorat(gouvernorat: Gouvernorat) {
    this.gouvernoratSlice = this.gouvernorat.filter(item => item.idGouvernorat !== gouvernorat.idGouvernorat);
    if (!this.sharedService.findItemInList(this.gouvernoratSlice , gouvernorat , 'nom')) {
      gouvernorat.isEditable = false;
      this.gouvernoratService.update(gouvernorat , this.idPays)
      .subscribe(gouvernoratResponse => {
          this.toastService.success('successUpdate');
      });
    } else {
      this.table.initRowEdit(gouvernorat);
      this.toastService.info('alreadyExists');
    }
  }

  deleteGouvernorat(idGouvernorat: number) {
    this.toastService.confirm('gouvernorat' , 'messageDelete' , 'titleDelete')
      .pipe(takeUntil(this._unsubscribeAll)).
       subscribe(() => {
        this.gouvernoratService.delete(idGouvernorat)
        .subscribe(data => {
           this.getGouvernorat(this.idPays);
           this.toastService.success('succesSuppression');
        }, err => {
          console.log(err);
        });
      });
    }
 
  getListVilles(idGouvernorat: number , idPays: number) {
    this.router.navigate(['/ville', idGouvernorat] , {queryParams: {'idPays' : idPays} });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
}

}
