import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { MenuItem } from 'primeng/primeng';
import { Tribunal } from '@shared/models/Tribunal';
import { TribunalService } from '@shared/services/tribunal.service';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '@shared/services/shared.service';
import { Router, Params } from '@angular/router';
import { ToastService } from '@shared/services/toast.service';
import { switchMap } from 'rxjs/operators';
import { Adresse } from '@shared/models/Adresse';

@Component({
  selector: 'app-list-tribunal',
  templateUrl: './list-tribunal.component.html',
  styleUrls: ['./list-tribunal.component.css']
})
export class ListTribunalComponent implements OnInit {
  @ViewChild('dt') private _table: Table;
  cols: any[];
  items: MenuItem[];
  listTribunal: Tribunal[];
  listAdresse:Adresse[];
  disableDelete = true;
  tribunalSelectionnez: Tribunal[];
  buttonTranslate: any;

  selectedTribunal: any[] = [];

  constructor(
    private tribunalService: TribunalService,
    private translate: TranslateService,
    private sharedService: SharedService,
    private toastService: ToastService,
    private router: Router
  ) {
    this.tribunalSelectionnez = [];
    this.items = [];
  }

  ngOnInit() {
    this.getAllTribunal();
  }

  addTribunal() {
    this.router.navigate(['/tribunal/ajout']);
    this.sharedService.editMode.next(false);
  }

  editTribunal() {
    if (this.selectedTribunal.length === 1) {
      this.router.navigate(['/tribunal/edit/',this.selectedTribunal[0].idTribunal]);
      this.sharedService.editMode.next(true);
    } else if (this.selectedTribunal.length > 1) {
      this.toastService.info( "selectionneUneLigne");
    } else {
      this.toastService.info( "selectionnePlusieursLigne");
    }
  }

  deleteTribunal() {
    if (this.selectedTribunal.length > 0) {
    let setlectedTribunalId: any[] = this.selectedTribunal.map(
      item => item.idTribunal
    );
    this.toastService
      .confirm('tribunal', 'messageDelete', 'titleDelete')
      .subscribe(() => {
        this.tribunalService
          .deleteAllDTribunals(setlectedTribunalId)
          .subscribe(result => {
            console.log(setlectedTribunalId);
            this.toastService.success(
              'succesSuppression'
            );
            this.tribunalService.deleteAllItems(this.selectedTribunal);
            this.selectedTribunal = [];
            setlectedTribunalId = [];
          });
      });
    } else {
      this.toastService.info('selectionnePlusieursLigne');
    }
  }

  getAllTribunal() {
    this.tribunalService.findAll().subscribe(
      data => {
        this.listTribunal = data;
        console.log(data);
      },
      err => {
        console.log(err);
      }
    );
  }
}
