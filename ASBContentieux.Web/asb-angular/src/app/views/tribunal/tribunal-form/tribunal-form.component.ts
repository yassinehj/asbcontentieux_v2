import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TribunalService } from '@shared/services/tribunal.service';
import { SharedService } from '@shared/services/shared.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastService } from '@shared/services/toast.service';
import { AppUtils } from '@shared/utils/app-utils';
import { Tribunal } from '@shared/models/Tribunal';
import { Adresse } from '@shared/models/Adresse';
import { Ville } from '@shared/models/ville';
import { PaysService } from '@shared/services/pays.service';
import { GouvernoratService } from '@shared/services/gouvernorat.service';
import { Gouvernorat } from '@shared/models/gouvernorat';
import { VilleService } from '@shared/services/ville.service';
import { switchMap } from 'rxjs/operators';
import { Pays } from '@shared/models/Pays';

@Component({
  selector: 'app-tribunal-form',
  templateUrl: './tribunal-form.component.html',
  styleUrls: ['./tribunal-form.component.scss']
})
export class TribunalFormComponent implements OnInit {
  listVilles : Ville[] = [];
  public listPays : Pays[] = [];
 
  listGouvernorat : Gouvernorat[] = [];
  form: FormGroup;
  editMode: boolean;
  error: any;
  idTribunalSuivante: any;
  idTribunal: any;
  idPays: any;
  idGouvernorat: any;
  tribunal: Tribunal = Object.assign({} , this.tribunal)

  /**
   * Inject the formBuilder
   * @param _formBuilder FormBuilder to create the FormGroup
   */
  constructor(
    private _formBuilder: FormBuilder,
    private tribunalService: TribunalService,
    private sharedService: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private paysService: PaysService,
    private gouvernoratService: GouvernoratService,
    private villeService: VilleService,
    
  ) {
    this.error = AppUtils.error;
  }


  ngOnInit() {
    this.initFormGroup();
    this.getPays();
    this.editMode = this.tribunalService.editMode;
    this.OnEditMode();
    this.editModeChanges();
  }


  initFormGroup() {
    
      this.form = this._formBuilder.group({
      idTribunal:[''],
      type: ['', Validators.required],
      numTel: ['', [Validators.required,Validators.pattern("[0-9]+"),Validators.minLength(8),Validators.maxLength(8)]],
      fax: ['', [Validators.required,Validators.pattern("[0-9]+"),Validators.minLength(8),Validators.maxLength(8)]],
      description: [],
      pays: [],
      gouvernorat: [],
      ville: []
      
    });
  }
  

  getPays() {
        this.paysService.findAll()
            .subscribe((val) => {
                this.listPays = val;
                this.listPays.map(row => {
                // row.isEditable = false;
                });
            });
    }

  

    editModeChanges() {
      this.sharedService.editMode
      .subscribe((editMode: boolean) => {
        this.editMode = editMode;
      });
    }


  OnSubmit() {
    if (!this.editMode) {
      this.addTribunal();
    } else {
      this.updateTribunal();
    }
  }


  addTribunal(){
    let adresse: Adresse = {
      description: this.form.value.description,
      ville : this.form.value.ville
    }
    
    this.tribunal.type = this.form.value.type;
    this.tribunal.numTel = this.form.value.numTel;
    this.tribunal.fax = this.form.value.fax;
   
    this.tribunal.adresse = adresse;
    this.tribunalService.save(this.tribunal)
    .subscribe((addedTribunall:any)=>{
      console.log(addedTribunall);
      if(addedTribunall){
      this.toastService.success('successAjout');
      this.router.navigate(['/tribunal/list']);
      }
    });
  }


  updateTribunal(){
    let adresse: Adresse = {
      description: this.form.value.description,
      ville : this.form.value.ville
    }
    this.tribunal.type = this.form.value.type;
    this.tribunal.numTel = this.form.value.numTel;
    this.tribunal.fax = this.form.value.fax;
    this.tribunal.adresse = adresse;
   
    this.toastService.confirm("tribunal","messageUpdate","titleUpdate")
      .pipe(switchMap(() => this.tribunalService.update(this.tribunal)))
      .subscribe((updatedTribunal: Tribunal) => {
        console.log(updatedTribunal);
        if (updatedTribunal) {
          this.toastService.success('successUpdate');
          this.router.navigate(['/tribunal/list']);
        }
      }); 

  }

  OnEditMode(){
    this.route.params
    .subscribe((params: Params) => {
      this.idTribunal = params.idTribunal;
    
      this.tribunalService.findOne(this.idTribunal)
        .subscribe((tribunal: Tribunal) => {
          console.log("aaaaaa");
          console.log(tribunal)
          this.form.patchValue(tribunal);
          this.form.setControl('description' , new FormControl(tribunal.adresse.description));
          this.form.setControl('ville' , new FormControl(tribunal.adresse.ville));
          this.form.setControl('gouvernorat' , new FormControl(tribunal.adresse.ville.gouvernorat));
          this.form.setControl('pays' , new FormControl(tribunal.adresse.ville.gouvernorat.pays));
          this.listVilles[0] = tribunal.adresse.ville
          this.listGouvernorat[0] = tribunal.adresse.ville.gouvernorat;
          this.listPays[0] = tribunal.adresse.ville.gouvernorat.pays;
        });
    });
  }

  filterChanged(selectedValue:any){
    this.idPays=selectedValue.idPays;
   // this.listPays = this.pays.filter((item) => item.idPays == selectedValue);
   this.gouvernoratService.findGouvernoratByPays(selectedValue.idPays).subscribe(value=>{
   // console.log(value);
    this.listGouvernorat=value;

  })
  }

  filterChangedGouv(selectedValue1:any){
    console.log('value is ',selectedValue1.idGouvernorat);
    this.idGouvernorat=selectedValue1.idGouvernorat;
   // this.listVilles = this.ville.filter((item) => item.idVille == selectedValue1);
   this.villeService.findVilleByGouvernorat(selectedValue1.idGouvernorat).subscribe(value=>{
    //console.log(value);
    this.listVilles=value;

  })
  }

}
