import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuxiliaireService } from '@shared/services/auxiliaire.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SharedService } from '@shared/services/shared.service';
import { ToastService } from '@shared/services/toast.service';
import { Auxiliaire } from '@shared/models/auxiliaire';
import { switchMap } from 'rxjs/operators';
import { AppUtils } from '@shared/utils/app-utils';

@Component({
  selector: 'app-form-auxiliaire',
  templateUrl: './form-auxiliaire.component.html',
  styleUrls: ['./form-auxiliaire.component.scss']
})
export class FormAuxiliaireComponent implements OnInit {

  form: FormGroup;
  formErrors: any;
  error: any;
  editMode: boolean;
  
  /**
   * Inject the formBuilder
   * @param _formBuilder FormBuilder to create the FormGroup
   */
  constructor(private _formBuilder: FormBuilder,
    private auxiliaireService: AuxiliaireService,
    private router: Router,
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private toastService: ToastService) {
      
        this.error = AppUtils.error;
      
   
  }

  ngOnInit() {
    this.OnEditMode();
    this.editModeChanges();
    this.initFormGroup();
  }

  initFormGroup() {
    this.form = this._formBuilder.group({
      idAuxiliaire: [''],
      activated : [true],
      nom: ['', Validators.required],
      prenom: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      numTel: ['', Validators.required],
      specialite: ['', Validators.required],
      type: ['', Validators.required],
      numTel2: ['', Validators.required]
    });
  }
  OnSubmit() {
    if (!this.editMode) {
      this.addAuxiliaire();
    } else {
      this.updateAuxiliaire();
    }
  }

  editModeChanges() {
    this.sharedService.editMode
    .subscribe((editMode: boolean) => {
      this.editMode = editMode;
    });
  }

  OnEditMode() {
    this.route.params
      .subscribe((params: Params) => {
        if (params.id) {
          this.sharedService.editMode.next(true);
          this.auxiliaireService.findOne(params.id)
          .subscribe((auxiliaire: Auxiliaire) => {
              this.form.patchValue(auxiliaire);
          });
      }
    });
  }

  
  addAuxiliaire() {
    this.auxiliaireService.save(this.form.value)
      .subscribe((addedAuxiliaire: Auxiliaire) => {
        if (addedAuxiliaire) {
          this.toastService.success('Auxiliaire is added with success');
          this.router.navigate(['/auxiliaire/list']);
        }
      });
  }

  updateAuxiliaire() {
    this.toastService.confirm('auxiliaire', 'messageUpdate' , 'titleUpdate')
      .pipe(switchMap(() => this.auxiliaireService.update(this.form.value)))
      .subscribe((updatedAuxiliaire: Auxiliaire) => {
        if (updatedAuxiliaire) {
          this.toastService.success('Auxiliaire is updated with success');
          this.router.navigate(['/auxiliaire/list']);
        }
      });
  }


}
