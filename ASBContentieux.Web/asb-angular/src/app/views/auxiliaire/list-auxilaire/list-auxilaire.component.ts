import { AfterViewInit, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Auxiliaire } from '@shared/models/auxiliaire';
import { AuxiliaireService } from '@shared/services/auxiliaire.service';
import { SharedService } from '@shared/services/shared.service';
import { ToastService } from '@shared/services/toast.service';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-list-auxilaire',
  templateUrl: './list-auxilaire.component.html',
  styleUrls: ['./list-auxilaire.component.scss']
})
export class ListAuxilaireComponent implements OnInit, AfterViewInit {

  @ViewChild("dt") private _table: Table;
  auxiliaires: Auxiliaire[];
  selectedAuxiliaires: any[] = [];
  listId: any[] = [];

  constructor(
    private auxiliaireService: AuxiliaireService,
    private sharedService: SharedService,
    private toastService: ToastService,
    private router: Router) { }

  ngOnInit() {
    this.getAllAuxiliaires();
  }

  getAllAuxiliaires() {
    this.auxiliaireService.findAll()
      .subscribe((listOfAuxiliaires: Auxiliaire[]) => {
        this.auxiliaires = listOfAuxiliaires;
      });
  }

  ngAfterViewInit(): void {
    /*filter nom prenom auxiliaire*/
    this._table.filterConstraints['customAuxiliaireFilter'] = (
      idAuxiliaire: number,
      filter
    ): boolean => {
      const auxiliaire = this.auxiliaireService.getItemById(idAuxiliaire, 'idAuxiliaire');
      let nomPrenom: string;
      let prenomNom: string;
      nomPrenom = (auxiliaire.prenom + " " + auxiliaire.nom).toUpperCase();
      prenomNom = (auxiliaire.nom + " " + auxiliaire.prenom).toUpperCase();
      return (
        nomPrenom.includes(filter.toUpperCase()) ||
        prenomNom.includes(filter.toUpperCase())
      );
    };
  }

  addAuxiliaire() {
    this.router.navigate(['/auxiliaire/ajout']);
    this.sharedService.editMode.next(false);
  }

  editAuxiliaire() {
    if (this.selectedAuxiliaires.length === 1) {
      this.router.navigate(['/auxiliaire/edit/', this.selectedAuxiliaires[0].idAuxiliaire]);
      this.sharedService.editMode.next(true);
    } else if (this.selectedAuxiliaires.length > 1) {
      this.toastService.info("Vous pouvez modifier qu'un seul auxiliaire à la fois");
    } else {
      this.toastService.info("Vous devez séléctionner au minimum un auxiliaire");
    }

  }

  deleteAuxiliaire() {
    if (this.selectedAuxiliaires.length > 0) {
      for (let i = 0; i < this.selectedAuxiliaires.length; i++) {
        this.listId.push(this.selectedAuxiliaires[i].idAuxiliaire);
      }
      this.toastService.confirm('auxiliaire', 'messageDelete', 'titleDelete')
        .pipe(switchMap(() => this.auxiliaireService.deleteAllAuxiliaires(this.listId)))
        .subscribe(result => {
          this.toastService.success("La suppression est effectué avec succés");
          this.auxiliaireService.deleteAllItems(this.selectedAuxiliaires);
          this.selectedAuxiliaires = [];
          this.listId = [];
        });
    }
    else {
      this.toastService.info("Vous devez séléctionner au minimum un utilisateur");
    }
  }

  concat2Chaine(auxiliaire: Auxiliaire) {
    return auxiliaire.prenom + " " + auxiliaire.nom;
  }
}
