import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TribunalComponent } from './tribunal/tribunal.component';
import { ListTribunalComponent } from './tribunal/list-tribunal/list-tribunal.component';
import { PieceAffaireComponent } from './piece-affaire/piece-affaire.component';
import { PieceAffaireListComponent } from './piece-affaire/piece-affaire-list/piece-affaire-list.component';
import { VilleComponent } from './ville/ville.component';
import { AudienceComponent } from './audience/audience.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import {
  FuseSidebarModule,
  FuseProgressBarModule,
  FuseThemeOptionsModule
} from '@fuse/components';
import { TableModule } from 'primeng/table';
import { FuseModule } from '@fuse/fuse.module';
import { PaysComponent } from './pays/pays.component';
import { ListPaysComponent } from './pays/list-pays/list-pays.component';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { GouvernoratComponent } from './gouvernorat/gouvernorat.component';
import { PieceAffaireDetailsComponent } from './piece-affaire/piece-affaire-details/piece-affaire-details.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HabilitationModule } from './habilitation/habilitation.module';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { DossierPrecListComponent } from './dossier-prec/dossier-prec-list/dossier-prec-list.component';
import { DossierPrecComponent } from './dossier-prec/dossier-prec.component';
import {
  MatIconModule,
  MatButtonModule,
  MatInputModule,
  MatCardModule,
  MatTableModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatButtonToggleModule,
  MatTooltipModule,
  MatDialogModule,
  MatSelectModule ,

} from "@angular/material";
import { CalendarModule } from "primeng/calendar";
import { ListTacheComponent } from "./tache/list-tache/list-tache.component";
import { TacheComponent } from "./tache/tache.component";
import { SplitButtonModule } from "primeng/splitbutton";
import { TieredMenuModule } from "primeng/tieredmenu";
import { AuxiliaireComponent } from './auxiliaire/auxiliaire.component';
import { ListAuxilaireComponent } from './auxiliaire/list-auxilaire/list-auxilaire.component';
import { FormAuxiliaireComponent } from './auxiliaire/form-auxiliaire/form-auxiliaire.component';

import { DialogPieceAffaireComponent } from './piece-affaire/dialog-piece-affaire/dialog-piece-affaire.component';
import { DialogPieceAffaireFolderComponent } from './piece-affaire/dialog-piece-affaire-folder/dialog-piece-affaire-folder.component';
import { TreeModule, FileUploadModule, DialogModule, DropdownModule, DataTableModule, ListboxModule, Listbox } from 'primeng/primeng';
import { TribunalFormComponent } from './tribunal/tribunal-form/tribunal-form.component';
import { AgenceComponent } from './agence/agence.component';
import { AgenceListComponent } from './agence/agence-list/agence-list.component';
import { AgenceFormComponent } from './agence/agence-form/agence-form.component';
import { DebiteurListComponent } from './debiteur/debiteur-list/debiteur-list.component';
import { DebiteurComponent } from './debiteur/debiteur.component';
import { DebiteurFormComponent } from './debiteur/debiteur-form/debiteur-form.component';
import { AjoutTacheComponent } from './tache/ajout-tache/ajout-tache.component';
import { DossierPrecEditComponent } from './dossier-prec/dossier-prec-edit/dossier-prec-edit.component';
import { HistoriqueRecouvrementComponent } from './dossier-prec/historiqueRecouvrement/historiqueRecouvrement.component';
import { SharedModule } from '@shared/shared.module';
import { ListAudienceComponent } from './audience/list-audience/list-audience.component';
import { AudienceFormComponent } from './audience/audience-form/audience-form.component';
import { MovePiecesAffairesFolderComponent } from './piece-affaire/move-pieces-affaires-folder/move-pieces-affaires-folder.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { TypeAuxiliaireComponent } from './configuration/type-auxiliaire/type-auxiliaire.component';


@NgModule({
  declarations: [
    TribunalComponent,
    ListTribunalComponent,
    PieceAffaireComponent,
    PieceAffaireListComponent,
    PieceAffaireDetailsComponent,
    DialogPieceAffaireComponent,
    DialogPieceAffaireFolderComponent,
    MovePiecesAffairesFolderComponent ,
    VilleComponent,
    AudienceComponent,
    PaysComponent,
    ListPaysComponent,
    GouvernoratComponent,
    TacheComponent,
    ListTacheComponent,
    DossierPrecComponent,
    DossierPrecListComponent,
    AjoutTacheComponent,
    AuxiliaireComponent,
    ListAuxilaireComponent,
    FormAuxiliaireComponent,
    TribunalFormComponent,
    AgenceComponent,
    AgenceListComponent,
    AgenceFormComponent,
    DebiteurComponent,
    DebiteurListComponent,
    DebiteurFormComponent,
    AjoutTacheComponent,
    DossierPrecEditComponent,
    HistoriqueRecouvrementComponent,
    ListAudienceComponent,
    AudienceFormComponent,
    CalendarComponent,
    ConfigurationComponent,
    TypeAuxiliaireComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FuseSharedModule,
    FuseSidebarModule,
    SharedModule,
    FuseModule,
    TableModule,
    FuseProgressBarModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatButtonToggleModule,
    ButtonModule,
    InputTextModule,
    MatIconModule,  
    MatCardModule,
    MatInputModule,
    MatCheckboxModule,
    MatTooltipModule,
    FlexLayoutModule,
    MatFormFieldModule,
    FuseSidebarModule,
    FuseThemeOptionsModule,
    MatTableModule,
    MatPaginatorModule,
    HabilitationModule,
    CalendarModule,
    SplitButtonModule,
    TieredMenuModule,
    DataTableModule,
    DialogModule,
    FileUploadModule,
    TreeModule,
    MatDialogModule,
    MatSelectModule ,
    MatDatepickerModule,
    MatNativeDateModule,
    ListboxModule
    
  ],
  providers: [MatDatepickerModule],
  entryComponents: [
    DialogPieceAffaireComponent,
    DialogPieceAffaireFolderComponent
  ]
})
export class ViewsModule {}
