import { Component, OnInit,ViewChild  } from '@angular/core';
import { Debiteur } from '@shared/models/Debiteur';
import { DebiteurService } from '@shared/services/debiteur.service';
import { Table } from "primeng/table";
import { Router } from '@angular/router';
import { ToastService } from '@shared/services/toast.service';

@Component({
  selector: 'app-debiteur-list',
  templateUrl: './debiteur-list.component.html',
  styleUrls: ['./debiteur-list.component.scss']
})
export class DebiteurListComponent implements OnInit {
  @ViewChild("dt") private _table: Table;


   selectedDebiteur : Debiteur[];
   debiteurs : Debiteur[];
   items: any[] = [];
   selected: any;
   fullName: string;


    constructor( private debiteurService : DebiteurService,
    private router: Router,
    private toastService: ToastService,
    ) { }

  ngOnInit() {
    
    this.getAllDebiteur();
        /*filter nom prenom debiteur*/
    this._table.filterConstraints["debiteurFilter"] = (
      idDebiteur: number,
      filter
    ): boolean => {
      const debiteur = this.debiteurService.getItemById(idDebiteur, 'idDebiteur');
      let nomPrenom: string;
      let prenomNom: string;
       nomPrenom = (debiteur.firstName + " " + debiteur.lastName).toUpperCase();
      prenomNom = (debiteur.lastName + " " + debiteur.firstName).toUpperCase(); 
      return (
        nomPrenom.includes(filter.toUpperCase()) ||
        prenomNom.includes(filter.toUpperCase())
      );
    };
  }



  getAllDebiteur(){


    this.debiteurService.findAll().subscribe((listOfDebiteurs: Debiteur[]) => {
      this.debiteurs = listOfDebiteurs;

      console.log(listOfDebiteurs);
    });



  }

  EditDebiteur(){

  
   this.router.navigate(['/debiteur/edit/' , this.selectedDebiteur[0].idDebiteur]);

    this.debiteurService.editMode = true;

  }




  deleteDebiteur(){

    let setlectedDebiteurId = [];

   this.selectedDebiteur.forEach(item => {

    setlectedDebiteurId.push(item.idDebiteur);
   })

   this.toastService.confirm('debiteur', 'messageDelete' , 'titleDelete')
   .subscribe(() => {
     this.debiteurService.deleteAllDebiteurs(setlectedDebiteurId)
     .subscribe(result => {
       console.log(setlectedDebiteurId)
       this.toastService.success("La suppression est effectué avec succés");
       this.debiteurService.deleteAllItems(this.selectedDebiteur);
     });
   });


  }

  tooltipDebiteur(debiteur: Debiteur) {
    return (
      "First Name : " +
      debiteur.firstName +
      "\n Last Name: " +
      debiteur.lastName 
    );
  }


  concat2Chaine(debiteur: Debiteur) {
    this.fullName = debiteur.firstName + " " + debiteur.lastName;
    return this.fullName;
  }

}
