import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormBuilder} from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastService } from '@shared/services/toast.service';
import { DebiteurService } from '@shared/services/debiteur.service';
import { Debiteur } from '@shared/models/Debiteur';

@Component({
  selector: 'app-debiteur-form',
  templateUrl: './debiteur-form.component.html',
  styleUrls: ['./debiteur-form.component.scss']
})
export class DebiteurFormComponent implements OnInit {

  form: FormGroup;
  formErrors: any;
  editMode: boolean;
  id: any;


  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private debiteurService : DebiteurService



    ) { }

  ngOnInit() {

    this.editMode = this.debiteurService.editMode;
    this.initFormGroup();
    this.OnEditMode();
  }

  OnSubmit(){
    this.updateDebiteur();
  }

  OnEditMode(){

    this.route.params
    .subscribe((params: Params) => {
      this.id = params.id;
      this.debiteurService.findOne(this.id)
        .subscribe((debiteur: Debiteur) => {
          console.log(debiteur)
          this.form.patchValue(debiteur);
        });
    });


  }


  initFormGroup() {

    this.form = this._formBuilder.group({
      idDebiteur: [null],
      firstName: [''],
      tel: [''],
      cin: [''],
      profession: [''],
      adresse: ['']
    });




  }


  updateDebiteur(){

    this.toastService.confirm('debiteur', 'messageUpdate' , 'titleUpdate')
          .subscribe(() => {
            this.debiteurService.update(this.form.value)
              .subscribe((updatedDebiteur: Debiteur) => {
                console.log(updatedDebiteur)
                if (updatedDebiteur) {
                  this.toastService.success('Debiteur is updated with success');
                  this.router.navigate(['/debiteur/list']);
                }
              });
          });



  }

}
