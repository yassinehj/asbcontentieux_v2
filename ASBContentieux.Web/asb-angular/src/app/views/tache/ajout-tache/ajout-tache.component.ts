import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Tache } from '@shared/models/tache';
import { TacheService } from 'src/app/shared/services/tache.service';
import { ToastService } from 'src/app/shared/services/toast.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SharedService } from 'src/app/shared/services/shared.service';
import { switchMap } from 'rxjs/operators';
import * as moment from 'moment';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AppUtils } from '@shared/utils/app-utils';

@Component({
  selector: 'app-ajout-tache',
  templateUrl: './ajout-tache.component.html',
  styleUrls: ['./ajout-tache.component.scss'],
  providers: []
})
export class AjoutTacheComponent implements OnInit {
  form: FormGroup;
  error: any;
  editMode: boolean;

  /**
   * Inject the formBuilder
   * @param _formBuilder FormBuilder to create the FormGroup
   */
  constructor(
    private _formBuilder: FormBuilder,
    private tacheService: TacheService,
    private router: Router,
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private toastService: ToastService
  ) {
    this.error = AppUtils.error;
  }

  ngOnInit() {
    this.OnEditMode();
    this.editModeChanges();
    this.initFormGroup();
  }

  initFormGroup() {
    this.form = this._formBuilder.group({
      idTache: [''],
      activated: [true],
      titre: ['', Validators.required],
      description: ['', [Validators.required]],
      dateDebut: ['', Validators.required],
      dateFin: ['', Validators.required],
      statut: ['', Validators.required],
      dureeNotif: ['', Validators.required]
    });
  }
  OnSubmit() {
    if (!this.editMode) {
      this.addTache();
    } else {
      this.updateTache();
    }
  }

  editModeChanges() {
    this.sharedService.editMode.subscribe((editMode: boolean) => {
      this.editMode = editMode;
    });
  }

  OnEditMode() {
    this.route.params.subscribe((params: Params) => {
      if (params.id) {
        this.sharedService.editMode.next(true);
        this.tacheService.findOne(params.id).subscribe((tache: Tache) => {
          tache.dateDebut = new Date(tache.dateDebut);
          tache.dateFin = new Date(tache.dateFin);
          this.form.patchValue(tache);
        });
      }
    });
  }

  addTache() {
    this.tacheService.save(this.form.value).subscribe((addedTache: Tache) => {
      if (addedTache) {
        this.toastService.success('successAjout');
        this.router.navigate(['/tache/list']);
      }
    });
  }

  updateTache() {
    this.toastService
      .confirm('tache', 'messageUpdate', 'titleUpdate')
      .pipe(switchMap(() => this.tacheService.update(this.form.value)))
      .subscribe((updatedTache: Tache) => {
        if (updatedTache) {
          this.toastService.success('successUpdate');
          this.router.navigate(['/tache/list']);
        }
      });
  }
}
