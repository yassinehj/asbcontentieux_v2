import { Component, OnInit } from '@angular/core';
import { TacheService} from "src/app/shared/services/tache.service";

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { Tache } from 'src/app/shared/models/tache';
import { DatePipe } from '@angular/common';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ToastService } from 'src/app/shared/services/toast.service';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-list-tache',
  templateUrl: './list-tache.component.html',
  styleUrls: ['./list-tache.component.css'],
  providers: [DatePipe]
})
export class ListTacheComponent implements OnInit {

  taches: Tache[];
  selectedTaches: any[] = [];
  listId: any[] = [];
  constructor(
    private tacheService: TacheService,
    private sharedService: SharedService,
    private toastService: ToastService,
    private router: Router) { }

  ngOnInit() {
    this.getAllTaches();
  }

  getAllTaches() {
    this.tacheService.findAll()
      .subscribe((listOfTaches: Tache[]) => {
        this.taches = listOfTaches;
      });
  }

  addTache() {
    this.router.navigate(['/tache/ajout']);
    this.sharedService.editMode.next(false);
  }

  editTache() {
    if (this.selectedTaches.length === 1) {
      this.router.navigate(['/tache/edit/', this.selectedTaches[0].idTache]);
      this.sharedService.editMode.next(true);
    } else if (this.selectedTaches.length > 1) {
      this.toastService.info('selectionneUneLigne');
    } else {
      this.toastService.info('selectionnePlusieursLigne');
    }

  }

  deleteTache() {
    if (this.selectedTaches.length > 0) {
      for (let i = 0; i < this.selectedTaches.length; i++) {
        this.listId.push(this.selectedTaches[i].idTache);
      }
      this.toastService.confirm('tache' , 'messageDelete', 'titleDelete')
      .pipe(switchMap(() => this.tacheService.deleteAllTaches(this.listId)))
      .subscribe(result => {
        this.toastService.success('succesSuppression');
        this.tacheService.deleteAllItems(this.selectedTaches);
        this.selectedTaches = [];
      });
    } else {
      this.toastService.info('selectionnePlusieursLigne');
    }
  }

}
