import { HabilitationService } from './../../../shared/services/habilitation.service';
import { PaysService } from './../../../shared/services/pays.service';
import { Component, OnInit } from '@angular/core';
import { Pays } from '@shared/models/Pays';
import { forEach } from '@angular/router/src/utils/collection';
import { Router } from '@angular/router';
import { Role } from '@shared/models/role';
import { switchMap } from 'rxjs/operators';

@Component({
    selector: 'app-role',
    templateUrl: './role.component.html',
    styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

    libelle: string;
    modules: any[] = [];
    selectedModule: any;

    constructor(private habilitationService: HabilitationService,
                private router: Router) { }

    ngOnInit() {
        this.habilitationService.getAllModules()
        .subscribe((listOfModules: any[]) => {
            this.modules = listOfModules;
        });
    }

    addRole(powers) {
        powers.forEach(power => {
         power.nomRole = this.libelle;
        });
        this.habilitationService.addRole(powers)
        .pipe(switchMap(() => this.habilitationService.getAllRoles()))      
            .subscribe((listOfRoles: Role[]) => {
              this.selectedModule = null;
            });
    }
}
