import { HabilitationService } from './../../../../shared/services/habilitation.service';
import { Component, OnInit } from '@angular/core';
import { Role } from '@shared/models/role';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent implements OnInit {

  roles: Role[] = [];

  constructor(private habilitationService: HabilitationService) { }

  ngOnInit() {
    this.habilitationService.getAllRoles()
    .subscribe((listOfRoles: Role[]) => {
      this.roles = listOfRoles;
      console.log(this.roles)
    });
   }

   deleteRole(nomRole) {
    this.habilitationService.deleteRole(nomRole)
    .subscribe(() => {
      const index = this.habilitationService.roles.findIndex((el: Role) => el.name === nomRole);
      this.habilitationService.roles.splice(index , 1 );
    });
   }
}
