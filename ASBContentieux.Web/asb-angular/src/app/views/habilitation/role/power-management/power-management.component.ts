import { Component, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { HabilitationService } from '@shared/services/habilitation.service';

@Component({
  selector: 'app-power-management',
  templateUrl: './power-management.component.html',
  styleUrls: ['./power-management.component.scss']
})
export class PowerManagementComponent implements OnChanges {

  availablePowers: any[];
  availablePowerSlice: any[] = [];
  selectedPowers: any[];
  draggedPower: any;
  @Input()
  selectedModule: string;
  @Output()
  powersEvent = new EventEmitter();

  constructor(private habilitationService: HabilitationService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedModule) {
      this.selectedPowers = [];
      this.habilitationService.getAllMenusByModule(this.selectedModule)
        .subscribe((listOfPowers: any[]) => {
          this.availablePowers = listOfPowers;
          this.availablePowerSlice = this.availablePowers.slice();
        });
    }
  }

  dragStart(power: any) {
    this.draggedPower = power;
  }

  drop(dropPosition) {
    if (this.draggedPower) {
      let draggedPowerIndex: any = null;
      if (dropPosition === 'array') {
        draggedPowerIndex = this.findIndex(this.draggedPower, this.availablePowers);
        this.selectedPowers = [...this.selectedPowers, this.draggedPower];
        this.availablePowers = this.availablePowers.filter((val, i) => i !== draggedPowerIndex);
        this.availablePowerSlice = this.availablePowers.slice();
      } else if (dropPosition === 'panel') {
        draggedPowerIndex = this.findIndex(this.draggedPower, this.selectedPowers);
        this.availablePowers = [...this.availablePowers, this.draggedPower];
        this.availablePowerSlice = this.availablePowers.slice();
        this.selectedPowers = this.selectedPowers.filter((val, i) => i !== draggedPowerIndex);
      }
      this.draggedPower = null;
    }
  }

  dragEnd() {
    this.draggedPower = null;
  }

  findIndex(car: any, array) {
    let index = -1;
    for (let i = 0; i < array.length; i++) {
      if (car.idMenu === array[i].idMenu) {
        index = i;
        break;
      }
    }
    return index;
  }

  filter($event) {
    if ($event.data) {
      this.availablePowers = this.availablePowers.filter((val, i) =>
        val.nomMenu.includes($event.data) ||
        val.nomMenu.includes($event.data.toUpperCase()));
    } else {
      this.availablePowers = this.availablePowerSlice;
    }
  }

  changeFlag(power: any, flag: any) {
    power[flag] = !power[flag];
  }

  setPowers() {
    this.powersEvent.emit(this.selectedPowers);
  }
}
