import { AppUtils } from '@shared/utils/app-utils';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { ToastService } from 'src/app/shared/services/toast.service';
import { Utilisateur } from 'src/app/shared/models/utilisateur';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { SharedService } from '@shared/services/shared.service';
import { Profession } from '@shared/models/profession';

@Component({
  selector: 'app-utilisateur-form',
  templateUrl: './utilisateur-form.component.html',
  styleUrls: ['./utilisateur-form.component.scss']
})
export class UtilisateurFormComponent implements OnInit {

  form: FormGroup;
  editMode: boolean;
  error: any;
  professions: Profession[];
  
  /**
   * Inject the formBuilder
   * @param _formBuilder FormBuilder to create the FormGroup
   */
  constructor(
    private _formBuilder: FormBuilder,
    private userService: UtilisateurService,
    private sharedService: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    private toastService: ToastService
  ) {
    this.error = AppUtils.error;
  }

  ngOnInit() {
    /**
     * Init the form group using the formBuilder
     */
    this.initFormGroup();
    this.getAllProfessions();
    /**
     * Method called when the editMode is activated
     */
    this.OnEditMode();
    /**
     * Subscribe to editMode Changes
     */
    this.editModeChanges();
  }


  initFormGroup() {
    this.form = this._formBuilder.group({
      id: [''],
      activated: [true],
      login: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      refProfession: ['', Validators.required],
      dateCreation : [new Date()]
    });
  }

  getAllProfessions() {
    this.userService.getAllProfessions()
    .subscribe((listOfProfessions: Profession[]) => {
      this.professions = listOfProfessions;
    });
  }

  editModeChanges() {
    this.sharedService.editMode
    .subscribe((editMode: boolean) => {
      this.editMode = editMode;
    });
  }

  OnEditMode() {
    this.route.params
      .subscribe((params: Params) => {
        if (params.id) {
          this.sharedService.editMode.next(true);
          this.userService.findOne(params.id)
          .subscribe((user: Utilisateur) => {
              this.form.patchValue(user);
              this.professions[0] = user.refProfession;
          });
      }
    });
  }

  OnSubmit() {
    if (!this.editMode) {
      this.addUser();
    } else {
      this.updateUser();
    }
  }

  addUser() {
    this.userService.save(this.form.value)
      .subscribe((addedUser: Utilisateur) => {
        if (addedUser) {
          this.toastService.success('successAjout');
          this.router.navigate(['/utilisateur/list']);
        }
      });
  }

  updateUser() {
    this.toastService.confirm('utilisateur' , 'messageUpdate', 'titleUpdate')
      .pipe(switchMap(() => this.userService.update(this.form.value)))
      .subscribe((updatedUser: Utilisateur) => {
        if (updatedUser) {
          this.toastService.success('successUpdate');
          this.router.navigate(['/utilisateur/list']);
        }
      });
  }
}
