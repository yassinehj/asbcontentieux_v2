import { AppUtils } from '@shared/utils/app-utils';
import { UtilisateurService } from '@shared/services/utilisateur.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Utilisateur } from 'src/app/shared/models/utilisateur';
import { ToastService } from '@shared/services/toast.service';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { SharedService } from '@shared/services/shared.service';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-list-utilisateur',
  templateUrl: './list-utilisateur.component.html',
  styleUrls: ['./list-utilisateur.component.scss'],
  animations: [
    AppUtils.triggerAnimation()
  ]
})
export class ListUtilisateurComponent implements OnInit, AfterViewInit {

  @ViewChild("dt") private _table: Table;
  users: Utilisateur[];
  selectedUsers: any[] = [];
  rowGroupMetadata: any;

  constructor(
    private userService: UtilisateurService,
    private sharedService: SharedService,
    private toastService: ToastService,
    private router: Router) { }

  ngOnInit() {
    this.getAllUsers();
  }

  ngAfterViewInit(): void {
    /*filter nom prenom user*/
    this._table.filterConstraints['customUserFilter'] = (
      id: number,
      filter
    ): boolean => {
      const user = this.userService.getItemById(id, 'id');
      let nomPrenom: string;
      let prenomNom: string;
      nomPrenom = (user.firstName + " " + user.lastName).toUpperCase();
      prenomNom = (user.lastName + " " + user.firstName).toUpperCase();
      return (
        nomPrenom.includes(filter.toUpperCase()) ||
        prenomNom.includes(filter.toUpperCase())
      );
    };
  }

  getAllUsers() {
    this.userService.findAll()
      .subscribe((listOfUsers: Utilisateur[]) => {
        this.users = listOfUsers;
        this.updateRowGroupMetaData();
      });
  }
  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.users) {
      for (let i = 0; i < this.users.length; i++) {
        const rowData = this.users[i];
        const libelle = rowData.refProfession.libelleProfession;
        if (i === 0) {
          this.rowGroupMetadata[libelle] = { index: 0, size: 1 };
        } else {
          const previousRowData = this.users[i - 1];
          const previousRowGroup = previousRowData.refProfession.libelleProfession;
          if (libelle === previousRowGroup) {
            this.rowGroupMetadata[libelle].size++;
          } else {
            this.rowGroupMetadata[libelle] = { index: i, size: 1 };
          }
        }
      }
    }
  }
  addUser() {
    this.sharedService.editMode.next(false);
    this.router.navigate(['/utilisateur/ajout']);
  }

  editUser() {
    if (this.selectedUsers.length === 1) {
      this.router.navigate(['/utilisateur/edit/', this.selectedUsers[0].id]);
      this.sharedService.editMode.next(true);
    } else if (this.selectedUsers.length > 1) {
      this.toastService.info('selectionneUneLigne');
    } else {
      this.toastService.info('selectionnePlusieursLigne');
    }

  }

  deleteUser() {
    if (this.selectedUsers.length > 0) {
      let idUsers: number[] = this.selectedUsers.map(selectedUser => selectedUser.id);
      this.toastService.confirm('utilisateur', 'messageDelete', 'titleDelete')
        .pipe(switchMap(() => this.userService.deleteAllUsers(idUsers)))
        .subscribe(result => {
          this.toastService.success('succesSuppression');
          this.userService.deleteAllItems(this.selectedUsers);
          this.selectedUsers = [];
          idUsers = [];
        });
    } else {
      this.toastService.info('selectionnePlusieursLigne');
    }
  }

  concat2Chaine(user: Utilisateur) {
    return user.firstName + " " + user.lastName;
  }
}
