import { ButtonModule } from 'primeng/button';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { UtilisateurFormComponent } from './utilisateur/utilisateur-form/utilisateur-form.component';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatIconModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { DragDropModule } from 'primeng/dragdrop';
import { DropdownModule}  from 'primeng/dropdown';

import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListUtilisateurComponent } from './utilisateur/list-utilisateur/list-utilisateur.component';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { TieredMenuModule } from 'primeng/primeng';
import { RoleComponent } from './role/role.component';
import { PanelModule } from 'primeng/panel';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PowerManagementComponent } from './role/power-management/power-management.component';
import {CardModule} from 'primeng/card';
import { RoleListComponent } from './role/role-list/role-list.component';
@NgModule({
  declarations: [
    UtilisateurComponent,
    UtilisateurFormComponent,
    ListUtilisateurComponent,
    RoleComponent,
    PowerManagementComponent,
    RoleListComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    FuseSharedModule,
    RouterModule,
    SharedModule,
    TableModule,
    InputTextModule,
    TieredMenuModule,
    ButtonModule,
    DragDropModule,
    DropdownModule,
    ScrollPanelModule,
    PanelModule,
    CardModule
  ]
})
export class HabilitationModule { }
