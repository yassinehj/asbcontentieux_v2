import { FormBuilder } from "@angular/forms";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-dossier-prec-edit",
  templateUrl: "./dossier-prec-edit.component.html",
  styleUrls: ["./dossier-prec-edit.component.scss"]
})
export class DossierPrecEditComponent implements OnInit {
  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {}
}
