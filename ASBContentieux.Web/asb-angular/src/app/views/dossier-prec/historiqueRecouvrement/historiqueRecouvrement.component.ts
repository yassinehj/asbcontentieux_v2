import { HistoriqueRecouvrementService } from "./../../../shared/services/historiqueRecouvrement.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { stringToKeyValue } from "@angular/flex-layout/extended/typings/style/style-transforms";
import { Table } from "primeng/table";

@Component({
  selector: "app-historiqueRecouvrement",
  templateUrl: "./historiqueRecouvrement.component.html",
  styleUrls: ["./historiqueRecouvrement.component.scss"]
})
export class HistoriqueRecouvrementComponent implements OnInit {
  @ViewChild("dt") private _table: Table;
  dateDebut: Date;
  dateFin: Date;
  constructor(
    private historiqueRecouvrementService: HistoriqueRecouvrementService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    /*filter date*/
    this._table.filterConstraints["date"] = (value, filter): boolean => {
      // Make sure the value and the filter are Dates
      return (
        new Date(value).toLocaleDateString() ===
        new Date(filter).toLocaleDateString()
      );
    };
    this.historiqueRecouvrementService
      .findAllByIdDossierPrec(
        this.activatedRoute.snapshot.paramMap.get("idDosPrec")
      )
      .subscribe(listHist => {
        this.historiqueRecouvrementService.historiqueRecouvrement = listHist;
      });
  }
}
