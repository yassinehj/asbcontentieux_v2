import { Router } from '@angular/router';
import { SharedService } from './../../../shared/services/shared.service';
import { DossierPrec } from './../../../shared/models/DossierPrec';
import { Table } from 'primeng/table';
import { Agence } from '../../../shared/models/agence';
import { Debiteur } from './../../../shared/models/Debiteur';
import { DossierPrecService } from '@shared/services/dossier-prec.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '@shared/services/toast.service';

@Component({
  selector: 'app-dossier-prec-list',
  templateUrl: './dossier-prec-list.component.html',
  styleUrls: ['./dossier-prec-list.component.scss']
})
export class DossierPrecListComponent implements OnInit {
  @ViewChild('dt') private _table: Table;
  date: Date;
  cols: any[];
  items: MenuItem[];
  archiver = false;
  disableDelete = true;
  dossierPrecSelectionnez: DossierPrec[];
  buttonTranslate: any;
  messageToastTranslate: any;
  confirmationDialogTranslate: any;
  /* items Action */

  constructor(
    private dossierPrecService: DossierPrecService,
    private translate: TranslateService,
    private toastService: ToastService,
    private router: Router
  ) {
    this.dossierPrecSelectionnez = [];
    this.items = [];
  }

  ngOnInit() {
    this.getAllDossierPrec();
    /* listen To language changes */

    /*filter date*/
    this._table.filterConstraints['date'] = (value, filter): boolean => {
      // Make sure the value and the filter are Dates
      return (
        new Date(value).toLocaleDateString() ===
        new Date(filter).toLocaleDateString()
      );
    };
    /*filter nom prenom debiteur*/
    this._table.filterConstraints['debiteur'] = (
      debiteur: Debiteur,
      filter
    ): boolean => {
      let nomPrenom: string;
      let prenomNom: string;
      nomPrenom = (debiteur.firstName + ' ' + debiteur.lastName).toUpperCase();
      prenomNom = (debiteur.lastName + ' ' + debiteur.firstName).toUpperCase();
      return (
        nomPrenom.includes(filter.toUpperCase()) ||
        prenomNom.includes(filter.toUpperCase())
      );
    };
  }
  onArchivage() {
    this.archiver = !this.archiver;
    this.dossierPrecService
      .findAllByArchiver(this.archiver)
      .subscribe(dossiersPrec => {
        this.dossierPrecService.dossierPrec = dossiersPrec;
      });
  }
  getAllDossierPrec() {
    this.dossierPrecService
      .findAllByArchiver(this.archiver)
      .subscribe(dossiersPrec => {
        this.dossierPrecService.dossierPrec = dossiersPrec;
      });
  }
  EditDossiersPrec() {
    if (
      this.dossierPrecSelectionnez &&
      (this.dossierPrecSelectionnez.length === 0 ||
        this.dossierPrecSelectionnez.length !== 1)
    ) {
      this.toastService.warning(this.messageToastTranslate.selectionneUnLigne);
    } else {
      this.router.navigate([
        '/dossier-prec/edit/',
        this.dossierPrecSelectionnez[0].idDossierPrec
      ]);
    }
  }
  goHistoriqueRecouvrement() {
    if (
      this.dossierPrecSelectionnez &&
      (this.dossierPrecSelectionnez.length === 0 ||
        this.dossierPrecSelectionnez.length !== 1)
    ) {
      this.toastService.warning(this.messageToastTranslate.selectionneUnLigne);
    } else {
      this.router.navigate([
        '/dossier-prec/historique/',
        this.dossierPrecSelectionnez[0].idDossierPrec
      ]);
    }
  }
  deleteDossiersPrec() {
    if (
      this.dossierPrecSelectionnez &&
      this.dossierPrecSelectionnez.length === 0
    ) {
      this.toastService.warning(
        this.messageToastTranslate.selectionnePlusieursLigne
      );
    } else {
      this.toastService
        .confirm(
          this.confirmationDialogTranslate.dossierPrec,
          this.confirmationDialogTranslate.dossierPrec.titleDelete,
          this.confirmationDialogTranslate.dossierPrec.messageDelete
        )
        .subscribe(success => {
          this.confirmationDelete(this.dossierPrecSelectionnez);
        });
    }
  }
  confirmationDelete(listDosPrec) {
    this.dossierPrecService.deleteListDossierPrec(listDosPrec).subscribe(
      dossierPrec => {
        this.dossierPrecService.deleteAllItems(listDosPrec);
        this.toastService.success(this.messageToastTranslate.succesSuppression);
      },
      error => {
        this.toastService.error(this.messageToastTranslate.errorSuppression);
      }
    );
  }

  archiverOrRestorerDossierPrec(typeOperation: boolean) {
    if (
      this.dossierPrecSelectionnez &&
      this.dossierPrecSelectionnez.length === 0
    ) {
      this.toastService.warning(
        this.messageToastTranslate.selectionnePlusieursLigne
      );
    } else {
      this.toastService
        .confirm(
          this.confirmationDialogTranslate.dossierPrec,
          this.confirmationDialogTranslate.dossierPrec.messageArchivage,
          this.confirmationDialogTranslate.dossierPrec.titleArchivage
        )
        .subscribe(success => {
          this.confirmationArchivageRestore(
            this.dossierPrecSelectionnez,
            typeOperation
          );
        });
    }
  }
  confirmationArchivageRestore(listDosPrec, archivage) {
    this.dossierPrecService
      .archiverRestoreListDosPrec(listDosPrec, archivage)
      .subscribe(
        dossierPrec => {
          this.onArchivage();

          this.toastService.success(
            this.messageToastTranslate.succesSuppression
          );
          this.dossierPrecSelectionnez = [];
        },
        error => {
          this.toastService.error(this.messageToastTranslate.errorSuppression);
        }
      );
  }
  tooltipDebiteur(debiteur: Debiteur) {
    return (
      'CIN : ' +
      debiteur.cin +
      '\n Email: ' +
      debiteur.email +
      '\n N° Télephone: ' +
      debiteur.tel +
      "\n Adresse: " +
      debiteur.adresse.description +
      " " +
      debiteur.adresse.ville.nom
    );
  }
  tooltipAgence(agence: Agence) {
    return (
      'Code devise: ' +
      agence.idInstitutionSiege +
      '\n Code agence: ' +
      agence.codeInstitution +
      "\n Adresse: " +
      agence.adresse.description +
      " " +
      agence.adresse.ville.nom
    );
  }
  concat2Chaine(debiteur: Debiteur) {
    return debiteur.firstName + ' ' + debiteur.lastName;
  }
}
