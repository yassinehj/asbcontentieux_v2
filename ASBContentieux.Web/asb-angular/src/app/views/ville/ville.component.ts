import { Component, OnInit, ViewChild , OnDestroy } from '@angular/core';
import { VilleService } from 'src/app/shared/services/ville.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Ville } from 'src/app/shared/models/ville';
import { Table } from 'primeng/table';
import { ToastService } from '@shared/services/toast.service';
import { SharedService } from '@shared/services/shared.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-ville',
  templateUrl: './ville.component.html',
  styleUrls: ['./ville.component.css']
})
export class VilleComponent implements OnInit , OnDestroy {

  @ViewChild('table') table: Table;
  villes: Ville[];
  villesSlice: Ville[] = [];
  idGouvernorat: any;
  idPays: any;
  idVille: any;
  first = 0;
  isEditable: true;
  clonedVille: { [s: string]: Ville; } = {};
  _unsubscribeAll: Subject<any>;

  constructor(
    private villeService: VilleService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sharedService: SharedService,
    private toastService: ToastService) {
      this._unsubscribeAll = new Subject();
     }

  ngOnInit() {
    this.idGouvernorat = this.activatedRoute.snapshot.paramMap.get('idGouvernorat');
    this.idPays = this.activatedRoute.snapshot.queryParamMap.get('idPays');
    this.getVille(this.idGouvernorat);
  }

  getVille(idGouvernorat: number) {
    this.villeService.findVilleByGouvernorat(idGouvernorat).subscribe((listOfVilles: Ville[]) => {
      this.villes = listOfVilles;
    });
  }

  onRowEditInit(ville: Ville) {
    this.clonedVille[ville.idVille] = { ...ville };
    this.villes.filter(row => row.isEditable).map(r => { r.isEditable = false; return r; });
    ville.isEditable = true;
  }

  onRowEditSave(ville: Ville) {
    if (ville.idVille != null) {
      this.updateVille(ville);
    } else {
      this.addVille(ville);
    }
  }

  getListGouvernorat(idVille: number) {
    this.router.navigate(['/gouvernorat', idVille]);
  }

  onRowEditCancel(ville: Ville, index: number) {
    if (ville.idVille != null) {
      this.villes[index] = this.clonedVille[ville.idVille];
      delete this.clonedVille[ville.idVille];
      } else {
        this.villes.splice(index , 1);
    }
  }
  
  addRow() {
    const villeItem: Ville = {
      idVille: null,
      nom: null,
      codePostal: null,
      isEditable: true
    };
    this.villeService.addFirstItem(villeItem);
    this.table.initRowEdit(villeItem);
    // this.first = this.ville.length - 1;
  }

  addVille(ville: Ville) {
    this.villesSlice = this.villes.slice(1 , this.villes.length);
    if (!this.sharedService.findItemInList(this.villesSlice , ville , 'nom')) {
      ville.isEditable = false;
      this.villeService.save(ville, this.idGouvernorat)
      .subscribe(villeResponse => {
          this.villeService.updateItem(villeResponse , ville);
          this.toastService.success('successAjout');
      });
    } else {
      this.table.initRowEdit(ville);
      this.toastService.info('alreadyExists');
    }
    }

    updateVille(ville: Ville) {
      this.villesSlice = this.villes.filter(item => item.idVille !== ville.idVille);
      if (!this.sharedService.findItemInList(this.villesSlice , ville , 'nom')) {
        ville.isEditable = false;
        this.villeService.update(ville, this.idGouvernorat)
        .subscribe(villeResponse => {
            this.toastService.success('successUpdate');
        });
      } else {
        this.table.initRowEdit(ville);
        this.toastService.info('alreadyExists');
      }
    }

  deleteVille(idVille: number) {
    this.toastService.confirm('ville' , 'messageDelete' , 'titleDelete')
      .pipe(takeUntil(this._unsubscribeAll)).
       subscribe(() => {
      this.villeService.delete(idVille)
        .subscribe(data => {
          this.getVille(this.idGouvernorat);
          this.toastService.success('succesSuppression');
        }, err => {
          console.log(err);
        });
    });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
}

}
