import { Component, OnInit, Input, OnDestroy  } from '@angular/core';
import { Dossier } from '@shared/models/Dossier';
import { DossierService } from '@shared/services/dossier.service';
import { ToastService } from '@shared/services/toast.service';
import { PieceAffaireService } from '@shared/services/piece-affaire.service';
import { SelectionModel } from '@angular/cdk/collections';
import { PieceAffaire } from '@shared/models/piece-affaire';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-move-pieces-affaires-folder',
  templateUrl: './move-pieces-affaires-folder.component.html',
  styleUrls: ['./move-pieces-affaires-folder.component.scss']
})
export class MovePiecesAffairesFolderComponent implements OnInit , OnDestroy {
  display =false;
  selectedFolder: Dossier;
  dossier: Dossier[];
  selectedPieceAffaire: PieceAffaire;
  nomDossier : string;
  idDossier: any;
  // Private
  private _unsubscribeAll: Subject<any>;
  @Input()
  selection = new SelectionModel<any>(true, []);

  constructor(private dossierService:DossierService,
    private toastService: ToastService,
    private pieceAffaireService: PieceAffaireService ) {
    this.selectedFolder = Object.assign({} , this.selectedFolder
      
      ),
      this.selectedPieceAffaire = Object.assign({} , this.selectedPieceAffaire
      
        )
     
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    const listPieceAffaires = [];
    const dataTable = this.pieceAffaireService.dataSource.data;
    this.getAllDossier();
    this.pieceAffaireService.onPiecesSelected
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe(selected => {
      this.selectedPieceAffaire = selected;
      console.log(this.selectedPieceAffaire)
    });
  }

  getAllDossier(){

    this.dossierService.findAll().subscribe((listofDossier:Dossier[]) =>{

      this.dossier = listofDossier;
    })


  }

  openFileToMoveDialog(){
  

      if(this.selection.selected.length > 0 ){
      this.selection.selected.forEach(item => {
        if (item.idPieceAffaire) {
          this.display = true;
        }
        if (item.idDossier) {
          this.toastService.info('selectionnePieceAffaire');
        }
        
      });
      }
      else{
        this.toastService.info('selectionnePieceAffaire');
      }

    }
   

  closePieceAffairedDialog(){

    this.display = false;

  }

  loadData(): void {
    this.dossierService.findAll().subscribe(dossiers => {
      //console.log(dossiers);
      const tableData= []
      dossiers.forEach(dossier => {
        tableData.push(dossier);
      });
      this.pieceAffaireService.getPieces().subscribe((data: any) => {
        data.forEach(element => {
          tableData.push(element);
        });
        this.pieceAffaireService.dataSource = new MatTableDataSource(
          tableData
        );
      });
    });
  }

  MovePieceAffaire(){
    console.log(this.selectedPieceAffaire)
    const dataTable = this.pieceAffaireService.dataSource.data;
    
    this.pieceAffaireService.movePieceAffaireInDossier(this.selectedPieceAffaire, this.selectedFolder.idDossier).subscribe((data:any)=> {


    // dataTable.push(data);

      this.loadData()
 /*      const index = this.pieceAffaireService.dataSource.data.findIndex(el => el === this.selectedPieceAffaire);
        console.log(this.pieceAffaireService.dataSource)
      this.pieceAffaireService.dataSource.data.splice(index , 1); */
      this.display = false;
      this.toastService.success('successAjout');
      
/*       this.selection.selected.forEach(el => {
        const index = dataTable.findIndex(
          item => item.idPieceAffaire && el === item.idPieceAffaire
          
        );
        dataTable.splice(index, 1);
       
        
      });
      this.pieceAffaireService.dataSource.data = dataTable;
 */
    });
    

  }

    /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }


}
