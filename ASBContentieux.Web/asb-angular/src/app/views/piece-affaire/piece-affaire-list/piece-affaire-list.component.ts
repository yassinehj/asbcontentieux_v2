import { DossierService } from './../../../shared/services/dossier.service';
import { Component, OnDestroy, OnInit, ViewChild, Input } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { fuseAnimations } from '@fuse/animations';
import { Observable, Subject } from 'rxjs';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { PieceAffaireService } from 'src/app/shared/services/piece-affaire.service';
import { SelectionModel } from '@angular/cdk/collections';
import { takeUntil } from 'rxjs/operators';
import { PieceAffaire } from 'src/app/shared/models/piece-affaire';
import { ActivatedRoute, Params } from '@angular/router';
import { SharedService } from '@shared/services/shared.service';
import { ToastService } from '@shared/services/toast.service';

@Component({
  selector: 'app-piece-affaire-list',
  templateUrl: './piece-affaire-list.component.html',
  styleUrls: ['./piece-affaire-list.component.scss'],
  animations: fuseAnimations
})
export class PieceAffaireListComponent implements OnInit, OnDestroy {
  PaginationItemPerPage: number = 10;
  piecesAffaires: PieceAffaire[];
  //dataSource = new MatTableDataSource();
  page: number;
  previousPage: number;
  itemsPerPage: number;
  totalCount: number;
  afficheDetails = false;
  
  displayedColumns: string[] = [
    'select',
    'icon',
    'name',
    'datecreation',
    'filename',
    'size'
  ];
  selected: any;
  files: any;
  data: PieceAffaire[];
  selectedRowIndex: number = -1;
  checked = false;
  idDossier: any;
  @Input()
  dataSource = new MatTableDataSource();
  tableData = [];
  selection = new SelectionModel<any>(true, []);
  private _unsubscribeAll: Subject<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  /*  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  } */

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private pieceAffaireService: PieceAffaireService,
    private dossierService: DossierService,
    private route: ActivatedRoute,
    private sharedService : SharedService,
    private toastService: ToastService,
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();

    this.itemsPerPage = this.PaginationItemPerPage;
    this.page = 1;
    this.previousPage = 1;
  }
  ngOnInit() {    
    this.pieceAffaireService.dataSource.paginator = this.paginator;

    this.pieceAffaireService.onPiecesChanged
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(files => {
        this.files = files;
      });

    this.pieceAffaireService.onPiecesSelected
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selected => {
        this.selected = selected;
      });

      this.dossierService.onDossierSelected
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selected =>{
        this.selected = selected;
      })
      this.route.queryParamMap.subscribe((params: any) => {
        this.idDossier = params.get('idDossier')
        if(this.idDossier) {
          this.getAllPiecesAffairesByDossier()
        } else {
          this.loadData()
          this.pieceAffaireService.onPiecesSelected.next(null);
          this.sharedService.createdFolderButtonEvent.next(true);
        }
        }) 
  }

  onSelect(selected: any) {
    console.log('----------selected-----');
    this.afficheDetails = true;
    this.pieceAffaireService.onPiecesSelected.next(selected);
    this.dossierService.onDossierSelected.next(selected);
  }

  onChange($event , selectedRow) {
    if ($event) {
       this.selection.toggle(selectedRow);
       this.pieceAffaireService.onPiecesSelected.next(selectedRow);
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;

    const numRows = this.pieceAffaireService.dataSource.data.length;

    return numSelected === numRows;
  }

  removeSelectedRows() {
    const listPieceAffaires = [];
    const listDossier = [];
    const dataTable = this.pieceAffaireService.dataSource.data;
    this.selection.selected.forEach(item => {
      if (item.idPieceAffaire) {
        listPieceAffaires.push(item.idPieceAffaire);
      }
      if (item.idDossier) {
        listDossier.push(item.idDossier);
      }
    });

    this.toastService.confirm('pieceAffaire', 'messageDelete' , 'titleDelete').subscribe(() => {
    if (listPieceAffaires.length > 0) {
      
      this.pieceAffaireService
        .deleteAllPiecesAffaires(listPieceAffaires)
        .subscribe(
          data => {
            this.toastService.success("succesSuppression");
            listPieceAffaires.forEach(el => {
              const index = dataTable.findIndex(
                item => item.idPieceAffaire && el === item.idPieceAffaire
                
              );
              dataTable.splice(index, 1);
             
              
            });
            this.pieceAffaireService.dataSource.data = dataTable;
          },
          
          error => console.log('ERROR: ' + error)
        );
    }
    if (listDossier.length > 0) {
      this.dossierService.deleteList(listDossier).subscribe(data => {
        listDossier.forEach(el => {
          const index = dataTable.findIndex(
            item => item.idDossier && el === item.idDossier
          );
          dataTable.splice(index, 1);
        });
        this.pieceAffaireService.dataSource.data = dataTable;
        this.pieceAffaireService.dataSource._updatePaginator(dataTable.length);
        this.selection = new SelectionModel<any>(true, []);
      });
    }
  });
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.pieceAffaireService.dataSource.data.forEach(
          (row: PieceAffaire) => {
            this.selection.select(row);
            console.log(row);
          }
        );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.page = page;
      this.previousPage = page;
      this.loadData();
    }
  }

  loadData(): void {
    this.dossierService.findAll().subscribe(dossiers => {
      //console.log(dossiers);
      this.tableData = []
      dossiers.forEach(dossier => {
        this.tableData.push(dossier);
      });
      this.pieceAffaireService.getPieces().subscribe((data: any) => {
        data.forEach(element => {
          this.tableData.push(element);
        });
        console.log(this.tableData)
        this.pieceAffaireService.dataSource = new MatTableDataSource(
          this.tableData
        );
        console.log(this.pieceAffaireService.dataSource)
        this.pieceAffaireService.dataSource.paginator = this.paginator;
      });
    });
  }

  getAllPiecesAffairesByDossier() : void{
    this.pieceAffaireService.getAllPiecesAffairesByDossier(this.idDossier).subscribe((data: any) => {
      
      this.tableData = []
      data.forEach(element => {
        this.tableData.push(element);
        
     
      
      });
      this.pieceAffaireService.dataSource = new MatTableDataSource(
        this.tableData
      );
      this.pieceAffaireService.dataSource.paginator = this.paginator;
    });
    
      }

  public doFilter = (value: string) => {
    this.pieceAffaireService.dataSource.filter = value
      .trim()
      .toLocaleLowerCase();
  };
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this.pieceAffaireService.onPiecesSelected.next(null);
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }



  
}
export class FilesDataSource extends DataSource<any> {
  constructor(private pieceAffaireService: PieceAffaireService) {
    super();
  }

  /**
   * Connect function called by the table to retrieve one stream containing the data to render.
   *
   * @returns {Observable<any[]>}
   */
  connect(): Observable<any[]> {
    return this.pieceAffaireService.onPiecesChanged;
  }

  /**
   * Disconnect
   */
  disconnect(): void {}
  addRow(newData, data) {
    data = [...data, newData];
  }

  removeRow(data) {
    data = data.slice(0, data.length - 1);
  }


  
}
