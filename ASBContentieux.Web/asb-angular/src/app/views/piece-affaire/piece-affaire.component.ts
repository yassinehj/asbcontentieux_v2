import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialog, MatPaginator } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { MatTableDataSource } from '@angular/material/table';
import { PieceAffaire } from 'src/app/shared/models/piece-affaire';
import { PieceAffaireService } from 'src/app/shared/services/piece-affaire.service';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-piece-affaire',
  templateUrl: './piece-affaire.component.html',
  styleUrls: ['./piece-affaire.component.scss']
})
export class PieceAffaireComponent implements OnInit , OnDestroy {
  selectedFiles: FileList;
  currentFileUpload: File;
  piecesAffaires: any;
  pieceAffaireList: Array<PieceAffaire> = [];
  uploadResponse = { status: '', message: '', filePath: '' };
  dataSource = new MatTableDataSource();
  selected: any;
  pathArr: string[];
  private _unsubscribeAll: Subject<any>;
 
  
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private unsubscribeAll: Subject<any>;

  constructor(
    private dialog: MatDialog,
    private fuseSidebarService: FuseSidebarService,
    private pieceAffaireService: PieceAffaireService,
    private formBuilder: FormBuilder,
    
  ) {

    this._unsubscribeAll = new Subject();
    this.pieceAffaireService.onPiecesSelected
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe(selected => {
      console.log('selected')
      console.log(selected)
        this.selected = selected;
    });
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  

  upload() {
    this.currentFileUpload = this.selectedFiles.item(0);

    this.pieceAffaireService
      .pushFileToStorage(this.currentFileUpload)
      .subscribe(event => {
        this.loadData();
      });

    this.selectedFiles = undefined;
  }

  ngOnInit() {
  }

  loadData(): void {
    this.pieceAffaireService.getPieces().subscribe((data: any) => {
      this.piecesAffaires = data;
      console.log(this.piecesAffaires);
      console.log('iilsds');
      console.log(data);
      this.dataSource = new MatTableDataSource(this.piecesAffaires);
      console.log(this.dataSource);
    });
  }

  ngOnDestroy(): void {
   this._unsubscribeAll.next();
   this._unsubscribeAll.complete();
  }
}
