import { PieceAffaireService } from 'src/app/shared/services/piece-affaire.service';
import { ToastService } from './../../../shared/services/toast.service';
import { Dossier } from './../../../shared/models/Dossier';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DossierService } from 'src/app/shared/services/dossier.service';
import { MatTableDataSource } from '@angular/material/table';
import { SharedService } from '@shared/services/shared.service';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-dialog-piece-affaire-folder',
  templateUrl: './dialog-piece-affaire-folder.component.html',
  styleUrls: ['./dialog-piece-affaire-folder.component.scss']
})
export class DialogPieceAffaireFolderComponent implements OnInit {
  uploadSuccessful = false;
  processValidation = false;
  public registrationForm: FormGroup;
  statusCode: number;
  dossierToUpdate = null;
  dataSource = new MatTableDataSource();
  piecesAffaires: any;
  display = false;
  dossier: Dossier;
  createdFolderButtonEvent:boolean ;
  constructor(
    private formBuilder: FormBuilder,
    private dossierService: DossierService,
    private toastService: ToastService,
    private pieceAffaireService: PieceAffaireService,
    private sharedService:SharedService,
    private router: Router
   
  ) {}
  

  dossierForm: FormGroup;

  ngOnInit() {
    this.dossierForm = this.formBuilder.group({
      idDossier:['',Validators.required],
      nom: ['', Validators.required],
      description: ['', Validators.required]
    });
    this.dossier = {idDossier:null, nom: null, description: null };

    this.createdFolderButtonEventChanges();
  }

 

  createdFolderButtonEventChanges() {
    this.sharedService.createdFolderButtonEvent
    .subscribe((createdFolderButtonEvent: boolean) => {
      this.createdFolderButtonEvent = createdFolderButtonEvent;
    });
  }
  saveDossier() {
    const dataTable = this.pieceAffaireService.dataSource.data;
    if (this.dossier.nom != null) {
      this.dossierService.save(this.dossier).subscribe((data: any) => {
        dataTable.unshift(data);
        this.dossierService.addItem(data);
        this.closeDialog();
        this.toastService.success('successAjout');

        this.pieceAffaireService.dataSource.data = dataTable;
        this.pieceAffaireService.dataSource._updatePaginator(dataTable.length);
      });
    }
  }
  closeDialog() {
    this.display = false;
  }
  openUploadFolderDialog() {
  
    this.display = true;
  }
 
  navigate() {
    this.pieceAffaireService.onPiecesSelected.next(null);
    this.router.navigate(['/piece-affaire/list']);
  }

  
 

}
