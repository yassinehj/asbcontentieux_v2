import { ToastService } from './../../../shared/services/toast.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PieceAffaire } from 'src/app/shared/models/piece-affaire';
import { MatTableDataSource } from '@angular/material/table';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { PieceAffaireService } from 'src/app/shared/services/piece-affaire.service';
import { FileUpload } from 'primeng/primeng';
import {  Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dialog-piece-affaire',
  templateUrl: './dialog-piece-affaire.component.html',
  styleUrls: ['./dialog-piece-affaire.component.scss']
})
export class DialogPieceAffaireComponent implements OnInit {
  @ViewChild('file') file;
  @ViewChild('fileUpload') fileUpload: FileUpload;
  uploadSuccessful = false;
  selectedFiles: FileList;
  currentFileUpload: File;
  piecesAffaires: any;
  pieceAffaireList: Array<PieceAffaire> = [];
  uploadResponse = { status: '', message: '', filePath: '' };
  dataSource = new MatTableDataSource();
  selected: any;
  pathArr: string[];
  display = false;
  uploadedFiles: any[] = [];
  idDossier: any;
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private pieceAffaireService: PieceAffaireService,
    private toastService: ToastService,
    private route: ActivatedRoute
  ) {

    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe((params: any) => {
      this.idDossier = params.get('idDossier')
    });
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  loadData(): void {
    this.pieceAffaireService.getPieces().subscribe((data: any) => {
      this.piecesAffaires = data;
      this.dataSource = new MatTableDataSource(this.piecesAffaires);
    });
  }

  myUploader(event) {
   
    for (let file of event.files) {
      this.currentFileUpload = file;
   
    }
    this.selectedFiles = undefined;
  }

  openUploadDialog() {
    this.display = true;
  }
  closeUploadDialog() {
    this.display = false;
    this.fileUpload.clear();
  }
  onUpload(event) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
  }
  addPieceAffaire() {
    const dataTable = this.pieceAffaireService.dataSource.data;

    this.fileUpload.files.forEach((file, idx) => {
      this.pieceAffaireService.pushFileToStorage(file , this.idDossier).subscribe((data: any)=> {
        if (idx === this.fileUpload.files.length - 1) {
          this.fileUpload.upload();
          dataTable.push(data);
          console.log(this.pieceAffaireService.dataSource)
          this.display = false;
          this.toastService.success('successAjout');
          this.pieceAffaireService.dataSource.data = dataTable;
          this.pieceAffaireService.dataSource._updatePaginator(dataTable.length);
        }
      });
    });

  }
}
