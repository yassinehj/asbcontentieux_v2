import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { PieceAffaireService } from 'src/app/shared/services/piece-affaire.service';
import { Router } from '@angular/router';
import { Dossier } from '@shared/models/Dossier';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-piece-affaire-details',
  templateUrl: './piece-affaire-details.component.html',
  styleUrls: ['./piece-affaire-details.component.scss'],
  animations: fuseAnimations
})
export class PieceAffaireDetailsComponent implements OnInit, OnDestroy {
  selected: any;
  fileName: string;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FileManagerService} _fileManagerService
   */
  constructor(
        private pieceAffaireService: PieceAffaireService ,
        private router: Router,
        private sharedService : SharedService) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.pieceAffaireService.onPiecesSelected
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selected => {
        this.selected = selected;
      });
  }

  DownloadPieceAffaire() {
    this.pieceAffaireService.downloadFile(this.selected.fileName).subscribe(
      (data: any) => {
        this.pieceAffaireService.downLoadFile(
          data,
          'application/octet-stream',
          this.selected.fileName
        );
      },
      error => console.log('ERROR: ' + error)
    );
  }


  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  openFolder(){

   this.sharedService.createdFolderButtonEvent.next(false);
   this.router.navigate(['/piece-affaire/list'] , {queryParams : {'idDossier' : this.selected.idDossier}});
 
   
  }
}
