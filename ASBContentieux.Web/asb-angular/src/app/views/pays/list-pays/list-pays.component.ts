import { takeUntil } from 'rxjs/operators';
import { ToastService } from './../../../shared/services/toast.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { PaysService } from '../../../shared/services/pays.service';
import { Pays } from '../../../shared/models/pays';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { Message } from 'primeng/components/common/api';
import { Subject } from 'rxjs';
import { SharedService } from '@shared/services/shared.service';


@Component({
    selector: 'app-list-pays',
    templateUrl: './list-pays.component.html',
})
export class ListPaysComponent implements OnInit, OnDestroy {

    @ViewChild('table') table: Table;
    pays: Pays[] = [];
    paysSlice: Pays[] = [];
    msgs: Message[] = [];
    first = 0;
    clonedPays: { [s: string]: Pays; } = {};
    _unsubscribeAll: Subject<any>;

    constructor(
        private paysService: PaysService,
        private sharedService: SharedService,
        private toastService: ToastService, private router: Router) {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
        this.getPays();
    }

    addRow() {
        const paysItem: Pays = {
            idPays: null,
            nom: null,
            isEditable: true
        };
        this.paysService.addFirstItem(paysItem);
        this.table.initRowEdit(paysItem);
        // this.first = this.pays.length - 1;
    }

    getPays() {
        this.paysService.findAll()
            .subscribe((pays: Pays[]) => {
                 this.pays = pays;
            });
    }


    getListGouvernorats(idPays: number) {
        this.router.navigate(['/gouvernorat', idPays]);
    }

    onRowEditInit(pays: Pays) {
        this.clonedPays[pays.idPays] = { ...pays };
        this.pays.filter(row => row.isEditable).map(r => r.isEditable = false);
        pays.isEditable = true;
    }

    onRowEditSave(pays: Pays) {
        if (pays.idPays != null) {
            this.updatePays(pays);
        } else {
            this.addPays(pays);
        }
    }

    onRowEditCancel(pays: Pays, index: number) {
        if (pays.idPays != null) {
            this.pays[index] = this.clonedPays[pays.idPays];
            delete this.clonedPays[pays.idPays];
        } else {
            this.pays.splice(index , 1);
        }

    }
    addPays(pays: Pays) {
        this.paysSlice = this.pays.slice(1 , this.pays.length);
        if (!this.sharedService.findItemInList(this.paysSlice , pays , 'nom')) {
            pays.isEditable = false;
            this.paysService.save(pays)
            .subscribe(paysResponse => {
                    this.paysService.updateItem(paysResponse , pays);
                    this.toastService.success('successAjout');
            });
        } else {
            this.table.initRowEdit(pays);
            this.toastService.info('alreadyExists');
        }
    }

    updatePays(pays: Pays) {
        this.paysSlice = this.pays.filter(item => item.idPays !== pays.idPays);
        if (!this.sharedService.findItemInList(this.paysSlice , pays , 'nom')) {
            pays.isEditable = false;
            this.paysService.update(pays)
            .subscribe(paysResponse => {
                    this.toastService.success('successUpdate');
            }, err => {
                console.log(err);
            });
        } else {
            this.table.initRowEdit(pays);
            this.toastService.info('alreadyExists');
          }
    }

    deletePays(idPays: number) {
        this.toastService.confirm('pays', 'messageDelete', 'titleDelete')
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.paysService.delete(idPays)
                    .subscribe(data => {
                        this.getPays();
                        this.toastService.success('succesSuppression');
                    }, err => {
                        console.log(err);
                    });
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
