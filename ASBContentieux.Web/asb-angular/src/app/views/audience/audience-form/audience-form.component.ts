import { Component, OnInit } from '@angular/core';
import { FormGroup ,FormBuilder, Validators, FormControl} from '@angular/forms';
import { SharedService } from '@shared/services/shared.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastService } from '@shared/services/toast.service';
import { AppUtils } from '@shared/utils/app-utils';
import { Audience } from '@shared/models/audience';
import { AudienceService } from '@shared/services/audience.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-audience-form',
  templateUrl: './audience-form.component.html',
  styleUrls: ['./audience-form.component.scss']
})
export class AudienceFormComponent implements OnInit {

  form: FormGroup;
  error: any;
  audience: Audience = Object.assign({} , this.audience);
  editMode: boolean;
  idAudience: any;

  /**
   * Inject the formBuilder
   * @param _formBuilder FormBuilder to create the FormGroup
   */
  constructor(
    private _formBuilder: FormBuilder,
    private sharedService: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private audienceService: AudienceService
  ) { 
    this.error = AppUtils.error;
  }

  ngOnInit() {
    this.initFormGroup();
    this.editMode = this.audienceService.editMode;
    this.editModeChanges();
    this.OnEditMode();
  }

  initFormGroup() {
    
    this.form = this._formBuilder.group({
    idAudience:[''],
    dateCreation: ['', Validators.required],
    description: ['', Validators.required],
    dateAudience: ['', Validators.required],
    etat: ['',Validators.required],
    dateFin: ['',Validators.required],
    dureeNotif: ['',[Validators.required,Validators.required,Validators.pattern("[0-9]+")]],
    notif: ['',Validators.required]
    
  });
}

OnSubmit() {
  if (!this.editMode) {
    this.addAudience();
  } else {
    this.editAudience();
  }
}

addAudience(){

  this.audience.dateCreation = this.form.value.dateCreation;
  this.audience.description = this.form.value.description;
  this.audience.dateAudience = this.form.value.dateAudience;
  this.audience.etat =this.form.value.etat;
  this.audience.dateFin =this.form.value.dateFin;
  this.audience.dureeNotif =this.form.value.dureeNotif;
  this.audience.notif =this.form.value.notif;

  this.audienceService.save(this.audience)
  .subscribe((addedAudience:any)=>{
    console.log(addedAudience);
    if(addedAudience){
    this.toastService.success('successAjout');
    this.router.navigate(['/audience/list']);
    }
  });
}


editAudience(){
  this.toastService
  .confirm('audience', 'messageUpdate', 'titleUpdate')
  .pipe(switchMap(() => this.audienceService.update(this.form.value)))
  .subscribe((updatedAudience: Audience) => {
    if (updatedAudience) {
      this.toastService.success('successUpdate');
      this.router.navigate(['/audience/list']);
    }
  });
}

editModeChanges() {
  this.sharedService.editMode
  .subscribe((editMode: boolean) => {
    this.editMode = editMode;
  });
}

OnEditMode(){
  this.route.params
  .subscribe((params: Params) => {
    this.idAudience = params.idAudience;
  
    this.audienceService.findOne(this.idAudience)
      .subscribe((audience: Audience) => {
        audience.dateCreation = new Date(audience.dateCreation);
        audience.dateAudience = new Date(audience.dateAudience);
        audience.dateFin = new Date(audience.dateFin);
        console.log("aaaaaa");
        console.log(audience)
        this.form.patchValue(audience);
      });
  });
}

updateTribunal(){
  
 
  }






}
