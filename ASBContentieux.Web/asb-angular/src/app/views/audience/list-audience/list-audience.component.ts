import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { MenuItem } from 'primeng/primeng';
import { Audience } from '@shared/models/audience';
import { AudienceService } from '@shared/services/audience.service';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '@shared/services/shared.service';
import { ToastService } from '@shared/services/toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-audience',
  templateUrl: './list-audience.component.html',
  styleUrls: ['./list-audience.component.scss']
})
export class ListAudienceComponent implements OnInit {

  @ViewChild('dt') private _table: Table;
  cols: any[];
  items: MenuItem[];
  listAudience: Audience[];
  disableDelete = true;
  audienceSelectionnez: Audience[];
  buttonTranslate: any;
  selectedAudience: any[] = [];

  constructor( private audienceService: AudienceService,
    private translate: TranslateService,
    private sharedService: SharedService,
    private toastService: ToastService,
    private router: Router
  ) {
    this.audienceSelectionnez = [];
    this.items = []
  }

  ngOnInit() {
    this.getAllAudience();
  }


  addAudience() {
    this.router.navigate(['/audience/ajout']);
    this.sharedService.editMode.next(false);
  }


  editAudience(){
    if (this.selectedAudience.length === 1) {
      this.router.navigate([
        '/audience/edit/',
        this.selectedAudience[0].idAudience
      ]);
      this.sharedService.editMode.next(true);
    } else if (this.selectedAudience.length > 1) {
      this.toastService.info(
        "selectionneUneLigne"
      );
    } else {
      this.toastService.info(
        "selectionnePlusieursLigne"
      );
    }
  }

  deleteAudience(){
    if (this.selectedAudience.length > 0) {
    
      let setlectedAudienceId: any[] = this.selectedAudience.map(
        item => item.idAudience
      );
      this.toastService
        .confirm('audience', 'messageDelete', 'titleDelete')
        .subscribe(() => {
          this.audienceService
            .deleteAllAudiences(setlectedAudienceId)
            .subscribe(result => {
              console.log(setlectedAudienceId);
              this.toastService.success(
                'succesSuppression'
              );
              this.audienceService.deleteAllItems(this.selectedAudience);
              this.selectedAudience = [];
              setlectedAudienceId = [];
            });
         
        });
      } else {
        this.toastService.info('selectionnePlusieursLigne');
      }
    
  }
  getAllAudience() {
    this.audienceService.findAll().subscribe(
      data => {
        this.listAudience = data;
        console.log(data);
      },
      err => {
        console.log(err);
      }
    );
  }

}
