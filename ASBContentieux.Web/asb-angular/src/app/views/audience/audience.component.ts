import { Component, OnInit } from "@angular/core";
import { AudienceService } from 'src/app/shared/services/audience.service';


@Component({
  selector: "app-audience",
  templateUrl: "./audience.component.html",
  styleUrls: ["./audience.component.scss"]
})
export class AudienceComponent implements OnInit {
  constructor(private audienceService: AudienceService) {}

  ngOnInit() {}
}
