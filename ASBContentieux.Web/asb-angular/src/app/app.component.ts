import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { navigation } from "@shared/utils/navigation/navigation";
import { FuseNavigationService } from "@fuse/components/navigation/navigation.service";
import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html"
})
export class AppComponent  {

  title = "GROUPE ADAMING SOLUTION";

  browserLanguage: string;
  navigation: any;
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _fuseNavigationService: FuseNavigationService,
    private translate: TranslateService
  ) {
    // Get default navigation
    this.navigation = navigation;

    // Register the navigation to the service
    this._fuseNavigationService.register("main", this.navigation);

    // Set the main navigation as our current navigation
    this._fuseNavigationService.setCurrentNavigation("main");

    const y = localStorage.getItem("langue");
    if (y === "" || y == null) {
      this.translate.setDefaultLang(navigator.language.substring(0, 2));
      localStorage.setItem("langue", navigator.language.substring(0, 2));
    } else {
      translate.setDefaultLang(y);
    }
  }

}
