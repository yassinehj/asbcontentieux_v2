import { environment } from 'src/environments/environment';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FormGroup } from '@angular/forms';

export class AppUtils {
  static getServerAbsolutePath() {
    const SEPARATOR = '/';
    let url: string;
    const protocol: string = window.location.protocol;
    const host: string = environment.SERVER_URL;
    const projectPath = 'ASBContentieux.Web';
    url = protocol + SEPARATOR + SEPARATOR + host + SEPARATOR + projectPath;
    return url;
  }

  static getBaseApiURL() {
    return this.getServerAbsolutePath() + '/api';
  }

  static triggerAnimation() {
    return trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ]);
  }

  /**
   * 
   * @param formGroup 
   * @param formControl 
   * @param errorKey 
   */
  static error(formGroup: FormGroup, formControl: string, errorKey: string) {
    const control = formGroup.get(formControl);
    return control && (control.touched || control.dirty) && !control.valid && control.hasError(errorKey);
  }
}
