/**
 * This is an interface that contains a definition of CRUD operations endPoints.
 */

interface ICrudEndPoints {
  _saveEndPoint?: string;
  _findAllEndPoint?: string;
  _findOneEndPoint?: string;
  _updateEndpoint?: string;
  _deleteEndPoint?: string;
  _deleteListEndPoint?: string;
}

/**
 * This class implements the ICrudEndPoints interface.
 * Each service API will have a specific CRUD endPoints implementation.
 */
export class CrudEndPoints implements ICrudEndPoints {
  constructor(
    public _saveEndPoint?: string,
    public _findAllEndPoint?: string,
    public _findOneEndPoint?: string,
    public _updateEndpoint?: string,
    public _deleteEndPoint?: string,
    public _deleteListEndPoint?: string
  ) {}
}
