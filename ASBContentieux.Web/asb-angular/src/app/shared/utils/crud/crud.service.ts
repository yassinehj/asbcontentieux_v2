import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CrudOperations } from './crud-operations';
import { CrudEndPoints } from './crud-endpoints';
import { AppUtils } from './../app-utils';
import { map } from 'rxjs/operators';

/**
 * This is a generic service that implements crud operations
 * @author aminebk
 * @see CrudOperations,CrudEndPoints
 * @description To get benefits from the crud operations, you need to extend the CrudService class
 * in the appropriate service then pass the concerned model and the type of the id as a parameters
 */
export abstract class CrudService<T, ID> implements CrudOperations<T, ID> {
  protected _baseURL: string;
  protected headers: HttpHeaders;
  protected itemsList: T[];
  /**
   *
   * @param _http Injection of HttpClient
   */
  constructor(protected _http: HttpClient, protected endPoints: CrudEndPoints) {
    this._baseURL = AppUtils.getBaseApiURL();
    this.headers = new HttpHeaders({
      Authorization: localStorage.getItem('token')
    });
    this.itemsList = [];
  }

  /**
   *
   * @param t the item that will be inserted into DB
   */
  save(t: any , param?: any): Observable<any> {
    return this._http.post<any>(this._baseURL + (param ? this.endPoints._saveEndPoint + param : this.endPoints._saveEndPoint), t, {
      headers: this.headers
    });
  }

  /**
   * find All Items
   */
  findAll(): Observable<T[]> {
    return this._http
      .get<T[]>(this._baseURL + this.endPoints._findAllEndPoint, {
        headers: this.headers
      })
      .pipe(
        map((listOfItems: T[]) => {
          this.itemsList = listOfItems;
          return this.itemsList;
        })
      );
  }

  /**
   *
   * @param param find the item by a specific parameter
   */
  findOne(param: any): Observable<T> {
    return this._http.get<T>(
      this._baseURL + this.endPoints._findOneEndPoint + '/' + param,
      { headers: this.headers }
    );
  }


  /**
   *
   * @param t the item that will be updated in DB
   */
  update(t: any , param?: any): Observable<T> {
    return this._http.put<T>(
      this._baseURL + (param ? this.endPoints._updateEndpoint + param : this.endPoints._updateEndpoint) ,
      t,
      { headers: this.headers }
    );
  }

  /**
   *
   * @param id delete the item by ID
   */
  delete(id: ID): Observable<T> {
    return this._http.delete<T>(
      this._baseURL + this.endPoints._deleteEndPoint + '/' + id,
      { headers: this.headers }
    );
  }
  deleteList(listID: any[]): Observable<any> {
    return this._http.delete<T>(
      this._baseURL + this.endPoints._deleteListEndPoint + '/' + listID,
      { headers: this.headers }
    );
  }

  addItem(item: T) {
    this.itemsList.push(item);
  }

  addFirstItem(item: T) {
    this.itemsList.unshift(item);
  }

  updateItem(newItem: T , oldItem: any) {
    const index = this.itemsList.findIndex(el => oldItem === el);
    this.itemsList.splice(index, 1, newItem);
  }


  getItemById(idItem: any , idField: string) {
   return this.itemsList.find((el: any) => el[idField] === idItem);
  }

  deleteItem(item: T) {
    const index = this.itemsList.findIndex(el => item === el);
    this.itemsList.splice(index, 1);
  }

  deleteAllItems(items: T[]) {
    items.forEach(item => {
      this.deleteItem(item);
    });
  }
}
