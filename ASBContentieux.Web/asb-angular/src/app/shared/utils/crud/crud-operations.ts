import { Observable } from 'rxjs/Observable';

/**
 * Interface that contains Default CRUD Operations
 *
 * Each operation returns an Observable of the modal passed as a parameter .
 * This interface will be implemented by the CRUD Service Class.
 */
export interface CrudOperations<T, ID> {
  save(t: T): Observable<T>;
  findAll(): Observable<T[]>;
  findOne(param: any): Observable<T>;
  update(t: T): Observable<T>;
  delete(id: ID): Observable<any>;
  deleteList(listID: any[0]): Observable<any>;
}
