import { FuseNavigation } from "@fuse/types";

export const navigation: FuseNavigation[] = [
  {
    id: "applications",
    title: "Applications",
    translate: "NAV.APPLICATIONS",
    type: "group",
    icon: "apps",
    children: [
      {
        id: "Dashboards",
        title: "Tableau de bord",
        translate: "NAV.DASHBOARDS",
        type: "item",
        icon: "home",
        url: "/acceuil"
      },
      {
        id: "Calendrier",
        title: "Calendrier",
        translate: "NAV.CALENDAR",
        type: "item",
        icon: "today",
        url: "/calendrier"
      },
      {
        id: "Pré-contentieux",
        title: "Pré-contentieux",
        translate: "NAV.PRECENTENTIEUX",
        type: "item",
        icon: "folder",
        url: "/dossier-prec"
      },
      {
        id: "Affaire",
        title: "Affaire",
        translate: "NAV.AFFAIRE.TITLE",
        type: "collapsable",
        icon: "gavel",
        url: "/apps/mail",
        children: [
          {
            id: "Encours",
            title: "En cours",
            translate: "NAV.AFFAIRE.ENCOURS",
            type: "item",
            icon: "list_alt",
            url: "/apps/dashboards/analytics"
          },
          {
            id: "Archiver",
            title: "Archiver",
            translate: "NAV.AFFAIRE.ARCHIVER",
            type: "item",
            icon: "list_alt",
            url: "/apps/dashboards/project"
          }
        ]
      },
      {
        id: "pieceAffaire",
        title: "Pièces des affaires",
        translate: "NAV.PIECEAFFAIRE",
        type: "item",
        icon: "attach_money",
        url: "/piece-affaire"
      },
      {
        id: "Honoraire",
        title: "Honoraire",
        translate: "NAV.HONORAIRE",
        type: "item",
        icon: "attach_money",
        url: "/apps/todo"
      },
      {
        id: "Tribunal",
        title: "Tribunal",
        translate: "NAV.TRIBUNAL",
        type: "item",
        icon: "account_balance",
        url: "/tribunal"
      },
      {
        id: "Audience",
        title: "Audience",
        translate: "NAV.AUDIENCE",
        type: "item",
        icon: "account_balance",
        url: "/audience"
      },
      {
        id: "auxiliaire",
        title: "Auxiliaire",
        translate: "NAV.AUXILIAIRE",
        type: "item",
        icon: "group",
        url: "/auxiliaire"
      },
      {
        id: "Debiteur",
        title: "Débiteur",
        translate: "NAV.DEBITEUR",
        type: "item",
        icon: "account_box",
        url: "/debiteur"
      },
      {
        id: "Agence",
        title: "Agence",
        translate: "NAV.AGENCE",
        type: "item",
        icon: "domain",
        url: "/agence"
      },
      {
        id: "Tache",
        title: "Tache",
        translate: "NAV.TACHE",
        type: "item",
        icon: "notes",
        url: "/tache"
      },
      {
        id: "Configuration",
        title: "Configuration",
        translate: "NAV.CONFIG.TITLE",
        type: "collapsable",
        icon: "settings_applications",
        url: "",
        children: [
          {
            id: "Adresse",
            title: "Adresse",
            translate: "NAV.CONFIG.ADRESSE",
            type: "item",
            icon: "settings",
            url: "/configuration/pays"
          },
          {
            id: "TypeAuxiliaire",
            title: "TypeAuxiliaire",
            translate: "NAV.CONFIG.TYPE_AUXILIAIRE",
            type: "item",
            icon: "settings",
            url: "/configuration/auxiliaire"
          }
        ]
      },
      {
        id: 'Utilisateur',
        title: 'Utilisateur',
        translate: 'NAV.UTILISATEUR',
        type: 'item',
        icon: 'account_box',
        url: '/utilisateur'
      },
      {
        id: 'Role',
        title: 'Role',
        translate: 'NAV.ROLE',
        type: 'item',
        icon: 'account_box',
        url: '/role'
      },
      {
        id: 'Notification',
        title: 'Notification',
        translate: 'NAV.NOTIFICATION',
        type: 'item',
        icon: 'notifications',
        badge: {
          title: "8",
          bg: "#F44336",
          fg: "#FFFFFF"
        },
        url: "/apps"
      }
    ]
  }
];
