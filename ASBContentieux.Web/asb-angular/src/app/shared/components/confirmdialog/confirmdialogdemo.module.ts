import { CodeHighlighterModule, TabViewModule, MessagesModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogDemo } from './confirmdialogdemo';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ButtonModule } from 'primeng/button';


@NgModule({
	imports: [
		CommonModule,
		ConfirmDialogModule,
		ButtonModule,
		MessagesModule,
		TabViewModule,
		CodeHighlighterModule
	],
	declarations: [
		ConfirmDialogDemo
	]
})
export class ConfirmDialogDemoModule { }
