import { Component } from '@angular/core';
import { ConfirmationService, Message } from 'primeng/components/common/api';

@Component({
    templateUrl: './confirmdialogdemo.html',
    styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }
    `],
})
export class ConfirmDialogDemo {

    msgs: Message[] = [];

    constructor(private confirmationService: ConfirmationService) { }

    confirm1() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to proceed?',
            header: 'Confirmation',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected' }];
            }
        });
    }

    confirm2() {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Confirmed', detail: 'Record deleted' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected' }];
            }
        });
    }
}
