import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-tieredmenu",
  templateUrl: "./tieredmenu.component.html",
  styleUrls: ["./tieredmenu.component.scss"]
})
export class TieredmenuComponent implements OnInit, OnChanges {
  buttonTranslate: any;
  titleTranslate: any;
  items: any[] = [];
  @Output() addAction = new EventEmitter<any>();
  @Output() deleteAction = new EventEmitter<any>();
  @Output() editAction = new EventEmitter<any>();
  @Output() restoreAction = new EventEmitter<any>();
  @Output() archiveAction = new EventEmitter<any>();
  @Output() historiqueAction = new EventEmitter<any>();
  @Input() archiver?: boolean;
  @Input() addItem?: boolean;
  @Input() historique?: boolean;
  constructor(private translate: TranslateService) {}

  ngOnInit() {
    /**
     * Subscribe to language change , then reload the items
     * translated with the new selected language
     */
    this.translate.onLangChange.subscribe(lang => {
      this.buttonTranslate = lang.translations.button;
      this.titleTranslate = lang.translations.divers;
      this.defaultItemsAction();
    });
    /**
     * Subscribe to translation at the first load of the component ,
     * then create the items, translated with the current language
     */
    this.translate.get(["button", "divers"]).subscribe((translations: any) => {
      this.buttonTranslate = translations.button;
      this.titleTranslate = translations.divers;
      this.defaultItemsAction();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.archiver && !changes.archiver.firstChange) {
      this.archiver = changes.archiver.currentValue;
      this.defaultItemsAction();
    }
  }

  defaultItemsAction() {
    this.items = [
      {
        label: this.buttonTranslate.edit,
        icon: "pi pi-pencil",
        command: () => {
          this.editAction.emit("Edit Action");
        },
        disabled: false,
        title: this.titleTranslate.titleEditItem
      },
      {
        label: this.buttonTranslate.delete,
        icon: "pi pi-trash",
        command: () => {
          this.deleteAction.emit("Delete Action");
        },
        title: this.titleTranslate.titleDeleteItem
      }
    ];
    this.customizeItemsAction();
  }

  customizeItemsAction() {
    if (this.addItem) {
      this.items.unshift({
        label: this.buttonTranslate.add,
        icon: "pi pi-plus",
        command: () => {
          this.addAction.emit("Add Action");
        },
        title: this.titleTranslate.titleAddItem
      });
    }
    if (this.archiver === true) {
      this.items.push({
        label: this.buttonTranslate.archive,
        icon: "pi pi-upload",
        command: () => {
          this.archiveAction.emit("Archive Action");
        },
        title: this.titleTranslate.titleRestoreItem
      });
    } else if (this.archiver === false) {
      this.items.push({
        label: this.buttonTranslate.restore,
        icon: "pi pi-download",
        command: () => {
          this.restoreAction.emit("Restore Action");
        },
        title: this.titleTranslate.titleArchiveItem
      });
    }
    if (this.historique === true) {
      this.items.push({
        label: this.buttonTranslate.historique,
        icon: "fa fa-history",
        command: () => {
          this.historiqueAction.emit("historique Action");
        }
      });
    }
  }
}
