import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ConfirmDialogModule, ConfirmationService, MessageService, TieredMenuModule, ButtonModule } from 'primeng/primeng';
import { ToastModule } from 'primeng/toast';
import { TieredmenuComponent } from './components/tieredmenu/tieredmenu.component';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../../assets/i18n/');
}

@NgModule({
  declarations: [TieredmenuComponent],
  imports: [
    CommonModule,
    ConfirmDialogModule,
    ToastModule,
    FormsModule,
    ReactiveFormsModule,
    TieredMenuModule,
    ButtonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [TranslateModule, ConfirmDialogModule, ToastModule, FormsModule, ReactiveFormsModule , TieredmenuComponent],
  providers: [ConfirmationService, MessageService]
})
export class SharedModule { }
