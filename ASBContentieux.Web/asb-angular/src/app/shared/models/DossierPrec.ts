import { CompteBancaire } from "./CompteBancaire";
import { Agence } from "./agence";
export interface DossierPrec {
  idDossierPrec?: number;
  idDossier: number;
  creationTime: Date;
  procInstanceId: string;
  nomTitulaire: string;
  telTitulaire: string;
  regularise: boolean;
  archiver: boolean;
  agence?: Agence;
  compteBancaire?: CompteBancaire;
}
