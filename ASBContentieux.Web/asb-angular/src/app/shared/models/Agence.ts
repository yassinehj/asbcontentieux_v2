import { Adresse } from "./adresse";
export interface Agence {
  id?: number;
  idInstitutionSiege: number;
  idInstitutionRegionale?: number;
  raisonSocial?: string;
  codeInstitution: number;
  nature?: string;
  numTelRespo?: string;
  emailRespo?: string;
  adresse?: Adresse;
}
