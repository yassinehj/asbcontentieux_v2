import { Agence } from './Agence';
import { Adresse } from './Adresse';

export interface Tribunal {
    idTribunal: number;
    type: string;
    numTel: string;
    fax: string;
    tribunalSuivate?: Tribunal;
    agence?: Agence;
    adresse?: Adresse;
}
  