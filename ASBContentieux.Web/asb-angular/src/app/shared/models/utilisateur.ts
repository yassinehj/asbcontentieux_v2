import { Profession } from './profession';

export interface Utilisateur {
    id?: number;
    activated: boolean;
    login: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
    refProfession: Profession;
    dateCreation?: Date;
}
