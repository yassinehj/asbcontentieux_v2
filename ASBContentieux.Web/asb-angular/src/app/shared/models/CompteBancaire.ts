import { Debiteur } from "./Debiteur";
export interface CompteBancaire {
  id?: number;
  rib: string;
  solde: number;
  debiteur: Debiteur;
}
