export interface Configuration {
    idConfiguration?: number;
    nom: string;
    nom_ar: string;
    type: string;
    isEditable?: boolean
    
}