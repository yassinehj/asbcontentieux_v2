export interface Pays {
  idPays?: number;
  nom?: string;
  isEditable?: boolean;
}
