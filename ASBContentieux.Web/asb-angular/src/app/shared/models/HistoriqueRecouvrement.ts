import { DossierPrec } from "./DossierPrec";
export interface HistoriqueRecouvrement {
  idHistoriqueRecouverement?: number;
  nom: string;
  creationTime: Date;
  endTime: Date;
  descriptionProlongation: string;
  durationProlongation: number;
  lettreSevere: boolean;
  lettreCommercial: boolean;
  appel: boolean;
  sommation: boolean;
  comite: boolean;
  agent: string;
  idRapportHn: string;
  idProcess: string;
  idDossier: number;
  dossierPrec?: DossierPrec;
}
