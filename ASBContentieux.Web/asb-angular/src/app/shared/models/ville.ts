import { Gouvernorat } from "./Gouvernorat";
export interface Ville {
    idVille?: number;
    nom?: string;
    codePostal?: string;
    gouvernorat?: Gouvernorat;
   // idGouvernorat?: number;
    isEditable?: boolean;
}
