export interface PieceAffaire {
    idPieceAffaire?: number;
    nom: string;
    dateCreation: Date;
    idAlfresco: string;
    fileType: string;
    fileName: string;
}
