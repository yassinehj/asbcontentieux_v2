export interface Auxiliaire {
     idAuxiliaire?: number;
     nom: string;
     prenom: string;
     email:string;
     numTel:string;
     specialite:string;
     type:string;
     numTel2:string;
}
