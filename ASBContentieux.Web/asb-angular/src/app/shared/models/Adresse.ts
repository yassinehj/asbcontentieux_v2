import { Ville } from "./ville";
export interface Adresse {
  idAdresse?: number;
  description: string;
  ville?: Ville;
}
