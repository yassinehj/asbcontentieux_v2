export interface Tache {
    idTache?: number;
    titre: string ;
    description: string;
    dateDebut: Date;
    dateFin: Date;
    dureeNotif: number;
    etat:string;
    statut:string;

}