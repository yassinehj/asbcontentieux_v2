import { Pays } from "./Pays";
export interface Gouvernorat {
  idGouvernorat?: number;
  nom: string;
  pays?: Pays;
  isEditable?: boolean;
}
