export interface Dossier {
  idDossier?: number;
  nom?: string;
  description: string;
  dateCreation?: Date;
}
