import { Phase } from './Phase';

export interface Audience {
     idAudience?: number;
     dateCreation: Date;
     description: string;
     dateAudience: Date;
     etat: string;
     dateFin: Date;
     dureeNotif: number;
     notif: String;
     phase?: Phase;
}
