export interface Profession {
    idRefProfession: number;
    codProfession?: string;
    libelleProfession: string;
}
