import { Adresse } from "./adresse";
export interface Debiteur {
  idDebiteur: number;
  firstName: string;
  lastName: string;
  tel: string;
  email: string;
  cin: string;
  passport: string;
  dateCin: Date;
  lieuCin: string;
  nature: string;
  profession: string;
  registreCommerce: string;
  raisonSocial: string;
  adresse: Adresse;
}
