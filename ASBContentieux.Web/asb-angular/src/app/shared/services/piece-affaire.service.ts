import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppUtils } from '../utils/app-utils';
import { map } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { PieceAffaire } from '../models/piece-affaire';
import { MatTableDataSource } from '@angular/material/table';





@Injectable({
  providedIn: 'root'
})
export class PieceAffaireService {
  private urlPath: string;
  fileData: File = null;
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  public pieceAffaireList: Array<PieceAffaire> = [];



  private isCreatedFolder : boolean = false;
  private isCreatedFolderEvent : BehaviorSubject<Boolean>;



  onPiecesChanged: BehaviorSubject<any>;
  onPiecesSelected: BehaviorSubject<any>;

  constructor(private http: HttpClient) {
    // Set the defaults
    this.onPiecesChanged = new BehaviorSubject({});
    this.onPiecesSelected = new BehaviorSubject(null);
    this.urlPath = AppUtils.getServerAbsolutePath() + '/file';
    this.isCreatedFolderEvent = new BehaviorSubject<Boolean>(false);
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {}

  /**
   * Get files
   *
   * @returns {Promise<any>}
   */
  getPieces() {
    return this.http
      .get<PieceAffaire[]>(this.urlPath + '/getPiecesAffaires', {
        headers: new HttpHeaders({
          Authorization: localStorage.getItem('token')
        })
      })
      .pipe(
        map((piecesAffaires: PieceAffaire[]) => {
          this.pieceAffaireList = piecesAffaires;
          return this.pieceAffaireList;
        })
      );
  }

  getAllPiecesAffairesByDossier(idDossier: number){


    return this.http.get(
      this.urlPath + '/getPiecesAffairesByDossier/'+idDossier,
      {

    headers: new HttpHeaders({
      Authorization:localStorage.getItem('token')
    })
      }
    );

  }

  deletePieceById(idPieceAffaire: number) {
    return this.http.delete(
      this.urlPath + '/pieceAffaire/delete/' + idPieceAffaire,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.getItem('token')
        })
      }
    );
  }

  downLoadFile(data: any, type: string, fileName: string) {
    var blob = new Blob([data], { type: type.toString() });
    var url = window.URL.createObjectURL(blob);
    saveAs(blob, fileName);
  }

  downloadFile(fileName: string) {
    return this.http.get(this.urlPath + '/download/' + fileName, {
      responseType: 'arraybuffer',
      headers: new HttpHeaders({
        Authorization: localStorage.getItem('token'),
        'Content-Type': 'application/octet-stream'
      })
    });
  }

  deleteAllPiecesAffaires(listPieceAffaireId: any[]) {
    return this.http.delete(
      this.urlPath + '/deleteAllPieces/' + listPieceAffaireId,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.getItem('token')
        })
      }
    );
  }

  pushFileToStorage(file: File , idDossier?: number) {
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post(this.urlPath + '/ajoutPieceAffaire' + (idDossier ? '?idDossier=' + idDossier : ''),
    formData,
    {
       headers: new HttpHeaders({
        Authorization: localStorage.getItem('token')
      }), observe : 'body'
    });
  }





  
    movePieceAffaireInDossier(pieceAffaire:PieceAffaire,idDossier?: number){

      return this.http.put(this.urlPath + '/updatePieceAffaire?idDossier=' + idDossier, pieceAffaire ,
      {
        headers: new HttpHeaders({
         Authorization: localStorage.getItem('token')
       }), observe : 'body'
     });




}




}
