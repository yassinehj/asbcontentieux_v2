import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CrudService } from '../utils/crud/crud.service';
import { CrudEndPoints } from '../utils/crud/crud-endpoints';
import { Configuration } from '@shared/models/Configuration';

const _configurationEndPoints: CrudEndPoints = new CrudEndPoints('/addConfiguration', '/getConfigurations',
  '/getConfiguration', '/updateConfiguration', '/deleteConfigurations');

  @Injectable({
    providedIn: 'root'
  })


  export class ConfigurationService extends CrudService<Configuration, number> {
    configurations: Configuration[];
    listId: any[] = [];
  
    constructor(protected _http: HttpClient) {
      super(_http, _configurationEndPoints);
    }
    deleteAllConfigurations(listId: any[]) {
      return this._http.delete(this._baseURL + '/deleteConfigurations/' + listId, { headers: this.headers });
    }
  
  }