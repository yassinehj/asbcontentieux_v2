import { BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: "root"
})
export class SharedService {
  public listnerLangue: BehaviorSubject<any> = new BehaviorSubject(
    localStorage.getItem("langue")
  );

  public editMode: BehaviorSubject<boolean> = new BehaviorSubject(false);
  
  public createdFolderButtonEvent: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor() { }

  /**
   * find an item in the specified list
   *
   * @param list the entered list where we should perform the serach of the item
   * @param item the searched item
   * @param property the property of the item
   */
  findItemInList(list: any[] , item: any , property: any) {
    return list.find(el => el[property] === item[property]);
  }
}
