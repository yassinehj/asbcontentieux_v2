import { HistoriqueRecouvrement } from "./../models/HistoriqueRecouvrement";
import { Injectable } from "@angular/core";
import { CrudService } from "@shared/utils/crud/crud.service";
import { CrudEndPoints } from "@shared/utils/crud/crud-endpoints";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
const _dossierPrecEndPoints: CrudEndPoints = new CrudEndPoints();
@Injectable({
  providedIn: "root"
})
export class HistoriqueRecouvrementService extends CrudService<
  HistoriqueRecouvrement,
  number
> {
  public historiqueRecouvrement: HistoriqueRecouvrement[];
  constructor(protected _http: HttpClient) {
    super(_http, _dossierPrecEndPoints);
  }
  findAllByIdDossierPrec(
    idDossierPrec: string
  ): Observable<HistoriqueRecouvrement[]> {
    return this._http.get<HistoriqueRecouvrement[]>(
      this._baseURL + "/getListHistRecoByIdDosPrec/" + idDossierPrec,
      { headers: this.headers }
    );
  }
}
