import { DossierPrec } from "./../models/DossierPrec";
import { HttpClient, HttpParams } from "@angular/common/http";
import { CrudEndPoints } from "./../utils/crud/crud-endpoints";
import { Injectable } from "@angular/core";
import { CrudService } from "../utils/crud/crud.service";
import { Observable } from "rxjs";

const _dossierPrecEndPoints: CrudEndPoints = new CrudEndPoints(
  null,
  "/getDossiersPrec"
);
@Injectable({
  providedIn: "root"
})
export class DossierPrecService extends CrudService<DossierPrec, number> {
  dossierPrec: DossierPrec[];
  constructor(protected _http: HttpClient) {
    super(_http, _dossierPrecEndPoints);
  }
  findAllByArchiver(archiver: boolean): Observable<DossierPrec[]> {
    return this._http.get<DossierPrec[]>(
      this._baseURL + "/getDossiersPrec/" + archiver,
      { headers: this.headers }
    );
  }
  deleteListDossierPrec(dossiersPrec: DossierPrec[]) {
    return this._http.post(this._baseURL + "/deleteAll", dossiersPrec, {
      headers: this.headers
    });
  }
  archiverRestoreListDosPrec(dossiersPrec: DossierPrec[], archivage: boolean) {
    return this._http.put<DossierPrec[]>(
      this._baseURL + "/archiverRestorerDosPrec/" + archivage,
      dossiersPrec,
      {
        headers: this.headers
      }
    );
  }
}
