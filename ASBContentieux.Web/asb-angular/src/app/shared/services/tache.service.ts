import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Tache } from '../models/tache';
import { CrudService } from '../utils/crud/crud.service';
import { CrudEndPoints } from '../utils/crud/crud-endpoints';

const _tacheEndPoints: CrudEndPoints = new CrudEndPoints('/addTache', '/getTaches',
  '/getTache', '/updateTache', '/deleteTache');

@Injectable({
  providedIn: 'root'
})
export class TacheService extends CrudService<Tache, number> {
  taches: Tache[];
  listId: any[] = [];

  constructor(protected _http: HttpClient) {
    super(_http, _tacheEndPoints);
  }
  deleteAllTaches(listId: any[]) {
    return this._http.delete(this._baseURL + '/deleteTaches/' + listId, { headers: this.headers });
  }

}
