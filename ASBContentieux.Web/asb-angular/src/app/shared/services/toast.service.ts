import { Injectable } from '@angular/core';
import { MessageService, ConfirmationService, Confirmation } from 'primeng/primeng';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private messageToast: any;
  private confirmationDialog: any;
  constructor(private messageService: MessageService,
              private confirmationService: ConfirmationService,
              private translate: TranslateService) {
    /**
     * Subscribe to language change , then get
     * translation toast object with the new selected language
     */
    this.translate.onLangChange.subscribe(lang => {
      this.messageToast = lang.translations.messageToast;
      this.confirmationDialog = lang.translations.confirmationDialog;
    });
    /**
     * Subscribe to translation at the first load of the component ,
     * then create the items, translated with the current language
     */
    this.translate.get(['messageToast', 'confirmationDialog']).subscribe((translations: any) => {
      this.messageToast = translations.messageToast;
      this.confirmationDialog = translations.confirmationDialog;
    });
  }

  success(msgToastKey: string) {
    this.messageService.add({ severity: 'success', detail: this.messageToast[msgToastKey] });
  }

  info(msgToastKey: string) {
    this.messageService.add({ severity: 'info', detail: this.messageToast[msgToastKey]});
  }

  warning(msgToastKey: string | number) {
    this.messageService.add({ severity: 'warn', detail: this.messageToast[msgToastKey]});
  }

  error(msgToastKey: string | number) {
    this.messageService.add({ severity: 'error', detail: this.messageToast[msgToastKey] });
  }

  showCustom(msgToastKey: string | number) {
    this.messageService.add({ key: 'custom', severity: 'info', detail: this.messageToast[msgToastKey] });
  }

  showTopLeft(msgToastKey: string | number) {
    this.messageService.add({ key: 'tl', severity: 'info', detail: this.messageToast[msgToastKey] });
  }

  showTopCenter(msgToastKey: string | number) {
    this.messageService.add({ key: 'tc', severity: 'warn', detail: this.messageToast[msgToastKey]});
  }

  confirm(modalKey: string , messageKey: string, titleKey: string) {
    const confirmation: Confirmation = {
      message: this.confirmationDialog[modalKey][messageKey],
      header: this.confirmationDialog[modalKey][titleKey],
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
      }
    };
    this.confirmationService.confirm(confirmation);
    return confirmation.acceptEvent;
  }
}
