import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Audience } from '../models/audience';
import { CrudService } from '../utils/crud/crud.service';
import { CrudEndPoints } from '../utils/crud/crud-endpoints';
import { AppUtils } from '@shared/utils/app-utils';


const _audienceEndPoints: CrudEndPoints = new CrudEndPoints('/saveAudience', '/getAllAudiences',
  '/getAudienceById', '/updateAudience', '/deleteAudience');

@Injectable({
  providedIn: 'root'
})
export class AudienceService extends CrudService<Audience, number> {
  protected urlPath: string;
  public editMode = false;

  constructor(protected _http: HttpClient ) {
    super(_http, _audienceEndPoints);
    this.urlPath = AppUtils.getServerAbsolutePath() + '/api';
  }

  deleteAllAudiences(listAudienceId : any[]){

    return this._http.delete(this.urlPath + '/deleteAudiences/'+listAudienceId  , { headers: this.headers })
 
 
 
   }

}
