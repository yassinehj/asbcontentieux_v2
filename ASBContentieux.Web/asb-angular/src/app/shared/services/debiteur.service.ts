import { Injectable } from '@angular/core';
import { CrudService } from '@shared/utils/crud/crud.service';
import { HttpClient} from '@angular/common/http';
import { Debiteur } from '@shared/models/Debiteur';
import { CrudEndPoints } from '@shared/utils/crud/crud-endpoints';
import { AppUtils } from '@shared/utils/app-utils';

const userEndPoints: CrudEndPoints = new CrudEndPoints('/createDebiteur','/getAllDebiteurs','/getDebiteurById' ,'/updateDebiteurs','/deleteDebiteur');

@Injectable({
  providedIn: 'root'
})
export class DebiteurService extends CrudService<Debiteur,number> {

  private urlPath: string;

  public editMode = false;

  constructor( http: HttpClient) {

    super(http, userEndPoints);


    this.urlPath = AppUtils.getServerAbsolutePath() + '/api';
   }


   deleteAllDebiteurs(listDebiteurId : any[]){

    return this._http.delete(this.urlPath + '/deleteDebiteurs/'+listDebiteurId  , { headers: this.headers })



   }

   
}
