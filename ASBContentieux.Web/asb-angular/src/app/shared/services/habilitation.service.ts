import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppUtils } from './../utils/app-utils';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Role } from '@shared/models/role';

@Injectable({
  providedIn: 'root'
})
export class HabilitationService {

  roles: Role[];
  headers: any;
  _baseURL: string;
  constructor(private _http: HttpClient) {
    this._baseURL = AppUtils.getBaseApiURL();
    this.headers =  new HttpHeaders({
        Authorization: localStorage.getItem('token')
      });
  }

  getAllMenusByModule(nomModule: string) {
    return this._http.get(this._baseURL + '/getAllMenusByModule?nomModule=' + nomModule , { headers: this.headers });
  }

  getAllModules() {
    return this._http.get(this._baseURL + '/modules' , { headers: this.headers });
  }

  addRole(powers) {
    return this._http.post(this._baseURL + '/saveRefPower' , powers , { headers: this.headers });
  }

  getAllRoles() {
    return this._http.get(this._baseURL + '/roles' , { headers: this.headers })
      .pipe(map((listOfRoles: Role[]) => {
        this.roles = listOfRoles;
        return this.roles;
      }));
  }

  deleteRole(nomRole) {
    return this._http.post(this._baseURL + '/deleteRoles' , nomRole , { headers: this.headers });
  }
}
