import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppUtils } from '../utils/app-utils';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwtToken;
  private urlPath: string;
  public currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  constructor(private http: HttpClient) {
    this.urlPath = AppUtils.getServerAbsolutePath();
    this.currentUserSubject = new BehaviorSubject<any>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }


  login(user) {
    return this.http.post(this.urlPath + '/login', user, {
      observe: 'response'
    });
  }

  getCurrentUserName() {
    return this.http.get(this.urlPath + '/api/login');
  }
  saveToken(jwt) {
    localStorage.setItem('token', jwt);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  loadToken(jwtToken) {
    this.jwtToken = jwtToken;
    this.saveToken(this.jwtToken);
  }
  logout() {
    this.jwtToken = null;
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
  getByLogin(login: string) {
    return this.http.get(this.urlPath + '/api/userByLogin/' + login, {
      headers: new HttpHeaders({
        Authorization: localStorage.getItem('token')
      })
    });
  }
}
