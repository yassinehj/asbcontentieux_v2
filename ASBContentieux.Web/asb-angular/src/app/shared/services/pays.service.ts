import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pays } from '../models/pays';
import { CrudService } from '../utils/crud/crud.service';
import { CrudEndPoints } from '../utils/crud/crud-endpoints';
import { Gouvernorat } from '@shared/models/Gouvernorat';

const _paysEndPoints: CrudEndPoints = new CrudEndPoints('/addPays', '/getPays',
  '/getPaysById', '/updatePays', '/deletePays');

@Injectable({
  providedIn: 'root'
})
export class PaysService extends CrudService<Pays, number> {

  constructor( protected _http: HttpClient) {
    super(_http, _paysEndPoints);
  }
  deleteAllPays(listId: any[]) {
    return this._http.delete(this._baseURL + '/deletePays/' + listId, { headers: this.headers });
  }


  public getPaysByGouvernorat(gouvernorat: Gouvernorat) {
      return this._http.post(this._baseURL + '/getPaysByGouvernorat' , gouvernorat , {headers: this.headers} );
  }
}
