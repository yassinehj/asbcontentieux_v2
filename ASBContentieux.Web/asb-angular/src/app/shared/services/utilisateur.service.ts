import { CrudEndPoints } from '../utils/crud/crud-endpoints';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../utils/crud/crud.service';
import { Utilisateur } from '../models/utilisateur';

const _userEndPoints: CrudEndPoints = new CrudEndPoints('/createUser', '/getAllUsers', '/getUserById', '/updateUser' , '/deleteUser');

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService extends CrudService<Utilisateur, number> {

  constructor(protected _http: HttpClient) {
    super(_http, _userEndPoints);
  }

  deleteAllUsers(idUsers: number[]) {
    return this._http.delete(this._baseURL + '/deleteUsers/' + idUsers , { headers: this.headers });
  }
  getByLogin(login: string) {
    return this._http.get(this._baseURL + '/userByLogin/' + login, { headers: this.headers });
  }
  findByEmail(email: string) {
    return this._http.get(this._baseURL + '/userByEmail/' + email, { headers: this.headers });
  }
  sendLinkResetPassword(link: string, email: string) {
    return this._http.get(
      this._baseURL + '/sendLinkResetPassword/' + email + '/' + link, { headers: this.headers }
    );
  }
  getAllProfessions() {
    return this._http.get(this._baseURL + '/getAllProfessions', { headers: this.headers });
  }
  resetPassword(emailCrypter: string, newPassword: string) {
    return this._http.get(
      this._baseURL + '/resetPassword/' + emailCrypter + '/' + newPassword, { headers: this.headers }
    );
  }
}
