import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { AppUtils } from '../utils/app-utils';
import { map } from 'rxjs/operators';
import { Agence } from '@shared/models/Agence';
import { CrudService } from '@shared/utils/crud/crud.service';
import { CrudEndPoints } from '@shared/utils/crud/crud-endpoints';

const _userEndPoints: CrudEndPoints = new CrudEndPoints('/createAgence','/getAllAgences','/getAgencesById' ,'/updateAgences','/deleteAgence');

@Injectable({
  providedIn: 'root'
})
export class AgenceService extends CrudService<Agence, number> {


  
  public agenceList:Array<Agence> = [];
  private urlPath: string;

  public editMode = false;

  constructor( _http: HttpClient) { 
    super(_http, _userEndPoints);

    
    this.urlPath = AppUtils.getServerAbsolutePath() + '/api';
  }

  deleteAllAgences(listAgenceId: any[]) {
    return this._http.delete(this.urlPath + '/deleteAgences/'+listAgenceId  , { headers: this.headers });
  }

  



}
