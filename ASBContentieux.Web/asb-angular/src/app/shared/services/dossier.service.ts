import { Dossier } from './../models/Dossier';
import { CrudEndPoints } from './../utils/crud/crud-endpoints';
import { CrudService } from '@shared/utils/crud/crud.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { AppUtils } from '../utils/app-utils';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
const _dossierEndPoints: CrudEndPoints = new CrudEndPoints(
  '/savedossier',
  '/getDossiers',
  null,
  null,
  null,
  '/deleteListDossies'
);
@Injectable({
  providedIn: 'root'
})
export class DossierService extends CrudService<Dossier, number> {
  private urlPath: string;
  dossiers: Dossier[];
  dataSource: MatTableDataSource<any> = new MatTableDataSource();

  onDossierChanged: BehaviorSubject<any>;
  onDossierSelected: BehaviorSubject<any>;

  constructor(private http: HttpClient) {
    super(http, _dossierEndPoints);

    this.onDossierChanged = new BehaviorSubject({});
    this.onDossierSelected = new BehaviorSubject({});
    //this.urlPath = AppUtils.getServerAbsolutePath() + '/folder';
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {}

  /**
   * Get files
   *
   * @returns {Promise<any>}
   */

  
}
