import { CrudEndPoints } from '../utils/crud/crud-endpoints';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../utils/crud/crud.service';
import { Auxiliaire } from '../models/auxiliaire';

const _auxiliaireEndPoints: CrudEndPoints = new CrudEndPoints('/addAuxiliaire', '/getAuxiliaires', '/getAuxiliaire', '/updateAuxiliaire' , '/deleteAuxiliaire');
@Injectable({
    providedIn: 'root'
  })
  export class AuxiliaireService extends CrudService<Auxiliaire, number> {

    constructor(protected _http: HttpClient) {
      super(_http, _auxiliaireEndPoints);
    }
  
    deleteAllAuxiliaires(listId: any[]) {
      return this._http.delete(this._baseURL + '/deleteAllAuxiliaires/'+ listId , { headers: this.headers });
      
    }
    getByName(nom: string) {
      return this._http.get(this._baseURL + '/auxiliaireByNom/' + nom, { headers: this.headers });
    }
    findByEmail(email: string) {
      return this._http.get(this._baseURL + '/auxiliaireByEmail/' + email, { headers: this.headers });
    }
    
   
  }
  