import { Injectable } from '@angular/core';
import { CrudEndPoints } from '../utils/crud/crud-endpoints';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CrudService } from '../utils/crud/crud.service';
import { Gouvernorat } from '../models/gouvernorat';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const _gouvernoratEndPoints: CrudEndPoints = new CrudEndPoints('/addGouvernorat/', '/getGouvernorat',
  '/getGouvernoratById', '/updateGouvernorat/', '/deleteGouvernorat');

@Injectable({
  providedIn: 'root'
})
export class GouvernoratService extends CrudService<Gouvernorat, number> {

  constructor(protected _http: HttpClient) {
    super(_http, _gouvernoratEndPoints);
  }

  findGouvernoratByPays(idPays: number): Observable<any> {
    return this._http.get(this._baseURL + '/getGouvernoratByPays/' + idPays, { headers: this.headers })
    .pipe(
      map((listOfGouvernorats: Gouvernorat[]) => {
        this.itemsList = listOfGouvernorats;
        return this.itemsList;
      })
    );
  }

}
