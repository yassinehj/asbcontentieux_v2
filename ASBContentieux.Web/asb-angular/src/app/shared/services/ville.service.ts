import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CrudEndPoints } from '../utils/crud/crud-endpoints';
import { CrudService } from '../utils/crud/crud.service';
import { Ville } from '../models/ville';
import { map } from 'rxjs/operators';

const _gouvernoratEndPoints: CrudEndPoints = new CrudEndPoints('/addVille/', '/getVille',
  '/getVilleById', '/updateVille/', '/deleteVille');


@Injectable({
  providedIn: 'root'
})
export class VilleService extends CrudService<Ville, number> {

  constructor(protected _http: HttpClient) {
    super(_http, _gouvernoratEndPoints);
  }

  findVilleByGouvernorat(idGouvernorat: number): Observable<any> {
    return this._http.get(this._baseURL + '/getVilleByGouvernorat/' + idGouvernorat, { headers: this.headers })
    .pipe(
      map((listOfVilles: Ville[]) => {
        this.itemsList = listOfVilles;
        return this.itemsList;
      })
    );
  }

}
