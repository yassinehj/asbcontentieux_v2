import { Injectable } from '@angular/core';
import { CrudService } from '@shared/utils/crud/crud.service';
import { Tribunal } from '@shared/models/Tribunal';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CrudEndPoints } from '@shared/utils/crud/crud-endpoints';
import { Observable } from 'rxjs';
import { AppUtils } from '@shared/utils/app-utils';

const _tribunalEndPoints: CrudEndPoints = new CrudEndPoints('/addTribunal', '/getTribunal',
  '/getTribunalById', '/updateTribunal', '/deleteTribunal');

@Injectable({
    providedIn: 'root'
  })
  export class TribunalService extends CrudService<Tribunal, number> {
      protected urlPath: string;
      public editMode = false;
      
      constructor(protected _http:HttpClient){
        super(_http, _tribunalEndPoints);
        this.urlPath = AppUtils.getServerAbsolutePath() + '/api';
      }


      deleteAllDTribunals(listTribunalId : any[]){

        return this._http.delete(this.urlPath + '/deleteTribunals/'+listTribunalId  , { headers: this.headers })
     
     
     
       }


     /* addTribunall(tribunal :Tribunal,idTribunalSuivante:Number ): Observable<any> {
        return this._http.post('http://localhost:8085/ASBContentieux.Web/api/addTribunall/'+tribunal, idTribunalSuivante,{
          headers: new HttpHeaders({Authorization: localStorage.getItem('token')
          })
      });
      }*/
  }  
