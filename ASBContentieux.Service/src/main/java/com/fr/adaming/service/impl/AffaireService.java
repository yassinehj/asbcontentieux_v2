package com.fr.adaming.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fr.adaming.entities.Affaire;
import com.fr.adaming.repositories.IAffaireRepository;
import com.fr.adaming.service.IAffaireService;


@Service
public class AffaireService implements IAffaireService {

	@Qualifier("IAffaireRepository")
	@Autowired
	private IAffaireRepository iaffaireRepository;

	@Override
	public List<Affaire> getAffaires() {
		// TODO Auto-generated method stub
		return iaffaireRepository.findAll();
	}

	@Override
	public void deleteAffaire(Integer idAffaire) {
		// TODO Auto-generated method stub
		iaffaireRepository.deleteById(idAffaire);
	}

	@Override
	public Affaire addAfaire(Affaire affaire) {
		// TODO Auto-generated method stub
		return iaffaireRepository.save(affaire);
	}

	@Override
	public Affaire updateAffaire(Affaire affaire) {
		// TODO Auto-generated method stub
		return iaffaireRepository.saveAndFlush(affaire);
	}
	
	

}
