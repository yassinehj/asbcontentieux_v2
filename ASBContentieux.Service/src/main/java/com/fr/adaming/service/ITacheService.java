package com.fr.adaming.service;

import com.fr.adaming.entities.Tache;

import java.util.List;

public interface ITacheService {

    public Tache saveTache(Tache tache);
    public List<Tache> getAll();
    public Tache getTacheById(Long id_tache);
    public Tache updateTache(Tache tache);
    public void deleteTache(Long id_tache);
    public void deleteAllTaches(List<Tache> taches);
 
}
