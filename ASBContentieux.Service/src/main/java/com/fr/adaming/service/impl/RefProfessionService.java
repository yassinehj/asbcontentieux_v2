package com.fr.adaming.service.impl;

import com.fr.adaming.entities.RefProfession;
import com.fr.adaming.repositories.IRefProfessionRepository;
import com.fr.adaming.service.IRefProfessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("RefProfessionService")
public class RefProfessionService implements IRefProfessionService {

	@Autowired
	@Qualifier("IRefProfessionRepository")
	private IRefProfessionRepository refProfessionRepository;

	@Override
	public List<RefProfession> findAll() {
		return refProfessionRepository.findAll();
	}

	@Override
	public RefProfession getById(Long id) {
		return refProfessionRepository.findById(id).get();
	}

	@Override
	public RefProfession getByCode(String id) {
		return refProfessionRepository.findByCodProfession(id);
	}
}
