package com.fr.adaming.service;

import com.fr.adaming.entities.RefProfession;
import com.fr.adaming.entities.Utilisateur;

import java.util.List;


/**
 * @author mbouhdida
 *
 */
public interface IUtilisateurService {

	/**
	 * recuperer une liste des utilisateurs.
	 * <p>
	 * 
	 * @return la liste récupérée
	 */
	public List<Utilisateur> getAllUsers();

	/**
	 * ajouter un utilisateur.
	 * <p>
	 * 
	 * @param user
	 * 
	 * @return l'objet ajouté
	 */
	public Utilisateur saveUser(Utilisateur user);

	/**
	 * recuperer l'objet utilisateur par id.
	 * <p>
	 * 
	 * @param id
	 *            l'identifiant de l'utilisateur
	 * 
	 * 
	 * @return l'objet récupéré
	 */
	public Utilisateur getById(Long id);

	/**
	 * recuperer une liste des utilisateurs non affecté.
	 * <p>
	 * 
	 * @return la liste récupérée
	 */
	public List<Utilisateur> findNotAffectedUser();

	/**
	 * recuperer l'objet utilisateur par login.
	 * <p>
	 * 
	 * @param login
	 * 
	 * @return l'objet récupéré
	 */
	Utilisateur getByLogin(String login);
	/**
	 * recuperer l'objet utilisateur par email.
	 * <p>
	 *
	 * @param email
	 *
	 * @return l'objet récupéré
	 */
	Utilisateur findByEmail(String email);

	List<Utilisateur> getLoggedInUsers();

	/**
	 * mettre a jour un utilisateur
	 * <p>
	 * 
	 * @param user
	 * 
	 * 
	 * @return l'objet modifié
	 */
	public Utilisateur updateUser(Utilisateur user);

	/**
	 * supprimer un utilisateur
	 * <p>
	 *
	 * @param idUser l'id de l'utilisateur a supprimé
	 *
	 */
	public void deleteUser(Long idUser);

	/**
	 * supprimer un utilisateur
	 * <p>
	 *
	 * @param users l'id de l'utilisateur a supprimé
	 *
	 */
	public void deleteAllUsers(List<Utilisateur> users);

	/**
	 * recuperer une liste des utilisateurs par role.
	 * <p>
	 * 
	 * @param authorityName
	 *            le nom du role
	 * 
	 * 
	 * @return la liste récupérée
	 */
	public List<Utilisateur> findByAuthorityName(String authorityName);

	/**
	 * recuperer une liste des utilisateurs par refProfession.
	 * <p>
	 * 
	 * @param refProfession
	 * 
	 * 
	 * @return la liste récupérée
	 */
	public List<Utilisateur> findByRefProf(RefProfession refProfession);

	/**
	 * recuperer une liste des utilisateurs affectés.
	 * <p>
	 * 
	 * @return la liste récupérée
	 */
	public List<Utilisateur> findAffectedUser();
    /**
     * recuperer une liste des utilisateurs affectés.
     * <p>
     *
     * @return la liste récupérée
     */
    public Boolean sendEmail(String objetMail,String email,String titreContent,String bodyContent);
}
