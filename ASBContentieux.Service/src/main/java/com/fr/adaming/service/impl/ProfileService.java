package com.fr.adaming.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fr.adaming.dto.ProfilDto;
import com.fr.adaming.entities.*;
import com.fr.adaming.repositories.*;
import com.fr.adaming.service.IProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("ProfileService")
public class ProfileService implements IProfileService {

	@Autowired
	@Qualifier("IProfileRepository")
	private IProfileRepository profileRepository;

	@Autowired
	@Qualifier("IRefInstitutionRepository")
	private IRefInstitutionRepository refInstitutionRepository;

	@Autowired
	@Qualifier("IRefMenuRepository")
	private IRefMenuRepository refMenuRepository;

	@Autowired
	@Qualifier("IRefPowerRepository")
	private IRefPowerRepository refPowerRepository;

	@Autowired
	@Qualifier("IUtilisateurRepository")
	private IUtilisateurRepository userRepository;

	// @Autowired
	// private IAccountService accountService;
	@Override
	public List<ProfilDto> getAllProfiles() {
		List<ProfilDto> profilsDto = new ArrayList<ProfilDto>();
		List<Profile> profiles = profileRepository.findAll();
		for (Profile profile : profiles) {
			ProfilDto profilDto = new ProfilDto();
			profilDto = this.convertProfilToProfilDto(profile);
			profilsDto.add(profilDto);
		}
		return profilsDto;
	}

	@Override
	public Profile findByNomProfile(String nomProfile) {
		return profileRepository.getProfilByNomProfile(nomProfile);

	}

	@Override
	public Profile addProfile(Profile profile) {
		return profileRepository.save(profile);
	}

	@Override
	public List<Profile> getAllProfiless() {
		return profileRepository.findAll();

	}

	@Override
	public List<Profile> getProfilesGroupByNom() {
		return profileRepository.getListProfile();

	}

	@Override
	public List<Role> getListRoleByNomProfile(String nomProf) {
		return profileRepository.getListRoleByNomProfile(nomProf);

	}

	@Override
	public List<Utilisateur> getListUserByNomProfile(String nomProf) {
		return profileRepository.getListUserByNomProfile(nomProf);
	}

	@Override
	public List<Profile> findListProfilofUserLogin(String designation) {
		return profileRepository.findListProfilofUserLogin(designation);

	}

	@Override
	public List<Profile> getListProfilesDistinct() {
		return profileRepository.getListProfileDistinct();

	}

	@Override
	public List<RefInstitution> getListInstitutionByNomProfile(String nomProf) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addProfils(List<ProfilDto> profiles) {
		List<Profile> pro = new ArrayList<Profile>();
		profiles.forEach(item -> {
			ProfileID profileId = new ProfileID();
			Profile profil = new Profile();
			profileId.setRoleId(item.getNomRole());
			profileId.setUserId(item.getIdUser());
			profil.setId(profileId);
			profil.setNomProfile(item.getNomProfile());
			pro.add(profil);
		});
		Utilisateur user = userRepository.findById(profiles.get(0).getIdUser()).get();
		user.setRefInstitution(refInstitutionRepository.findById(profiles.get(0).getIdInstitution()).get());
		// accountService.initReinitialiserPassword(user.getEmail());
		profileRepository.saveAll(pro);
	}

	@Override
	public List<ProfilDto> findProfileByNom(String nom) {
		List<ProfilDto> profilsDto = Arrays.asList();
		List<Profile> profiles = profileRepository.getListProfileByNom(nom);
		/*
		 * JAVA 8
		 */
		profilsDto = profiles.stream().map(n -> this.convertProfilToProfilDto(n)).collect(Collectors.toList());

		return profilsDto;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateProfile(List<ProfilDto> profilsDto) {
		profileRepository.deleteProfile(profilsDto.get(0).getNomProfile());
		List<Profile> profiles = new ArrayList<Profile>();
		List<Profile> profilesForUser = new ArrayList<Profile>();

		profilsDto.forEach(item -> {
			Profile profile = new Profile();
			ProfileID profilId = new ProfileID();

			profilId.setRoleId(item.getNomRole());
			profilId.setUserId(item.getIdUser());
			profile.setId(profilId);
			profile.setNomProfile(item.getNomProfile());
			profiles.add(profile);
		});
		Utilisateur user = userRepository.findById(profilsDto.get(0).getIdUser()).get();
		profilesForUser = profileRepository.findListProfilofUserId(user.getId());
		// if (profilesForUser.isEmpty()) {
		// accountService.initReinitialiserPassword(user.getEmail());
		// }
		RefInstitution institutionSiege = refInstitutionRepository.getByflagLocal(true);
		user.setRefInstitution(refInstitutionRepository.findOneByCodeInstitutionAndIdInstitutionSiege(
				profilsDto.get(0).getCodeInstitution(), institutionSiege.getIdInstitution()));

		userRepository.save(user);
		profileRepository.saveAll(profiles);
	}

	@Override
	public List<Profile> getListProfilByRole(String nom) {
		List<Profile> profiles = profileRepository.getListProfilByRole(nom);
		return profiles;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteProfile(String nom) {
		List<Profile> profil = profileRepository.getListProfileByNom(nom);
		Utilisateur user = userRepository.findById(profil.get(0).getId().getUserId()).get();
		user.setRefInstitution(null);
		userRepository.save(user);
		profileRepository.deleteProfile(nom);

	}

	public ProfilDto convertProfilToProfilDto(Profile profile) {
		ProfilDto profilDto = new ProfilDto();
		List<RefPower> refPowers = refPowerRepository.getRefPowerByRole(profile.getId().getRoleId());
		if (!refPowers.isEmpty()) {
			RefMenu menu = refMenuRepository.findById(refPowers.get(0).getRefPowerId().getIdRefMenu()).get();
			profilDto.setNomModule(menu.getRefModule().getNomModule());
		}
		Utilisateur user = userRepository.findById(profile.getId().getUserId()).get();
		RefInstitution refInst = refInstitutionRepository.findById(user.getRefInstitution().getIdInstitution()).get();
		profilDto.setCodeInstitution(user.getRefInstitution().getCodeInstitution());
		profilDto.setRaisonSociale(refInst.getRaisonSociale());
		profilDto.setFirstName(user.getFirstName());
		profilDto.setLastName(user.getLastName());
		profilDto.setIdUser(user.getId());
		profilDto.setNomRole(profile.getId().getRoleId());
		profilDto.setNomProfile(profile.getNomProfile());
		return profilDto;
	}

}
