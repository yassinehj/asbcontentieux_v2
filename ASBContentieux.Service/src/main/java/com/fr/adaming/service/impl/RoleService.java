package com.fr.adaming.service.impl;

import java.util.List;

import com.fr.adaming.entities.Role;
import com.fr.adaming.repositories.IProfileRepository;
import com.fr.adaming.repositories.IRefPowerRepository;
import com.fr.adaming.repositories.IRoleRepository;
import com.fr.adaming.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("RoleService")
public class RoleService implements IRoleService {

	@Autowired
	@Qualifier("IRoleRepository")
	private IRoleRepository roleRepository;
	
	@Autowired
	@Qualifier("IProfileRepository")
	private IProfileRepository profileRepository;
	
	@Autowired
	@Qualifier("IRefPowerRepository")
	private IRefPowerRepository refPowerRepository;

	@Override
	public List<Role> findAll() {
		return roleRepository.findAll();

	}

	@Override
	public List<Role> findAuthorityByModule(String nomModule) {
		return roleRepository.findAuthorityByModule(nomModule);

	}

	@Override
	@Transactional
	public void deleteRole(String nom) {
		profileRepository.deleteProfileByRole(nom);
		refPowerRepository.deleteByRole(nom);
		roleRepository.deleteById(nom);
	}

}
