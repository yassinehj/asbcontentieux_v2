package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Auxiliaire;
import com.fr.adaming.repositories.IAuxiliaireRepository;
import com.fr.adaming.service.IAuxiliaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("AuxiliaireService")

    public class AuxiliaireService implements IAuxiliaireService {

    @Autowired
    @Qualifier("IAuxiliaireRepository")
    private IAuxiliaireRepository auxiliaireRepository;

    @Override
    public Auxiliaire saveAuxiliaire(Auxiliaire auxiliaire) {
        return auxiliaireRepository.save(auxiliaire);
    }

    @Override
    public List<Auxiliaire> getAll() {
        return auxiliaireRepository.findAll();
    }

    @Override
    public Auxiliaire getAuxiliaireById(Long id_auxiliaire) {
        return auxiliaireRepository.findById(id_auxiliaire).get();
    }

    @Override
    public Auxiliaire updateAuxiliaire(Auxiliaire auxiliaire) {
        return auxiliaireRepository.saveAndFlush(auxiliaire);
    }

    @Override
    public void deleteAuxiliaire(Long id_auxiliaire) {
        auxiliaireRepository.deleteById(id_auxiliaire);
    }
}
