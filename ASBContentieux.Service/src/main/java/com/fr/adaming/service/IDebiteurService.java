package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.dto.DebiteurCompteBancaireDto;
import com.fr.adaming.entities.Debiteur;
import com.fr.adaming.wsdl.ClientContentieux;

public interface IDebiteurService {
    public Debiteur addDebiteur(Debiteur debiteur);

    Debiteur getDebiteurDistinctFirstByCin(String cin);

    Debiteur addDebiteurFromRecouvrement(ClientContentieux clientRecouvrement);
    
    List<Debiteur> getAllDebiteur();
    
    Debiteur updateDebiteur(Debiteur debiteur);
    
    void deleteDebiteurById(Long idDebiteur);
    
    Debiteur getDebiteurById(Long idDebiteur);
}
