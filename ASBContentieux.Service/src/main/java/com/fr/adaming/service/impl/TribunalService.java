package com.fr.adaming.service.impl;

import com.fr.adaming.dto.TribunalDto;
import com.fr.adaming.entities.Tribunal;
import com.fr.adaming.repositories.ITribunalRepository;
import com.fr.adaming.service.ITribunalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service("TribunalService")
public class TribunalService implements ITribunalService {

    @Autowired
    @Qualifier("ITribunalRepository")
    ITribunalRepository iTribunalRepository;

    @Override
    public Tribunal saveTribunal(Tribunal tribunal) {
        return iTribunalRepository.save(tribunal);
    }

    @Override
    public List<Tribunal> getAll() {
        return iTribunalRepository.findAll();
    }

    @Override
    public Tribunal getTribunalById(Long idTribunal) {
        return iTribunalRepository.findById(idTribunal).get();
    }

    @Override
    public Tribunal updateTribunal(Tribunal tribunal) {
        return iTribunalRepository.saveAndFlush(tribunal);
    }

    @Override
    public void deleteTribunal(Long id_tribunal) {
        iTribunalRepository.deleteById(id_tribunal);
    }

    @Override
    public void deleteAllTribunals(List<Tribunal> tribunals) {
        iTribunalRepository.deleteAll();
    }
}
