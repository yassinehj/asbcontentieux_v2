package com.fr.adaming.service.impl;

import com.fr.adaming.entities.CompteBancaire;
import com.fr.adaming.repositories.ICompteBancaireRepository;
import com.fr.adaming.service.ICompteBancaireService;
import org.springframework.stereotype.Service;

@Service
public class CompteBancaireService implements ICompteBancaireService {
    private ICompteBancaireRepository compteBancaireRepository;

    public CompteBancaireService(ICompteBancaireRepository compteBancaireRepository) {
        this.compteBancaireRepository = compteBancaireRepository;
    }
    @Override
    public CompteBancaire getCompteBancaireDistinctFirstByRib(String rib){
        return compteBancaireRepository.findDistinctFirstByRib(rib);
    }
    @Override
    public CompteBancaire addCompteBancaire(CompteBancaire compteBancaire){
            return compteBancaireRepository.saveAndFlush(compteBancaire);
    }
}
