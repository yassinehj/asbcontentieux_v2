package com.fr.adaming.service.impl;

import com.fr.adaming.entities.DossierPrec;
import com.fr.adaming.entities.HistoriqueRecouverement;
import com.fr.adaming.repositories.IHistoriqueRecouvrementRepository;
import com.fr.adaming.service.IHistoriqueRecouvrementService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoriqueRecouvrementService implements IHistoriqueRecouvrementService {
    private IHistoriqueRecouvrementRepository historiqueRecouvrementRepository;

    public HistoriqueRecouvrementService(IHistoriqueRecouvrementRepository historiqueRecouvrementRepository) {
        this.historiqueRecouvrementRepository = historiqueRecouvrementRepository;
    }
    @Override
    public HistoriqueRecouverement addHistoriqueRecouverement(HistoriqueRecouverement historiqueRecouverement){
        return historiqueRecouvrementRepository.saveAndFlush(historiqueRecouverement);
    }
    @Override
    public void DeleteListHistoReco(List<HistoriqueRecouverement> listHistReco) {
        historiqueRecouvrementRepository.deleteAll(listHistReco);
    }
    @Override
    public List<HistoriqueRecouverement> getAllHistoRecByDosPrec(Long idDossierPrec){
        return historiqueRecouvrementRepository.findAllByDossierPrec_IdDossierPrec(idDossierPrec);
    }
}
