package com.fr.adaming.service;

import com.fr.adaming.entities.RefModule;

import java.util.List;


/**
 * @author mbouhdida
 *
 */
public interface IRefModuleService {
	/**
	 * recuperer une liste des refModules.
	 * <p>
	 * 
	 * @return la liste récupérée
	 */
	public List<RefModule> getAllModules();

	/**
	 * recuperer l'objet refModule par nomModule.
	 * <p>
	 * 
	 * @param nomModule
	 *            le module
	 * 
	 * 
	 * @return l'objet récupéré
	 */
	public RefModule findByNomModule(String nomModule);

	/**
	 * recuperer une liste des refModules par roles.
	 * <p>
	 * 
	 * @param roles
	 *            liste des roles de l'utilisateur connecté
	 * 
	 * 
	 * @return la liste récupérée
	 */
	public List<RefModule> getListModuleByRoles(List<String> roles);
}
