package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Ville;
import com.fr.adaming.repositories.IVilleRepository;
import com.fr.adaming.service.IVilleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author nbelhajyahia
 *
 */
@Service("VilleService")
public class VilleService implements IVilleService {
    @Autowired
    @Qualifier("IVilleRepository")
    private IVilleRepository villeRepository;

    @Override
    public Ville saveVille(Ville ville) {
       if(getVilleByNom(ville.getNom()).size() == 0)  {
           return villeRepository.save(ville);
       }
          return null;
    }
    @Override
    public Ville saveWithoutUniqueVille(Ville ville) {
            return villeRepository.save(ville);
    }

    @Override
    public List<Ville> getAll() {
        return villeRepository.findAll();
    }

    @Override
    public Ville getVilleById(Long id_ville) {
        return villeRepository.findById(id_ville).get();
    }
    @Override
    public List<Ville> getVilleByNom(String nom) {
        return villeRepository.findByNom(nom);
    }
    @Override
    public Ville getVilleDistinctFirstByNom(String nom) {
        return villeRepository.findDistinctFirstByNom(nom);
    }
    @Override
    public Ville updateVille(Ville ville) {
            return villeRepository.saveAndFlush(ville);
    }

    @Override
    public void deleteVille(Long id_ville) {
        villeRepository.deleteById(id_ville);
    }

    @Override
    public List<Ville> findAllByGouvernorat_IdGouvernorat(Long idGouvernorat) {
        return villeRepository.findAllByGouvernorat_IdGouvernorat(idGouvernorat);
    }
}
