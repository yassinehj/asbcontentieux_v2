package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Dossier;
import com.fr.adaming.entities.PieceAffaire;
import com.fr.adaming.repositories.IDossierRepository;
import com.fr.adaming.repositories.IPieceAffaireRepository;
import com.fr.adaming.service.IPieceAffaireService;
import com.fr.adaming.utils.Utilitaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class PieceAffaireService  implements IPieceAffaireService {
	
	Logger log = Logger.getLogger(PieceAffaireService.class.getName());

    @Autowired
    @Qualifier("IPieceAffaireRepository")
    private IPieceAffaireRepository iPieceAffaireRepository;
    
    
    @Autowired
    private IDossierRepository iDossierRepository;

    @Override
    public PieceAffaire addPieceAffaire(PieceAffaire pieceAffaire , MultipartFile file) {
        pieceAffaire.setFileName(file.getOriginalFilename());
        pieceAffaire.setIdAlfresco(Utilitaire.addFileAlfresco(file, "test", null));
        return iPieceAffaireRepository.saveAndFlush(pieceAffaire);
    }

    @Override
    public List<PieceAffaire> getAllPieceAffaire() {
        return iPieceAffaireRepository.findAllPiecesAffaires();

    }

    @Override
    public void deletePieceAffaire(Long idPieceAffaire) {
       iPieceAffaireRepository.deleteById(idPieceAffaire);
    }

    

    @Override
    public PieceAffaire getPieceAffaireByID(Long idPieceAffaire) {
       PieceAffaire pieceAffaire = iPieceAffaireRepository.findById(idPieceAffaire).get();

       return  pieceAffaire;
    }

	@Override
	public void deleteAllPiecesAffaires(List<PieceAffaire> piecesAffaire) {
		// TODO Auto-generated method stub
		
		iPieceAffaireRepository.deleteAll(piecesAffaire);
		
	}

	@Override
	public List<PieceAffaire> findAllPieceAffaireByDossier(Long idDossier) {
		// TODO Auto-generated method stub
		return iPieceAffaireRepository.findAllPieceByDossier(idDossier);
	}

	@Override
	public PieceAffaire addPieceAffaireInDossier(PieceAffaire pieceAffaire ,Long idDossier, MultipartFile file) {
        pieceAffaire.setFileName(file.getOriginalFilename());
        pieceAffaire.setIdAlfresco(Utilitaire.addFileAlfresco(file, "test", null));
		Dossier dossier = iDossierRepository.getOne(idDossier);
        if (dossier != null) {
        pieceAffaire.setDossier(dossier);
        }
        return iPieceAffaireRepository.saveAndFlush(pieceAffaire);
	}

	@Override
	public PieceAffaire updatePieceAffaire(PieceAffaire pieceAffaire, Long idDossier) {
		Dossier dossier = iDossierRepository.getOne(idDossier);
		 if (dossier != null) {
	         pieceAffaire.setDossier(dossier);
		 }
        return iPieceAffaireRepository.saveAndFlush(pieceAffaire);
		
	}



}
