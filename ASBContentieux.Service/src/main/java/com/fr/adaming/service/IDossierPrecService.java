package com.fr.adaming.service;


import com.fr.adaming.entities.DossierPrec;

import java.util.List;

public interface IDossierPrecService {
    List<DossierPrec> getAllDossierPrec();

    List<DossierPrec> getAllDossierPrecByArchiver(Boolean archiver);

    void deleteAllDossierPrec(List<DossierPrec> dossiersPrec);

    List<DossierPrec> archiverOrRestorerDossierPrec(List<DossierPrec> dossiersPrec, Boolean operation);

    void synchronizeWithRecouvrement();
}
