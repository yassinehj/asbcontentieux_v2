package com.fr.adaming.service;

import com.fr.adaming.dto.MenuDto;
import com.fr.adaming.entities.RefMenu;

import java.util.List;


/**
 * @author mbouhdida
 *
 */
public interface IRefMenuService {

	/**
	 * recuperer une liste des refMenus fils par refModule.
	 * <p>
	 * 
	 * @param refModule
	 *            le module
	 * 
	 * @return la liste récupérée
	 */
	List<RefMenu> findByRefModule(Long refModule);

	/**
	 * recuperer une liste des refMenus Pere par refModule et utilisateur Connecté.
	 * <p>
	 * 
	 * @param module
	 *            le module
	 * 
	 * @param login
	 * 
	 * @return la liste récupérée
	 */
	public List<RefMenu> getEcranPereByUserAndModule(String login, String module);

	/**
	 * recuperer une liste des refMenus Pere et fils par refModule et utilisateur
	 * Connecté.
	 * <p>
	 * 
	 * @param module
	 *            le module
	 * 
	 * @param login
	 * 
	 * @return la liste récupérée
	 */
	public List<MenuDto> getEcransByUserAndModule(String module, String login);

	/**
	 * verifier si l'utilisateur a le droit de visualiser le menu
	 * <p>
	 * 
	 * @param nomMnu
	 *            le nom du menu
	 * 
	 * @param login
	 * 
	 * @return variable boolean
	 */
	public boolean getEcranByUser(String login, String nomMnu);

	/**
	 * recuperer une liste des refMenus fils par refModule et utilisateur Connecté.
	 * <p>
	 * 
	 * @param nomModule
	 *            le module
	 * 
	 * @param login
	 * 
	 * @return la liste récupérée
	 */

	public List<RefMenu> findListRefMenuProcess(String login, String nomModule);

}
