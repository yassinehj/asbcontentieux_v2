package com.fr.adaming.service;

import com.fr.adaming.entities.Ville;

import java.util.List;

/**
 * @author nbelhajyahia
 *
 */

public interface IVilleService {

    public Ville saveVille(Ville ville );

    Ville saveWithoutUniqueVille(Ville ville);

    public List<Ville> getAll();
    public Ville getVilleById(Long id_ville);

    List<Ville> getVilleByNom(String nom);

    Ville getVilleDistinctFirstByNom(String nom);

    public Ville updateVille(Ville ville);
    public void deleteVille(Long id_ville);
    public List<Ville> findAllByGouvernorat_IdGouvernorat(Long idGouvernorat);

}
