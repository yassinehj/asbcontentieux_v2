package com.fr.adaming.service;

import com.fr.adaming.dto.RefPowerDto;
import com.fr.adaming.entities.RefPower;

import java.util.List;


/**
 * @author mbouhdida
 *
 */
public interface IRefPowerService {

	/**
	 * ajouter une liste des refPowers.
	 * <p>
	 * 
	 * @param refPowers
	 *            liste des refPowers
	 * 
	 * 
	 */
	public void save(List<RefPowerDto> refPowers);

	/**
	 * recuperer une liste des refPower par role.
	 * <p>
	 * 
	 * @param name
	 *            le nom du role
	 * 
	 * @return la liste récupérée
	 */
	public List<RefPowerDto> getRefPowerByRole(String name);

	/**
	 * mettre a jour une liste des refPower.
	 * <p>
	 * 
	 * @param refPowers
	 * 
	 */
	public void update(List<RefPowerDto> refPowers);

	/**
	 * recuperer l'objet refPower par menu et utilisateur Connecté.
	 * <p>
	 * 
	 * @param menu
	 *            le nom de l'ecran
	 * 
	 * @param login
	 * 
	 * @return l'objet récupéré
	 */
	public RefPower getPowerByUserAndMenu(String menu, String login);

}
