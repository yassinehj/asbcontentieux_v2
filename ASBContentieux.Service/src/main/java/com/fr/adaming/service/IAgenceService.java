package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entities.Agence;
import com.fr.adaming.wsdl.Institution;

import java.util.List;

public interface IAgenceService {
    Agence getAgenceByCodeInstitution(String codeInstitution);

    Agence addAgence(Agence agence);

    Agence addAgenceFromRecouvrement(Institution agenceRecouvrement);
    
    List<Agence> getAllAgence();
    
    Agence updateAgence(Agence agence);
    
    Agence getByID(Long id);
    
    void deleteAllAgences(List<Agence> agences);
    
    void deleteAgenceByID(Long id);
}
