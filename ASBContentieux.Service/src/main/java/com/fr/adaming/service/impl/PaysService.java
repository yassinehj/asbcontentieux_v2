package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Gouvernorat;
import com.fr.adaming.entities.Pays;
import com.fr.adaming.repositories.IPaysRepository;
import com.fr.adaming.service.IPaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author nbelhajyahia
 *
 */
@Service("PaysService")
public class PaysService implements IPaysService {

    @Autowired
    @Qualifier("IPaysRepository")
    private IPaysRepository paysRepository;

    @Override
    public Pays savePays(Pays pays) {
        if(getPaysByNom(pays.getNom()) == null){
            return paysRepository.save(pays);
            }
        return null;
    }
    @Override
    public Pays saveWithoutUniquePays(Pays pays) {
            return paysRepository.save(pays);
    }

    @Override
    public List<Pays> getAll() {
        return paysRepository.findAll();
    }

    @Override
    public Pays getPaysById(Long id_pays) {
        return paysRepository.findById(id_pays).get();
    }
    @Override
    public Pays getPaysByNom(String nom) {
        return paysRepository.findByNom(nom);
    }

    @Override
    public Pays findPaysByGouvernorat(Gouvernorat gouvernorat) {
        return paysRepository.findPaysByGouvernorat(gouvernorat);
    }

    @Override
    public Pays updatePays( Pays pays) {
            return paysRepository.saveAndFlush(pays);
    }
    @Override
    public Pays getPaysDistinctFirstByNom(String nom) {
        return paysRepository.findDistinctFirstByNom(nom);
    }
    @Override
    public void deletePays(Long id_pays) {
        paysRepository.deleteById(id_pays);
    }
}
