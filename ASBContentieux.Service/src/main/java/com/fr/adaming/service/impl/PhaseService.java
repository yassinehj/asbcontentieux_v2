package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Phase;
import com.fr.adaming.repositories.IPhaseRepository;
import com.fr.adaming.service.IPhaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("PhaseService")
public class PhaseService implements IPhaseService {

    @Autowired
    @Qualifier("IPhaseRepository")
    IPhaseRepository phaseRepository;

    @Override
    public Phase saveAudience(Phase phase) {
        return phaseRepository.save(phase);
    }

    @Override
    public List<Phase> getAll() {
        return phaseRepository.findAll();
    }

    @Override
    public Phase getPhaseById(Long id_phase) {
        return phaseRepository.findById(id_phase).get();
    }

    @Override
    public Phase updatePhase(Phase phase) {
        return phaseRepository.saveAndFlush(phase);
    }

    @Override
    public void deletePhase(Long id_phase) {
         phaseRepository.deleteById(id_phase);
    }
}
