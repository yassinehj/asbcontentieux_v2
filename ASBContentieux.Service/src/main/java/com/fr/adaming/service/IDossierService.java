package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entities.Dossier;

public interface IDossierService {
	
	public List<Dossier> getDossiers();
	public void deleteDossier(Long idDossier);

	void deleteListDossiers(List<Dossier> dossiers);

	public Dossier addDossier(Dossier dossier);
	public Dossier updateDossier(Dossier dossier);
	public Dossier getDossierByID(Long idDossier);
	

}
