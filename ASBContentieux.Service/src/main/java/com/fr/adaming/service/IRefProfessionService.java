package com.fr.adaming.service;


import com.fr.adaming.entities.RefProfession;

import java.util.List;

/**
 * @author mbouhdida
 *
 */
public interface IRefProfessionService {

	/**
	 * recuperer une liste des RefProfession.
	 * <p>
	 *
	 *
	 * @return une liste des RefProfession.
	 */
	 List<RefProfession> findAll();

	/**
	 * recuperer l'objet refProfession par id.
	 * <p>
	 * 
	 * @param id
	 *            l'identifiant de la profession
	 * 
	 * @return l'objet récupéré
	 */
	RefProfession getById(Long id);

	/**
	 * recuperer l'objet refProfession par code profession.
	 * <p>
	 * 
	 * @param id
	 *            code de la profession
	 * 
	 * @return l'objet récupéré
	 */
	RefProfession getByCode(String id);
}
