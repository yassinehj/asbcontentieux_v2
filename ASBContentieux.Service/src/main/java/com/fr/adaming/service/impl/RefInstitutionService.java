package com.fr.adaming.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.fr.adaming.dto.RefInstitutionDto;
import com.fr.adaming.entities.RefInstitution;
import com.fr.adaming.repositories.IRefInstitutionRepository;
import com.fr.adaming.service.IRefInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("RefInstitutionService")
public class RefInstitutionService implements IRefInstitutionService {

	@Autowired
	private IRefInstitutionRepository refInstitutionRepo;

	@Override
	public RefInstitution getBanqueLocale() {
		return refInstitutionRepo.getByflagLocal(true);

	}

	@Override
	public List<RefInstitution> getAgencesLocales() {
		RefInstitution institutionLocal = getBanqueLocale();
		if(institutionLocal != null){
		return refInstitutionRepo.findAllByIdInstitutionSiege(institutionLocal.getIdInstitution());
		}
		else{
			return null;
		}
	}

	@Override
	public Page<RefInstitution> getAgencesLocales(Pageable p) {
		return refInstitutionRepo.findAllByIdInstitutionSiege(getBanqueLocale().getIdInstitution(), p);

	}

	@Override
	public void updateInstitution(List<RefInstitution> institutions) {
		institutions.forEach(institution -> {
			refInstitutionRepo.updateInstitution(institution.getIdInstitution(), institution.getFlagRegional());
		});
	}

	@Override
	public List<RefInstitution> getAgencesRegionale() {
		return refInstitutionRepo.findAllByIdInstitutionSiegeAndFlagRegional(getBanqueLocale().getIdInstitution(),
				true);
	}

	@Override
	public RefInstitution getAgenceById(Long id) {
		return refInstitutionRepo.findById(id).get();

	}

	@Override
	public List<RefInstitution> getAgenceLocaleById(Long id) {
		List<RefInstitution> refInstitutions = new ArrayList<RefInstitution>();
		RefInstitution refInstitution = refInstitutionRepo.findById(id).get();
		refInstitutions = refInstitutionRepo
				.findAllByIdInstitutionSiegeAndIdInstitutionRegionaleOrIdInstitutionRegionaleNull(id,
						refInstitution.getIdInstitutionSiege());
		return refInstitutions;
	}

	@Override
	public List<String> getListOfCodeInstitution() {
		List<String> resultat = new ArrayList<String>();
		refInstitutionRepo.findAll().forEach(institution -> {
			resultat.add(institution.getCodeInstitution());
		});
		return resultat;
	}

	@Override
	public void updateInstitutionLocale(RefInstitutionDto institutions) {
		institutions.getInstitutions().forEach(institution -> {
			refInstitutionRepo.updateInstitutionLocale(institution.getIdInstitution(),
					institutions.getIdInstitutionRegionale());
		});
	}

	@Override
	public List<RefInstitution> getListInstitution() {
		return refInstitutionRepo.findAll();

	}

	@Override
	public List<RefInstitution> getListInstitutionByCode(String code) {
		List<RefInstitution> refInstitutions = new ArrayList<RefInstitution>();
		RefInstitution refInstitutionSiege = refInstitutionRepo.getByflagLocal(true);
		RefInstitution refInstitution = refInstitutionRepo.findByCodeInstitutionAndIdInstitutionSiege(code,
				refInstitutionSiege.getIdInstitution());
		if (code.equals("000")) {
			refInstitutions = refInstitutionRepo.findAllByIdInstitutionSiege(refInstitutionSiege.getIdInstitution());
		}
		if (refInstitution.getFlagRegional()!= null &&refInstitution.getFlagRegional()) {
			refInstitutions = refInstitutionRepo.findByIdInstitutionRegionaleAndIdInstitutionSiege(
					refInstitutionSiege.getIdInstitution(), refInstitution.getIdInstitution());
		}
		if (refInstitution.getIdInstitutionRegionale()!=null) {
			refInstitutions.add(refInstitution);
		}
		return refInstitutions;
	}

}
