package com.fr.adaming.service;



import com.fr.adaming.entities.Auxiliaire;

import java.util.List;

public interface IAuxiliaireService {

    public Auxiliaire saveAuxiliaire(Auxiliaire auxiliaire);
    public List<Auxiliaire> getAll();
    public Auxiliaire getAuxiliaireById(Long id_auxiliaire);
    public Auxiliaire updateAuxiliaire(Auxiliaire auxiliaire);
    public void deleteAuxiliaire(Long id_auxiliaire);
}
