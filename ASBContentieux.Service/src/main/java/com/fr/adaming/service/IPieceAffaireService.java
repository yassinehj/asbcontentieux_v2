package com.fr.adaming.service;


import com.fr.adaming.entities.PieceAffaire;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

public interface IPieceAffaireService {
	
	

    public PieceAffaire addPieceAffaire(PieceAffaire pieceAffaire, MultipartFile file);
    public List<PieceAffaire> getAllPieceAffaire();
    public void deletePieceAffaire(Long idPieceAffaire);
    public PieceAffaire updatePieceAffaire(PieceAffaire pieceAffaire,Long idPieceAffaire);
    public PieceAffaire getPieceAffaireByID(Long idPieceAffaire);
    public void deleteAllPiecesAffaires(List<PieceAffaire> piecesAffaire);
    public List<PieceAffaire> findAllPieceAffaireByDossier(Long idDossier);
    public PieceAffaire addPieceAffaireInDossier(PieceAffaire pieceAffaire , Long idDossier , MultipartFile file);
   
   
    




}
