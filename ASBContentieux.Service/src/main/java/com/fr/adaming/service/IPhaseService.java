package com.fr.adaming.service;



import com.fr.adaming.entities.Phase;

import java.util.List;

public interface IPhaseService {

    public Phase saveAudience(Phase phase);
    public List<Phase> getAll();
    public Phase getPhaseById(Long id_phase);
    public Phase updatePhase(Phase phase);
    public void deletePhase(Long id_phase);
}
