package com.fr.adaming.service.impl;

import com.fr.adaming.dto.DebiteurCompteBancaireDto;
import com.fr.adaming.entities.*;
import com.fr.adaming.repositories.IDebiteurRepository;
import com.fr.adaming.service.*;
import com.fr.adaming.wsdl.ClientContentieux;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("DebiteurService")
public class DebiteurService implements IDebiteurService {
    @Autowired
    @Qualifier("IDebiteurRepository")
    private IDebiteurRepository debiteurRepository;
    private IAdresseService adresseService;
    private IVilleService villeService;
    private IGouvernanteService gouvernanteService;
    private IPaysService paysService;
    private ICompteBancaireService compteBancaireService;


    public DebiteurService(IAdresseService adresseService, IVilleService villeService, IGouvernanteService gouvernanteService, IPaysService paysService, ICompteBancaireService compteBancaireService) {
        this.adresseService = adresseService;
        this.villeService = villeService;
        this.gouvernanteService = gouvernanteService;
        this.paysService = paysService;
        this.compteBancaireService = compteBancaireService;
    }

    @Override
    public Debiteur addDebiteur(Debiteur debiteur) {
        return debiteurRepository.saveAndFlush(debiteur);
    }
    @Override
    public Debiteur getDebiteurDistinctFirstByCin(String cin){return debiteurRepository.findDistinctFirstByCin(cin);}
    @Override
    public Debiteur addDebiteurFromRecouvrement(ClientContentieux clientRecouvrement){
        Ville ville;
        Gouvernorat gouvernorat;
        Pays pays;
        Adresse adr;
        Debiteur debiteur;
        //CompteBancaire compteBancaire;
        //DebiteurCompteBancaireDto debiteurCompteBancaireDto=new DebiteurCompteBancaireDto();
        debiteur = debiteurRepository.findDistinctFirstByCin(clientRecouvrement.getCin());
        //compteBancaire=compteBancaireService.getCompteBancaireDistinctFirstByRib(clientRecouvrement.getRib());
        //si le debiteur n'existe pas dans la base (tester par rapport aux CIN)
        if (debiteur == null) {
            //creation d'un nouveau débiteur avec les données de recouvrement
            debiteur = new Debiteur(clientRecouvrement.getNom(), clientRecouvrement.getPrenom(), clientRecouvrement.getNumTel(), clientRecouvrement.getMail(), clientRecouvrement.getCin(), "Physique");
            adr = adresseService.getAdresseDistinctFirstByNom(clientRecouvrement.getAdresseSoap().getAdresse());
            //si l'adresse n'exist pas dans la base de données
            if (adr == null) {
                adr = new Adresse(clientRecouvrement.getAdresseSoap().getAdresse());
                ville = villeService.getVilleDistinctFirstByNom(clientRecouvrement.getAdresseSoap().getVille());
                if (ville == null) {
                    ville = new Ville(clientRecouvrement.getAdresseSoap().getVille(), clientRecouvrement.getAdresseSoap().getCodePostal());
                    gouvernorat = gouvernanteService.getGouvernoratDistinctFirstByNom(clientRecouvrement.getAdresseSoap().getGouvernorat());
                    if (gouvernorat == null) {
                        gouvernorat = new Gouvernorat(clientRecouvrement.getAdresseSoap().getGouvernorat());
                        pays = paysService.getPaysDistinctFirstByNom(clientRecouvrement.getAdresseSoap().getPays());
                        if (pays == null) {
                            pays = new Pays(clientRecouvrement.getAdresseSoap().getPays());
                        }
                        gouvernorat.setPays(pays);
                    }
                    ville.setGouvernorat(gouvernorat);
                }
                adr.setVille(ville);
            }
            debiteur.setAdresse(adr);
            debiteur=addDebiteur(debiteur);
        }
        /*if(compteBancaire==null){
            compteBancaire=new CompteBancaire(clientRecouvrement.getRib());
            compteBancaire.setDebiteur(debiteur);
            compteBancaire=compteBancaireService.addCompteBancaire(compteBancaire);
            debiteurCompteBancaireDto.setCompteBancaire(compteBancaire);
        }*/
        return debiteur;
    }

	@Override
	public List<Debiteur> getAllDebiteur() {
		// TODO Auto-generated method stub
		return debiteurRepository.findAll();
	}

	@Override
	public Debiteur updateDebiteur(Debiteur debiteur) {
		// TODO Auto-generated method stub
		return debiteurRepository.save(debiteur);
	}

	@Override
	public void deleteDebiteurById(Long idDebiteur) {
		// TODO Auto-generated method stub
		 debiteurRepository.deleteById(idDebiteur);
	}

	@Override
	public Debiteur getDebiteurById(Long idDebiteur) {
		// TODO Auto-generated method stub
		return debiteurRepository.findById(idDebiteur).get();
	}
    
    
    
}
