package com.fr.adaming.service;

import com.fr.adaming.entities.Role;

import java.util.List;


/**
 * @author mbouhdida
 *
 */
public interface IRoleService {

	/**
	 * recuperer une liste des roles.
	 * <p>
	 * 
	 * 
	 * @return la liste récupérée
	 */
	public List<Role> findAll();

	/**
	 * recuperer une liste des roles par refModule.
	 * <p>
	 * 
	 * @param nomModule
	 *            le module
	 * 
	 * @return la liste récupérée
	 */
	public List<Role> findAuthorityByModule(String nomModule);

	/**
	 * supprimer un role par nom.
	 * <p>
	 * 
	 * @param nom
	 *            le nom du role
	 * 
	 */
	public void deleteRole(String nom);

}
