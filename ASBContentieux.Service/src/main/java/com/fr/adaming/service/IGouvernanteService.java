package com.fr.adaming.service;

import com.fr.adaming.entities.Gouvernorat;

import java.util.List;

/**
 * @author nbelhajyahia
 *
 */

public interface IGouvernanteService {

    public Gouvernorat saveGouvernorat(Gouvernorat gouvernorat);

    Gouvernorat saveWithoutUniqueGouvernorat(Gouvernorat gouvernorat);

    public List<Gouvernorat> getAll();
    public Gouvernorat getGouvernoratById(Long id_gouvernorat);

    List<Gouvernorat> getGouvernoratByNom(String nom);
    Gouvernorat getGouvernoratDistinctFirstByNom(String nom);

    public Gouvernorat updateGouvernorat(Gouvernorat gouvernorat);
    public void deleteGouvernorat(Long id_gouvernorat);
    public List<Gouvernorat> findAllByPays_IdPays(Long idPays);
}
