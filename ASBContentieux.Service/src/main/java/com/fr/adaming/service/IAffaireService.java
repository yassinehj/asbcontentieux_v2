package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entities.Affaire;

public interface IAffaireService {
	
	public List<Affaire> getAffaires();
	public void deleteAffaire(Integer idAffaire);
	public Affaire addAfaire(Affaire affaire);
	public Affaire updateAffaire(Affaire affaire);

}
