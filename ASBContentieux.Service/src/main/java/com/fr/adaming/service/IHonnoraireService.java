package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entities.Honnoraire;

public interface IHonnoraireService {
	
	public Honnoraire saveHonnoraire(Honnoraire honnoraire);
	public Honnoraire updateHonnoraire(Honnoraire honnoraire);
	public void deleteHonnoraire(Long idHonnoraire);
	public List<Honnoraire> getHonnoraires();
	public Honnoraire getHonnoraireById(Long idHonnoraire);

}
