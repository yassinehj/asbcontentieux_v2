package com.fr.adaming.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fr.adaming.entities.Dossier;
import com.fr.adaming.repositories.IDossierRepository;
import com.fr.adaming.service.IDossierService;
import com.fr.adaming.utils.AlfrescoOpenCmis;
import com.fr.adaming.utils.Utilitaire;

@Service
public class DossierService implements IDossierService {
	
	@Autowired
    @Qualifier("IDossierRepository")
	private IDossierRepository idossierRepository;

	@Override
	public List<Dossier> getDossiers() {
		// TODO Auto-generated method stub
		return idossierRepository.findAll();
	}

	@Override
	public void deleteDossier(Long idDossier) {
		// TODO Auto-generated method stub
		idossierRepository.deleteById(idDossier);
	}
	@Override
	public void deleteListDossiers(List<Dossier> dossiers){
		idossierRepository.deleteAll(dossiers);
	}

	@Override
	public Dossier addDossier(Dossier dossier) {
		// TODO Auto-generated method stub
		
		
		//dossier.setIdDossier(AlfrescoOpenCmis.createFolder(fileInputStream, name, length, mimeTypes)
		
		return idossierRepository.saveAndFlush(dossier);
	}

	@Override
	public Dossier updateDossier(Dossier dossier) {
		// TODO Auto-generated method stub
		return idossierRepository.saveAndFlush(dossier);
	}

	@Override
	public Dossier getDossierByID(Long idDossier) {

		Optional<Dossier> dossier = idossierRepository.findById(idDossier);

		if (dossier.isPresent()){
			return dossier.get();
		}

		return null;
	}


}
