package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Tache;
import com.fr.adaming.repositories.ITacheRepository;
import com.fr.adaming.service.ITacheService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("TacheService")
public class TacheService implements ITacheService {

	 @Autowired
	 @Qualifier("ITacheRepository")
    private ITacheRepository tacheRepository;

    @Override
    public Tache saveTache(Tache tache) {
        return tacheRepository.save(tache);
    }

    @Override
    public List<Tache> getAll() {
        return tacheRepository.findAll();
    }

    @Override
    public Tache getTacheById(Long id_tache) {
        return tacheRepository.findById(id_tache).get();

    }

    @Override
    public Tache updateTache(Tache tache) {
        return tacheRepository.saveAndFlush(tache);
    }

    @Override
    public void deleteTache(Long id_tache) {
        tacheRepository.deleteById(id_tache);

    }
    @Override
	public void deleteAllTaches(List<Tache> taches) {
    	tacheRepository.deleteAll(taches);
	}
}
