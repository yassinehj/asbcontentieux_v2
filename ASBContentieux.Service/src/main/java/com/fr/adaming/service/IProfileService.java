package com.fr.adaming.service;

import com.fr.adaming.dto.ProfilDto;
import com.fr.adaming.entities.Profile;
import com.fr.adaming.entities.RefInstitution;
import com.fr.adaming.entities.Role;
import com.fr.adaming.entities.Utilisateur;

import java.util.List;


/**
 * @author mbouhdida
 *
 */
public interface IProfileService {

	/**
	 * recuperer la liste de tous les profils.
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */
	public List<ProfilDto> getAllProfiles();

	/**
	 * recuperer le profil par nomProfile .
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profile.
	 * @return l'objet récupéré.
	 */
	public Profile findByNomProfile(String nomProfile);

	/**
	 * ajouter un profil.
	 * <p>
	 * 
	 * @param profile
	 * 
	 * @return l'objet ajouté.
	 */

	public Profile addProfile(Profile profile);

	/**
	 * recuperer la liste de tous les profils.
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */

	public List<Profile> getAllProfiless();

	/**
	 * recuperer la liste des profils regroupé par nomProfile .
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */

	public List<Profile> getProfilesGroupByNom();

	/**
	 * recuperer la liste des roles par nom de profile .
	 * <p>
	 * 
	 * @param nomProf
	 *            le nom du profile.
	 * @return la liste récupérée.
	 */

	public List<Role> getListRoleByNomProfile(String nomProf);

	/**
	 * recuperer la liste des utilisateurs par nomProfile .
	 * <p>
	 * 
	 * @param nomProf
	 *            le nom du profile.
	 * @return la liste récupérée.
	 */

	public List<Utilisateur> getListUserByNomProfile(String nomProf);

	/**
	 * recuperer la liste des profils par utilisateur connecté .
	 * <p>
	 * 
	 * @param designation
	 *            le login de l'utilisateur.
	 * 
	 * @return la liste récupérée.
	 */

	public List<Profile> findListProfilofUserLogin(String designation);

	/**
	 * recuperer la liste des profils distincts.
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */

	public List<Profile> getListProfilesDistinct();

	/**
	 * recuperer la liste des institutions par nomProfile .
	 * <p>
	 * 
	 * @param nomProf
	 *            le nom du profile.
	 * 
	 * @return la liste récupérée.
	 */

	public List<RefInstitution> getListInstitutionByNomProfile(String nomProf);

	/**
	 * ajouter un profil.
	 * <p>
	 * 
	 * @param profiles
	 *            liste des profiles à ajoutés.
	 * 
	 * 
	 */

	public void addProfils(List<ProfilDto> profiles);

	/**
	 * recuperer la liste des profiles par nomProfile .
	 * <p>
	 * 
	 * @param nom
	 *            le nom du profile.
	 * 
	 * @return la liste récupérée.
	 */

	public List<ProfilDto> findProfileByNom(String nom);

	/**
	 * mettre a jour la liste des profils .
	 * <p>
	 * 
	 * @param profils
	 * 
	 */

	public void updateProfile(List<ProfilDto> profils);

	/**
	 * recuperer la liste des profils par role .
	 * <p>
	 * 
	 * @param nom
	 *            le nom du role.
	 * @return la liste récupérée.
	 */

	public List<Profile> getListProfilByRole(String nom);

	/**
	 * supprimer le profil par nomProfile .
	 * <p>
	 * 
	 * @param nom
	 *            le nom du profile.
	 * 
	 */

	public void deleteProfile(String nom);
}
