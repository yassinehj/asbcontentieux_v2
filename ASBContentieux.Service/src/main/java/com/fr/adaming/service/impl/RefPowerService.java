package com.fr.adaming.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.fr.adaming.dto.RefPowerDto;
import com.fr.adaming.entities.RefMenu;
import com.fr.adaming.entities.RefPower;
import com.fr.adaming.entities.RefPowerId;
import com.fr.adaming.entities.Role;
import com.fr.adaming.repositories.IRefMenuRepository;
import com.fr.adaming.repositories.IRefPowerRepository;
import com.fr.adaming.repositories.IRoleRepository;
import com.fr.adaming.service.IRefPowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("RefPowerService")
public class RefPowerService implements IRefPowerService {

	@Autowired
	@Qualifier("IRoleRepository")
	private IRoleRepository roleRepository;
	
	@Autowired
	@Qualifier("IRefPowerRepository")
	private IRefPowerRepository refPowerRepository;
	
	@Autowired
	@Qualifier("IRefMenuRepository")
	private IRefMenuRepository refMenuRepository;

	@Override
	public void save(List<RefPowerDto> refPowers) {
		List<RefPower> refPouvoirs = new ArrayList<RefPower>();
		Role authority = new Role(refPowers.get(0).getNomRole());
		roleRepository.save(authority);
		refPouvoirs = this.convertRefPowerDtotoRefPower(refPowers);
		refPowerRepository.saveAll(refPouvoirs);
	}

	@Override
	public List<RefPowerDto> getRefPowerByRole(String name) {
		List<RefPowerDto> refPowersDto = new ArrayList<RefPowerDto>();
		List<RefMenu> refMenusByRole = new ArrayList<RefMenu>();
		List<RefPower> refPowers = refPowerRepository.getRefPowerByRole(name);
		RefMenu menu = refMenuRepository.findById(refPowers.get(0).getRefPowerId().getIdRefMenu()).get();
		List<RefMenu> menusByModule = refMenuRepository.findByRefModuleAndEcranPereNotNull(menu.getRefModule().getIdModule());

		refPowers.forEach(refPower -> {
			RefMenu refMenu = refMenuRepository.findById(refPower.getRefPowerId().getIdRefMenu()).get();
			refMenusByRole.add(refMenu);
		});

		menusByModule.forEach(item -> {
			if (!refMenusByRole.contains(item)) {
				RefPowerId refPowerId = new RefPowerId();
				refPowerId.setIdRefMenu(item.getIdMenu());
				refPowerId.setIdRole(name);
				RefPowerDto refPouvoirDto = new RefPowerDto(item.getIdMenu(), item.getNomMenu(), item.getNomMenuEn());
				refPouvoirDto.setNomRole(name);
				refPouvoirDto.setFlagConsultation(false);
				refPouvoirDto.setFlagCreation(false);
				refPouvoirDto.setFlagModification(false);
				refPouvoirDto.setFlagSuppression(false);
				refPouvoirDto.setNomModule(item.getRefModule().getNomModule());
				refPowersDto.add(refPouvoirDto);
			}
		});

		refPowers.forEach(item -> {
			RefMenu refMenu = refMenuRepository.findById(item.getRefPowerId().getIdRefMenu()).get();
			RefPowerId refPowerId = new RefPowerId();
			refPowerId.setIdRefMenu(item.getRefPowerId().getIdRefMenu());
			refPowerId.setIdRole(item.getRefPowerId().getIdRole());
			RefPowerDto refPouvoirDto = new RefPowerDto(refMenu.getIdMenu(), refMenu.getNomMenu(),
					refMenu.getNomMenuEn());
			refPouvoirDto.setNomRole(item.getRefPowerId().getIdRole());
			refPouvoirDto.setFlagConsultation(item.isFlagConsultation());
			refPouvoirDto.setFlagCreation(item.isFlagCreation());
			refPouvoirDto.setFlagModification(item.isFlagModification());
			refPouvoirDto.setFlagSuppression(item.isFlagSupression());
			refPouvoirDto.setNomModule(refMenu.getRefModule().getNomModule());
			refPowersDto.add(refPouvoirDto);
		});
		return refPowersDto;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(List<RefPowerDto> refPowersDto) {
		List<RefPower> refPowers = new ArrayList<RefPower>();
		refPowerRepository.deleteByRole(refPowersDto.get(0).getNomRole());
		refPowers = this.convertRefPowerDtotoRefPower(refPowersDto);
		refPowerRepository.saveAll(refPowers);
	}

	@Override
	public RefPower getPowerByUserAndMenu(String menu, String login) {
		return refPowerRepository.getPowerByUserAndMenu(menu, login);

	}

	public List<RefPower> convertRefPowerDtotoRefPower(List<RefPowerDto> refPowersDto) {
		List<RefPower> refPouvoirs = new ArrayList<RefPower>();
		Role authority = new Role(refPowersDto.get(0).getNomRole());
		refPowersDto.forEach(refPower -> {
			RefPowerId refPowerId = new RefPowerId();
			refPowerId.setIdRefMenu(refPower.getIdMenu());
			refPowerId.setIdRole(refPower.getNomRole());
			RefPower power = new RefPower();
			power.setRefPowerId(refPowerId);
			power.setFlagConsultation(refPower.isFlagConsultation());
			power.setFlagCreation(refPower.isFlagCreation());
			power.setFlagModification(refPower.isFlagModification());
			power.setFlagSupression(refPower.isFlagSuppression());
			power.setRole(authority);
			refPouvoirs.add(power);
		});

		return refPouvoirs;
	}
}
