package com.fr.adaming.service.impl;

import java.util.ArrayList;

import java.util.Collection;

import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import com.fr.adaming.service.IAccountService;



@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	IUtilisateurService userService;

	@Autowired
	IAccountService accountService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub

		Utilisateur user = accountService.findUserByUsername(username);

		if (user == null)
			throw new UsernameNotFoundException(username);
//		if (user.isActivated()) {
			Collection<GrantedAuthority> authorities = new ArrayList<>();
//			List<Profile> listProfil = profileService.findListProfilofUserLogin(username);
//			for (int i = 0; i < listProfil.size(); i++) {
//				authorities.add(new SimpleGrantedAuthority(listProfil.get(i).getId().getRoleId()));
//			}
//
		return new User(user.getLogin(), user.getPassword(), authorities);
//		} else {
//			 throw new UserNotActivatedException("User " + username + " was not activated");
//		}
	}

}
