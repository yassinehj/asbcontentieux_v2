package com.fr.adaming.service;


import com.fr.adaming.entities.Utilisateur;

public interface IAccountService {
	public Utilisateur findUserByUsername(String login);
	public void initReinitialiserPassword(String login);
	public Utilisateur getTheCurrentUser();
	public String getTheCurrentUserLogin();


}