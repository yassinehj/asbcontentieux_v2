package com.fr.adaming.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fr.adaming.entities.Configuration;
import com.fr.adaming.repositories.IConfigurationRepository;
import com.fr.adaming.service.IConfigurationService;

@Service
public class ConfigurationService implements IConfigurationService {
	
	@Autowired
	private IConfigurationRepository iConfigurationRepository;

	@Override
	public List<Configuration> getConfigurations() {
		return iConfigurationRepository.findAll();
	}

	@Override
	public void deleteConfiguration(Long idConfiguration) {
		
		iConfigurationRepository.deleteById(idConfiguration);		
	}

	@Override
	public Configuration addConfiguration(Configuration configuration) {
		return iConfigurationRepository.save(configuration);
	}

	@Override
	public Configuration updateConfiguration(Configuration configuration) {
		return iConfigurationRepository.saveAndFlush(configuration);
	}
	
}
