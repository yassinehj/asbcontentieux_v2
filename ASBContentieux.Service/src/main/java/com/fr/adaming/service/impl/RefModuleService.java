package com.fr.adaming.service.impl;

import java.util.List;

import com.fr.adaming.entities.RefModule;
import com.fr.adaming.repositories.IRefModuleRepository;
import com.fr.adaming.service.IRefModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("RefModuleService")
public class RefModuleService implements IRefModuleService {

	@Autowired
	@Qualifier("IRefModuleRepository")
	private IRefModuleRepository refModuleRepository;

	@Override
	public List<RefModule> getAllModules() {
		return refModuleRepository.findAll();

	}

	@Override
	public RefModule findByNomModule(String nomModule) {
		return refModuleRepository.getByNomModule(nomModule);

	}

	@Override
	public List<RefModule> getListModuleByRoles(List<String> roles) {
		return refModuleRepository.getListModuleByRoles(roles);

	}

}
