package com.fr.adaming.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fr.adaming.entities.Honnoraire;
import com.fr.adaming.repositories.IHonnoraireRepository;
import com.fr.adaming.service.IHonnoraireService;

@Service
public class HonnoraireService implements IHonnoraireService {

	
	@Autowired
	private IHonnoraireRepository ihonnoraireRepository;
	
	@Override
	public Honnoraire saveHonnoraire(Honnoraire honnoraire) {
		// TODO Auto-generated method stub
		return ihonnoraireRepository.save(honnoraire);
	}

	@Override
	public Honnoraire updateHonnoraire(Honnoraire honnoraire) {
		return ihonnoraireRepository.saveAndFlush(honnoraire);
		
	}

	@Override
	public void deleteHonnoraire(Long idHonnoraire) {
		// TODO Auto-generated method stub
		 ihonnoraireRepository.deleteById(idHonnoraire);
	}

	@Override
	public List<Honnoraire> getHonnoraires() {
		// TODO Auto-generated method stub
		return ihonnoraireRepository.findAll();
	}

	@Override
	public Honnoraire getHonnoraireById(Long idHonnoraire) {
		// TODO Auto-generated method stub
		return ihonnoraireRepository.findById(idHonnoraire).get();
	}
	
	

}
