package com.fr.adaming.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.fr.adaming.dto.MenuDto;
import com.fr.adaming.entities.RefMenu;
import com.fr.adaming.repositories.IRefMenuRepository;
import com.fr.adaming.service.IRefMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;



@Service("RefMenuService")
public class RefMenuService implements IRefMenuService {

	@Autowired
	@Qualifier("IRefMenuRepository")
	private IRefMenuRepository refMenuRepository;

	@Override
	public List<RefMenu> findByRefModule(Long refModule) {
		return refMenuRepository.findByRefModuleAndEcranPereNotNull(refModule);

	}

	@Override
	public List<RefMenu> getEcranPereByUserAndModule(String login, String module) {
		return refMenuRepository.getEcranPereByUserAndModule(login, module);

	}

	@Override
	public List<MenuDto> getEcransByUserAndModule(String module, String login) {
		List<RefMenu> menusPere = new ArrayList<RefMenu>();
		List<MenuDto> menusDto = new ArrayList<MenuDto>();
		menusPere = refMenuRepository.getEcranPereByUserAndModule(login, module);
		menusPere.forEach(menuPere -> {
			List<RefMenu> menusFils = new ArrayList<RefMenu>();
			MenuDto menuDto = new MenuDto();
			menusFils = refMenuRepository.findListRefMenyuByRefModule(module, login, menuPere.getIdMenu());
			menuDto.setEcranPereFr(menuPere.getNomMenu());
			menuDto.setEcranPereEn(menuPere.getNomMenuEn());
			menuDto.setEcranPereAr(menuPere.getNomMenuAr());
			menuDto.setIconPere(menuPere.getIconEcran());
			menuDto.setEcransfils(menusFils);
			menusDto.add(menuDto);
		});

		return menusDto;
	}

	@Override
	public boolean getEcranByUser(String login, String nomMenu) {
		List<RefMenu> refMenus = new ArrayList<RefMenu>();
		refMenus = refMenuRepository.findListRefMenyuByUser(login);
		return refMenus.stream().anyMatch(e -> e.getNomMenu().contains(nomMenu));
	}

	@Override
	public List<RefMenu> findListRefMenuProcess(String login, String nomModule) {
		List<RefMenu> refMenus = new ArrayList<RefMenu>();
		refMenus = refMenuRepository.findListRefMenuProcess(nomModule, login);
		return refMenus;
	}

}
