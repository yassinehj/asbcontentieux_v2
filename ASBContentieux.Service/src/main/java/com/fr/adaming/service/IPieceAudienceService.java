package com.fr.adaming.service;



import com.fr.adaming.entities.PieceAudience;

import java.util.List;

public interface IPieceAudienceService {

    public PieceAudience saveAudience(PieceAudience pieceAudience);
    public List<PieceAudience> getAll();
    public PieceAudience getPieceAudienceById(Long id_piece_audience);
    public PieceAudience updateAudience(PieceAudience pieceAudience);
    public void deletePieceAudience(Long id_piece_audience);
}
