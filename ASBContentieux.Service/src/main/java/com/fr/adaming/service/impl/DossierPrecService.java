package com.fr.adaming.service.impl;
import com.fr.adaming.dto.DebiteurCompteBancaireDto;
import com.fr.adaming.entities.*;
import com.fr.adaming.repositories.IDossierPrecRepository;
import com.fr.adaming.service.*;
import com.fr.adaming.service.interfacageRecouvrement.ClientsClient;
import com.fr.adaming.service.interfacageRecouvrement.HistoricClient;
import com.fr.adaming.service.interfacageRecouvrement.InstitutionClient;
import com.fr.adaming.service.interfacageRecouvrement.OverviewClient;
import com.fr.adaming.utils.Utilitaire;
import com.fr.adaming.wsdl.ClientContentieux;
import com.fr.adaming.wsdl.HistoricSoap;
import com.fr.adaming.wsdl.Institution;
import com.fr.adaming.wsdl.Overview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service("DossierPrecService")
public class DossierPrecService implements IDossierPrecService {
    //ws soap
    private ClientsClient wsDebiteurRecouvrement;
    private HistoricClient wsHistoriqueRecouvrement;
    private OverviewClient wsDossierPrecRecouvrement;
    private InstitutionClient wsAgenceRecouvrement;
    //services
    private IDebiteurService debiteurService;
    private ICompteBancaireService compteBancaireService;
    private IPaysService paysService;
    private IVilleService villeService;
    private IGouvernanteService gouvernanteService;
    private  IAdresseService adresseService;
    private IAgenceService agenceService;
    private IHistoriqueRecouvrementService historiqueRecouvrementService;
    //repository
    @Autowired
    @Qualifier("IDossierPrecRepository")
    private IDossierPrecRepository dossierPrecRepository;

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public DossierPrecService(ClientsClient wsDebiteurRecouvrement, HistoricClient wsHistoriqueRecouvrement, OverviewClient wsDossierPrecRecouvrement, InstitutionClient wsAgenceRecouvrement, IDebiteurService debiteurService, ICompteBancaireService compteBancaireService, IPaysService paysService, IVilleService villeService, IGouvernanteService gouvernanteService, IAdresseService adresseService, IAgenceService agenceService, IHistoriqueRecouvrementService historiqueRecouvrementService) {
        this.wsDebiteurRecouvrement = wsDebiteurRecouvrement;
        this.wsHistoriqueRecouvrement = wsHistoriqueRecouvrement;
        this.wsDossierPrecRecouvrement = wsDossierPrecRecouvrement;
        this.wsAgenceRecouvrement = wsAgenceRecouvrement;
        this.debiteurService = debiteurService;
        this.compteBancaireService = compteBancaireService;
        this.paysService = paysService;
        this.villeService = villeService;
        this.gouvernanteService = gouvernanteService;
        this.adresseService = adresseService;
        this.agenceService = agenceService;
        this.historiqueRecouvrementService = historiqueRecouvrementService;
    }
    @Override
    public List<DossierPrec> getAllDossierPrec(){
        return dossierPrecRepository.findAll();
    }
    @Override
    public List<DossierPrec> getAllDossierPrecByArchiver(Boolean archiver){
        return dossierPrecRepository.findAllByArchiver(archiver);
    }
    @Override
    public void deleteAllDossierPrec(List<DossierPrec> dossiersPrec){
        dossierPrecRepository.deleteAll(dossiersPrec);
    }
    @Override
    public List<DossierPrec> archiverOrRestorerDossierPrec(List<DossierPrec> dossiersPrec, Boolean archivage){
        List<DossierPrec> newDossiersPrec=dossiersPrec.stream().map(d->{d.setArchiver(archivage); return d;}).collect(Collectors.toList());
        return dossierPrecRepository.saveAll(newDossiersPrec);
    }
    @Override
    public void synchronizeWithRecouvrement(){
        List<ClientContentieux> debiteurs=this.wsDebiteurRecouvrement.getListClients();
        List<Overview> dossierPrec=this.wsDossierPrecRecouvrement.getListOverviewFromRecouvrement();
        List<Institution> agencesRecouvrement=this.wsAgenceRecouvrement.getListInstitutionFromRecouvrement();

        debiteurs.stream().forEach(p-> {
            //add debiteur or return debiteur if exist
            Debiteur  debiteur=debiteurService.addDebiteurFromRecouvrement(p);
            //test if a compte debiteur exist or not
            CompteBancaire compteBancaireExist =compteBancaireService.getCompteBancaireDistinctFirstByRib(p.getRib());
            if(compteBancaireExist==null){
                //get dossier precontentieux of this compte from ws overview
                dossierPrec.stream()
                        .filter(dos->dos.getRib().equals(p.getRib()))
                        .forEach(dos->{

                            //add agence
                            String idInstitutionSiege=p.getRib().substring(0,2);
                            String codeInstitution=p.getRib().substring(2,5);
                            agencesRecouvrement.stream()
                                    .filter(
                                            agenceRecouvrement->(agenceRecouvrement.getCodeInstitution().equals(codeInstitution)
                                           && agenceRecouvrement.getIdInstitutionSiege().equals(idInstitutionSiege)))
                                    .forEach(agenceRecouvrement->{
                                        Agence agence=agenceService.addAgenceFromRecouvrement(agenceRecouvrement);
                                        //add compte debiteur
                                        CompteBancaire compteBancaire=new CompteBancaire(p.getRib(),dos.getSolde());
                                        compteBancaire.setDebiteur(debiteur);
                                        compteBancaire=compteBancaireService.addCompteBancaire(compteBancaire);
                                        DossierPrec dosprec=new DossierPrec(dos.getIdDossier(), Utilitaire.toDate(dos.getCreationTime()), Utilitaire.toDate(dos.getDueDate()), dos.getProcInstanceId(), dos.getIdTitulaire(), dos.getNomTitulaire(), dos.getTelTtitulaire(), dos.isIsRegularise(), false);
                                        dosprec.setCompteBancaire(compteBancaire);
                                        dosprec.setAgence(agence);
                                        DossierPrec finalDosprec=dossierPrecRepository.saveAndFlush(dosprec);
                                        System.out.println(dos.getProcInstanceId());
                                        String idProc=dos.getProcInstanceId();
                                        System.out.println(idProc);
                                        List<HistoricSoap> historiquesRecouvrement=wsHistoriqueRecouvrement.getListHistoricDossier(dos.getProcInstanceId());

                                        historiquesRecouvrement.stream().forEach(hist->{
                                            HistoriqueRecouverement historiqueRecouverement=new HistoriqueRecouverement(hist.getName(),Utilitaire.toDate(hist.getCreateTime()), Utilitaire.toDate(hist.getEndTime()), hist.getDescriptionProlongation(), hist.getDurationProlongation(), hist.isLettreSevereEffectue(), hist.isLettreCommercialEffectue(), hist.isAppelEffectue(), hist.isSommationEffectue(), hist.isCommiteEffectue(), hist.getActionnaire(), hist.getIdRapportHn(), hist.getIdProcess(), hist.getIdDossier(), finalDosprec);
                                            historiqueRecouvrementService.addHistoriqueRecouverement(historiqueRecouverement);

                                        });
                                    });
                            //add dossier precontentieux for this compte debiteur
                        });
            }
        });
    }
}
