package com.fr.adaming.service;


import com.fr.adaming.entities.Audience;

import java.util.List;

public interface IAudienceService {

    public Audience saveAudience(Audience audience);
    public List<Audience> getAll();
    public Audience getAudienceById(Long id_audience);
    public Audience updateAudience(Audience audience);
    public void deleteAudience(Long id_audience);
    void deleteAllAudiences(List<Audience> audiences);
}