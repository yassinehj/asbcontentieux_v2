package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.dto.RefInstitutionDto;
import com.fr.adaming.entities.RefInstitution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * @author mbouhdida
 *
 */
public interface IRefInstitutionService {

	/**
	 * recuperer l'institution qui a le flag local est égale true .
	 * <p>
	 *
	 * @return l'objet récupéré.
	 */
	RefInstitution getBanqueLocale();

	/**
	 * recuperer la liste des institutions locaux.
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */

	List<RefInstitution> getAgencesLocales();

	/**
	 * recuperer la liste des institutions locaux par page.
	 * <p>
	 * 
	 * @param p
	 * @return la liste récupérée.
	 */

	Page<RefInstitution> getAgencesLocales(Pageable p);

	/**
	 * mettre a jour la liste des institutions.
	 * <p>
	 * 
	 * @param institution
	 *            liste des institutions
	 */

	void updateInstitution(List<RefInstitution> institution);

	/**
	 * recuperer la liste des institutions regionaux.
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */

	List<RefInstitution> getAgencesRegionale();

	/**
	 * recuperer ll'objet institution par id.
	 * <p>
	 * 
	 * @return l'objet récupéré.
	 */

	RefInstitution getAgenceById(Long id);

	/**
	 * recuperer la liste des institutions locaux par id institution regionale.
	 * <p>
	 * 
	 * @param id
	 *            l'identifiant de l'institution regionale
	 * 
	 * @return la liste récupérée.
	 */

	List<RefInstitution> getAgenceLocaleById(Long id);

	/**
	 * recuperer la liste des codes d'institution.
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */

	List<String> getListOfCodeInstitution();

	/**
	 * mettre a jour une institution.
	 * <p>
	 * 
	 * @param institutions
	 */
	void updateInstitutionLocale(RefInstitutionDto institutions);

	/**
	 * recuperer la liste des institutions.
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */
	List<RefInstitution> getListInstitution();

	/**
	 * recuperer la liste des institutions par code.
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */
	List<RefInstitution> getListInstitutionByCode(String code);
}
