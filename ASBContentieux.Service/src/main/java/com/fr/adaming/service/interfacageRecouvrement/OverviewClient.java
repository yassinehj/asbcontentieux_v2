package com.fr.adaming.service.interfacageRecouvrement;

import com.fr.adaming.soap.config.SoapUtils;
import com.fr.adaming.wsdl.GetOverviewResponse;
import com.fr.adaming.wsdl.Overview;
//import fr.adaming.lawyersoft.lawyer_soft_persistence.dto.OverviewDTO;
//import fr.adaming.lawyersoft.lawyer_soft_service.OverviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class OverviewClient {
	
//	@Autowired
//	OverviewService overviewService;
	
	public List<Overview> getListOverviewFromRecouvrement() {
		List<Overview> listOverViewFromRecouvrement = new ArrayList<>();
		GetOverviewResponse response = SoapUtils.quoteClient.getOverview();
		listOverViewFromRecouvrement = response.getOverview();
		/**
		 * Insérer dans la BD chaque overview
		 */
//		for (Overview overview : listOverViewFromRecouvrement) {
//			/**
//			 * Vérifier l'existence de l'overview
//			 */
//			OverviewDTO ov = overviewService.getOverviewByIdProcess(overview.getProcInstanceId());
//			if(ov.getProcInstanceId() == null) {
//				overviewService.addOverview(overview);
//			}
//		}
		return listOverViewFromRecouvrement;
	}
}
