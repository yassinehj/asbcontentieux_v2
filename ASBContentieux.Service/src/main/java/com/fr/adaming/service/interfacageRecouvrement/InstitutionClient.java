package com.fr.adaming.service.interfacageRecouvrement;

import com.fr.adaming.soap.config.SoapUtils;
import com.fr.adaming.wsdl.GetInstitutionResponse;
import com.fr.adaming.wsdl.Institution;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InstitutionClient {

	public List<Institution> getListInstitutionFromRecouvrement() {
		List<Institution> listInstitutionFromRecouvrement = new ArrayList<>();
		GetInstitutionResponse response = SoapUtils.quoteClient.getInstitution();
		listInstitutionFromRecouvrement = response.getInstitution();
		return listInstitutionFromRecouvrement;
	}


}
