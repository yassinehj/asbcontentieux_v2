package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Adresse;
import com.fr.adaming.entities.Agence;
import com.fr.adaming.entities.Ville;
import com.fr.adaming.repositories.IAgenceRepository;
import com.fr.adaming.service.IAdresseService;
import com.fr.adaming.service.IAgenceService;
import com.fr.adaming.service.IVilleService;
import com.fr.adaming.wsdl.Institution;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class AgenceService implements IAgenceService {
    private IAgenceRepository agenceRepository;
    private IAdresseService adresseService;
    private IVilleService villeService;

    public AgenceService(IAgenceRepository agenceRepository, IAdresseService adresseService, IVilleService villeService) {
        this.agenceRepository = agenceRepository;
        this.adresseService = adresseService;
        this.villeService = villeService;
    }

    @Override
    public Agence getAgenceByCodeInstitution(String codeInstitution){
        return agenceRepository.findDistinctFirstByCodeInstitution(codeInstitution);
    }
    @Override
    public Agence addAgence(Agence agence){
        return agenceRepository.saveAndFlush(agence) ;
    }
    @Override
    public Agence addAgenceFromRecouvrement(Institution agenceRecouvrement){
        Agence agence=agenceRepository.findDistinctFirstByCodeInstitutionAndIdInstitutionSiege(agenceRecouvrement.getCodeInstitution(),agenceRecouvrement.getIdInstitutionSiege());
       if(agence==null){
           agence=new Agence(agenceRecouvrement.getIdInstitutionSiege(),agenceRecouvrement.getCodeInstitution());
           Adresse adresse=adresseService.getAdresseDistinctFirstByNom(agenceRecouvrement.getAdresse());
           if(adresse==null){
               adresse=new Adresse(agenceRecouvrement.getAdresse());
               Ville ville=villeService.getVilleDistinctFirstByNom(agenceRecouvrement.getVille());
               if(ville==null){
                   ville=new Ville(agenceRecouvrement.getVille(),null);
               }
               adresse.setVille(ville);
           }
           agence.setAdresse(adresse);
           agence=addAgence(agence);
       }
       return agence;
    }

	@Override
	public List<Agence> getAllAgence() {
		// TODO Auto-generated method stub
		return agenceRepository.findAll();
	}

	
	
	@Override
	public Agence updateAgence(Agence agence) {
		// TODO Auto-generated method stub
		return agenceRepository.save(agence);
	}

	@Override
	public Agence getByID(Long id) {
		// TODO Auto-generated method stub
		return agenceRepository.findById(id).get();
	}

	@Override
	public void deleteAllAgences(List<Agence> agences) {
		// TODO Auto-generated method stub
		
		agenceRepository.deleteAll();
		
	}

	@Override
	public void deleteAgenceByID(Long id) {
		// TODO Auto-generated method stub
		
		agenceRepository.deleteById(id);
		
	}
    
	
	
	
	
	
	
    
    
}
