package com.fr.adaming.service.interfacageRecouvrement;

import com.fr.adaming.soap.config.SoapUtils;
import com.fr.adaming.wsdl.GetHistoricResponse;
import com.fr.adaming.wsdl.HistoricSoap;
//import fr.adaming.lawyersoft.lawyer_soft_persistence.dto.HistoricSoapDTO;
//import fr.adaming.lawyersoft.lawyer_soft_service.HistoricTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HistoricClient {
	
//	@Autowired
//	HistoricTaskService historicTaskService;
	
	public List<HistoricSoap> getListHistoricDossier(String idProcess){
		List<HistoricSoap> listHistoricOfDataRecouvrement = new ArrayList<>();
		GetHistoricResponse response = SoapUtils.quoteClient.getHistoric(idProcess);
		listHistoricOfDataRecouvrement = response.getHistoricSoap();
		/**
		 * Insérer dans la BD chaque historicTask
		 */
//		for (HistoricSoap historicTask : listHistoricOfDataRecouvrement) {
//			/**
//			 * Vérifier l'existence de l'historicTask
//			 */
//			HistoricSoapDTO historic = historicTaskService.getHistoricTaskById(historicTask.getIdTask()) ;
//			if(historic.getIdTask() == null) {
//				historicTaskService.addHistoricTask(historicTask);
//			}
//		}
		return listHistoricOfDataRecouvrement;
	}
}
