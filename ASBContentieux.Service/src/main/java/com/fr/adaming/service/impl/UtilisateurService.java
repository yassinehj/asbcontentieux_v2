package com.fr.adaming.service.impl;

import java.util.List;

import com.fr.adaming.entities.RefProfession;
import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.repositories.IUtilisateurRepository;
import com.fr.adaming.service.IUtilisateurService;
import com.fr.adaming.utils.EnvoiMailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;


@Service("UtilisateurService")
public class UtilisateurService implements IUtilisateurService {

	@Autowired
	//@Qualifier("IUtilisateurRepository")
	private IUtilisateurRepository utilisateurRepository;

	@Override
	public List<Utilisateur> getAllUsers() {
		return utilisateurRepository.findAll();

	}

	@Override
	public Utilisateur saveUser(Utilisateur user) {
		return utilisateurRepository.save(user);

	}

	@Override
	public Utilisateur getById(Long id) {
		return utilisateurRepository.findById(id).get();

	}

	@Override
	public List<Utilisateur> findNotAffectedUser() {
		return utilisateurRepository.findNotAffectedUser();
	}

	@Override
	public Utilisateur getByLogin(String login) {
		return utilisateurRepository.findByLogin(login);

	}

	@Override
	public Utilisateur findByEmail(String email) {
		return utilisateurRepository.findByEmail(email);
	}

	@Override
	public List<Utilisateur> getLoggedInUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Utilisateur updateUser(Utilisateur user) {
		return utilisateurRepository.save(user);

	}

	@Override
	public void deleteUser(Long idUser) {
		 utilisateurRepository.deleteById(idUser);
	}

	@Override
	public void deleteAllUsers(List<Utilisateur> users) {
		utilisateurRepository.deleteAll(users);
	}

	@Override
	public List<Utilisateur> findByAuthorityName(String authorityName) {
		return utilisateurRepository.findUserByAthorityName(authorityName);

	}

	@Override
	public List<Utilisateur> findAffectedUser() {
		// TODO Auto-generated method stub
		return utilisateurRepository.findByPasswordNotNull();
	}

	@Override
	public Boolean sendEmail(String objetMail,String email,String titreContent,String bodyContent)  {
		try {
			EnvoiMailUtil.sendMail(email,objetMail,titreContent,bodyContent,true);
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Utilisateur> findByRefProf(RefProfession refProfession) {
		// TODO Auto-generated method stub
		return utilisateurRepository.findByRefProfession(refProfession);
	}

}
