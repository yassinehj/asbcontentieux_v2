package com.fr.adaming.service;

import com.fr.adaming.entities.DossierPrec;
import com.fr.adaming.entities.HistoriqueRecouverement;

import java.util.List;

public interface IHistoriqueRecouvrementService {
    HistoriqueRecouverement addHistoriqueRecouverement(HistoriqueRecouverement historiqueRecouverement);

    void DeleteListHistoReco(List<HistoriqueRecouverement> listHistReco);

    List<HistoriqueRecouverement> getAllHistoRecByDosPrec(Long idDossierPrec);
}
