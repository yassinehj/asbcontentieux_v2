package com.fr.adaming.service.impl;

import com.fr.adaming.entities.PieceAudience;
import com.fr.adaming.repositories.IPiecesAudienceRepository;
import com.fr.adaming.service.IPieceAudienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @author nbelhajyahia
 *
 */

@Service("PieceAudienceService")
public class PieceAudienceService implements IPieceAudienceService {

    @Autowired
    @Qualifier("IPiecesAudienceRepository")
    private IPiecesAudienceRepository piecesAudienceRepository;

    @Override
    public PieceAudience saveAudience(PieceAudience pieceAudience) {
        return piecesAudienceRepository.save(pieceAudience);
    }

    @Override
    public List<PieceAudience> getAll() {
        return piecesAudienceRepository.findAll();
    }

    @Override
    public PieceAudience getPieceAudienceById(Long id_piece_audience)
    {
        return piecesAudienceRepository.findById(id_piece_audience).get();
    }

    @Override
    public PieceAudience updateAudience(PieceAudience pieceAudience)
    {
        return piecesAudienceRepository.saveAndFlush(pieceAudience);
    }

    @Override
    public void deletePieceAudience(Long id_piece_audience)
    {
        piecesAudienceRepository.deleteById(id_piece_audience);
    }


}
