package com.fr.adaming.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.fr.adaming.entities.JustifHonoraire;
import com.fr.adaming.repositories.IJustifHonnoraireRepository;
import com.fr.adaming.service.IJustifHonnoraireService;

/**
 * 
 * @author azaaboub
 *
 */

@Service
public class JustifHonnoraireService implements IJustifHonnoraireService {

	@Autowired
	private IJustifHonnoraireRepository ijustifRepository;

	@Override
	public JustifHonoraire addJustifHonnoraire(JustifHonoraire justifHonoraire)
			 {
		// TODO Auto-generated method stub
		return ijustifRepository.save(justifHonoraire);
	}
	
	

	
	
	

}
