package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Adresse;
import com.fr.adaming.repositories.IAdresseRepository;
import com.fr.adaming.service.IAdresseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AdresseService implements IAdresseService {

    @Qualifier("IAdresseRepository")
    @Autowired
    private IAdresseRepository adresseRepository;


    @Override
    public Adresse getAdresseById(Long id){
        return adresseRepository.findById(id).get();
    }
    @Override
    public Adresse getAdresseByNom(String nom){
        return adresseRepository.findByDescription(nom);
    }
    @Override
    public Adresse getAdresseDistinctFirstByNom(String nom){
        return adresseRepository.findDistinctFirstByDescription(nom);
    }
}
