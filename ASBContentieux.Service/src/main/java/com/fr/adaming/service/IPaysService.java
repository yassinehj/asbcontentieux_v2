package com.fr.adaming.service;

import com.fr.adaming.entities.Gouvernorat;
import com.fr.adaming.entities.Pays;

import java.util.List;

/**
 * @author nbelhajyahia
 *
 */

public interface IPaysService {
    public Pays savePays(Pays pays);
    Pays saveWithoutUniquePays(Pays pays);
    public List<Pays> getAll();
    public Pays getPaysById(Long id_pays);
    public Pays getPaysByNom(String nom);
    public Pays findPaysByGouvernorat(Gouvernorat gouvernorat);
    public Pays updatePays(Pays pays);
    public Pays getPaysDistinctFirstByNom(String nom);
    public void deletePays(Long id_pays);

}
