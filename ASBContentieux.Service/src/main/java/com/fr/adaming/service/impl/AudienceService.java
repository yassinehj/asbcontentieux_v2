package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Audience;
import com.fr.adaming.repositories.IAudienceRepository;
import com.fr.adaming.service.IAudienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author nbelhajyahia
 *
 */

@Service
public class AudienceService implements IAudienceService {

    @Autowired
   @Qualifier("IAudienceRepository")
    IAudienceRepository iAudienceRepository;

    @Override
    public Audience saveAudience(Audience audience) {
        return iAudienceRepository.save(audience);
    }

    @Override
    public List<Audience> getAll() {
        return iAudienceRepository.findAll();
    }

    @Override
    public Audience getAudienceById(Long id_audience) {
        return iAudienceRepository.findById(id_audience).get();

    }

    @Override
    public Audience updateAudience(Audience audience) {
        return iAudienceRepository.saveAndFlush(audience);
    }

    @Override
    public void deleteAudience(Long id_audience) {
         iAudienceRepository.deleteById(id_audience);
    }

    @Override
    public void deleteAllAudiences(List<Audience> audiences) {
        iAudienceRepository.deleteAll();
    }
}
