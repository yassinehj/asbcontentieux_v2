package com.fr.adaming.service.interfacageRecouvrement;

import com.fr.adaming.soap.config.SoapUtils;
import com.fr.adaming.wsdl.ClientContentieux;
import com.fr.adaming.wsdl.GetClientsResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientsClient {
	
	public List<ClientContentieux> getListClients() {
		List<ClientContentieux> listClientsFromRecouvrement = new ArrayList<>();
		GetClientsResponse response = SoapUtils.quoteClient.getClients();
		listClientsFromRecouvrement = response.getClientContentieux();
		return listClientsFromRecouvrement;
	}
	
}
