package com.fr.adaming.service;

import com.fr.adaming.entities.CompteBancaire;
import org.springframework.stereotype.Repository;


public interface ICompteBancaireService {
    CompteBancaire getCompteBancaireDistinctFirstByRib(String rib);

    CompteBancaire addCompteBancaire(CompteBancaire compteBancaire);
}
