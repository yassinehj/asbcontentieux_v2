package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.repositories.IUtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fr.adaming.service.IAccountService;

@Service
public class AccountService implements IAccountService {

	@Qualifier("IUtilisateurRepository")
	@Autowired
	IUtilisateurRepository utilisateurRepository;

	@Override
	public Utilisateur findUserByUsername(String login) {
		return utilisateurRepository.findByLogin(login);
	}

	@Override
	public void initReinitialiserPassword(String login) {
		// TODO Auto-generated method stub

	}

	@Override
	public Utilisateur getTheCurrentUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTheCurrentUserLogin() {
		// TODO Auto-generated method stub
		return null;
	}

}
