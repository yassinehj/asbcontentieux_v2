package com.fr.adaming.service;

import com.fr.adaming.entities.Adresse;

public interface IAdresseService {
    Adresse getAdresseById(Long id);

    Adresse getAdresseByNom(String nom);

    Adresse getAdresseDistinctFirstByNom(String nom);
}
