package com.fr.adaming.service;

import com.fr.adaming.entities.Tribunal;

import java.util.List;

public interface ITribunalService  {

    public Tribunal saveTribunal(Tribunal tribunal);
    public List<Tribunal> getAll();
    public Tribunal getTribunalById(Long idTribunal);
    public Tribunal updateTribunal(Tribunal tribunal);
    public void deleteTribunal(Long id_tribunal);
    public void deleteAllTribunals(List<Tribunal> tribunals);
}
