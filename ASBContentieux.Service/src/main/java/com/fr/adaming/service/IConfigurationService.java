package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entities.Configuration;


public interface IConfigurationService {
	public List<Configuration> getConfigurations();
	public void deleteConfiguration (Long idConfiguration);
	public Configuration addConfiguration(Configuration configuration);
	public Configuration updateConfiguration(Configuration configuration);
}
