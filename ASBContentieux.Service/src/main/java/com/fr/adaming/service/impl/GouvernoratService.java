package com.fr.adaming.service.impl;

import com.fr.adaming.entities.Gouvernorat;
import com.fr.adaming.repositories.IGouvernoratRepository;
import com.fr.adaming.service.IGouvernanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author nbelhajyahia
 *
 */
@Service("GouvernoratService")
public class GouvernoratService implements IGouvernanteService {

    @Autowired
    @Qualifier("IGouvernoratRepository")
    private IGouvernoratRepository gouvernoratRepository;

    @Override
    public Gouvernorat saveGouvernorat(Gouvernorat gouvernorat) {
        if (getGouvernoratByNom(gouvernorat.getNom()).size() == 0) {
            return gouvernoratRepository.save(gouvernorat);
        }
        return null;


    }
    @Override
    public Gouvernorat saveWithoutUniqueGouvernorat(Gouvernorat gouvernorat) {
            return gouvernoratRepository.save(gouvernorat);
    }

    @Override
    public List<Gouvernorat> getAll() {
        return gouvernoratRepository.findAll();
    }

    @Override
    public Gouvernorat getGouvernoratById(Long id_gouvernorat) {
        return gouvernoratRepository.findById(id_gouvernorat).get();
    }
    @Override
    public List<Gouvernorat> getGouvernoratByNom(String nom) {
        return gouvernoratRepository.findByNom(nom);
    }
    @Override
    public Gouvernorat getGouvernoratDistinctFirstByNom(String nom) {
        return gouvernoratRepository.findDistinctFirstByNom(nom);
    }

    @Override
    public Gouvernorat updateGouvernorat(Gouvernorat gouvernorat) {
            return gouvernoratRepository.saveAndFlush(gouvernorat);
    }

    @Override
    public void deleteGouvernorat(Long id_gouvernorat) {
        gouvernoratRepository.deleteById(id_gouvernorat);
    }

    @Override
    public List<Gouvernorat> findAllByPays_IdPays(Long idPays) {
        return gouvernoratRepository.findAllByPays_IdPays(idPays);
    }

}
