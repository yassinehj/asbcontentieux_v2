package com.fr.adaming.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.repositories.IUtilisateurRepository;
import com.fr.adaming.service.impl.UtilisateurService;
import com.fr.adaming.utils.LoggerUtil;

/**
 * 
 * @author mabelkaid: Cette classe permet d'effectuer les test unitaires
 *         nécessaires pour la couche service de l'utilisateur. Pour créer un
 *         nouveau test , il faut ajouter l'annotation @Test au dessus de la
 *         méthode implémentée.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UtilisateurServiceTest {

	@Autowired
	private UtilisateurService utilisateurService;

	/**
	 * Injecter un faux utilisateurRepository car dans cette classe de test , on
	 * veut se focaliser uniquement sur la partie utilisateurService
	 */
	@MockBean
	private IUtilisateurRepository utilisateurRepository;

	/**
	 * Tester le service saveUser
	 */
	@Test
	public void testAddUser() {

		LoggerUtil.log("--------------- Constructing User ---------------");
		Utilisateur utilisateur = getUser();

		LoggerUtil.log("--------------- Mocking save() method of IUtilisateurRepository ---------------");
		/**
		 * Ignorer la couche repository et Retourner directement l'utilisateur introduit
		 * lors de l'exécution de la méthode utilisateurRepository.save(utilisateur) ,
		 * car dans ce context on veut tester uniquement le service saveUser().
		 */
		Mockito.when(utilisateurRepository.save(utilisateur)).thenReturn(utilisateur);
		LoggerUtil.log("--------------- Method Mocked ---------------");
		LoggerUtil.log("--------------- Verifying results ---------------");

		/**
		 * Comparer l'utilisateur retourné par le service saveUser() avec l'utilisateur
		 * introduit.
		 */
		assertEquals(utilisateurService.saveUser(utilisateur), utilisateur);
	}

	/**
	 * Tester le service getUserById
	 */
	@Test
	public void testGetUserById() {

		LoggerUtil.log("--------------- Constructing User ---------------");
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setId(1L);
		utilisateur.setLogin("aminebk");
		utilisateur.setPassword("1234");
		utilisateur.setFirstName("Amine");
		utilisateur.setLastName("Belkaied");
		utilisateur.setEmail("aminebelkaied@gmail.com");
		utilisateur.setActivated(true);
		LoggerUtil.log("--------------- Mocking findById(id) method of IUtilisateurRepository ---------------");
		/**
		 * Convertir l'utilisateur en Optional car le typr de retour de la méthode
		 * findById fourni par spring data est Optional<T> Puis , retourner directement
		 * l'utilisateur introduit lors de l'exécution de la méthode
		 * utilisateurRepository.findById(id) , car dans ce context on veut tester
		 * uniquement le service getById(id).
		 */
		Optional<Utilisateur> utilisateurOptional = Optional.of(utilisateur);
		Mockito.when(utilisateurRepository.findById(Mockito.anyLong())).thenReturn(utilisateurOptional);
		LoggerUtil.log("--------------- Method Mocked ---------------");
		LoggerUtil.log("--------------- Verifying results ---------------");

		/** assertNotNull(utilisateurService.getById(1L)); **/
		/**
		 * Comparer l'utilisateur retourné par le service getById(id) avec l'utilisateur
		 * introduit.
		 */
		assertThat(utilisateurService.getById(4L)).isEqualTo(utilisateur);
	}

	/**
	 * Tester le service getAllUsers
	 */
	@Test
	public void testGetAllUsers() {
		LoggerUtil.log("--------------- Constructing Users ---------------");
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setId(1L);
		utilisateur1.setLogin("aminebk");
		utilisateur1.setPassword("1234");
		utilisateur1.setFirstName("Amine");
		utilisateur1.setLastName("Belkaied");
		utilisateur1.setEmail("aminebelkaied@gmail.com");
		utilisateur1.setActivated(true);

		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setId(1L);
		utilisateur2.setLogin("yassinehj");
		utilisateur2.setPassword("1234");
		utilisateur2.setFirstName("Yassine");
		utilisateur2.setLastName("Hajji");
		utilisateur2.setEmail("yassinehajji@gmail.com");
		utilisateur2.setActivated(true);

		/**
		 * Remplir la liste des utilisateurs
		 */
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		utilisateurs.add(utilisateur1);
		utilisateurs.add(utilisateur2);

		LoggerUtil.log("--------------- Mocking findAll() method of IUtilisateurRepository ---------------");
		/**
		 * Ignorer la couche repository et retourner directement la liste des
		 * utilisateurs fournie lors de l'exécution de la méthode
		 * utilisateurRepository.findAll() , car dans ce context on veut tester
		 * uniquement le service getAllUsers().
		 */

		Mockito.when(utilisateurRepository.findAll()).thenReturn(utilisateurs);
		LoggerUtil.log("--------------- Method Mocked ---------------");
		LoggerUtil.log("--------------- Verifying results ---------------");
		/**
		 * Comparer la liste des utilisateurs retourné par le service getAllUsers() avec
		 * la liste des utilisateurs introduite.
		 */
		assertThat(utilisateurService.getAllUsers()).isEqualTo(utilisateurs);
	}

	/**
	 * Tester le service findByEmail
	 */
	@Test
	public void testGetUserByEmail() {

		LoggerUtil.log("--------------- Constructing User ---------------");
		Utilisateur utilisateur = getUser();
		LoggerUtil.log("--------------- Mocking findByEmail(email) method of IUtilisateurRepository ---------------");
		/**
		 * Retourner directement l'utilisateur introduit lors de l'exécution de la
		 * méthode utilisateurRepository.findByEmail(email) , car dans ce context on
		 * veut tester uniquement le service findByEmail(email).
		 */
		Mockito.when(utilisateurRepository.findByEmail("aminebelkaied@gmail.com")).thenReturn(utilisateur);
		LoggerUtil.log("--------------- Method Mocked ---------------");
		LoggerUtil.log("--------------- Verifying results ---------------");

		/**
		 * Comparer l'utilisateur retourné par le service getById(id) avec l'utilisateur
		 * introduit.
		 */
		assertThat(utilisateurService.findByEmail("aminebelkaied@gmail.com")).isEqualTo(utilisateur);
	}

	private Utilisateur getUser() {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setId(1L);
		utilisateur.setLogin("aminebk");
		utilisateur.setPassword("1234");
		utilisateur.setFirstName("Amine");
		utilisateur.setLastName("Belkaied");
		utilisateur.setEmail("aminebelkaied@gmail.com");
		utilisateur.setActivated(true);
		return utilisateur;
	}
}
