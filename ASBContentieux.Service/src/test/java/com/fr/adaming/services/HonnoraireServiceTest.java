//package com.fr.adaming.services;
//
//
//import com.fr.adaming.entities.Honnoraire;
//import com.fr.adaming.repositories.IHonnoraireRepository;
//import com.fr.adaming.service.impl.HonnoraireService;
//import com.fr.adaming.utils.LoggerUtil;
//import org.junit.Assert;
//import static org.junit.Assert.assertEquals;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import javax.xml.ws.soap.MTOM;
//import java.util.Date;
//
///**
// * @author azaaboub    Cette classe permet d'effectuer les test unitaires
// *           nécessaires pour la couche service de l'honnoraire. Pour créer un
// *           nouveau test , il faut ajouter l'annotation @Test au dessus de la
// *           méthode implémentée.
// */
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class HonnoraireServiceTest {
//
//    @Autowired
//    private HonnoraireService honnoraireService;
//
//
//    /**
//     * Injecter un faux honnoraireRepository car dans cette classe de test , on
//     * veut se focaliser uniquement sur la partie honnoraireService
//     */
//
//    @MockBean
//    private IHonnoraireRepository honnoraireRepository;
//
//      @Test
//     public void testAddHonnoraire(){
//
//         LoggerUtil.log("-------constructing honnoraire-------");
//         Honnoraire honnoraire = getHonnoraires();
//         LoggerUtil.log("-------------Mocking save() method of IHonnoraireRepository-------------");
//
//
//         /**
//          * Ignorer la couche repository et Retourner directement l'utilisateur introduit
//          * lors de l'exécution de la méthode utilisateurRepository.save(utilisateur) ,
//          * car dans ce context on veut tester uniquement le service saveUser().
//          */
//
//
//         Mockito.when(honnoraireRepository.save(honnoraire)).thenReturn(honnoraire);
//         LoggerUtil.log("--------------- Method Mocked ---------------");
//         LoggerUtil.log("--------------- Verifying results ---------------");
//
//         /**
//          * Comparer l'honnoraire retourné par le service saveHonnoraire() avec l'honnoraire
//          * introduit.
//          */
//
//
//
//         assertEquals(honnoraireService.saveHonnoraire(honnoraire),honnoraire);
//
//
//
//
//     }
//
//
//
//
//
//
//
//    private Honnoraire getHonnoraires(){
//
//        Honnoraire honnoraire = new Honnoraire();
//
//        honnoraire.setDateHonnoraire(new Date());
//        honnoraire.setMontant(2500);
//        honnoraire.setTypePaiement("cheque");
//        honnoraire.setTypeOperation("virement");
//        honnoraire.setPaye(true);
//        honnoraire.setTitre("reglement");
//
//
//        return  honnoraire;
//
//    }
//
//
//}
