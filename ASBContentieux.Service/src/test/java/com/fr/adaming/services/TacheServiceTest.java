//package com.fr.adaming.services;
//
//import com.fr.adaming.entities.Tache;
//import com.fr.adaming.enumeration.State;
//import com.fr.adaming.repositories.ITacheRepository;
//import com.fr.adaming.service.impl.TacheService;
//import com.fr.adaming.utils.LoggerUtil;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.Date;
//
//import static org.junit.Assert.assertEquals;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class TacheServiceTest {
//    @Autowired
//    private TacheService tacheService;
//    @MockBean
//    private ITacheRepository iTacheRepository;
//    @Test
//    public void testAddTache() {
//
//        LoggerUtil.log("--------------- Constructing task ---------------");
//        Tache tache=getTache();
//        Mockito.when(iTacheRepository.save(tache)).thenReturn(tache);
//        LoggerUtil.log("--------------- Method Mocked ---------------");
//
//        LoggerUtil.log("--------------- Verifying results ---------------");
//        assertEquals(tacheService.saveTache(tache), tache);
//}
//    private Tache getTache() {
//        Tache tache = new Tache();
//        tache.setIdTache(1l);
//        tache.setTitre("taskone");
//        tache.setDescription("describe this task");
//        tache.setDateDebut(new Date());
//        tache.setDateFin(new Date());
//        tache.setDureeNotif(3.5f);
//        tache.setStatut("satut");
//        tache.setEtat(State.EN_COURS);
//        return tache;
//    }
//}
