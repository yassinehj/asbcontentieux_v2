package com.fr.adaming.rest;

import com.fr.adaming.entities.Tache;
import com.fr.adaming.service.ITacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nbelhajyahia
 *  <p>
 *  REST controller pour la gestion des taches. <strong><u>CrossOrigin
 *  is allowed.</u></strong> Class URL : <strong>/api</strong>
 */

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class TacheRessource {

    @Autowired
    private ITacheService tacheService;

    @PostMapping(value="addTache",produces = MediaType.APPLICATION_JSON_VALUE)
    public Tache saveTache(@RequestBody Tache tache,@RequestHeader(value="Authorization") String headerStr) {
        return tacheService.saveTache(tache);
    }
    @GetMapping(value="getTaches",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Tache> getAll(@RequestHeader(value="Authorization") String headerStr) {
        return tacheService.getAll();
    }
    @GetMapping(value="getTache/{id_tache}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Tache getTacheById(@PathVariable Long id_tache ,@RequestHeader(value="Authorization") String headerStr) {
        return tacheService.getTacheById(id_tache);
    }
    @PutMapping(value="updateTache",produces = MediaType.APPLICATION_JSON_VALUE)
    public Tache updateTache(@RequestBody Tache tache,@RequestHeader(value="Authorization") String headerStr) {
        return tacheService.updateTache(tache);
    }
    @DeleteMapping(value="deleteTache/{id_tache}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteTache(@PathVariable Long id_tache){
        tacheService.deleteTache(id_tache);
    }
    @DeleteMapping(value="deleteTaches/{idTaches}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteTache(@PathVariable List<Long> idTaches){
    	
       idTaches.stream().forEach(idTache -> {
    	   tacheService.deleteTache(idTache);
       });
    }
}
