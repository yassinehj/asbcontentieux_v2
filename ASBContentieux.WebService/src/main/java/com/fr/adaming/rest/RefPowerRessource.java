package com.fr.adaming.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.dto.RefPowerDto;
import com.fr.adaming.entities.RefPower;
import com.fr.adaming.service.IRefPowerService;

/**
 * @author mbouhdida
 *         <p>
 * 
 *         REST controller pour la gestion des pouvoirs. <strong><u>CrossOrigin
 *         is allowed.</u></strong> Class URL : <strong>/api</strong>
 */
@RestController
@RequestMapping("/api")
@CrossOrigin
public class RefPowerRessource {

	@Autowired
	@Qualifier("RefPowerService")
	private IRefPowerService refPowerService;

	/**
	 * ajouter la liste des refPower . <br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/saveRefPower</strong> ;
	 * <p>
	 *
	 * @param refPowers
	 *            la liste des RefPower à ajouter
	 * 
	 */
	@RequestMapping(value = "/saveRefPower", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void save(@RequestBody List<RefPowerDto> refPowers) {
		refPowerService.save(refPowers);
	}

	/**
	 * recuperer la liste des refPower par role. <br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/getrefPowerByRole</strong> ;
	 * <p>
	 *
	 * @param nom
	 *            le nom du role
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of refPower in
	 *         body
	 */
	@RequestMapping(value = "/getrefPowerByRole", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RefPowerDto> getRefPowerByNom(@RequestBody String nom) {
		return refPowerService.getRefPowerByRole(nom);
	}

	/**
	 * mettre a jour la liste des refPower . <br>
	 * HTTP Method : <strong>PUT</strong> <br>
	 * HTTP URL : <strong>/updaterefPower</strong> ;
	 * <p>
	 *
	 * @param refPowers
	 *            la liste des refPower à modifier
	 * 
	 */
	@RequestMapping(value = "/updaterefPower", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateRefPowerByNom(@RequestBody List<RefPowerDto> refPowers) {
		refPowerService.update(refPowers);
	}

	/**
	 * recuperer un refPower par nom de l'ecran et login de l'utilisateur connecté
	 * <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getPowerByUserAndMenu</strong> ;
	 * <p>
	 *
	 * @param menu
	 * @param login
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of refPower in
	 *         body
	 */
	@RequestMapping(value = "/getPowerByUserAndMenu", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public RefPower getPowerByUserAndMenu(@RequestParam("menu") String menu, @RequestParam("login") String login) {
		RefPower refPower = new RefPower();
		refPower = refPowerService.getPowerByUserAndMenu(menu, login);
		return refPower;
	}
}
