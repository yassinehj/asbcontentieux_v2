package com.fr.adaming.rest;

import com.fr.adaming.entities.Gouvernorat;
import com.fr.adaming.entities.Pays;
import com.fr.adaming.entities.Ville;
import com.fr.adaming.service.IGouvernanteService;
import com.fr.adaming.service.IPaysService;
import com.fr.adaming.service.IVilleService;
import com.fr.adaming.service.impl.PaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nbelhajyahia
 *          REST controller pour la gestion des Ressources.
 *           <strong><u>CrossOrigin is allowed.</u></strong> Class URL :
 *         <strong>/api</strong>
 *
 */
@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class GouvernoratRessource {

    @Autowired
    @Qualifier("GouvernoratService")
    private IGouvernanteService gouvernanteService;

    @Autowired
    private IPaysService paysService;
    @Autowired
    private IVilleService villeService;

    @PostMapping(value="/addGouvernorat/{idPays}",  produces = MediaType.APPLICATION_JSON_VALUE)
    public Gouvernorat saveGouvernorat(@RequestBody Gouvernorat gouvernorat,@PathVariable (name = "idPays", required = false)Long idPays,@RequestHeader(value="Authorization") String headerStr) {
        Pays pays =paysService.getPaysById(idPays);
        if (pays != null) {
            gouvernorat.setPays(pays);
        }
        return gouvernanteService.saveGouvernorat(gouvernorat);
    }
    @GetMapping(value="/getGouvernorat" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Gouvernorat> getAll() {
        return gouvernanteService.getAll();
    }


    @GetMapping(value="/getGouvernoratById/{idGouvernorat}" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Gouvernorat getGouvernoratById(@PathVariable  Long idGouvernorat,@RequestHeader(value="Authorization") String headerStr) {
        return gouvernanteService.getGouvernoratById(idGouvernorat);
    }
    @PutMapping(value="/updateGouvernorat/{idPays}",  produces = MediaType.APPLICATION_JSON_VALUE)
    public Gouvernorat updateGouvernorat(@RequestBody Gouvernorat gouvernorat,@PathVariable (name = "idPays", required = false)Long idPays,@RequestHeader(value="Authorization") String headerStr) {
        Pays pays =paysService.getPaysById(idPays);
        if (pays != null) {
            gouvernorat.setPays(pays);
        }
        return gouvernanteService.updateGouvernorat(gouvernorat);
    }
    @DeleteMapping(value="/deleteGouvernorat/{id_gouvernorat}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteGouvernorat(@PathVariable Long id_gouvernorat,@RequestHeader(value="Authorization") String headerStr) {

            Gouvernorat gouvernorat= gouvernanteService.getGouvernoratById(id_gouvernorat);
            if(gouvernorat!=null) {
                if (gouvernorat.getVilles() != null){
                    List<Ville> villes = gouvernorat.getVilles();
                    villes.stream().forEach(p->{
                        villeService.deleteVille(p.getIdVille());
                    });
                }

                gouvernanteService.deleteGouvernorat(id_gouvernorat);
            }

    }
    @GetMapping(value="/getGouvernoratByPays/{idPays}" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Gouvernorat> findAllByPays_IdPays(@PathVariable Long idPays,@RequestHeader(value="Authorization") String headerStr) {
        return gouvernanteService.findAllByPays_IdPays(idPays);
    }
}
