package com.fr.adaming.rest;

import java.util.ArrayList;
import java.util.List;

import com.fr.adaming.entities.RefModule;
import com.fr.adaming.service.IRefModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author mbouhdida
 *         <p>
 * 
 *         REST controller pour la gestion des modules. <strong><u>CrossOrigin
 *         is allowed.</u></strong> Class URL : <strong>/api</strong>
 */
@RestController
@RequestMapping("/api")
@CrossOrigin
public class RefModuleRessource {

	@Autowired
	@Qualifier("RefModuleService")
	private IRefModuleService refModuleService;

	/**
	 * recuperer la liste des modules <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/modules</strong> ;
	 * <p>
	 *
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of module in
	 *         body
	 */
	@RequestMapping(value = "/modules", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RefModule> getAllModules() {
		return refModuleService.getAllModules();
	}

	/**
	 * recuperer la liste des modules par role<br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/getAllModulesByRoles</strong> ;
	 * <p>
	 *
	 * @param roles
	 * @return the ResponseEntity with status 200 (OK) and the list of module in
	 *         body
	 */
	@RequestMapping(value = "/getAllModulesByRoles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RefModule> getAllModulesByRoles(@RequestBody List<String> roles) {
		List<RefModule> modules = new ArrayList<RefModule>();
		modules = refModuleService.getListModuleByRoles(roles);
		return modules;
	}
}
