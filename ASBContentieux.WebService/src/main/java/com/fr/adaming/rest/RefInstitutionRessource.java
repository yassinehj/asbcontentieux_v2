package com.fr.adaming.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.fr.adaming.dto.RefInstitutionDto;
import com.fr.adaming.entities.RefInstitution;
import com.fr.adaming.rest.util.PaginationUtil;
import com.fr.adaming.service.IRefInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author mbouhdida
 *         <p>
 * 
 *         REST controller pour la gestion des institutions.
 *         <strong><u>CrossOrigin is allowed.</u></strong> Class URL :
 *         <strong>/api</strong>
 */
@RestController
@RequestMapping("/api")
@CrossOrigin
public class RefInstitutionRessource {

	@Autowired
	@Qualifier("RefInstitutionService")
	private IRefInstitutionService institutionService;

	/**
	 * recuperer la liste des agences locaux avec pagination<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAgencesLocalesPages</strong> ;
	 * <p>
	 *
	 * @param search
	 * @param p
	 *            paginaton
	 * @return the ResponseEntity with status 200 (OK) and the list of institution
	 *         in body
	 */
	@GetMapping(value = "/getAgencesLocalesPages", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<RefInstitution>> getAgencesLocales(Pageable p, @RequestParam("searchtext") String search)
			throws URISyntaxException {
		Page<RefInstitution> pageInstitutions = institutionService.getAgencesLocales(p);
		HttpHeaders httpHeaders = PaginationUtil.generatePaginationHttpHeaders(pageInstitutions, "/api/agencesLocales");
		return new ResponseEntity<>(pageInstitutions.getContent(), httpHeaders, HttpStatus.OK);
	}

	/**
	 * recuperer la liste des agences locaux <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAgencesLocales</strong> ;
	 * <p>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of institution
	 *         in body
	 */
	@GetMapping(value = "/getAgencesLocales", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<RefInstitution>> getAgencesLocales() throws URISyntaxException {
		List<RefInstitution> institutions = new ArrayList<>();
		institutions = institutionService.getAgencesLocales();
		HttpHeaders httpHeaders = new HttpHeaders();
		List<String> exposedHeaders = new ArrayList<>();
		exposedHeaders.add("*");
		httpHeaders.setAccessControlExposeHeaders(exposedHeaders);
		if (institutions != null) {
			httpHeaders.add("X-Total-Count", "" + institutions.size());
		} else {
			httpHeaders.add("X-Total-Count", "" + 0);

		}
		return new ResponseEntity<>(institutions, httpHeaders, HttpStatus.OK);
	}

	/**
	 * mettre a jour liste des institutions <br>
	 * HTTP Method : <strong>PUT</strong> <br>
	 * HTTP URL : <strong>/updateAgence</strong> ;
	 * <p>
	 * 
	 * @param refInstitution
	 *            liste des institutions à modifier
	 */
	@PutMapping(value = "/updateAgence", produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateRefInstitution(@RequestBody List<RefInstitution> refInstitution) {
		institutionService.updateInstitution(refInstitution);
	}

	/**
	 * recuperer la liste des institutions regionaux <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAgencesLocalesRegionales</strong> ;
	 * <p>
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of institution
	 *         in body
	 */
	@GetMapping(value = "/getAgencesLocalesRegionales", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<RefInstitution>> getAgencesLocalesRegionales() throws URISyntaxException {
		List<RefInstitution> institutions = institutionService.getAgencesRegionale();
		HttpHeaders httpHeaders = new HttpHeaders();
		List<String> exposedHeaders = new ArrayList<>();
		exposedHeaders.add("*");
		httpHeaders.setAccessControlExposeHeaders(exposedHeaders);
		httpHeaders.add("X-Total-Count", "" + institutions.size());
		return new ResponseEntity<>(institutions, httpHeaders, HttpStatus.OK);
	}

	/**
	 * recuperer une institutions par id <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAgenceById/{idInstitution}</strong> ;
	 * <p>
	 * 
	 * @param idInstitution
	 *            l'identifiant de l'institution
	 * @return the ResponseEntity with status 200 (OK) and the institution in body
	 */
	@GetMapping(value = "/getAgenceById/{idInstitution}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RefInstitution> getAgenceById(@PathVariable Long idInstitution) throws URISyntaxException {
		RefInstitution institution = institutionService.getAgenceById(idInstitution);
		HttpHeaders httpHeaders = new HttpHeaders();
		return new ResponseEntity<>(institution, httpHeaders, HttpStatus.OK);
	}

	/**
	 * recuperer une institutions local par id <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAgenceLocaleById/{idInstitution}</strong> ;
	 * <p>
	 * 
	 * @param idInstitution
	 *            l'identifiant de l'institution
	 * @return the ResponseEntity with status 200 (OK) and the institution in body
	 */
	@GetMapping(value = "/getAgenceLocaleById/{idInstitution}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<RefInstitution>> getAgenceLocaleById(@PathVariable Long idInstitution)
			throws URISyntaxException {
		List<RefInstitution> institutions = institutionService.getAgenceLocaleById(idInstitution);
		HttpHeaders httpHeaders = new HttpHeaders();
		List<String> exposedHeaders = new ArrayList<>();
		exposedHeaders.add("*");
		httpHeaders.setAccessControlExposeHeaders(exposedHeaders);
		httpHeaders.add("X-Total-Count", "" + institutions.size());
		return new ResponseEntity<>(institutions, httpHeaders, HttpStatus.OK);
	}

	/**
	 * mettre a jour une institution <br>
	 * HTTP Method : <strong>PUT</strong> <br>
	 * HTTP URL : <strong>/updateAgenceLocales</strong> ;
	 * <p>
	 * 
	 * @param refInstitution
	 *            l'institution à modifier
	 */
	@PutMapping(value = "/updateAgenceLocales", produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateAgenceLocales(@RequestBody RefInstitutionDto refInstitution) {
		institutionService.updateInstitutionLocale(refInstitution);
	}

	/**
	 * recuperer une institution par son code <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getInstitutionByCode</strong> ;
	 * <p>
	 * 
	 * @param codeAgence
	 *            le code de l'institution
	 * @return the ResponseEntity with status 200 (OK) and the list of institution
	 *         in body
	 */
	@RequestMapping(value = "/getInstitutionByCode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RefInstitution> getInstitutionByCode(
			@RequestParam(value = "codeAgence", required = true) String codeAgence) {
		List<RefInstitution> refInstitutions = new ArrayList<RefInstitution>();
		refInstitutions = institutionService.getListInstitutionByCode(codeAgence);
		return refInstitutions;
	}

	/**
	 * recuperer une institution locale <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/agences</strong> ;
	 * <p>
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of institution
	 *         in body
	 */
	@GetMapping(value = "/agences", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RefInstitution> getAgences() {
		List<RefInstitution> institutions = institutionService.getAgencesLocales();
		return institutions;
	}
}
