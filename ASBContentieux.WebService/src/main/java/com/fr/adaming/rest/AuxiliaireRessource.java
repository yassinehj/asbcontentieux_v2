package com.fr.adaming.rest;

import com.fr.adaming.entities.Auxiliaire;

import com.fr.adaming.service.IAuxiliaireService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nbelhajyahia
 *  <p>
 *  REST controller pour la gestion des auxilieres. <strong><u>CrossOrigin
 *  is allowed.</u></strong> Class URL : <strong>/api</strong>
 */

@RestController
@RequestMapping("/api")
@CrossOrigin

public class AuxiliaireRessource {

    @Autowired
    @Qualifier("AuxiliaireService")
    private IAuxiliaireService auxiliaireService;

    @PostMapping(value="/addAuxiliaire",produces = MediaType.APPLICATION_JSON_VALUE)
    public Auxiliaire saveAuxiliaire(@RequestBody Auxiliaire auxiliaire) {
        return auxiliaireService.saveAuxiliaire(auxiliaire);
    }

    @GetMapping(value="/getAuxiliaires",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Auxiliaire> getAll() {
        return auxiliaireService.getAll();
    }
    @GetMapping(value="/getAuxiliaire/{id_auxiliaire}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Auxiliaire getAuxiliaireById(@PathVariable  Long id_auxiliaire ) {
        return auxiliaireService.getAuxiliaireById(id_auxiliaire);
    }
    @PutMapping(value="/updateAuxiliaire",produces = MediaType.APPLICATION_JSON_VALUE)
    public Auxiliaire updateAuxiliaire(@RequestBody Auxiliaire auxiliaire) {
        return auxiliaireService.updateAuxiliaire(auxiliaire);
    }
    @DeleteMapping(value="/deleteAuxiliaire/{id_auxiliaire}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAuxiliaire(@PathVariable Long id_auxiliaire ) {
        auxiliaireService.deleteAuxiliaire(id_auxiliaire);
    }
    
    @DeleteMapping(value="deleteAllAuxiliaires/{idAuxiliaires}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAuxiliaire(@PathVariable List<Long> idAuxiliaires){
    	
    	idAuxiliaires.stream().forEach(idAuxiliaire -> {
    		auxiliaireService.deleteAuxiliaire(idAuxiliaire);
       });
    }
}
