package com.fr.adaming.rest;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.fr.adaming.entities.JustifHonoraire;
import com.fr.adaming.service.IJustifHonnoraireService;
import com.fr.adaming.utils.Utilitaire;

/**
 * 
 * @author azaaboub
 *
 */

@RestController
@RequestMapping("/files")
@CrossOrigin
public class JustifHonnoraireResource {
	
	
	@Autowired
	private IJustifHonnoraireService iJustifHonnoraireService;
	
	
	@PostMapping(value = "/ajoutJustifHonnoraire")
	public  JustifHonoraire ajoutJustif(@RequestHeader(value="Authorization")String headerStr, @RequestParam(value = "file1", required = true) MultipartFile file, @Valid JustifHonoraire justifHonnoraire) {
	
	
		 justifHonnoraire.setIdAlfresco(Utilitaire.addFileAlfresco(file, "test", null));
	
		return iJustifHonnoraireService.addJustifHonnoraire(justifHonnoraire);
	}
	
	
	
	
		
	
	
		/*private String genererNomPDF(JustifHonoraire justifHonnoraire) {
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmm");
			int dotPosition = justifHonnoraire.getFileName().lastIndexOf('.');
			String extension = "";
			if (dotPosition != -1) {
				extension = justifHonnoraire.getFileName().substring(dotPosition);
			}
			return  " " + justifHonnoraire.getFileType() + " " + justifHonnoraire.getFileName() + " - "
					+ df.format(new Date()) + extension;
		}*/
		
		
		

}
