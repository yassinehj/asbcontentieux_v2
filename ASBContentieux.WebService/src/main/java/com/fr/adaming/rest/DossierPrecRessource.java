package com.fr.adaming.rest;

import com.fr.adaming.entities.Debiteur;
import com.fr.adaming.entities.DossierPrec;
import com.fr.adaming.entities.HistoriqueRecouverement;
import com.fr.adaming.service.IDebiteurService;
import com.fr.adaming.service.IDossierPrecService;
import com.fr.adaming.service.IHistoriqueRecouvrementService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author yhajji
 *  <p>
 *  REST controller pour la gestion des Dossier pré-contentieux. <strong><u>CrossOrigin
 *  is allowed.</u></strong> Class URL : <strong>/api</strong>
 */

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class DossierPrecRessource {
    private IHistoriqueRecouvrementService historiqueRecouvrementService;
    private IDossierPrecService dossierPrecService;

    public DossierPrecRessource(IHistoriqueRecouvrementService historiqueRecouvrementService, IDossierPrecService dossierPrecService) {
        this.historiqueRecouvrementService = historiqueRecouvrementService;
        this.dossierPrecService = dossierPrecService;
    }

    @PostMapping(value = "/synchronizeWithRecouvrement",  produces = MediaType.APPLICATION_JSON_VALUE)
    public void synchronizeWithRecouvrement() {
        dossierPrecService.synchronizeWithRecouvrement();
    }

    @GetMapping(value = "/getDossiersPrec" ,  produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DossierPrec> getListDossiesPrec(){
        return dossierPrecService.getAllDossierPrec();
    }
    @GetMapping(value = "/getDossiersPrec/{archiver}" ,  produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DossierPrec> getListDossiesPrecByArchiver(@PathVariable("archiver") Boolean archiver){
        return dossierPrecService.getAllDossierPrecByArchiver(archiver);
    }
    @RequestMapping(value = "/deleteAll",  produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAllDossiersPrec(@RequestBody List<DossierPrec> dossiersPrec){
        dossiersPrec.stream().forEach(dos->{
            List<HistoriqueRecouverement> listHistReco=historiqueRecouvrementService.getAllHistoRecByDosPrec(dos.getIdDossierPrec());
            if(!listHistReco.isEmpty()){
                historiqueRecouvrementService.DeleteListHistoReco(listHistReco);
            }
        });
        dossierPrecService.deleteAllDossierPrec(dossiersPrec);
    }
    @PutMapping(value = "/archiverRestorerDosPrec/{archivage}",  produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DossierPrec> archiverRestorerDosPrec(@RequestBody List<DossierPrec> dossiersPrec,@PathVariable("archivage") Boolean archivage){
        return dossierPrecService.archiverOrRestorerDossierPrec(dossiersPrec,archivage);
    }
}
