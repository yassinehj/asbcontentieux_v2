package com.fr.adaming.rest;

import java.util.Date;
import java.util.List;

import com.fr.adaming.utils.AlfrescoOpenCmis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.fr.adaming.entities.Dossier;
import com.fr.adaming.service.IDossierService;

/**
 * REST controller for Dossier.
 * @author azaaboub
 *
 */


@RestController
@RequestMapping("/api")
@CrossOrigin
public class DossierResource {
	
	private final Logger log = LoggerFactory.getLogger(DossierResource.class);
	
	@Autowired
	private IDossierService iDossierService;
	
	 @PostMapping(value = "/savedossier")
	public Dossier saveDossier( @RequestBody Dossier dossier) {
        dossier.setDateCreation(new Date());
	    return iDossierService.addDossier(dossier);
	}
	
	@RequestMapping(value = "/getDossiers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Dossier> getAllDosiers(){
		
		List<Dossier> dossier = iDossierService.getDossiers();
		return dossier;
		
	}
	
	@RequestMapping(value = "/deleteDosById/{idDossier}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteDossier(@PathVariable Long idDossier) {
		
		log.info("REST request to delete Dossier : {}", idDossier);
		iDossierService.deleteDossier(idDossier);
		
	}
	
	
	
	@RequestMapping(value = "/updateDossier", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateDossier(@RequestBody Dossier dossier) {
		
		dossier.setDateCreation(new Date());
		iDossierService.updateDossier(dossier);
		
	}
	@RequestMapping(value = "/getDosById/{idDossier}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Dossier getDossierById(@PathVariable Long idDossier){
		return  iDossierService.getDossierByID(idDossier);
	}
	@DeleteMapping(value = "/deleteListDossies/{listDossiersId}")
	public void deleteAllPieces(@PathVariable List<Long> listDossiersId) {

		listDossiersId.stream().forEach(idDos -> {
			iDossierService.deleteDossier(idDos);
		});
	}



}
