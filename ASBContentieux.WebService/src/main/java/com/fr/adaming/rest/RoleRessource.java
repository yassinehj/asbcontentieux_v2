package com.fr.adaming.rest;

import java.util.List;

import com.fr.adaming.entities.Role;
import com.fr.adaming.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author mbouhdida
 *         <p>
 * 
 *         REST controller pour la gestion des roles. <strong><u>CrossOrigin is
 *         allowed.</u></strong> Class URL : <strong>/api</strong>
 */
@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class RoleRessource {

	@Autowired
	@Qualifier("RoleService")
	private IRoleService authorityService;

	/**
	 * recuperer tous les roles. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/roles</strong> ;
	 * <p>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of roles in body
	 */
	@RequestMapping(value = "/roles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Role> getAllRoles() {
		return authorityService.findAll();
	}

	/**
	 * recuperer la liste des roles par nom du module. <br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/rolesByModule</strong> ;
	 * <p>
	 *
	 * @param nomModule
	 *            le nom du module
	 * @return the ResponseEntity with status 200 (OK) and the list of roles in body
	 */
	@RequestMapping(value = "/rolesByModule", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Role> getAllRolesByModule(@RequestBody String nomModule) {

		return authorityService.findAuthorityByModule(nomModule);
	}

	/**
	 * supprimer un role par son nom. <br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/deleteRoles</strong> ;
	 * <p>
	 *
	 * @param nom
	 *            le nom du role
	 */
	@RequestMapping(value = "/deleteRoles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteRole(@RequestBody String nom) {
		authorityService.deleteRole(nom);
	}

}
