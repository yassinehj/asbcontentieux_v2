package com.fr.adaming.rest;

import com.fr.adaming.entities.PieceAudience;
import com.fr.adaming.service.IPieceAudienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author nbelhajyahia
 *  <p>
 *  REST controller pour la gestion des piecesAudience. <strong><u>CrossOrigin
 *  is allowed.</u></strong> Class URL : <strong>/api</strong>
 */

@RestController
@RequestMapping("/api")
@CrossOrigin
public class PieceAudienceRessource {

    @Autowired
    private IPieceAudienceService pieceAudienceService;

    @PostMapping(value="/addPieceAudience",produces = MediaType.APPLICATION_JSON_VALUE)
    public PieceAudience saveAudience(PieceAudience pieceAudience,@RequestHeader(value="Authorization") String headerStr) {
        return pieceAudienceService.saveAudience(pieceAudience);
    }
    @GetMapping(value="/getAllPiecesAudience",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PieceAudience> getAll(@RequestHeader(value="Authorization") String headerStr) {
        return pieceAudienceService.getAll();
    }
    @GetMapping(value="getPieceAudience/{id_piece_audience}",produces = MediaType.APPLICATION_JSON_VALUE)
    public PieceAudience getPieceAudienceById(@PathVariable Long id_piece_audience, @RequestHeader(value="Authorization") String headerStr) {
        return pieceAudienceService.getPieceAudienceById(id_piece_audience);
    }
    @PutMapping(value="/updatePieceAudience",produces = MediaType.APPLICATION_JSON_VALUE)
    public PieceAudience updateAudience(@RequestBody PieceAudience pieceAudience,@RequestHeader(value="Authorization") String headerStr) {
        return pieceAudienceService.updateAudience(pieceAudience);
    }
    @DeleteMapping(value="/deletePieceAudience/{id_piece_audience}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deletePieceAudience(@PathVariable Long id_piece_audience,@RequestHeader(value="Authorization") String headerStr) {
        pieceAudienceService.deletePieceAudience(id_piece_audience);
    }
}
