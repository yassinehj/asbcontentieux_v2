package com.fr.adaming.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.entities.Agence;
import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.service.IAgenceService;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class AgenceResource {
	
	@Autowired
	private IAgenceService agenceService;
	
	 @GetMapping(value="/getAllAgences" ,produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<Agence> getAll(){
		return agenceService.getAllAgence();
		 
		 
	 }
	 
	 @RequestMapping(value = "/updateAgences", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Agence> updateAgence(@RequestBody Agence agence) {
			
			agenceService.updateAgence(agence);
			return new ResponseEntity<Agence>(agence, HttpStatus.OK);
		}
	
	
	
	 @GetMapping(value = "/getAgencesById/{id}")
		public ResponseEntity<Agence> getAgenceById(@PathVariable Long id) {
			
			Agence agence = new Agence();
			 
			try {
				agence = agenceService.getByID(id);
				return new ResponseEntity<Agence>(agence, HttpStatus.OK);
			} catch (Exception e) {
			}
			return null;
		}
	
	
	
	 
	 @RequestMapping(value = "/deleteAgence/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<HttpStatus> deleteAgence(@PathVariable Long id) {
			try {
				
				agenceService.deleteAgenceByID(id);
				return new ResponseEntity<HttpStatus>(HttpStatus.OK);
			}
			catch (Exception e){
			}
			return null;
		}

	 
	 
	 @DeleteMapping(value = "/deleteAgences/{idAgences}")
		public ResponseEntity<HttpStatus> deleteUser(@PathVariable List<Long> idAgences) {
			try {
				
				idAgences.stream().forEach(idAgence -> {
					agenceService.deleteAgenceByID(idAgence);
		        });
				return new ResponseEntity<HttpStatus>(HttpStatus.OK);
			}
			catch (Exception e){
			}
			return null;
		}

}
