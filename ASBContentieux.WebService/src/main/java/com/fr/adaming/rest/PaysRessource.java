package com.fr.adaming.rest;

import com.fr.adaming.entities.Gouvernorat;
import com.fr.adaming.entities.Pays;
import com.fr.adaming.service.IGouvernanteService;
import com.fr.adaming.service.IPaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nbelhajyahia
 *  <p>
 *  REST controller pour la gestion des pays. <strong><u>CrossOrigin
 *  is allowed.</u></strong> Class URL : <strong>/api</strong>
 */

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class PaysRessource {

    @Autowired
    @Qualifier("PaysService")
    private IPaysService paysService;

    @Autowired
    private IGouvernanteService gouvernoratService;

    @PostMapping(value="/addPays", produces = MediaType.APPLICATION_JSON_VALUE)
    public Pays savePays(@RequestBody Pays pays)
    {
        return paysService.savePays(pays);
    }


    /**
     * Get all the Pays. <br>
     * HTTP Method : <strong>GET</strong> <br>
     * HTTP URL : <strong>/getAllPays</strong> ;
     * <p>
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pays in body
     */
    @GetMapping(value="/getPays",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Pays> getAll() {
        return paysService.getAll();
    }

    @GetMapping(value="/getPays/{idPays}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Pays getPaysById(@PathVariable Long idPays) {
        return paysService.getPaysById(idPays);
    }

    @PutMapping(value="/updatePays",produces = MediaType.APPLICATION_JSON_VALUE)
    public Pays updatePays(@RequestBody Pays pays) {

        return paysService.updatePays(pays);
    }

    @DeleteMapping(value="deletePays/{id_pays}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deletePays(@PathVariable Long id_pays) {

        Pays pays = paysService.getPaysById(id_pays);
        if(pays!=null){
            if(pays.getGouvernorat()!=null){
                List<Gouvernorat> gouvernorat = pays.getGouvernorat();
                gouvernorat.stream().forEach(g->{
                    gouvernoratService.deleteGouvernorat(g.getIdGouvernorat());
                });


            }
            paysService.deletePays(id_pays);
        }

    }

    @PostMapping(value="/getPaysByGouvernorat",produces = MediaType.APPLICATION_JSON_VALUE)
    public Pays getPaysByGouvernorat(@RequestBody Gouvernorat gouvernorat) {
        return paysService.findPaysByGouvernorat(gouvernorat);
    }
}
