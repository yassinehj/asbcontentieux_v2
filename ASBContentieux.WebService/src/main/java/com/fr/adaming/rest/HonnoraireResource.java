package com.fr.adaming.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.entities.Honnoraire;
import com.fr.adaming.service.IHonnoraireService;

/**
 *  REST controller for Honnoraire.
 * @author azaaboub
 *
 */



@RestController
@RequestMapping("/api")
@CrossOrigin
public class HonnoraireResource {
	
	private final Logger log = LoggerFactory.getLogger(HonnoraireResource.class);

	@Autowired
	private IHonnoraireService ihonnoraireService;
	
	@RequestMapping(value = "/savehonnoraires", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveHonnoraire(@RequestBody Honnoraire honnoraire) {
		
		ihonnoraireService.saveHonnoraire(honnoraire);
	}
	
	
	
	@RequestMapping(value = "/honnoraire", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Honnoraire> getAllHonnoraire(){
		List<Honnoraire> honnoraires = ihonnoraireService.getHonnoraires();
		return honnoraires;
		
		
	}
	
	@RequestMapping(value = "/honnoraire/delete/{idHonnoraire}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteHonnoraire(@PathVariable Long idHonnoraire) {
		
		log.info("REST request to delete Honnoraire : {}", idHonnoraire);
		ihonnoraireService.deleteHonnoraire(idHonnoraire);
	}
	
	
	
	
	@RequestMapping(value = "/updateHonnoraire", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateHonnoraire(@RequestBody Honnoraire honnoraire) {
		
		ihonnoraireService.updateHonnoraire(honnoraire);
	}
	
	
	
	
	
	
}
