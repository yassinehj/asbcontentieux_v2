package com.fr.adaming.rest;

import com.fr.adaming.entities.Audience;
import com.fr.adaming.service.IAudienceService;
import com.fr.adaming.service.IProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nbelhajyahia
 *  <p>
 *  REST controller pour la gestion des audiences. <strong><u>CrossOrigin
 *  is allowed.</u></strong> Class URL : <strong>/api</strong>
 */

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class AudienceRessource {

    @Autowired
    private IAudienceService audienceService;

    /** author nbelhajyahia
     * 
     * @param audience
     * @return
     */
    @PostMapping(value = "/saveAudience",  produces = MediaType.APPLICATION_JSON_VALUE)
    public Audience saveAudience(@RequestBody Audience audience , @RequestHeader(value="Authorization")String headerStr) {
        return audienceService.saveAudience(audience);
    }
    @GetMapping(value = "/getAllAudiences", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Audience> getAllAudiences(@RequestHeader(value="Authorization")String headerStr) {
        return audienceService.getAll();
    }
    @GetMapping(value = "/getAudienceById/{id_audience}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Audience getAudienceById(@PathVariable Long id_audience ) {
        return audienceService.getAudienceById(id_audience);
    }
    @PutMapping(value = "/updateAudience", produces = MediaType.APPLICATION_JSON_VALUE)
    public Audience updateAudience(@RequestBody Audience audience) {
        return audienceService.updateAudience(audience);
    }

    @DeleteMapping(value = "/deleteAudience/{id_audience}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAudience(@PathVariable Long id_audience) {
         audienceService.deleteAudience(id_audience);
    }

    @DeleteMapping(value = "/deleteAudiences/{idAudiences}")
    public ResponseEntity<HttpStatus> deleteAudience(@PathVariable List<Long> idAudiences) {
        try {

            idAudiences.stream().forEach(idAudience -> {
                audienceService.deleteAudience(idAudience);
            });
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        }
        catch (Exception e){
        }
        return null;
    }
}
