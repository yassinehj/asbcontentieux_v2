package com.fr.adaming.rest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fr.adaming.dto.UtilisateurDto;
import com.fr.adaming.entities.RefProfession;
import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.service.IRefProfessionService;
import com.fr.adaming.service.IUtilisateurService;
import com.fr.adaming.utils.Utilitaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;


/**
 * @author mbouhdida
 *         <p>
 * 
 *         REST controller pour la gestion des utilisateurs.
 *         <strong><u>CrossOrigin is allowed.</u></strong> Class URL :
 *         <strong>/api</strong>
 */
@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class UtilisateurRessource {

	@Autowired
	@Qualifier("UtilisateurService")
	private IUtilisateurService userService;

	@Autowired
	@Qualifier("RefProfessionService")
	private IRefProfessionService refProfessionService;

	private BCryptPasswordEncoder bCryptPasswordEncoder;

	/**
	 * Get all the users. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAllUsers</strong> ;
	 * <p>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of users in body
	 */
	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
	public ResponseEntity<List<Utilisateur>> getAllUsers() {
		List<Utilisateur> users = new ArrayList<>();
		users = userService.getAllUsers();
		return new ResponseEntity<>(users, HttpStatus.OK);
	}

	/**
	 * modifier un utilisateur. <br>
	 * HTTP Method : <strong>PUT</strong> <br>
	 * HTTP URL : <strong>/updateUser</strong> ;
	 * <p>
	 *
	 * @param user
	 *            l'utilisateur à modifier
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         user.
	 */
	@RequestMapping(value = "/updateUser", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Utilisateur> updateUser(@RequestBody Utilisateur user) {
		userService.updateUser(user);
		return new ResponseEntity<Utilisateur>(user, HttpStatus.OK);
	}

	/**
	 * Ajouter un nouveau utilisateur. <br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/createUser</strong> ;
	 * <p>
	 *
	 * @param user
	 *            l'utilisateur à ajouter
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         user.
	 */
	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public ResponseEntity<Utilisateur> createUser(@RequestBody Utilisateur user) {
		List<Utilisateur> users = new ArrayList<>();
		users = userService.getAllUsers();
		if (users.stream().filter(x -> x.getLogin() != null)
				.anyMatch(e -> e.getLogin().equals(user.getLogin()))) {
			return new ResponseEntity<Utilisateur>(new Utilisateur(), HttpStatus.NOT_ACCEPTABLE);
		} else {
			userService.saveUser(user);
			return new ResponseEntity<Utilisateur>(user, HttpStatus.OK);
		}
	}

	/**
	 * Supprimer un utilisateur. <br>
	 * HTTP Method : <strong>DELETE</strong> <br>
	 * HTTP URL : <strong>/deleteUser</strong> ;
	 * <p>
	 *
	 * @param idUser
	 *            l'id de l'utilisateur à supprimer
	 * @return the ResponseEntity with status 201 (Deleted)
	 */
	@RequestMapping(value = "/deleteUser/{idUser}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HttpStatus> deleteUser(@PathVariable Long idUser) {
		try {
			userService.deleteUser(idUser);
			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
		}
		catch (Exception e){
		}
		return null;
	}

	/**
	 * Supprimer liste des utilisateurs. <br>
	 * HTTP Method : <strong>DELETE</strong> <br>
	 * HTTP URL : <strong>/deleteUsers</strong> ;
	 * <p>
	 *
	 * @param idUsers
	 *            les id des utilisateurs à supprimer
	 * @return the ResponseEntity with status 201 (Deleted)
	 */
	@DeleteMapping(value = "/deleteUsers/{idUsers}")
	public ResponseEntity<HttpStatus> deleteUser(@PathVariable List<Long> idUsers) {
		try {
			idUsers.stream().forEach(idUser -> {
				userService.deleteUser(idUser);
			});
			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
		}
		catch (Exception e){
		}
		return null;
	}

	/**
	 * Récupérer un utilisateur par son id. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getUserById/{idUser}</strong> ;
	 * <p>
	 *
	 * @param idUser
	 *            l'identifiant de l'utilisateur
	 * @return the ResponseEntity with status 200 (Ok) and with body the user.
	 */
	@RequestMapping(value = "/getUserById/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Utilisateur> getUserById(@PathVariable Long idUser) {
		Utilisateur user = new Utilisateur();
		try {
			user = userService.getById(idUser);
			return new ResponseEntity<Utilisateur>(user, HttpStatus.OK);
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * recuperer la liste des utilisateurs non affecté. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/notAffectedUser</strong> ;
	 * <p>
	 *
	 * @return the ResponseEntity with status 200 (Ok) and the list of users in body
	 */
	@RequestMapping(value = "/notAffectedUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UtilisateurDto>> findNotAffectedUser() {
		List<Utilisateur> user = new ArrayList<Utilisateur>();
		List<UtilisateurDto> userDto = new ArrayList<UtilisateurDto>();
		try {
			user = userService.findNotAffectedUser();
			for (Utilisateur utilisateur : user) {
				UtilisateurDto utilisateurDto = new UtilisateurDto(utilisateur);
				// utilisateurDto.setIdProfession(utilisateur.getRefProfession().getIdRefProfession());
				userDto.add(utilisateurDto);
			}
			return new ResponseEntity<List<UtilisateurDto>>(userDto, HttpStatus.OK);
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * recuperer la liste des utilisateurs affecté. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/AffectedUser</strong> ;
	 * <p>
	 *
	 * @return the ResponseEntity with status 200 (Ok) and the list of users in body
	 */
	@RequestMapping(value = "/AffectedUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UtilisateurDto>> findAffectedUser() {
		List<Utilisateur> user = new ArrayList<Utilisateur>();
		List<UtilisateurDto> userDto = new ArrayList<UtilisateurDto>();
		try {
			user = userService.findAffectedUser();
			for (Utilisateur utilisateur : user) {
				UtilisateurDto utilisateurDto = new UtilisateurDto(utilisateur);
				// utilisateurDto.setIdProfession(utilisateur.getRefProfession().getIdRefProfession());
				userDto.add(utilisateurDto);
			}
			return new ResponseEntity<List<UtilisateurDto>>(userDto, HttpStatus.OK);
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * recuperer un utilisateurs par son login. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/userByLogin</strong> ;
	 * <p>
	 * 
	 * @param login
	 *
	 * @return the ResponseEntity with status 200 (Ok) and the user in body
	 */
	@GetMapping(value = "/userByLogin/{login}")
	public Utilisateur getByLogin(@PathVariable String login) {
		return userService.getByLogin(login);
	}

	/**
	 * recuperer la liste des utilisateurs par sa profession. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/userProf/{refProf}</strong> ;
	 * <p>
	 * 
	 * @param refProf
	 *
	 * @return the ResponseEntity with status 200 (Ok) and the list of users in body
	 */
	@RequestMapping(value = "/userProf/{refProf}")
	public ResponseEntity<List<Utilisateur>> findByRefProf(@PathVariable RefProfession refProf) {
		return new ResponseEntity<>(userService.findByRefProf(refProf), HttpStatus.OK);
	}
	/**
	 * recuperer un utilisateurs par son login. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/userByLogin</strong> ;
	 * <p>
	 *
	 * @param email
	 *
	 * @return the ResponseEntity with status 200 (Ok) and the user in body
	 */
	@GetMapping(value = "/userByEmail/{email}")
	public Utilisateur findByEmail(@PathVariable String email) {
		return userService.findByEmail(email);
	}
	/**
	 * envoyer un email avec l'url de reset password <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/userByLogin</strong> ;
	 * <p>
	 *
	 * @param email
	 *
	 * @return the ResponseEntity with status 200 (Ok) and the user in body
	 */
	@GetMapping(value = "/sendLinkResetPassword/{email}/{link}")
	public Boolean sendLinkResetPassword(@PathVariable String email,@PathVariable String link) {
		Utilisateur user= userService.findByEmail(email);
		if(user == null){
			return false;
		}
		String emailCrypted = null;
		try {
			emailCrypted=Utilitaire.encrypt(email);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String linkReset="http://"+link+"/#/reset-password/"+emailCrypted;
		
		return userService.sendEmail("Password reset",email,"Bonjour "+user.getFirstName()+" "+user.getLastName(),linkReset);
	}
	/**
	 * envoyer un email avec l'url de reset password <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/userByLogin</strong> ;
	 * <p>
	 *
	 * @param emailCrypter
	 * @param password
	 *
	 * @return the ResponseEntity with status 200 (Ok) and the user in body
	 */
	@GetMapping(value = "/resetPassword/{emailCrypter}/{password}")
	public Boolean resetPassword(@PathVariable String emailCrypter,@PathVariable String password) {
		String emaildecrpter = null;
		try {
			emaildecrpter = Utilitaire.decrypt(emailCrypter);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		Utilisateur user=userService.findByEmail(emaildecrpter);
		if (user != null) {
			user.setPassword(bCryptPasswordEncoder.encode(password));
			user.setResetDate(new Date());
			userService.updateUser(user);
			return true;
		}

		return null;
	}

	/**
	 * recuperer la liste des professions. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAllProfessions</strong> ;
	 * <p>
	 *
	 * @return the ResponseEntity with status 200 (Ok) and the list of refProfessions in body
	 */
	@RequestMapping(value = "/getAllProfessions", method = RequestMethod.GET)
	public List<RefProfession> getAllProfessions() {
		return refProfessionService.findAll();
	}

	/**
	 * recuperer la liste des utilisateurs par idprofession. <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/usersByProf/{idRefPro}</strong> ;
	 * <p>
	 *
	 * @param idRefPro
	 * @return the ResponseEntity with status 200 (Ok) and the list of users in body
	 */
	@RequestMapping(value = "/usersByProf/{idRefPro}", method = RequestMethod.GET)
	public List<Utilisateur> getUsersByIdRefProfession(@PathVariable Long idRefPro) {
		RefProfession refPro = refProfessionService.getById(idRefPro);
		return userService.findByRefProf(refPro);
	}

	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String getCurrentUser() {
		Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		String username = loggedInUser.getName();
		return username;
	}
}
