package com.fr.adaming.rest;

import java.util.ArrayList;
import java.util.List;

import com.fr.adaming.dto.MenuDto;
import com.fr.adaming.dto.RefPowerDto;
import com.fr.adaming.entities.RefMenu;
import com.fr.adaming.entities.RefModule;
import com.fr.adaming.service.IRefMenuService;
import com.fr.adaming.service.IRefModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author mbouhdida
 *         <p>
 * 
 *         REST controller pour la gestion des menus. <strong><u>CrossOrigin is
 *         allowed.</u></strong> Class URL : <strong>/api</strong>
 */
@RestController
@RequestMapping("/api")
@CrossOrigin
public class RefMenuRessource {

	@Autowired
	@Qualifier("RefMenuService")
	private IRefMenuService refMenuService;

	@Autowired
	@Qualifier("RefModuleService")
	private IRefModuleService refModuleService;

	/**
	 * recuperer la liste des menus par nom du module<br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/getAllMenusByModule</strong> ;
	 * <p>
	 *
	 * @param nom
	 *            nom du module
	 * @return the ResponseEntity with status 200 (OK) and the list of menu in body
	 */
	@RequestMapping(value = "/getAllMenusByModule", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RefPowerDto> getAllMenusByModule(@RequestParam(name = "nomModule") String nom) {
		List<RefMenu> refMenus = new ArrayList<RefMenu>();
		List<RefPowerDto> refPowersDto = new ArrayList<RefPowerDto>();
		RefModule refModule = refModuleService.findByNomModule(nom);
		if (refModule != null) {
			refMenus = refMenuService.findByRefModule(refModule.getIdModule());
		}
		for (RefMenu menu : refMenus) {
			RefPowerDto refPowerDto = new RefPowerDto(menu.getIdMenu(), menu.getNomMenu(), menu.getNomMenuEn());
			refPowersDto.add(refPowerDto);
		}
		return refPowersDto;
	}

	/**
	 * recuperer la liste des menus par nom du module et utilisateur connecté<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAllMenusByModuleAndUSer</strong> ;
	 * <p>
	 *
	 * @param nomModule
	 *            nom du module
	 * @param login
	 *            login de l'utilisateur
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of menu in body
	 */
	@RequestMapping(value = "/getAllMenusByModuleAndUSer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MenuDto> getAllMenusByModuleAndUSer(@RequestParam(name = "nomModule") String nomModule,
													@RequestParam(name = "login") String login) {
		List<MenuDto> refMenus = new ArrayList<MenuDto>();
		refMenus = refMenuService.getEcransByUserAndModule(nomModule, login);
		return refMenus;
	}

	/**
	 * recuperer la liste des menus pere par nom du module et utilisateur
	 * connecté<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getAllMenusPereByModuleAndUSer</strong> ;
	 * <p>
	 *
	 * @param nomModule
	 *            nom du module
	 * @param login
	 *            login de l'utilisateur
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of menu in body
	 */
	@RequestMapping(value = "/getAllMenusPereByModuleAndUSer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RefMenu> getAllMenusPereByModuleAndUSer(@RequestParam(name = "nomModule") String nomModule,
			@RequestParam(name = "login") String login) {
		List<RefMenu> refMenus = new ArrayList<RefMenu>();
		refMenus = refMenuService.getEcranPereByUserAndModule(login, nomModule);
		return refMenus;
	}

	/**
	 * verifier si l'utilisateur connecté a le droit de visualiser l'ecran
	 * connecté<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/hasAuthority</strong> ;
	 * <p>
	 *
	 * @param nomMenu
	 *            nom du menu
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of menu in body
	 */
	@RequestMapping(value = "/hasAuthority", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean hasAuthority(@RequestParam(name = "nomMenu") String nomMenu,
			@RequestParam(name = "login") String login) {
		return refMenuService.getEcranByUser(login, nomMenu);
	}
}
