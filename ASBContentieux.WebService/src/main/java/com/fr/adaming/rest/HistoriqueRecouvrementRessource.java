package com.fr.adaming.rest;

import com.fr.adaming.entities.HistoriqueRecouverement;
import com.fr.adaming.service.IHistoriqueRecouvrementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nbelhajyahia
 *          REST controller pour la gestion des Ressources.
 *           <strong><u>CrossOrigin is allowed.</u></strong> Class URL :
 *         <strong>/api</strong>
 *
 */
@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class HistoriqueRecouvrementRessource {
    @Autowired
    private IHistoriqueRecouvrementService historiqueRecouvrementService;
    @GetMapping(value="/getListHistRecoByIdDosPrec/{idDossierPrec}" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<HistoriqueRecouverement> getListHistRecoByIdDosPrec(@PathVariable Long idDossierPrec) {
        return historiqueRecouvrementService.getAllHistoRecByDosPrec(idDossierPrec);
    }
}
