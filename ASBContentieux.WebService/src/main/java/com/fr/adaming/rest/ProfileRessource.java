package com.fr.adaming.rest;

import java.util.ArrayList;
import java.util.List;

import com.fr.adaming.dto.ProfilDto;
import com.fr.adaming.entities.Profile;
import com.fr.adaming.entities.RefInstitution;
import com.fr.adaming.entities.Role;
import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.service.IProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author mbouhdida
 *         <p>
 * 
 *         REST controller pour la gestion des profils. <strong><u>CrossOrigin
 *         is allowed.</u></strong> Class URL : <strong>/api</strong>
 */
@RestController
@RequestMapping("/api")
@CrossOrigin
public class ProfileRessource {

	@Autowired
	@Qualifier("ProfileService")
	private IProfileService profileService;

	/**
	 * recuperer la liste des profiles <br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/findAllProfiles</strong> ;
	 * <p>
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of profiles in
	 *         body
	 */
	@RequestMapping(value = "/findAllProfiles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProfilDto>> findAllProfiles() {
		List<ProfilDto> profiles = new ArrayList<>();
		profiles = profileService.getAllProfiles();
		if (profiles.isEmpty()) {
			return new ResponseEntity<>(profiles, HttpStatus.OK);
		}
		return new ResponseEntity<>(profiles, HttpStatus.OK);
	}

	/**
	 * recuperer un profil par son nom<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>//findProfilesByNom/{nomProfile}</strong> ;
	 * <p>
	 * 
	 * @param nomProfile
	 *            nom du profil
	 * @return the ResponseEntity with status 200 (OK) and the profiles in body
	 */
	@RequestMapping(value = "/findProfilesByNom/{nomProfile}", method = RequestMethod.GET)
	public ResponseEntity<Profile> findByNom(@RequestParam String nomProfile) {
		Profile profile = new Profile();
		profile = profileService.findByNomProfile(nomProfile);
		if (profile != null) {
			return new ResponseEntity<>(profile, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Profile>(profile, HttpStatus.OK);
	}

	/**
	 * recuperer la liste des profile regoupé par nom<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/profiles/groupBy</strong> ;
	 * <p>
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the profiles in body
	 */
	@RequestMapping(value = "/profiles/groupBy", method = RequestMethod.GET)
	public ResponseEntity<List<Profile>> getAllGroupBy() {
		return new ResponseEntity<>(profileService.getProfilesGroupByNom(), HttpStatus.OK);
	}

	/**
	 * recuperer la liste des roles par nom du profil<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getRolesByNomProfile/{nomProfile}</strong> ;
	 * <p>
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the liste of roles in
	 *         body
	 */
	@RequestMapping(value = "/getRolesByNomProfile/{nomProfile}", method = RequestMethod.GET)
	public ResponseEntity<List<Role>> getRolesByNomProfile(@PathVariable String nomProfile) {
		return new ResponseEntity<>(profileService.getListRoleByNomProfile(nomProfile), HttpStatus.OK);
	}

	/**
	 * recuperer la liste des profils distinct<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/profilesDistinct/{nomProfile}</strong> ;
	 * <p>
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the liste of profiles in
	 *         body
	 */
	@RequestMapping(value = "/profilesDistinct", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Profile>> findAllProfilesDistinct() {
		List<Profile> profiles = new ArrayList<>();
		profiles = profileService.getListProfilesDistinct();
		if (profiles.isEmpty()) {
			return new ResponseEntity<>(profiles, HttpStatus.OK);
		}
		return new ResponseEntity<>(profiles, HttpStatus.OK);
	}

	/**
	 * ajouter un profil<br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/saveprofiles</strong> ;
	 * <p>
	 * 
	 * @param profils
	 *            liste des profils à ajouter
	 */
	@RequestMapping(value = "/saveprofiles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveProfiles(@RequestBody List<ProfilDto> profils) {
		profileService.addProfils(profils);
	}

	/**
	 * recuperer liste des profils par nom<br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/findProfileByNom</strong> ;
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profile
	 * @return the ResponseEntity with status 200 (OK) and the liste of profiles in
	 *         body
	 */
	@RequestMapping(value = "/findProfileByNom", method = RequestMethod.POST)
	public ResponseEntity<List<ProfilDto>> findProfileByNom(@RequestBody String nomProfile) {
		List<ProfilDto> profilsDto = new ArrayList<ProfilDto>();
		profilsDto = profileService.findProfileByNom(nomProfile);
		return new ResponseEntity<List<ProfilDto>>(profilsDto, HttpStatus.OK);
	}

	/**
	 * recuperer liste des utilisateurs par nom de profil<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getListUsersByNomProfile/{nomProfile}</strong> ;
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profile
	 * @return the ResponseEntity with status 200 (OK) and the liste of users in
	 *         body
	 */
	@RequestMapping(value = "/getListUsersByNomProfile/{nomProfile}", method = RequestMethod.GET)
	public ResponseEntity<List<Utilisateur>> getListUsersByNomProfile(@PathVariable String nomProfile) {
		List<Utilisateur> users = new ArrayList<>();
		if (nomProfile != null) {
			users = profileService.getListUserByNomProfile(nomProfile);
			return new ResponseEntity<>(users, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
	}

	/**
	 * recuperer liste des institutions par nom de profil<br>
	 * HTTP Method : <strong>GET</strong> <br>
	 * HTTP URL : <strong>/getListInstitutionByNomProfile/{nomProfile}</strong> ;
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profile
	 * @return the ResponseEntity with status 200 (OK) and the liste of institutions
	 *         in body
	 */
	@RequestMapping(value = "/getListInstitutionByNomProfile/{nomProfile}", method = RequestMethod.GET)
	public ResponseEntity<List<RefInstitution>> getListInstitutionByNomProfile(@PathVariable String nomProfile) {
		List<RefInstitution> refInst = new ArrayList<>();
		if (nomProfile != null) {
			refInst = profileService.getListInstitutionByNomProfile(nomProfile);
			return new ResponseEntity<>(refInst, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
	}

	/**
	 * mettre a jour un profil<br>
	 * HTTP Method : <strong>PUT</strong> <br>
	 * HTTP URL : <strong>/updateprofile</strong> ;
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profile
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/updateprofile", method = RequestMethod.PUT)
	public ResponseEntity<Void> updateProfile(@RequestBody List<ProfilDto> nomProfile) {
		profileService.updateProfile(nomProfile);
		return new ResponseEntity<>(HttpStatus.OK);

	}

	/**
	 * supprimer un profil par son nom<br>
	 * HTTP Method : <strong>POST</strong> <br>
	 * HTTP URL : <strong>/deleteprofile</strong> ;
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profile * @return the ResponseEntity with status 200
	 *            (OK)
	 */
	@RequestMapping(value = "/deleteprofile", method = RequestMethod.POST)
	public ResponseEntity<Void> deleteProfile(@RequestBody String nomProfile) {
		profileService.deleteProfile(nomProfile);
		return new ResponseEntity<>(HttpStatus.OK);

	}
}
