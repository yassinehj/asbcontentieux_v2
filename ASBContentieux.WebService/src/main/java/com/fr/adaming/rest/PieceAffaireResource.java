package com.fr.adaming.rest;


import com.fr.adaming.entities.Dossier;
import com.fr.adaming.entities.PieceAffaire;
import com.fr.adaming.service.IDossierService;
import com.fr.adaming.service.IPieceAffaireService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpHeaders;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author azaaboub
 */

@RestController
@RequestMapping("/file")
@CrossOrigin("*")
public class PieceAffaireResource {

    private final Logger log = LoggerFactory.getLogger(PieceAffaireResource.class);
    private static final String EXTERNAL_FILE_PATH = "/opt/tomcat-8085/archive/test/";

    @Autowired
    private IPieceAffaireService iPieceAffaireService;
    
   

    @PostMapping(value = "/ajoutPieceAffaire")
    public PieceAffaire addPiece(@RequestParam("file") MultipartFile file , @RequestParam(value="idDossier" , required= false) Long idDossier){

        PieceAffaire pieceAffaire = new PieceAffaire();
        pieceAffaire.setFileName(file.getOriginalFilename());
        
        pieceAffaire.setSize(file.getSize());
        
        pieceAffaire.setDateCreation(new Date());
        
        pieceAffaire.setNom(file.getName());
        
       
        

        if (idDossier != null) {
            return iPieceAffaireService.addPieceAffaireInDossier(pieceAffaire, idDossier, file);
        }


        return iPieceAffaireService.addPieceAffaire(pieceAffaire,file);

    }


    


    @RequestMapping(value = "/getPiecesAffaires", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        public List<PieceAffaire> getAllPieceAffaire(){

        List<PieceAffaire> pieceAffaires = iPieceAffaireService.getAllPieceAffaire();

        return  pieceAffaires;

        }

    @RequestMapping(value = "/pieceAffaire/delete/{idPieceAffaire}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
        public void deletePieceAffaire(@PathVariable Long idPieceAffaire){

        iPieceAffaireService.deletePieceAffaire(idPieceAffaire);

    }

     @RequestMapping(value = "/updatePieceAffaire", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
     public PieceAffaire updatePieceAffaire(@RequestParam(value="idDossier" , required= false) Long idDossier,@RequestBody PieceAffaire pieceAffaire ){
    	 
    	
    	 

      return  iPieceAffaireService.updatePieceAffaire(pieceAffaire, idDossier);


}


    @RequestMapping(value = "/GetpieceAffaireByID/{idPieceAffaire}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
      public PieceAffaire getByIdPieceAffaire(@PathVariable Long idPieceAffaire){


        PieceAffaire  pieceAffaire = iPieceAffaireService.getPieceAffaireByID(idPieceAffaire);

        return pieceAffaire;

      }

    
    @DeleteMapping(value = "/deleteAllPieces/{listPieceAffaireId}")
	public void deleteAllPieces(@PathVariable List<Long> listPieceAffaireId) {

    	listPieceAffaireId.stream().forEach(idPiece -> {
            iPieceAffaireService.deletePieceAffaire(idPiece);
        });
	}
    
    
   
    
    @RequestMapping(path = "/download/{fileName}", method = RequestMethod.GET)
    public ResponseEntity<Resource> download(HttpServletRequest request, HttpServletResponse response,@PathVariable("fileName") String fileName) throws IOException {
    	
    	File file = new File(EXTERNAL_FILE_PATH +fileName);
    	
    	System.out.println("++++++++++++"+file);
    	HttpHeaders headers = new HttpHeaders(); 
    	headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        headers.add("Content-Disposition", "filename=" + fileName);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        Path path = Paths.get(file.getAbsolutePath());
        
        System.out.println("-----------"+path);
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                //text/pdf; charset=utf-8"
                .body(resource);
    }
    

    
    @RequestMapping(value = "/getPiecesAffairesByDossier/{idDossier}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PieceAffaire> getAllPieceAffaireByDossier(@PathVariable("idDossier") Long idDossier  ){

    	
    	
    List<PieceAffaire> pieceAffaires = iPieceAffaireService.findAllPieceAffaireByDossier(idDossier);

    return  pieceAffaires;

    }

    
    
    

}
