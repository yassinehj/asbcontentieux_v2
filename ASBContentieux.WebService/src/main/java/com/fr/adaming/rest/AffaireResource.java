package com.fr.adaming.rest;

import java.util.Date;
import java.util.List;

import com.fr.adaming.service.interfacageRecouvrement.ClientsClient;
import com.fr.adaming.wsdl.ClientContentieux;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.entities.Affaire;
import com.fr.adaming.service.IAffaireService;


/**
 * REST controller for Affaire.
 * @author azaaboub
 *
 */

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AffaireResource {
	
	private final Logger log = LoggerFactory.getLogger(AffaireResource.class);
	
	@Autowired
	private IAffaireService iaffaireService;
	@Autowired
	private ClientsClient agenceBancaire;

	@RequestMapping(value = "/clientContentieux", method = RequestMethod.GET)
	public List<ClientContentieux> getAllAgences(){
		List<ClientContentieux> clientContentieux = agenceBancaire.getListClients();

		return clientContentieux;


	}
	@ResponseStatus(value=HttpStatus.OK)
	@RequestMapping(value = "/saveAffaire", method = RequestMethod.POST,  produces = "application/pdf")
	public void saveAffaire(@RequestBody Affaire affaire) {
		
		
		affaire.setDateCreation(new Date());
		
		iaffaireService.addAfaire(affaire);
		
	}
	
	@RequestMapping(value = "/getAffaires", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Affaire> getAllAfaires(){
		List<Affaire> affaires = iaffaireService.getAffaires();
		
		return affaires;
		
		
	}
	
	@RequestMapping(value = "/deleteAffaire/{idAffaire}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteAffaire(@PathVariable Integer idAffaire) {
		
		log.info("REST request to delete Affaire : {}", idAffaire);
		iaffaireService.deleteAffaire(idAffaire);
		
	}
	
	
	
	@RequestMapping(value = "/updateAffaire", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateAffaire(@RequestBody Affaire affaire) {
		
		iaffaireService.updateAffaire(affaire);
	}
	
	
	
	

}
