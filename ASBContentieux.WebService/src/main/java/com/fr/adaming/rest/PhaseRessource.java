package com.fr.adaming.rest;


import com.fr.adaming.entities.Phase;
import com.fr.adaming.service.IPhaseService;
import com.fr.adaming.service.impl.PhaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nbelhajyahia
 *  <p>
 *  REST controller pour la gestion des phases. <strong><u>CrossOrigin
 *  is allowed.</u></strong> Class URL : <strong>/api</strong>
 */

@RestController
@RequestMapping("/api")
@CrossOrigin

public class PhaseRessource {

    @Autowired
    @Qualifier("PhaseService")
    private IPhaseService phaseService;

    @PostMapping(value="addPhase", produces = MediaType.APPLICATION_JSON_VALUE)
    public Phase saveAudience(@RequestBody  Phase phase ,@RequestHeader(value="Authorization") String headerStr) {
        return phaseService.saveAudience(phase);
    }
    @PostMapping(value="getPhases", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Phase> getAll() {
        return phaseService.getAll();
    }
    @PostMapping(value="getPhase/{id_phase}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Phase getPhaseById(@PathVariable Long id_phase) {
        return phaseService.getPhaseById(id_phase);
    }
    @PutMapping(value = "updatePhase", produces = MediaType.APPLICATION_JSON_VALUE)
    public Phase updatePhase(@RequestBody Phase phase) {
        return phaseService.updatePhase(phase);
    }
    @DeleteMapping(value="deletePhase/{id_phase}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deletePhase(@PathVariable Long id_phase) {
        phaseService.deletePhase(id_phase);
    }
}
