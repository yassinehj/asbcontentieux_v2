package com.fr.adaming.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fr.adaming.entities.Debiteur;
import com.fr.adaming.service.IDebiteurService;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class DebiteurResource {
	
	
	@Autowired
	private IDebiteurService debiteurService;
	
	
	@GetMapping(value="/getAllDebiteurs" ,produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<Debiteur> getAll(){
		return debiteurService.getAllDebiteur();
		 
		 
	 }
	
	
	
	 @RequestMapping(value = "/updateDebiteurs", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Debiteur> updateDebiteur(@RequestBody Debiteur debiteur) {
			
			debiteurService.updateDebiteur(debiteur);
			return new ResponseEntity<Debiteur>(debiteur, HttpStatus.OK);
		}
	
	
	 @GetMapping(value = "/getDebiteurById/{idDebiteur}")
		public ResponseEntity<Debiteur> getDebiteurByID(@PathVariable Long idDebiteur) {
			
			
			
			Debiteur debiteur = new Debiteur();
			 
			try {
				debiteur = debiteurService.getDebiteurById(idDebiteur);
				return new ResponseEntity<Debiteur>(debiteur, HttpStatus.OK);
			} catch (Exception e) {
			}
			return null;
		}
	
	
	
	 @RequestMapping(value = "/deleteDebiteur/{idDebiteur}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<HttpStatus> deleteDebiteur(@PathVariable Long idDebiteur) {
			try {
				
				
				debiteurService.deleteDebiteurById(idDebiteur);
				return new ResponseEntity<HttpStatus>(HttpStatus.OK);
			}
			catch (Exception e){
			}
			return null;
		}
	 
	 
	 
	
	 @DeleteMapping(value = "/deleteDebiteurs/{idDebiteur}")
		public ResponseEntity<HttpStatus> deleteDebiteurs(@PathVariable List<Long> idDebiteur) {
			try {
				
				idDebiteur.stream().forEach(idDebiteurs -> {
					debiteurService.deleteDebiteurById(idDebiteurs);
		        });
				return new ResponseEntity<HttpStatus>(HttpStatus.OK);
			}
			catch (Exception e){
			}
			return null;
		}
	 
	 
	 
	 
	 
	 
	

}
