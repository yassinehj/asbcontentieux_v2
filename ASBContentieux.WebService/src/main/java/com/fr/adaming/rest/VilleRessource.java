package com.fr.adaming.rest;


import com.fr.adaming.entities.Gouvernorat;
import com.fr.adaming.entities.Ville;
import com.fr.adaming.service.IGouvernanteService;
import com.fr.adaming.service.IVilleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nbelhajyahia
 *
 */
@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class VilleRessource  {

    @Autowired
    @Qualifier("VilleService")
    private IVilleService villeService;
    @Autowired
    private IGouvernanteService gouvernanteService;

    @PostMapping(value = "/addVille/{idGouvernorat}" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Ville saveVille(@RequestBody Ville ville,@PathVariable(name = "idGouvernorat", required = false)Long idGouvernorat) {
        Gouvernorat gouvernorat = gouvernanteService.getGouvernoratById(idGouvernorat);

        if(gouvernorat!=null){
            ville.setGouvernorat(gouvernorat);
        }
        ville.setGouvernorat(gouvernorat);
        return villeService.saveVille(ville);
    }
    @GetMapping(value="/getVille" ,produces = MediaType.APPLICATION_JSON_VALUE )
    public List<Ville> getAll()

    {
        return villeService.getAll();
    }

    @GetMapping(value="/getVilles/{id_ville}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Ville getVilleById(@PathVariable Long id_ville,@RequestHeader(value="Authorization") String headerStr) {
        return villeService.getVilleById(id_ville);
    }
    @PutMapping(value = "/updateVille/{idGouvernorat}" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Ville updateVille(@RequestBody Ville ville,@PathVariable(name = "idGouvernorat", required = false)Long idGouvernorat) {
        Gouvernorat gouvernorat = gouvernanteService.getGouvernoratById(idGouvernorat);

        if(gouvernorat!=null){
            ville.setGouvernorat(gouvernorat);
        }
        ville.setGouvernorat(gouvernorat);
        return villeService.updateVille(ville);
    }
    @DeleteMapping(value="/deleteVille/{id_ville}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteVille(@PathVariable Long id_ville,@RequestHeader(value="Authorization") String headerStr) {
            villeService.deleteVille(id_ville);
    }
    @GetMapping(value="/getVilleByGouvernorat/{idGouvernorat}" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Ville> findAllByGouvernorat_IdGouvernorat(@PathVariable Long idGouvernorat,@RequestHeader(value="Authorization") String headerStr) {
        return villeService.findAllByGouvernorat_IdGouvernorat(idGouvernorat);
    }
}
