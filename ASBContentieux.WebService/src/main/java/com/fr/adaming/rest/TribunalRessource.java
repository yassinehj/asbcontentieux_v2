package com.fr.adaming.rest;


import com.fr.adaming.dto.TribunalDto;
import com.fr.adaming.entities.Adresse;
import com.fr.adaming.entities.Gouvernorat;
import com.fr.adaming.entities.Ville;
import com.fr.adaming.entities.Pays;
import com.fr.adaming.entities.Tribunal;
import com.fr.adaming.repositories.IAdresseRepository;
import com.fr.adaming.repositories.IPaysRepository;
import com.fr.adaming.service.ITribunalService;
import com.fr.adaming.utils.LoggerUtil;
import jdk.nashorn.internal.runtime.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author nbelhajyahia
 *  <p>
 *  REST controller pour la gestion des tribunals. <strong><u>CrossOrigin
 *  is allowed.</u></strong> Class URL : <strong>/api</strong>
 */

@RestController
@RequestMapping("/api")
@CrossOrigin
public class TribunalRessource {

    @Autowired
    @Qualifier("TribunalService")
    private ITribunalService tribunalService;
    @Autowired
    @Qualifier("IAdresseRepository")
    private IAdresseRepository adresseRepository;
    @Autowired
    @Qualifier("IPaysRepository")
    private IPaysRepository paysRepository;

    @PostMapping(value = "/addTribunal",  produces = MediaType.APPLICATION_JSON_VALUE)
    public Tribunal saveTribunal(@RequestBody Tribunal tribunal, @PathVariable(name = "idTribunalSuivante", required = false) Long idTribunalSuivante) {
        if(tribunal.getAdresse() != null) {

            Adresse adresse=adresseRepository.save(tribunal.getAdresse());
            tribunal.setAdresse(adresse);
        }
        if(idTribunalSuivante !=null){
            Tribunal tribunalSuivante=tribunalService.getTribunalById(idTribunalSuivante);
             if(tribunalSuivante!=null){
                 tribunal.setIdTribunal(idTribunalSuivante);
             }
        }

        return tribunalService.saveTribunal(tribunal);
        /*if(tribunalDto.getTribunal().getTribunalSuivante()!=null){
            return tribunalService.saveTribunal(tribunalDto);
        }*/
    }
    @GetMapping(value="/getTribunal",produces = MediaType.APPLICATION_JSON_VALUE )
    public List<Tribunal> getAll(@RequestHeader(value="Authorization") String headerStr) {
        return tribunalService.getAll();
    }
    @GetMapping(value="/getTribunalById/{idTribunal}",produces = MediaType.APPLICATION_JSON_VALUE )
    public Tribunal getTibunalById(@PathVariable  Long idTribunal,@RequestHeader(value="Authorization") String headerStr) {
        return tribunalService.getTribunalById(idTribunal);
    }
    @PutMapping(value="/updateTribunal" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Tribunal updateTribunal(@RequestBody Tribunal tribunal) {
        return tribunalService.updateTribunal(tribunal);
    }
    @DeleteMapping(value = "/deleteTribunal/{id_tribunal}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteTribunal(@PathVariable Long id_tribunal) {
        tribunalService.deleteTribunal(id_tribunal);
    }

    @DeleteMapping(value = "/deleteTribunals/{idTribunals}")
    public ResponseEntity<HttpStatus> deleteTribunal(@PathVariable List<Long> idTribunals) {
        try {

            idTribunals.stream().forEach(idTribunal -> {
                tribunalService.deleteTribunal(idTribunal);
            });
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        }
        catch (Exception e){
        }
        return null;
    }
}
