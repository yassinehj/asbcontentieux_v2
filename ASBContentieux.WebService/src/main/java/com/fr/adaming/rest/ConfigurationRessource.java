package com.fr.adaming.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.entities.Configuration;
import com.fr.adaming.service.IConfigurationService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ConfigurationRessource {
	private final Logger log = LoggerFactory.getLogger(AffaireResource.class);

	@Autowired
	private IConfigurationService iConfigurationService ;
	
	@PostMapping(value="addConfiguration",produces = MediaType.APPLICATION_JSON_VALUE)
    public Configuration saveConfiguration(@RequestBody Configuration configuration,@RequestHeader(value="Authorization") String headerStr) {
        return iConfigurationService.addConfiguration(configuration);
    }
	
    @GetMapping(value="getConfigurations",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Configuration> getAll(@RequestHeader(value="Authorization") String headerStr) {
        return iConfigurationService.getConfigurations();
    }
   
    
    @PutMapping(value="updateConfiguration",produces = MediaType.APPLICATION_JSON_VALUE)
    public Configuration updateConfiguration(@RequestBody Configuration configuration,@RequestHeader(value="Authorization") String headerStr) {
        return iConfigurationService.updateConfiguration(configuration);
    }
  
    @DeleteMapping(value="deleteConfigurations/{idConfigurations}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteConfiguration(@PathVariable List<Long> idConfigurations){
    	
       idConfigurations.stream().forEach(idConfiguration -> {
    	   iConfigurationService.deleteConfiguration(idConfiguration);
       });
    }

}
