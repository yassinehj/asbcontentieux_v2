package com.fr.adaming;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author bjrad: Designed to be implemented in the test classes in the Rest
 *         Controller Layer. You need to implement the @Test annotation for any
 *         method you want to classify as a test. You need to add
 *         the @SpringBootTest annotation with classes parameter for the Spring
 *         Boot entry class You need to override the subclass constructor and
 *         call the super class constructor
 * 
 */
@WebAppConfiguration
@RunWith(SpringRunner.class)
public abstract class AbstractAdamingTestRestController extends AbstractAdamingTest {
	@Autowired
	WebApplicationContext webApplicationContext;
	/**
	 * Used to mock the Web Context
	 */
	protected MockMvc mockMvc;
	/**
	 * Used for the web service adressing. You need to initiate it in the subclasses
	 * constructors
	 */
	protected String uri;

	public AbstractAdamingTestRestController() {
		super();
	}

	/**
	 * Setting up the Mock MVC with the Web Context
	 */
	@Before
	public final void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		MockitoAnnotations.initMocks(this);

	}

}
