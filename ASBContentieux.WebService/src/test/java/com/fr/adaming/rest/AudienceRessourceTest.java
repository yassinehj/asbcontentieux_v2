//package com.fr.adaming.rest;
//
//import com.fr.adaming.AbstractAdamingTestRestController;
//import com.fr.adaming.SpringBootRunner;
//import com.fr.adaming.entities.Audience;
//import com.fr.adaming.entities.Auxiliaire;
//import com.fr.adaming.entities.Utilisateur;
//import com.fr.adaming.service.impl.AudienceService;
//import com.fr.adaming.service.impl.UtilisateurService;
//import com.fr.adaming.utils.LoggerUtil;
//import org.junit.Test;
//import org.mockito.Mockito;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.mock.web.MockHttpServletResponse;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.RequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertEquals;
//
///**
// *
// * @author nbelhajyahia: Cette classe permet d'effectuer les test unitaires
// *         nécessaires pour la couche web service de l'audience. Pour créer
// *         un nouveau test , il faut ajouter l'annotation @Test au dessus de la
// *         méthode implémentée. NB: Dans cette classe de test , on utilise le
// *         concept de mocking.
// */
//@SpringBootTest(classes = SpringBootRunner.class)
//public class AudienceRessourceTest extends AbstractAdamingTestRestController {
//
//    /**
//     * Injecter un faux audienceService car dans cette classe de test , on veut
//     * se focaliser uniquement sur la partie web services (rest)
//     */
//    @MockBean
//    private AudienceService audienceService;
//
//    /**
//     * Récupérer les données de configuration du context hérités de la classe
//     * AbstractAdamingTestRestController
//     */
//    public AudienceRessourceTest() {
//        super();
//        this.uri = "/api";
//
//    }
//    /**
//     * Tester le web service (rest) /createAudience
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testAddAudience() throws Exception {
//        LoggerUtil.log("--------------- Constructing User ---------------");
//        Audience audience = getAudience();
//
//        /**
//         * Convertir l'objet audience crée en JSON
//         */
//        String inputInJson = this.mapToJson(audience);
//
//        LoggerUtil.log("--------------- Mocking saveAudience() method of AudienceService ---------------");
//        /**
//         * Ignorer la couche service et retourner directement l'audience introduit
//         * lors de l'exécution de la méthode audienceService.saveAudience(audience) ,
//         * car dans ce context on veut tester uniquement le web service (rest)
//         * createAudience.
//         */
//        Mockito.when(audienceService.saveAudience(Mockito.any(Audience.class))).thenReturn(audience);
//        LoggerUtil.log("--------------- Method Mocked ---------------");
//        LoggerUtil.log("--------------- Verifying results ---------------");
//        /**
//         * Introduire les parametres nécessaire pour l'exécution du web service.
//         */
//        String URI = uri + "/addAudience";
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
//                .content(inputInJson).contentType(MediaType.APPLICATION_JSON);
//
//        /**
//         * Exécuter la requête HTTP à travers la méthode perform(requestBuilder) puis
//         * récupérer la réponse à partir de MvcResult
//         */
//        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//        MockHttpServletResponse response = result.getResponse();
//
//        String outputInJson = response.getContentAsString();
//        /**
//         * Comparer la réponse avec l'objet audience converti en JSON (inputInJson) .
//         */
//        System.out.println("input :"+inputInJson);
//        System.out.println("output :"+outputInJson);
//        assertThat(outputInJson).isEqualTo(outputInJson);
//        /**
//         *
//         * Vérifier si la web service est exécuté avec succés . C'est à dire le
//         * HttpStatus est égal à 200.
//         */
//        assertEquals(HttpStatus.OK.value(), response.getStatus());
//
//    }
//
//
//
//
//    private Audience getAudience() {
//
//        Audience audience = new Audience();
//        audience.setDateCreation(new Date());
//        audience.setDescription("aaa");
//        audience.setDateAudience(new Date());
//        audience.setEtat("aaaaa");
//        audience.setDateFin(new Date());
//        audience.setDureeNotif(2f);
//        audience.setNotif("aaaaaa");
//        return audience;
//    }
//}
