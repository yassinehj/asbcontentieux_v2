package com.fr.adaming.rest;

import com.fr.adaming.AbstractAdamingTestRestController;
import com.fr.adaming.SpringBootRunner;
import com.fr.adaming.entities.Auxiliaire;

import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.service.impl.AuxiliaireService;
import com.fr.adaming.utils.LoggerUtil;
import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author nbelhajyahia Cette classe permet d'effectuer les test unitaires
 *         nécessaires pour la couche web service de l'auxiliaire. Pour créer
 *         un nouveau test , il faut ajouter l'annotation @Test au dessus de la
 *         méthode implémentée. NB: Dans cette classe de test , on utilise le
 *         concept de mocking.
 */
@SpringBootTest(classes = SpringBootRunner.class)

public class AuxiliaireRessourceTest extends AbstractAdamingTestRestController {

    /**
     * Injecter un faux utilisateurService car dans cette classe de test , on veut
     * se focaliser uniquement sur la partie web services (rest)
     */
    @MockBean
    private AuxiliaireService auxiliaireService;

    /**
     * Récupérer les données de configuration du context hérités de la classe
     * AbstractAdamingTestRestController
     */
    public AuxiliaireRessourceTest() {
        super();
        this.uri = "/api";

    }

    /**
     * Tester le web service (rest) /createUser
     *
     * @throws Exception
     */
    @Test
    public void testAddAuxiliaire() throws Exception {
        LoggerUtil.log("--------------- Constructing User ---------------");
        Auxiliaire auxiliaire = getAuxiliaire();
       System.out.println(getAuxiliaire());

        /**
         * Convertir l'objet auxiliaire crée en JSON
         */
        String inputInJson = this.mapToJson(auxiliaire);

        LoggerUtil.log("--------------- Mocking saveUAuxiliaire() method of AuxiliaireService ---------------");
        /**
         * Ignorer la couche service et retourner directement l'auxiliaire introduit
         * lors de l'exécution de la méthode auxiliaireService.saveAuxiliaire(auxiliaire) ,
         * car dans ce context on veut tester uniquement le web service (rest)
         * addAuxiliaire.
         */
        Mockito.when(auxiliaireService.saveAuxiliaire(Mockito.any(Auxiliaire.class))).thenReturn(auxiliaire);
        LoggerUtil.log("--------------- Method Mocked ---------------");
        LoggerUtil.log("--------------- Verifying results ---------------");
        /**
         * Introduire les parametres nécessaire pour l'exécution du web service.
         */
        String URI = uri + "/addAuxiliaire";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
                .content(inputInJson).contentType(MediaType.APPLICATION_JSON);

        /**
         * Exécuter la requête HTTP à travers la méthode perform(requestBuilder) puis
         * récupérer la réponse à partir de MvcResult
         */
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        System.out.println("la reponse est :"+response);

        String outputInJson = response.getContentAsString();
        LoggerUtil.log("--------------- Récuperation response---------------");
        /**
         * Comparer la réponse avec l'objet auxiliaire converti en JSON (inputInJson) .
         */

        System.out.println("input:"+inputInJson);
        System.out.println("output:"+outputInJson);

        assertThat(outputInJson).isEqualTo(inputInJson);
        LoggerUtil.log("--------------- Comparaison Réussite---------------");
        /**
         * Vérifier si la web service est exécuté avec succés . C'est à dire le
         * HttpStatus est égal à 200.
         */
        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    /**
     * Tester le web service (rest) createAuxiliaire /getUAuxiliaireById
     *
     * @throws Exception
     */
    @Test
    public void testGetAuxiliaireById() throws Exception {
        LoggerUtil.log("--------------- Constructing User ---------------");
        Auxiliaire auxiliaire = getAuxiliaire();

        /**
         * Convertir l'objet auxiliaire crée en JSON
         */
        String expectedJson = this.mapToJson(auxiliaire);
        LoggerUtil.log("--------------- Mocking getById() method of AuxiliaireService ---------------");
        /**
         * Ignorer la couche service et retourner directement l'auxiliaire introduit
         * lors de l'exécution de la méthode auxiliaireService.getById(email) , car
         * dans ce context on veut tester uniquement le web service (rest) getUserById.
         */
        Mockito.when(auxiliaireService.getAuxiliaireById(1L)).thenReturn(auxiliaire);

        LoggerUtil.log("--------------- Method Mocked ---------------");
        LoggerUtil.log("--------------- Verifying results ---------------");
        /**
         * Introduire les parametres nécessaire pour l'exécution du web service.
         */
        String URI = uri + "/getAuxiliaire/1";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);

        /**
         * Exécuter la requête HTTP à travers la méthode perform(requestBuilder). Puis ,
         * comparer la réponse avec l'objet auxiliaire converti en JSON (expectedJson).
         */
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        String outputInJson = result.getResponse().getContentAsString();
        assertThat(outputInJson).isEqualTo(expectedJson);
    }



    private Auxiliaire getAuxiliaire() {
        Auxiliaire auxiliaire = new Auxiliaire();
        auxiliaire.setNom("bhy");
        auxiliaire.setPrenom("Nidhal");
        auxiliaire.setNumTel("22021548");
        auxiliaire.setSpecialite("aaaaa");
        auxiliaire.setEmail("nbelhadjyahia@gmail.com");
        auxiliaire.setType("azerty");
        auxiliaire.setNumTel2("22587987");
        return auxiliaire;
    }
}
