//package com.fr.adaming.rest;
//
//import com.fr.adaming.AbstractAdamingTestRestController;
//import com.fr.adaming.SpringBootRunner;
//import com.fr.adaming.entities.Pays;
//import com.fr.adaming.service.impl.PaysService;
//import com.fr.adaming.utils.LoggerUtil;
//import org.junit.Test;
//import org.mockito.Mockito;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.mock.web.MockHttpServletResponse;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.RequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertEquals;
//
///**
// *
// * @author nbelhajyahia : Cette classe permet d'effectuer les test unitaires
// *         nécessaires pour la couche web service de pays. Pour créer un nouveau
// *         test , il faut ajouter l'annotation @Test au dessus de la méthode
// *         implémentée. NB: Dans cette classe de test , on utilise le concept de
// *         mocking.
// */
//@SpringBootTest(classes = SpringBootRunner.class)
//public class PaysRessourceTest extends AbstractAdamingTestRestController {
//
//    /**
//     * Injecter un faux PaysService car dans cette classe de test , on veut se
//     * focaliser uniquement sur la partie web services (rest)
//     */
//    @MockBean
//    private PaysService paysService;
//
//    /**
//     * Récupérer les données de configuration du context hérités de la classe
//     * AbstractAdamingTestRestController
//     */
//    public PaysRessourceTest() {
//        super();
//        this.uri = "/api";
//
//    }
//    /**
//     * Tester le web service (rest) /createPays
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testAddPays() throws Exception {
//        LoggerUtil.log("--------------- Constructing User ---------------");
//        Pays pays = getPays();
//
//        /**
//         * Convertir l'objet pays crée en JSON
//         */
//        String inputInJson = this.mapToJson(pays);
//
//        LoggerUtil.log("--------------- Mocking saveUser() method of PaysService ---------------");
//        /**
//         * Ignorer la couche service et retourner directement pays introduit
//         * lors de l'exécution de la méthode paysService.savePays(pays) ,
//         * car dans ce context on veut tester uniquement le web service (rest)
//         * createPays.
//         */
//        Mockito.when(paysService.savePays(Mockito.any(Pays.class))).thenReturn(true);
//        LoggerUtil.log("--------------- Method Mocked ---------------");
//        LoggerUtil.log("--------------- Verifying results ---------------");
//        /**
//         * Introduire les parametres nécessaire pour l'exécution du web service.
//         */
//        String URI = uri + "/addPays";
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
//                .content(inputInJson).contentType(MediaType.APPLICATION_JSON);
//
//        /**
//         * Exécuter la requête HTTP à travers la méthode perform(requestBuilder) puis
//         * récupérer la réponse à partir de MvcResult
//         */
//        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//        MockHttpServletResponse response = result.getResponse();
//
//        String outputInJson = response.getContentAsString();
//        System.out.println("aaaaa "+outputInJson);
//        /**
//         * Comparer la réponse avec l'objet pays converti en JSON (inputInJson) .
//         */
//        assertThat(outputInJson).isEqualTo(inputInJson);
//        /**
//         * Vérifier si la web service est exécuté avec succés . C'est à dire le
//         * HttpStatus est égal à 200.
//         */
//        assertEquals(HttpStatus.OK.value(), response.getStatus());
//
//    }
//
//    /**
//     * Tester le web service (rest) createUser /getUserById
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testGetPaysById() throws Exception {
//        LoggerUtil.log("--------------- Constructing Pays ---------------");
//        Pays pays = getPays();
//
//        /**
//         * Convertir l'objet pays crée en JSON
//         */
//        String expectedJson = this.mapToJson(pays);
//        LoggerUtil.log("--------------- Mocking getById() method of PaysService ---------------");
//        /**
//         * Ignorer la couche service et retourner directement pays introduit lors de
//         * l'exécution de la méthode paysService.getById(idays) , car dans ce context on
//         * veut tester uniquement le web service (rest) getPaysById;
//         */
//        Mockito.when(paysService.getPaysById(1L)).thenReturn(pays);
//
//        LoggerUtil.log("--------------- Method Mocked ---------------");
//        LoggerUtil.log("--------------- Verifying results ---------------");
//        /**
//         * Introduire les parametres nécessaire pour l'exécution du web service.
//         */
//        String URI = uri + "/getPays/1";
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);
//
//        /**
//         * Exécuter la requête HTTP à travers la méthode perform(requestBuilder). Puis ,
//         * comparer la réponse avec l'objet pays converti en JSON (expectedJson).
//         */
//        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//        String outputInJson = result.getResponse().getContentAsString();
//        System.out.println("expectedJson " + expectedJson);
//        System.out.println("outputInJson " + outputInJson);
//        assertThat(outputInJson).isEqualTo(expectedJson);
//    }
//
//    private Pays getPays() {
//
//        Pays pays = new Pays();
//        pays.setIdPays(1L);
//        pays.setNom("aaaa");
//        return pays;
//    }
//}
