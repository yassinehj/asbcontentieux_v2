//package com.fr.adaming.rest;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertEquals;
//
//import org.junit.Test;
//import org.mockito.Mockito;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.mock.web.MockHttpServletResponse;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.RequestBuilder;
//
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import com.fr.adaming.AbstractAdamingTestRestController;
//import com.fr.adaming.SpringBootRunner;
//import com.fr.adaming.entities.Utilisateur;
//import com.fr.adaming.service.impl.UtilisateurService;
//import com.fr.adaming.utils.LoggerUtil;
//
///**
// *
// * @author mabelkaid: Cette classe permet d'effectuer les test unitaires
// *         nécessaires pour la couche web service de l'utilisateur. Pour créer
// *         un nouveau test , il faut ajouter l'annotation @Test au dessus de la
// *         méthode implémentée. NB: Dans cette classe de test , on utilise le
// *         concept de mocking.
// */
//@SpringBootTest(classes = SpringBootRunner.class)
//public class UtilisateurRessourceTest extends AbstractAdamingTestRestController {
//
//	/**
//	 * Injecter un faux utilisateurService car dans cette classe de test , on veut
//	 * se focaliser uniquement sur la partie web services (rest)
//	 */
//	@MockBean
//	private UtilisateurService utilisateurService;
//
//	/**
//	 * Récupérer les données de configuration du context hérités de la classe
//	 * AbstractAdamingTestRestController
//	 */
//	public UtilisateurRessourceTest() {
//		super();
//		this.uri = "/api";
//
//	}
//
//	/**
//	 * Tester le web service (rest) /createUser
//	 *
//	 * @throws Exception
//	 */
//	@Test
//	public void testAddUser() throws Exception {
//		LoggerUtil.log("--------------- Constructing User ---------------");
//		Utilisateur utilisateur = getUser();
//
//		/**
//		 * Convertir l'objet utilisateur crée en JSON
//		 */
//		String inputInJson = this.mapToJson(utilisateur);
//
//		LoggerUtil.log("--------------- Mocking saveUser() method of UtilisateurService ---------------");
//		/**
//		 * Ignorer la couche service et retourner directement l'utilisateur introduit
//		 * lors de l'exécution de la méthode utilisateurService.saveUser(utilisateur) ,
//		 * car dans ce context on veut tester uniquement le web service (rest)
//		 * createUser.
//		 */
//		Mockito.when(utilisateurService.saveUser(Mockito.any(Utilisateur.class))).thenReturn(utilisateur);
//		LoggerUtil.log("--------------- Method Mocked ---------------");
//		LoggerUtil.log("--------------- Verifying results ---------------");
//		/**
//		 * Introduire les parametres nécessaire pour l'exécution du web service.
//		 */
//		String URI = uri + "/createUser";
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
//				.content(inputInJson).contentType(MediaType.APPLICATION_JSON);
//
//		/**
//		 * Exécuter la requête HTTP à travers la méthode perform(requestBuilder) puis
//		 * récupérer la réponse à partir de MvcResult
//		 */
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		MockHttpServletResponse response = result.getResponse();
//
//		String outputInJson = response.getContentAsString();
//		/**
//		 * Comparer la réponse avec l'objet utilisateur converti en JSON (inputInJson) .
//		 */
//		assertThat(outputInJson).isEqualTo(inputInJson);
//		/**
//		 * Vérifier si la web service est exécuté avec succés . C'est à dire le
//		 * HttpStatus est égal à 200.
//		 */
//		assertEquals(HttpStatus.OK.value(), response.getStatus());
//
//	}
//
//	/**
//	 * Tester le web service (rest) createUser /getUserById
//	 *
//	 * @throws Exception
//	 */
//	@Test
//	public void testGetUserById() throws Exception {
//		LoggerUtil.log("--------------- Constructing User ---------------");
//		Utilisateur utilisateur = getUser();
//
//		/**
//		 * Convertir l'objet utilisateur crée en JSON
//		 */
//		String expectedJson = this.mapToJson(utilisateur);
//		LoggerUtil.log("--------------- Mocking getById() method of UtilisateurSeervice ---------------");
//		/**
//		 * Ignorer la couche service et retourner directement l'utilisateur introduit
//		 * lors de l'exécution de la méthode utilisateurService.getById(email) , car
//		 * dans ce context on veut tester uniquement le web service (rest) getUserById.
//		 */
//		Mockito.when(utilisateurService.getById(20L)).thenReturn(utilisateur);
//
//		LoggerUtil.log("--------------- Method Mocked ---------------");
//		LoggerUtil.log("--------------- Verifying results ---------------");
//		/**
//		 * Introduire les parametres nécessaire pour l'exécution du web service.
//		 */
//		String URI = uri + "/getUserById/20";
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);
//
//		/**
//		 * Exécuter la requête HTTP à travers la méthode perform(requestBuilder). Puis ,
//		 * comparer la réponse avec l'objet utilisateur converti en JSON (expectedJson).
//		 */
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		String outputInJson = result.getResponse().getContentAsString();
//		assertThat(outputInJson).isEqualTo(expectedJson);
//	}
//
//	/**
//	 * Tester le web service (rest) createUser /userByEmail
//	 *
//	 * @throws Exception
//	 */
//	@Test
//	public void testGetUserByEmail() throws Exception {
//		LoggerUtil.log("--------------- Constructing User ---------------");
//		Utilisateur utilisateur = getUser();
//
//		/**
//		 * Convertir l'objet utilisateur crée en JSON
//		 */
//		String expectedJson = this.mapToJson(utilisateur);
//		LoggerUtil.log("--------------- Mocking getById() method of UtilisateurService ---------------");
//		/**
//		 * Ignorer la couche service et retourner directement l'utilisateur introduit
//		 * lors de l'exécution de la méthode utilisateurService.findByEmail(email) , car
//		 * dans ce context on veut tester uniquement le web service (rest) userByEmail.
//		 */
//		Mockito.when(utilisateurService.findByEmail("aminebelkaied@gmail.com")).thenReturn(utilisateur);
//		LoggerUtil.log("--------------- Method Mocked ---------------");
//		LoggerUtil.log("--------------- Verifying results ---------------");
//		/**
//		 * Introduire les parametres nécessaire pour l'exécution du web service.
//		 */
//		String URI = uri + "/userByEmail/aminebelkaied@gmail.com";
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);
//
//		/**
//		 * Exécuter la requête HTTP à travers la méthode perform(requestBuilder). Puis ,
//		 * comparer la réponse avec l'objet utilisateur converti en JSON (expectedJson).
//		 */
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		String outputInJson = result.getResponse().getContentAsString();
//		assertThat(outputInJson).isEqualTo(expectedJson);
//
//	}
//
//	private Utilisateur getUser() {
//		Utilisateur utilisateur = new Utilisateur();
//		utilisateur.setId(20L);
//		utilisateur.setLogin("aminebk");
//		utilisateur.setPassword("1234");
//		utilisateur.setFirstName("Amine");
//		utilisateur.setLastName("Belkaied");
//		utilisateur.setEmail("aminebelkaied@gmail.com");
//		utilisateur.setActivated(true);
//		return utilisateur;
//	}
//}
