//package com.fr.adaming.rest;
//
//import com.fr.adaming.AbstractAdamingTestRestController;
//import com.fr.adaming.SpringBootRunner;
//import com.fr.adaming.entities.Gouvernorat;
//import com.fr.adaming.entities.Pays;
//import com.fr.adaming.service.impl.GouvernoratService;
//import com.fr.adaming.service.impl.PaysService;
//import com.fr.adaming.service.impl.UtilisateurService;
//import com.fr.adaming.utils.LoggerUtil;
//import org.junit.Test;
//import org.mockito.Mockito;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.mock.web.MockHttpServletResponse;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.RequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertEquals;
//
///**
//        * @author nbelhajyahia   : Cette classe permet d'effectuer les test unitaires
//        *         nécessaires pour la couche web service de pays. Pour créer
//        *         un nouveau test , il faut ajouter l'annotation @Test au dessus de la
//        *         méthode implémentée. NB: Dans cette classe de test , on utilise le
//        *         concept de mocking.
//        */
//          @SpringBootTest(classes = SpringBootRunner.class)
//          public class GouvernoratRessourceTest extends AbstractAdamingTestRestController {
//
//    /**
//     * Injecter un faux utilisateurService car dans cette classe de test , on veut
//     * se focaliser uniquement sur la partie web services (rest)
//     */
//    @MockBean
//    private GouvernoratService gouvernoratService;
//
//    @MockBean
//    private PaysService paysService;
//
//    /**
//     * Récupérer les données de configuration du context hérités de la classe
//     * AbstractAdamingTestRestController
//     */
//    public GouvernoratRessourceTest() {
//        super();
//        this.uri = "/api";
//
//    }
//
//
//    /**
//     * Tester le web service (rest) /createGouvernorat
//     *
//     * @throws Exception
//     */
//    /*@Test
//    public void testAddGouvernorat() throws Exception {
//        LoggerUtil.log("--------------- Constructing User ---------------");
//        Gouvernorat gouvernorat = getGouvernorat();
//
//        *//**
//         * Convertir l'objet pays crée en JSON
//         *//*
//        String inputInJson = this.mapToJson(gouvernorat);
//
//        LoggerUtil.log("--------------- Mocking saveGouvernorat() method of GouvernoratService ---------------");
//        *//**
//         * Ignorer la couche service et retourner directement pays introduit
//         * lors de l'exécution de la méthode gouvernoratService.saveGouvernorat(Gouvernorat) ,
//         * car dans ce context on veut tester uniquement le web service (rest)
//         * createGouvernorat.
//         *//*
//        Mockito.when(paysService.getPaysById(Mockito.anyLong())).thenReturn(gouvernorat.getPays());
//        LoggerUtil.log("--------------- Method Mocked ---------------");
//        LoggerUtil.log("--------------- Verifying results ---------------");
//        *//**
//         * Introduire les parametres nécessaire pour l'exécution du web service.
//         *//*
//        String URI = uri + "/addGouvernorat";
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
//                .content(inputInJson).contentType(MediaType.APPLICATION_JSON);
//
//        *//**
//         * Exécuter la requête HTTP à travers la méthode perform(requestBuilder) puis
//         * récupérer la réponse à partir de MvcResult
//         *//*
//        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//        System.out.println("result "+result);
//        MockHttpServletResponse response = result.getResponse();
//        System.out.println(response);
//        String outputInJson = response.getContentAsString();
//        System.out.println("aaaaa "+outputInJson);
//
//        *//**
//         * Comparer la réponse avec l'objet pays converti en JSON (inputInJson) .
//         *//*
//        System.out.println("inputInJson "+inputInJson);
//        System.out.println("outputInJson "+outputInJson);
//        assertThat(outputInJson).isEqualTo(inputInJson);
//        *//**
//         * Vérifier si la web service est exécuté avec succés . C'est à dire le
//         * HttpStatus est égal à 200.
//         *//*
//        assertEquals(HttpStatus.OK.value(), response.getStatus());
//
//    }
//*/
//    private Gouvernorat getGouvernorat(){
//        Pays pays = new Pays();
//        pays.setIdPays(1L);
//        Gouvernorat gouvernorat = new Gouvernorat();
//        gouvernorat.setIdGouvernorat(4L);
//        gouvernorat.setNom("Beja");
//        gouvernorat.setPays(pays);
//        return  gouvernorat;
//
//    }
//}
