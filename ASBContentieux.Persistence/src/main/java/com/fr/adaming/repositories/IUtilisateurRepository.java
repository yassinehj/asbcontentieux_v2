package com.fr.adaming.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.RefProfession;
import com.fr.adaming.entities.Utilisateur;

/**
 * @author mbouhdida
 *
 */
@Repository("IUtilisateurRepository")
public interface IUtilisateurRepository extends JpaRepository<Utilisateur, Long> {

	/**
	 * recuperer l'objet utilisateur par login.
	 * <p>
	 * 
	 * @param login
	 * 
	 * 
	 * @return l'objet récupéré
	 */
	Utilisateur findByLogin(String login);

	/**
	 * recuperer l'objet utilisateur par login et reset key.
	 * <p>
	 * 
	 * @param login
	 * 
	 * @param resetKey
	 * 
	 * @return l'objet récupéré
	 */
	Utilisateur findByLoginAndResetKey(String login, String resetKey);

	/**
	 * recuperer l'objet utilisateur par email.
	 * <p>
	 * 
	 * @param email
	 * 
	 * 
	 * @return l'objet récupéré
	 */
	Utilisateur findByEmail(String email);

	/**
	 * recuperer la liste des utilisateur non affecté.
	 * <p>
	 * 
	 * @return la liste récupérée
	 */
	@Query("from Utilisateur where id not in (select id.userId from Profile) and activated = true")
	public List<Utilisateur> findNotAffectedUser();

	/**
	 * recuperer la liste des utilisateurs par role.
	 * <p>
	 * 
	 * @param authorityName
	 *            nom d'un role
	 * 
	 * 
	 * @return la liste récupérée
	 */
	@Query("from Utilisateur where id in (select id.userId from Profile where id.roleId =:x)")
	public List<Utilisateur> findUserByAthorityName(@Param("x") String authorityName);

	/**
	 * recuperer la liste des utilisateurs par refProfession.
	 * <p>
	 * 
	 * @param refProfession
	 * 
	 * @return la liste récupérée
	 */
	public List<Utilisateur> findByRefProfession(RefProfession refProfession);

	/**
	 * recuperer la liste des utilisateurs par password .
	 * <p>
	 * 
	 * @return la liste récupérée
	 */
	public List<Utilisateur> findByPasswordNotNull();
}
