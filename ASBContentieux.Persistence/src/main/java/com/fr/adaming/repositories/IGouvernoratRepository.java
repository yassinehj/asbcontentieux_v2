package com.fr.adaming.repositories;

import com.fr.adaming.entities.Pays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Gouvernorat;

import java.util.List;

/**
 * Spring Data JPA repository for the Gouvernorat entity.
 * @author azaaboub
 *
 */
@Repository("IGouvernoratRepository")
public interface IGouvernoratRepository extends JpaRepository<Gouvernorat, Long> {

    public List<Gouvernorat> findByNom(String nom);
    public Gouvernorat findDistinctFirstByNom(String nom);

    public List<Gouvernorat> findAllByPays_IdPays(Long idPays);


}
