package com.fr.adaming.repositories;

import com.fr.adaming.entities.DossierPrec;
import com.fr.adaming.entities.HistoriqueRecouverement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IHistoriqueRecouvrementRepository extends JpaRepository<HistoriqueRecouverement,Long> {
    List<HistoriqueRecouverement> findAllByDossierPrec_IdDossierPrec(Long idDossierPrec);
}
