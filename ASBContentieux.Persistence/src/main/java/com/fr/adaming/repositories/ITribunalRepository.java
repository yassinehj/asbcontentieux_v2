package com.fr.adaming.repositories;

import com.fr.adaming.entities.Tribunal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Tribunal entity.
 * @author nbelhajyahia
 */

@Repository("ITribunalRepository")
public interface ITribunalRepository extends JpaRepository<Tribunal,Long> {
}
