package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Dossier;

/**
 * Spring Data JPA repository for the Dossier entity.
 * @author azaaboub
 *
 */

@Repository("IDossierRepository")
public interface IDossierRepository extends JpaRepository<Dossier, Long> {

}
