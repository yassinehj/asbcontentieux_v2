package com.fr.adaming.repositories;

import com.fr.adaming.entities.Auxiliaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * Spring Data JPA repository for the Auxiliaire entity.
 * @author nbelhajyahia
 */
@Repository("IAuxiliaireRepository")
public interface IAuxiliaireRepository extends JpaRepository<Auxiliaire,Long> {

  public String findByNom(String nom);
}
