package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Affaire;

/**
 * Spring Data JPA repository for the Affaire entity.
 * @author azaaboub
 *
 */

@Repository("IAffaireRepository")
public interface IAffaireRepository extends JpaRepository<Affaire,Integer>  {

}
