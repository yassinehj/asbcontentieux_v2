package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.RefProfession;

/**
 * @author mbouhdida
 *
 */
@Repository("IRefProfessionRepository")
public interface IRefProfessionRepository extends JpaRepository<RefProfession, Long> {

	/**
	 * recuperer l'objet refProfession par code profession.
	 * <p>
	 * 
	 * @param code
	 *            code de la profession
	 * @return l'objet récupéré
	 */
	public RefProfession findByCodProfession(String code);

}
