package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Pays;
import com.fr.adaming.entities.Gouvernorat;
/**
 * Spring Data JPA repository for the Pays entity.
 * @author azaaboub
 *
 */

@Repository("IPaysRepository")
public interface IPaysRepository extends JpaRepository<Pays, Long> {
    public Pays findByNom(String nom);
    public Pays findPaysByGouvernorat(Gouvernorat gouvernorat);
    public Pays findDistinctFirstByNom(String nom);

}
