package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Adresse;

/**
 * Spring Data JPA repository for the Adresse entity.
 * @author azaaboub
 *
 */

@Repository("IAdresseRepository")
public interface IAdresseRepository  extends JpaRepository<Adresse, Long>{
    public Adresse findByDescription(String description);
    public Adresse findDistinctFirstByDescription(String description);
}
