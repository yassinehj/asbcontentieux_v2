package com.fr.adaming.repositories;

import com.fr.adaming.entities.Phase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * Spring Data JPA repository for the Phase entity.
 * @author nbelhajyahia
 */

@Repository("IPhaseRepository")
public interface IPhaseRepository extends JpaRepository<Phase,Long> {

}
