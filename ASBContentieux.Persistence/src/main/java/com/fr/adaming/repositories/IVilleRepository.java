package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Ville;

import java.util.List;

/**
 * Spring Data JPA repository for the Ville entity.
 * @author azaaboub
 *
 */

@Repository("IVilleRepository")
public interface IVilleRepository extends JpaRepository<Ville, Long> {

    public List<Ville> findByNom(String nom);
    public Ville findDistinctFirstByNom(String nom);
    public List<Ville> findAllByGouvernorat_IdGouvernorat(Long idGouvernorat);

}
