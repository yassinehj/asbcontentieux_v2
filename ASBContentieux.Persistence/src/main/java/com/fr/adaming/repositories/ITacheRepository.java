package com.fr.adaming.repositories;

import com.fr.adaming.entities.Tache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Tache entity.
 * @author nbelhajyahia
 */

@Repository("ITacheRepository")
public interface ITacheRepository extends JpaRepository<Tache,Long> {
}
