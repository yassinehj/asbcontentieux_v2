package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.fr.adaming.entities.PieceAffaire;
import java.util.List;


/**
 * Spring Data JPA repository for the PieceAffaire entity.
 * @author azaaboub
 *
 */

@Repository("IPieceAffaireRepository")
public interface IPieceAffaireRepository extends JpaRepository<PieceAffaire, Long> {
	
	@Query("select p from PieceAffaire p where p.dossier.idDossier IS NULL")
	 List<PieceAffaire>findAllPiecesAffaires();
	        
	@Query("select p from PieceAffaire p where p.dossier.idDossier =:idDossier")
	   List<PieceAffaire>findAllPieceByDossier(@Param("idDossier") Long idDossier);
	
	
	
	
	
	
	
	
	
	

}
