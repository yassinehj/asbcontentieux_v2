package com.fr.adaming.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Role;

/**
 * @author mbouhdida
 *
 */
@Repository("IRoleRepository")

public interface IRoleRepository extends JpaRepository<Role, String> {

	/**
	 * recuperer l'objet role par nom du role.
	 * <p>
	 * 
	 * @param nom
	 *            du role
	 * 
	 * @return l'objet récupéré
	 */
	public Role findByName(String nom);

	/**
	 * recuperer une liste des roles par nomModule.
	 * <p>
	 * 
	 * @param nomModule
	 *            le module
	 * 
	 * @return la liste récupérée
	 */
	@Query("from Role where name in (select refPowerId.idRole from RefPower where refPowerId.idRefMenu in (select idMenu from RefMenu where refModule.idModule in (select idModule from RefModule where nomModule = :nomModule))) ")
	public List<Role> findAuthorityByModule(@Param("nomModule") String nomModule);

}
