package com.fr.adaming.repositories;

import java.util.List;

import com.fr.adaming.entities.Profile;
import com.fr.adaming.entities.Role;
import com.fr.adaming.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * @author mbouhdida
 *
 */
@Repository("IProfileRepository")
public interface IProfileRepository extends JpaRepository<Profile, Long> {
	/**
	 * recuperer le profil par nomProfile .
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profile.
	 * @return l'objet récupéré.
	 */

	public Profile getProfilByNomProfile(String nomProfile);

	/**
	 * recuperer liste des profiles par l'utilisateur connecte .
	 * <p>
	 * 
	 * @param username
	 *            le login de l'utilisateur.
	 * @return la liste récupérée.
	 */

	@Query("from Profile where id.userId in (select id from Utilisateur where login =:x)")
	List<Profile> findListProfilofUserLogin(@Param("x") String username);

	/**
	 * recuperer la liste des profiles par identifiant de l'utilisateur .
	 * <p>
	 * 
	 * @param id
	 *            l'identifiant de l'utilisateur.
	 * @return la liste récupérée.
	 */

	@Query("from Profile where id.userId =:x")
	List<Profile> findListProfilofUserId(@Param("x") Long id);

	/**
	 * recuperer la liste distinct des profiles .
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */

	@Query("select distinct a.nomProfile from Profile a")
	public List<Profile> getListProfile();

	/**
	 * recuperer la liste des roles par nom du profil .
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profil.
	 * @return la liste récupérée.
	 */
	@Query("select distinct a.role from Profile a where a.nomProfile= :nomProfile")
	public List<Role> getListRoleByNomProfile(@Param(value = "nomProfile") String nomProfile);

	/**
	 * recuperer la liste des utilisateurs par nom du profil .
	 * <p>
	 * 
	 * @param nomProfile
	 *            le nom du profile.
	 * @return la liste récupérée.
	 */
	@Query("select distinct a.user from Profile a where a.nomProfile= :nomProfile")
	public List<Utilisateur> getListUserByNomProfile(@Param(value = "nomProfile") String nomProfile);

	// @Query("select distinct a.institution from Profile a where a.nomProfile=
	// :nomProfile")
	// public List<RefInstitution> getListInstitutionByNomProfile(@Param(value =
	// "nomProfile") String nomProfile);

	/**
	 * recuperer la liste distinct des profiles .
	 * <p>
	 * 
	 * @return la liste récupérée.
	 */
	@Query("select distinct a.nomProfile from Profile a")
	public List<Profile> getListProfileDistinct();

	/**
	 * recuperer la liste des profils par nom du profil.
	 * <p>
	 * 
	 * @param name
	 *            nom du profil
	 * @return la liste récupérée.
	 */
	@Query("from Profile p where p.nomProfile = :name")
	public List<Profile> getListProfileByNom(@Param("name") String name);

	/**
	 * supprimer un profil par nom du profil.
	 * <p>
	 * 
	 * @param nom
	 *            nom du profil
	 * 
	 */
	@Modifying
	@Query("delete from Profile p where p.nomProfile = :nom")
	public void deleteProfile(@Param("nom") String nom);

	/**
	 * recuperer la liste des profils par nom du role.
	 * <p>
	 * 
	 * @param nom
	 *            nom du role
	 * @return la liste récupérée.
	 */
	@Query("from Profile p where p.id.roleId = :nom")
	public List<Profile> getListProfilByRole(@Param("nom") String nom);

	/**
	 * supprimer la liste des profils par nom du role.
	 * <p>
	 * 
	 * @param nom
	 *            nom du role
	 */
	@Modifying
	@Query("delete from Profile p where p.id.roleId = :nom")
	public void deleteProfileByRole(@Param("nom") String nom);

}
