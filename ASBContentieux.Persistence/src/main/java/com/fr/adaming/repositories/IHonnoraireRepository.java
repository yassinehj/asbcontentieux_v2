package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Honnoraire;

/**
 * Spring Data JPA repository for the Honnoraire entity.
 * @author azaaboub
 *
 */

@Repository("IHonnoraireRepository")
public interface IHonnoraireRepository extends JpaRepository<Honnoraire, Long> {
	
	

}
