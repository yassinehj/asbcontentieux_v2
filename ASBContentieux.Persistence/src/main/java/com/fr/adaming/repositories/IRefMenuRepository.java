package com.fr.adaming.repositories;

import java.util.List;

import com.fr.adaming.entities.RefMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * @author mbouhdida
 *
 */
@Repository("IRefMenuRepository")
public interface IRefMenuRepository extends JpaRepository<RefMenu, Long> {

	/**
	 * recuperer une liste des refMenus fils par refModule.
	 * <p>
	 * 
	 * @param refModule
	 *            le module
	 * 
	 * @return la liste récupérée
	 */
	@Query("from RefMenu r where r.ecranPere is not null and r.refModule.idModule = :refmodule")
	List<RefMenu> findByRefModuleAndEcranPereNotNull(@Param("refmodule") Long refModule);

	/**
	 * recuperer une liste des refMenus par refModule , utilisateurConnecté,
	 * idEcranPere.
	 * <p>
	 * 
	 * @param refModule
	 *            le module
	 * @param login
	 *            login de l'utilisateur connecté
	 * 
	 * @param id
	 *            l'identifiant de l'ecran pere
	 * @return la liste récupérée
	 */

	@Query("from RefMenu r where r.ecranPere.idMenu = :id and r.refModule.nomModule = :refmodule and r.idMenu in (select p.refPowerId.idRefMenu from RefPower p where p.refPowerId.idRole in (select pr.id.roleId from Profile pr where pr.id.userId in(select u.id from Utilisateur u where u.login = :login))) order by r.rang")
	List<RefMenu> findListRefMenyuByRefModule(@Param("refmodule") String refModule, @Param("login") String login,
                                              @Param("id") Long id);

	/**
	 * recuperer une liste des refMenus Pere par refModule et utilisateur Connecté.
	 * <p>
	 * 
	 * @param module
	 *            le module
	 * 
	 * @param login
	 * 
	 * @return la liste récupérée
	 */
	@Query("from RefMenu ref where ref.ecranPere = null and ref.idMenu in(select r.ecranPere.idMenu from RefMenu r where r.refModule.nomModule = :module and r.idMenu in (select p.refPowerId.idRefMenu from RefPower p where p.refPowerId.idRole in (select pr.id.roleId from Profile pr where pr.id.userId in(select u.id from Utilisateur u where u.login = :login)))) order by ref.rang")
	public List<RefMenu> getEcranPereByUserAndModule(@Param("login") String login, @Param("module") String module);

	/**
	 * recuperer une liste des refMenus par utiisateur connecté.
	 * <p>
	 * 
	 * @param login
	 *            le login
	 * 
	 * @return la liste récupérée
	 */
	@Query("from RefMenu r where r.idMenu in (select p.refPowerId.idRefMenu from RefPower p where p.refPowerId.idRole in (select pr.id.roleId from Profile pr where pr.id.userId in(select u.id from Utilisateur u where u.login = :login)))")
	List<RefMenu> findListRefMenyuByUser(@Param("login") String login);

	/**
	 * recuperer une liste des refMenus fils processs par refModule et utilisateur
	 * connecté.
	 * <p>
	 * 
	 * @param refModule
	 *            le module
	 * @param login
	 * 
	 * @return la liste récupérée
	 */
	@Query("from RefMenu r where r.ecranPere!= null and r.refModule.nomModule = :refmodule and r.idMenu in (select p.refPowerId.idRefMenu from RefPower p where p.refPowerId.idRole in (select pr.id.roleId from Profile pr where pr.id.userId in(select u.id from Utilisateur u where u.login = :login))) order by r.rang")
	List<RefMenu> findListRefMenuProcess(@Param("refmodule") String refModule, @Param("login") String login);

}
