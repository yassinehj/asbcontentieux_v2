package com.fr.adaming.repositories;

import java.util.List;

import com.fr.adaming.entities.RefPower;
import com.fr.adaming.entities.RefPowerId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * @author mbouhdida
 *
 */
@Repository("IRefPowerRepository")
public interface IRefPowerRepository extends JpaRepository<RefPower, RefPowerId> {

	/**
	 * recuperer une liste des refPower par role.
	 * <p>
	 * 
	 * @param name
	 *            nom du role
	 * 
	 * 
	 * @return la liste récupérée
	 */
	@Query("from RefPower p where p.refPowerId.idRole = :name")
	public List<RefPower> getRefPowerByRole(@Param("name") String name);

	/**
	 * supprimer les refPowers par nom du role.
	 * <p>
	 * 
	 * @param name
	 *            nom du role
	 * 
	 */
	@Modifying
	@Query("delete from RefPower p where p.refPowerId.idRole = :name")
	public void deleteByRole(@Param("name") String name);

	/**
	 * recuperer un objet refPower par refMenu et utilisateur connecté.
	 * <p>
	 * 
	 * @param menu
	 *            nom de l'ecran
	 * 
	 * 
	 * @param login
	 *            le login de l'utilisateur connecté
	 * @return l'objet récupéré
	 */
	@Query("from RefPower p where p.refPowerId.idRefMenu in (select idMenu from RefMenu  m where m.nomMenu = :menu) and p.refPowerId.idRole in (select id.roleId from Profile pr where pr.id.userId in (select id from Utilisateur u where u.login = :login))")
	public RefPower getPowerByUserAndMenu(@Param("menu") String menu, @Param("login") String login);
}
