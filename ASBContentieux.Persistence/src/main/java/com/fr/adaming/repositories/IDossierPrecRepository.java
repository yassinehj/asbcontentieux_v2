package com.fr.adaming.repositories;

import com.fr.adaming.entities.DossierPrec;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("IDossierPrecRepository")
public interface IDossierPrecRepository extends JpaRepository<DossierPrec,Long> {
    public List<DossierPrec> findAllByArchiver(Boolean archiver);
}
