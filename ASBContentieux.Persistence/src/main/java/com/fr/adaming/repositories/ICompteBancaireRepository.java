package com.fr.adaming.repositories;

import com.fr.adaming.entities.CompteBancaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICompteBancaireRepository extends JpaRepository<CompteBancaire,Long> {
    CompteBancaire findDistinctFirstByRib(String rib);
}
