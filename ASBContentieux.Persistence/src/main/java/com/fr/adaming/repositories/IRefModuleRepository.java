package com.fr.adaming.repositories;

import java.util.List;

import com.fr.adaming.entities.RefModule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * @author mbouhdida
 *
 */
@Repository("IRefModuleRepository")
public interface IRefModuleRepository extends JpaRepository<RefModule, Long> {

	/**
	 * recuperer une liste des refModules par nomModule.
	 * <p>
	 * 
	 * @param nomModule
	 *            le nom de la module
	 * 
	 * @return l'objet récupéré
	 */
	@Query("from RefModule r where r.nomModule like :nomModule")
	RefModule getByNomModule(@Param("nomModule") String nomModule);

	/**
	 * recuperer une liste des refModules par role.
	 * <p>
	 * 
	 * @param roles
	 *            liste des roles de l'utilisateur connecté
	 * 
	 * @return la liste récupérée
	 */
	@Query("from RefModule where idModule in (select refModule.idModule from RefMenu m where m.idMenu in (select refPowerId.idRefMenu from RefPower p  where p.refPowerId.idRole in :roles))")
	List<RefModule> getListModuleByRoles(@Param("roles") List<String> roles);
}
