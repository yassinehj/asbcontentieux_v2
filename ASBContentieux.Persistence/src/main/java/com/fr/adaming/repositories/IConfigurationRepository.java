package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.Configuration;

/**
 * 
 * @author hhboumaiza
 *
 */
@Repository("IConfigurationRepository")
public interface IConfigurationRepository   extends JpaRepository <Configuration,Long> {


}
