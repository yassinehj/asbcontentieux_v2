package com.fr.adaming.repositories;

import com.fr.adaming.entities.PieceAudience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * Spring Data JPA repository for the PieceAudience entity.
 * @author nbelhajyahia
 */
@Repository("IPiecesAudienceRepository")
public interface IPiecesAudienceRepository extends JpaRepository<PieceAudience,Long> {
}
