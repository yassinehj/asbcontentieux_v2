package com.fr.adaming.repositories;

import com.fr.adaming.entities.Debiteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Debiteur entity.
 * @author yassine
 *
 */

@Repository("IDebiteurRepository")
public interface IDebiteurRepository extends JpaRepository<Debiteur,Long> {
    public Debiteur findDistinctFirstByCin(String cin);
}
