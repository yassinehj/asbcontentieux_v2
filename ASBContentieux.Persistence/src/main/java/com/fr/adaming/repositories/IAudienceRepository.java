package com.fr.adaming.repositories;

import com.fr.adaming.entities.Audience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Audience entity.
 * @author nbelhajyahia
 */
@Repository("IAudienceRepository")
public interface IAudienceRepository extends JpaRepository<Audience,Long> {
}
