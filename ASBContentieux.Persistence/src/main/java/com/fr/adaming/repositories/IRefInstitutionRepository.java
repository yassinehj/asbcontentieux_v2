package com.fr.adaming.repositories;

import java.util.List;

import com.fr.adaming.entities.RefInstitution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * @author mbouhdida
 *
 */
@Repository
public interface IRefInstitutionRepository extends JpaRepository<RefInstitution, Long> {

	/**
	 * recuperer l'institution par flaglocal.
	 * <p>
	 * 
	 * @param flaglocal
	 * @return l'objet récupéré.
	 */
	@Query("from RefInstitution r where r.flagLocal = :flaglocal")
	public RefInstitution getByflagLocal(@Param("flaglocal") boolean flaglocal);

	/**
	 * recuperer la liste des institutions par idInstitutionSiege.
	 * <p>
	 * 
	 * @param idInstitutionSiege
	 *            l'identifiant de l'insttution siege
	 * @return la liste récupérée.
	 */
	public List<RefInstitution> findAllByIdInstitutionSiege(Long idInstitutionSiege);

	/**
	 * recuperer la liste des institutions par idInstitutionSiege avec une
	 * pagination.
	 * <p>
	 * 
	 * @param idInstitutionSiege
	 *            l'identifiant de l'insttution siege
	 * @param p
	 * @return la liste récupérée.
	 */
	public Page<RefInstitution> findAllByIdInstitutionSiege(Long idInstitutionSiege, Pageable p);

	/**
	 * recuperer l'institution par idInstitutionSiege et code institution.
	 * <p>
	 * 
	 * @param idInstitution
	 *            l'identifiant de l'insttution siege
	 * @param codeInstitution
	 *            le code de l'institution
	 * @return l'objet récupéré.
	 */
	public RefInstitution findOneByCodeInstitutionAndIdInstitutionSiege(String codeInstitution, Long idInstitution);

	/**
	 * recuperer la liste des institutions par idInstitutionSiege et flag regional.
	 * <p>
	 * 
	 * @param idInstitutionSiege
	 *            l'identifiant de l'insttution siege
	 * @param flagRegional
	 * @return la liste récupérée.
	 */
	@Query("from RefInstitution r where r.idInstitutionSiege = :idInstitutionSiege and r.flagRegional = :flagRegional")
	public List<RefInstitution> findAllByIdInstitutionSiegeAndFlagRegional(
            @Param("idInstitutionSiege") Long idInstitutionSiege, @Param("flagRegional") boolean flagRegional);

	/**
	 * mettre a jour une institution récupérée l'identifiant et flag regional.
	 * <p>
	 * 
	 * @param id
	 *            l'identifiant de l'insttution
	 * @param fregional
	 */

	@Modifying
	@Query("update RefInstitution r set r.flagRegional = :fregional where r.idInstitution = :id")
	public void updateInstitution(@Param("id") Long id, @Param("fregional") boolean fregional);

	/**
	 * recuperer la liste des institutions par idInstitutionSiege et id institution
	 * regionale.
	 * <p>
	 * 
	 * @param idInstitutionSiege
	 *            l'identifiant de l'institution siege
	 * @return la liste récupérée
	 */
	@Query("from RefInstitution r where r.idInstitutionSiege = :idInstitutionSiege and (r.idInstitutionRegionale = :id or r.idInstitutionRegionale = null) and r.codeInstitution != '000' and (flagRegional = false or flagRegional = null)")
	public List<RefInstitution> findAllByIdInstitutionSiegeAndIdInstitutionRegionaleOrIdInstitutionRegionaleNull(
            @Param("id") Long id, @Param("idInstitutionSiege") Long idInstitutionSiege);

	/**
	 * mettre a jour une institution recupere par idInstitutionRegionale et id
	 * institution.
	 * <p>
	 * 
	 * @param idRegionale
	 *            l'identifiant de l'institution regionale
	 * @param id
	 *            l'identifiant de l'institution
	 */
	@Modifying
	@Query("update RefInstitution r set r.idInstitutionRegionale = :idRegional where r.idInstitution = :id")
	public void updateInstitutionLocale(@Param("id") Long id, @Param("idRegional") Long idRegionale);

	/**
	 * recuperer une institution par idInstitutionSiege et code institution.
	 * <p>
	 * 
	 * @param codeInstitution
	 *            le code de l'institution
	 * @param idInstitutionSiege
	 *            l'identifiant de l'institution siege
	 * 
	 * @return l'objet récupéré
	 */
	public RefInstitution findByCodeInstitutionAndIdInstitutionSiege(String codeInstitution, Long idInstitutionSiege);

	/**
	 * recuperer une institution par idInstitutionSiege et code institution.
	 * <p>
	 * 
	 * @param idInstitutionRegionale
	 *            l'identifiant de l'institution regionale
	 * 
	 * @param idInstitutionSiege
	 *            l'identifiant de l'institution siege
	 * 
	 * @return l'objet récupéré
	 */
	public RefInstitution findByidInstitutionAndIdInstitutionSiege(Long idInstitutionRegionale,
                                                                   Long idInstitutionSiege);

	/**
	 * recuperer la liste des institutions par idInstitutionSiege et
	 * idInstitutionRegional.
	 * <p>
	 * 
	 * @param idInstitutionRegionale
	 *            l'identifiant de l'institution regionale
	 * @param idInstitutionSiege
	 *            l'identifiant de l'institution siege
	 * 
	 * @return la liste récupérée
	 */
	@Query("from RefInstitution r where r.idInstitutionSiege = :idInstitutionSiege and r.idInstitutionRegionale = :idInstitutionRegionale")
	public List<RefInstitution> findByIdInstitutionRegionaleAndIdInstitutionSiege(
            @Param("idInstitutionSiege") Long idInstitutionSiege,
            @Param("idInstitutionRegionale") Long idInstitutionRegionale);
}
