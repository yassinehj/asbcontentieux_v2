package com.fr.adaming.repositories;

import com.fr.adaming.entities.Agence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAgenceRepository extends JpaRepository<Agence,Long> {
    public Agence findDistinctFirstByCodeInstitution(String codeInstitution);
    public Agence findDistinctFirstByCodeInstitutionAndIdInstitutionSiege(String codeInstitution,String idInstitutionSiege);
}
