package com.fr.adaming.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entities.JustifHonoraire;

/**
 * Spring Data JPA repository for the JustifHonnoraire entity.
 * @author azaaboub
 *
 */

@Repository("IJustifHonnoraireRepository")
public interface IJustifHonnoraireRepository extends JpaRepository<JustifHonoraire, Long> {

}
