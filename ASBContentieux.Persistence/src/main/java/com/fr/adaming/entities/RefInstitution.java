package com.fr.adaming.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fr.adaming.entities.Utilisateur;

/**
 * <b>RefInstitution est la classe représentant une agence bancaire.</b> une
 * Une agence a une liste des utilisateurs
 * 
 * @author mbouhdida
 */
@Entity
@Table(name = "REF_INSTITUTION")
public class RefInstitution implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	/** L'id institution. */
	@Id
	private Long idInstitution;

	/** La nature. */
	private Integer nature;

	/** L'id institution siege. */
	private Long idInstitutionSiege;

	/** L'id institution regionale. */
	private Long idInstitutionRegionale;

	/** La raison sociale. */
	private String raisonSociale;

	/** La ville. */
	private String ville;

	/** L'adresse. */
	private String adresse;

	/** L'id represantant. */
	private Long idRepresantant;

	/** Le code institution. */
	private String codeInstitution;

	/** Le flag local. */
	private Boolean flagLocal;

	/** Le flag regional. */
	private Boolean flagRegional;
	/** L'abr. */
	private String abr;

	/** Le flag actif. */
	private Boolean flagActif;

	/** Le flag modifiable. */
	private Boolean flagModifiable;

	@OneToMany
	private List<Utilisateur> utilisateur = new ArrayList<>();

	public RefInstitution() {
		super();
	}

	public RefInstitution(Long idInstitution, Integer nature, Long idInstitutionSiege, Long idInstitutionRegionale,
			String raisonSociale, String ville, String adresse, Long idRepresantant, String codeInstitution,
			Boolean flagLocal, Boolean flagRegional, String abr, Boolean flagActif, Boolean flagModifiable,
			List<Utilisateur> utilisateur) {
		super();
		this.idInstitution = idInstitution;
		this.nature = nature;
		this.idInstitutionSiege = idInstitutionSiege;
		this.idInstitutionRegionale = idInstitutionRegionale;
		this.raisonSociale = raisonSociale;
		this.ville = ville;
		this.adresse = adresse;
		this.idRepresantant = idRepresantant;
		this.codeInstitution = codeInstitution;
		this.flagLocal = flagLocal;
		this.flagRegional = flagRegional;
		this.abr = abr;
		this.flagActif = flagActif;
		this.flagModifiable = flagModifiable;
		this.utilisateur = utilisateur;
	}

	public RefInstitution(Long idInstitution, Integer nature, Long idInstitutionSiege, String raisonSociale,
			String ville, String adresse, Long idRepresantant, String codInstitution, String abr) {
		this.idInstitution = idInstitution;
		this.nature = nature;
		this.idInstitutionSiege = idInstitutionSiege;
		this.raisonSociale = raisonSociale;
		this.ville = ville;
		this.adresse = adresse;
		this.idRepresantant = idRepresantant;
		this.codeInstitution = codInstitution;
		this.abr = abr;
	}

	public RefInstitution(Long idInstitution, Integer nature, String codeInstitution) {
		this.idInstitution = idInstitution;
		this.nature = nature;
		this.codeInstitution = codeInstitution;
	}

	public RefInstitution(RefInstitution refInstitution) {
		super();
		this.idInstitution = refInstitution.getIdInstitution();
		this.nature = refInstitution.getNature();
		this.idInstitutionSiege = refInstitution.getIdInstitutionSiege();
		this.raisonSociale = refInstitution.getRaisonSociale();
		this.ville = refInstitution.getVille();
		this.adresse = refInstitution.getAdresse();
		this.idRepresantant = refInstitution.getIdRepresantant();
		this.codeInstitution = refInstitution.getCodeInstitution();
		this.flagLocal = refInstitution.getFlagLocal();
		this.abr = refInstitution.getAbr();
		this.flagActif = refInstitution.getFlagActif();
		this.flagModifiable = refInstitution.getFlagModifiable();
	}

	public RefInstitution(Long idInstitution, Integer nature, Long idInstitutionSiege, String raisonSociale,
			String ville, String adresse, Long idRepresantant, String codeInstitution) {
		this.idInstitution = idInstitution;
		this.nature = nature;
		this.idInstitutionSiege = idInstitutionSiege;
		this.raisonSociale = raisonSociale;
		this.ville = ville;
		this.adresse = adresse;
		this.idRepresantant = idRepresantant;
		this.codeInstitution = codeInstitution;

	}

	public Long getIdInstitution() {
		return idInstitution;
	}

	public void setIdInstitution(Long idInstitution) {
		this.idInstitution = idInstitution;
	}

	public Integer getNature() {
		return nature;
	}

	public void setNature(Integer nature) {
		this.nature = nature;
	}

	public Long getIdInstitutionSiege() {
		return idInstitutionSiege;
	}

	public void setIdInstitutionSiege(Long idInstitutionSiege) {
		this.idInstitutionSiege = idInstitutionSiege;
	}

	public Long getIdInstitutionRegionale() {
		return idInstitutionRegionale;
	}

	public void setIdInstitutionRegionale(Long idInstitutionRegionale) {
		this.idInstitutionRegionale = idInstitutionRegionale;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Long getIdRepresantant() {
		return idRepresantant;
	}

	public void setIdRepresantant(Long idRepresantant) {
		this.idRepresantant = idRepresantant;
	}

	public String getCodeInstitution() {
		return codeInstitution;
	}

	public void setCodeInstitution(String codeInstitution) {
		this.codeInstitution = codeInstitution;
	}

	public Boolean getFlagLocal() {
		return flagLocal;
	}

	public void setFlagLocal(Boolean flagLocal) {
		this.flagLocal = flagLocal;
	}

	public Boolean getFlagRegional() {
		return flagRegional;
	}

	public void setFlagRegional(Boolean flagRegional) {
		this.flagRegional = flagRegional;
	}

	public String getAbr() {
		return abr;
	}

	public void setAbr(String abr) {
		this.abr = abr;
	}

	public Boolean getFlagActif() {
		return flagActif;
	}

	public void setFlagActif(Boolean flagActif) {
		this.flagActif = flagActif;
	}

	public Boolean getFlagModifiable() {
		return flagModifiable;
	}

	public void setFlagModifiable(Boolean flagModifiable) {
		this.flagModifiable = flagModifiable;
	}

	public List<Utilisateur> getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(List<Utilisateur> utilisateur) {
		this.utilisateur = utilisateur;
	}

}
