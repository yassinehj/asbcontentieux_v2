package com.fr.adaming.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * Une Phase  est caractérisé par les informations suivantes :
 * <ul>
 * <li>id_phase, nom, date_debut, date_fin, reference_tribunal, qualite_client, status</li>
 * </ul>
 * @author nbelhajyahia
 * @since 01-04-2019
 */
@Table(name="PHASE")
@Entity
public class Phase implements Serializable {
    private static final long serialVersionUID = 1L;
    /** L 'id */
    @Id
    @Column(name = "ID_PHASE")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idPhase;
    /** nom du phase*/
    @Column(name = "NOM")
    private String nom;
    /** date  debut*/
    @Column(name = "DATE_DEBUT")
    private Date date_debut;
    /** date fin*/
    @Column(name = "DATE_FIN")
    private Date date_fin;
    /** reference tribunal*/
    @Column(name = "REFERENCE_TRIBUNAL")
    private String reference_tribunal;
    /** qualite client*/
    @Column(name = "Qualite_CLIENT")
    private String qualite_client;
    /** status*/
    @Column(name = "STATUS")
    private String status;

    @OneToMany(mappedBy="phase",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Audience> audiences = new ArrayList<>();

    @OneToMany(mappedBy="phase",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Tache> taches = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="ID_TRIBUNAL")
    private Tribunal tribunal;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Phase_Auxiliere",
            joinColumns = { @JoinColumn(name = "ID_PHASE") },
            inverseJoinColumns = { @JoinColumn(name = "ID_AUXILIERE") })
    private List<Auxiliaire> auxilieres = new ArrayList<>();
    
    
    @ManyToOne
    @JoinColumn(name="ID_AFFAIRE")
    private Affaire affaire;


    public Long getIdPhase() { return idPhase; }

    public void setIdPhase(Long idPhase) { this.idPhase = idPhase; }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public Date getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }


    public Date getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }


    public String getReference_tribunal() {
        return reference_tribunal;
    }

    public void setReference_tribunal(String reference_tribunal) {
        this.reference_tribunal = reference_tribunal;
    }


    public String getQualite_client() {
        return qualite_client;
    }

    public void setQualite_client(String qualite_client) {
        this.qualite_client = qualite_client;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


	public List<Audience> getAudiences() {
		return audiences;
	}

	public void setAudiences(List<Audience> audiences) {
		this.audiences = audiences;
	}


	public List<Tache> getTaches() {
		return taches;
	}

	public void setTaches(List<Tache> taches) {
		this.taches = taches;
	}


	public Tribunal getTribunal() {
		return tribunal;
	}

	public void setTribunal(Tribunal tribunal) {
		this.tribunal = tribunal;
	}


	public List<Auxiliaire> getAuxilieres() {
		return auxilieres;
	}

	public void setAuxilieres(List<Auxiliaire> auxilieres) {
		this.auxilieres = auxilieres;
	}


	public Affaire getAffaire() {
		return affaire;
	}

	public void setAffaire(Affaire affaire) {
		this.affaire = affaire;
	}


    public Phase() {
    }

    public Phase(String nom, Date date_debut, Date date_fin, String reference_tribunal, String qualite_client, String status, List<Audience> audiences, List<Tache> taches, Tribunal tribunal, List<Auxiliaire> auxilieres, Affaire affaire) {
        this.nom = nom;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.reference_tribunal = reference_tribunal;
        this.qualite_client = qualite_client;
        this.status = status;
        this.audiences = audiences;
        this.taches = taches;
        this.tribunal = tribunal;
        this.auxilieres = auxilieres;
        this.affaire = affaire;
    }

    @Override
    public String toString() {
        return "Phase{" +
                "idPhase=" + idPhase +
                ", nom='" + nom + '\'' +
                ", date_debut=" + date_debut +
                ", date_fin=" + date_fin +
                ", reference_tribunal='" + reference_tribunal + '\'' +
                ", qualite_client='" + qualite_client + '\'' +
                ", status='" + status + '\'' +
                ", audiences=" + audiences +
                ", taches=" + taches +
                ", tribunal=" + tribunal +
                ", auxilieres=" + auxilieres +
                ", affaire=" + affaire +
                '}';
    }
}
