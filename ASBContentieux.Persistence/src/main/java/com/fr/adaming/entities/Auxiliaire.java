package com.fr.adaming.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * Un Auxiliaire est caractérisé par les informations suivantes :
 * <ul>
 * <li>id_auxiliere, nom, prenom, email,numTel,specialite, type, numTel2</li>
 * </ul>
 * 
 * @author nbelhajyahia
 * @since 01-04-2019
 */
@Table(name = "AUXILIARE")
@Entity
public class Auxiliaire implements Serializable {

	private static final long serialVersionUID = 1L;
	/** id */
	@Id
	@Column(name = "ID_AUXILIERE")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idAuxiliaire;
	/** nom */
	@Column(name = "NOM")
	private String nom;
	/** prenom */
	@Column(name = "PRENOM")
	private String prenom;
	/** email */
	@Column(name = "EMAIL")
	private String email;
	/** numero telephone */
	@Column(name = "NUMTEL")
	private String numTel;
	/** specialite */
	@Column(name = "SPECIALITE")
	private String specialite;
	/** type */
	@Column(name = "TYPE")
	private String type;
	/** numero telephone 2 */
	@Column(name = "NUMTEL2")
	private String numTel2;

	@ManyToMany(mappedBy = "auxilieres")
	private List<Phase> phases;

	public Long getIdAuxiliaire() {
		return idAuxiliaire;
	}

	public void setIdAuxiliaire(Long idAuxiliaire) {
		this.idAuxiliaire = idAuxiliaire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumTel2() {
		return numTel2;
	}

	public void setNumTel2(String numTel2) {
		this.numTel2 = numTel2;
	}

	public List<Phase> getPhases() {
		return phases;
	}

	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}

	public Auxiliaire() {
	}

	public Auxiliaire(Long idAuxiliaire, String nom, String prenom, String email, String numTel, String specialite,
					  String type, String numTel2) {
		this.idAuxiliaire = idAuxiliaire;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.numTel = numTel;
		this.specialite = specialite;
		this.type = type;
		this.numTel2 = numTel2;
	}

	@Override
	public String toString() {
		return "Auxiliere{" + "idAuxiliaire=" + idAuxiliaire + ", nom='" + nom + '\'' + ", prenom='" + prenom + '\''
				+ ", email='" + email + '\'' + ", numTel='" + numTel + '\'' + ", specialite='" + specialite + '\''
				+ ", type='" + type + '\'' + ", numTel2='" + numTel2 + '\'' + '}';
	}
}
