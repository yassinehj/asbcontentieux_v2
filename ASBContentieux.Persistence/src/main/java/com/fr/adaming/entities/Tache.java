package com.fr.adaming.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fr.adaming.enumeration.State;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * Une Tache Audience est caractérisé par les informations suivantes :
 * <ul>
 * <li>id_tache, titre, description, date_debut, date_fin, duree_notif, statut</li>
 * </ul>
 * @author nbelhajyahia
 * @since 01-04-2019
 */
@Table(name="TACHE")
@Entity
public class Tache implements Serializable {

    private static final long serialVersionUID = 1L;
    /** id*/
    @Id
    @Column(name="ID_TACHE")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idTache;
    /** titre*/
    @Column(name="TITRE")
    private String titre;
    /** description*/
    @Column(name="DESCRIPTION")
    private String description;
    /** date debut*/
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DATE_DEBUT")
    private Date dateDebut;
    /** date fin*/
    @Column(name="DATE_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFin;
    /** duree du  notification*/
    @Column(name="DUREE_NOTIF")
    private Float dureeNotif;
    /** statut*/
    @Column(name="STATUT")
    private String statut;


    @Column(name="ETAT")
    private State etat;
    @ManyToOne
    @JoinColumn(name="ID_PHASE")
    private Phase phase;

    @ManyToOne
    @JoinColumn(name="ID_USER")
    private Utilisateur utilisateur;

    public Long getIdTache() { return idTache; }

    public void setIdTache(Long idTache) { this.idTache = idTache; }


    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }


    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }


    public Float getDureeNotif() {
        return dureeNotif;
    }

    public void setDureeNotif(Float dureeNotif) {
        this.dureeNotif = dureeNotif;
    }


    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }


    public Phase getPhase() { return phase; }

    public void setPhase(Phase phase) { this.phase = phase; }

    public State getEtat() {
        return etat;
    }

    public void setEtat(State etat) {
        this.etat = etat;
    }


    public Utilisateur getUtilisateur() { return utilisateur; }

    public void setUtilisateur(Utilisateur utilisateur) { this.utilisateur = utilisateur; }


    public Tache() {
    }

    public Tache(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Tache(String titre, String description, Date dateDebut, Date dateFin, Float dureeNotif, String statut, Phase phase, Utilisateur utilisateur,State etat) {
        this.titre = titre;
        this.description = description;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dureeNotif = dureeNotif;
        this.statut = statut;
        this.phase = phase;
        this.etat=etat;
        this.utilisateur = utilisateur;
    }

    @Override
    public String toString() {
        return "Tache{" +
                "idTache=" + idTache +
                ", titre='" + titre + '\'' +
                ", description='" + description + '\'' +
                ", dateDebut=" + dateDebut +
                ", dateFin=" + dateFin +
                ", dureeNotif=" + dureeNotif +
                ", statut='" + statut + '\'' +
                ", etat=" + etat +
                ", phase=" + phase +
                ", utilisateur=" + utilisateur +
                '}';
    }
}
