package com.fr.adaming.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * <p>
 * Un honnoraire est caractérisé par les informations suivantes :
 * <ul>
 * <li>idHonnoraire,montant,type de Paiement,date Honnoraire,type Operation,paye</li>
 * </ul>
 * @author azaaboub
 * @since 29/03/2019
 */

@Entity
@Table(name="HONNORAIRE")
public class Honnoraire implements Serializable {
	
	
	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_HONNORAIRE", nullable=false)
	private Long idHonnoraire ;
	
	
	@Column(name="MONTANT")
	private double montant ;
	
	@Column(name="TYPE_Paiement")
	private String typePaiement;
	
	@Column(name="Titre")
	private String titre;
	
	@Column(name="DATE_HONORAIRE")
	private Date dateHonnoraire;
	
	@Column(name="TYPE_OPERATION")
	private String typeOperation;
	
	@Column(name="PAYE")
	private Boolean paye;
	
	@ManyToOne
	@JoinColumn(name="ID_JUSTIF_HONORAIRE")
	private JustifHonoraire justifHonoraire;
	
	@ManyToOne
	@JoinColumn(name="ID_UTILISATEUR")
	private Utilisateur utilisateur;
	
	
	@ManyToOne
	@JoinColumn(name="ID_AFFAIRE")
	private Affaire affaire;
	
	/**
	 * Gets the idHonnoraire
	 * @return idHonnoraire
	 */
	public Long getIdHonnoraire() {
		return idHonnoraire;
	}
	
	/**
	 * sets the idHonnoraire
	 * @param idHonnoraire
	 */

	public void setIdHonnoraire(Long idHonnoraire) {
		this.idHonnoraire = idHonnoraire;
	}
	
	/**
	 * Gets the montant
	 * @return montant
	 */

	public double getMontant() {
		return montant;
	}
	
	/**
	 * sets the montant
	 * @param montant
	 */

	public void setMontant(double montant) {
		this.montant = montant;
	}

	/**
	 * Gets the typepaiement
	 * @return type paiement
	 */
	public String getTypePaiement() {
		return typePaiement;
	}
	
	/**
	 * sets the typePaiement
	 * @param typePaiement
	 */

	public void setTypePaiement(String typePaiement) {
		this.typePaiement = typePaiement;
	}
	
	/**
	 * Gets the titre
	 * @return titre
	 */

	public String getTitre() {
		return titre;
	}
	
	/**
	 * Sets titre
	 * @param titre
	 */

	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	/**
	 * Gests dateHonnoraire
	 * @return dateHonnoraire
	 */

	public Date getDateHonnoraire() {
		return dateHonnoraire;
	}
	
	/**
	 * Sets dateHonnoraire
	 * @param dateHonnoraire
	 */

	public void setDateHonnoraire(Date dateHonnoraire) {
		this.dateHonnoraire = dateHonnoraire;
	}

	
	/**
	 * Gets typeOperation
	 * @return typeOperation
	 */
	public String getTypeOperation() {
		return typeOperation;
	}
	
	/**
	 * Sets typeOperation
	 * @param typeOperation
	 */

	public void setTypeOperation(String typeOperation) {
		this.typeOperation = typeOperation;
	}

	/**
	 * Gets paye
	 * @return paye
	 */
	public Boolean getPaye() {
		return paye;
	}

	
	/**
	 * Sets paye
	 * @param paye
	 */
	public void setPaye(Boolean paye) {
		this.paye = paye;
	}

	public JustifHonoraire getJustifHonoraire() {
		return justifHonoraire;
	}

	public void setJustifHonoraire(JustifHonoraire justifHonoraire) {
		this.justifHonoraire = justifHonoraire;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	

	
	public Affaire getAffaire() {
		return affaire;
	}

	public void setAffaire(Affaire affaire) {
		this.affaire = affaire;
	}

	public Honnoraire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Honnoraire(Long idHonnoraire, double montant, String typePaiement, String titre, Date dateHonnoraire,
			String typeOperation, Boolean paye, JustifHonoraire justifHonoraire, Utilisateur utilisateur,
			Affaire affaire) {
		super();
		this.idHonnoraire = idHonnoraire;
		this.montant = montant;
		this.typePaiement = typePaiement;
		this.titre = titre;
		this.dateHonnoraire = dateHonnoraire;
		this.typeOperation = typeOperation;
		this.paye = paye;
		this.justifHonoraire = justifHonoraire;
		this.utilisateur = utilisateur;
		this.affaire = affaire;
	}

	@Override
	public String toString() {
		return "Honnoraire [idHonnoraire=" + idHonnoraire + ", montant=" + montant + ", typePaiement=" + typePaiement
				+ ", titre=" + titre + ", dateHonnoraire=" + dateHonnoraire + ", typeOperation=" + typeOperation
				+ ", paye=" + paye + ", justifHonoraire=" + justifHonoraire + ", utilisateur=" + utilisateur
				+ ", affaire=" + affaire + "]";
	}

	
	
	
	
	
	

}
