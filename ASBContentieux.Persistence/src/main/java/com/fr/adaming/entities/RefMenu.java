package com.fr.adaming.entities;

import com.fr.adaming.entities.RefModule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * <b>RefMenu est la classe représentant un ecran dans l'application.</b>
 * <p>
 * Un RefMenu est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un identifiant, unique attribué définitivement.</li>
 * <li>Un nom, Il existe en français , anglais , arabe.</li>
 * <li>Un ordre, ce menu sera affiché dans un ordre bien précis.</li>
 * <li>Un url.</li>
 * <li>Un "module", ce menu appartient à un et un seul module.</li>
 * </ul>
 * 
 * @see RefModule
 * @author mbouhdida
 */
@Entity
@Table(name = "REF_Menu")
public class RefMenu {

	/** L'id menu. */

	@Id
	private Long idMenu;

	/** Le nom menu. */

	@Column(name = "NOM_MENU", length = 50)
	private String nomMenu;

	/** Le nom menu en. */

	@Column(name = "NOM_MENU_EN", length = 50)
	private String nomMenuEn;

	/** Le nom menu ar. */

	@Column(name = "NOM_MENU_AR", length = 50)
	private String nomMenuAr;

	/** Le rang. */

	@Column(name = "RANG", precision = 22, scale = 0)
	private Long rang;

	/** L'action menu. */

	@Column(name = "ACTION_MENU", length = 50)
	private String actionMenu;

	/** L'ecran pere. */

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ECRAN_PER", nullable = true)
	private RefMenu ecranPere;

	/** Le ref module. */

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idModule", nullable = false)
	private RefModule refModule;

	/** L'icon ecran. */

	@Column(name = "icon_ecran", length = 50)
	private String iconEcran;

	/** Le path image. */

	@Column(name = "PATH_IMAGE", length = 50)
	private String pathImage;

	/** Le color. */

	@Column(name = "COLOR", length = 50)
	private String color;

	public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}

	public String getNomMenu() {
		return nomMenu;
	}

	public void setNomMenu(String nomMenu) {
		this.nomMenu = nomMenu;
	}

	public String getNomMenuEn() {
		return nomMenuEn;
	}

	public void setNomMenuEn(String nomMenuEn) {
		this.nomMenuEn = nomMenuEn;
	}

	public String getNomMenuAr() {
		return nomMenuAr;
	}

	public void setNomMenuAr(String nomMenuAr) {
		this.nomMenuAr = nomMenuAr;
	}

	public Long getRang() {
		return rang;
	}

	public void setRang(Long rang) {
		this.rang = rang;
	}

	public String getActionMenu() {
		return actionMenu;
	}

	public void setActionMenu(String actionMenu) {
		this.actionMenu = actionMenu;
	}

	public RefMenu getEcranPere() {
		return ecranPere;
	}

	public void setEcranPere(RefMenu ecranPere) {
		this.ecranPere = ecranPere;
	}

	public String getIconEcran() {
		return iconEcran;
	}

	public void setIconEcran(String iconEcran) {
		this.iconEcran = iconEcran;
	}

	public String getPathImage() {
		return pathImage;
	}

	public void setPathImage(String pathImage) {
		this.pathImage = pathImage;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public RefModule getRefModule() {
		return refModule;
	}

	public void setRefModule(RefModule refModule) {
		this.refModule = refModule;
	}

}
