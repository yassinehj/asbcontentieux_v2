package com.fr.adaming.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author azaaboub
 *@since 02/04/2019
 */

@Entity
@Table(name="GOUVERNORAT")
public class Gouvernorat implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_GOUVERNORAT", nullable=false)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long idGouvernorat;

	@Column(name = "NOM")
	private String nom;
	
	@OneToMany(mappedBy="gouvernorat",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Ville> villes = new ArrayList<>();
	
	 @ManyToOne
	 @JoinColumn(name="ID_PAYS")
	 private Pays pays;

	public Long getIdGouvernorat() {
		return idGouvernorat;
	}

	public void setIdGouvernorat(Long idGouvernorat) {
		this.idGouvernorat = idGouvernorat;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public List<Ville> getVilles() {
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

	public Gouvernorat(String nom, List<Ville> villes, Pays pays) {
		this.nom = nom;
		this.villes = villes;
		this.pays = pays;
	}

	public Gouvernorat(String nom) {
		this.nom = nom;
	}

	public Gouvernorat(String nom, Pays pays) {
		this.nom = nom;
		this.pays = pays;
	}

	public Gouvernorat() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Gouvernorat{" +
				"idGouvernorat=" + idGouvernorat +
				", nom='" + nom + '\'' +
				", villes=" + villes +
				", pays=" + pays +
				'}';
	}
}
