package com.fr.adaming.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <b>RefModule est la classe représentant un module dans l'application.</b>
 * <p>
 * Un RefModule est caractérisé par les informations suivantes :
 * <ul>
 * <li>Unidentifiant, unique attribué définitivement.</li>
 * <li>Une liste des menus.</li>
 * <li>Un nom, Il existe en français , anglais , arabe.</li>
 * <li>Un ordre, ce module sera affiché dans un ordre bien précis.</li>
 * <li>Un url d'image.</li>
 * </ul>
 * 
 * @see RefMenu
 * @author mbouhdida
 */
@Entity
@Table(name = "REF_MODULE")
public class RefModule {

	/** L'id module. */

	@Id
	private Long idModule;

	/** Ref menu. */

	@JsonIgnore
	@OneToMany(mappedBy = "refModule", cascade = CascadeType.ALL)
	private List<RefMenu> refMenus;

	/** Le nom module. */

	@Column(name = "NOM_MODULE", length = 50)
	private String nomModule;

	/** Le nom module en. */

	@Column(name = "NOM_MODULE_EN", length = 50)
	private String nomModuleEn;

	/** Le nom module ar. */

	@Column(name = "NOM_MODULE_AR", length = 50)
	private String nomModuleAr;

	/** Le path image. */

	@Column(name = "PATH_IMAGE", length = 50)
	private String pathImage;

	/** Le rang. */

	@Column(name = "RANG", precision = 22, scale = 0)
	private Integer rang;

	public Long getIdModule() {
		return idModule;
	}

	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}

	public List<RefMenu> getRefMenus() {
		return refMenus;
	}

	public void setRefMenus(List<RefMenu> refMenus) {
		this.refMenus = refMenus;
	}

	public String getNomModule() {
		return nomModule;
	}

	public void setNomModule(String nomModule) {
		this.nomModule = nomModule;
	}

	public String getNomModuleEn() {
		return nomModuleEn;
	}

	public void setNomModuleEn(String nomModuleEn) {
		this.nomModuleEn = nomModuleEn;
	}

	public String getNomModuleAr() {
		return nomModuleAr;
	}

	public void setNomModuleAr(String nomModuleAr) {
		this.nomModuleAr = nomModuleAr;
	}

	public String getPathImage() {
		return pathImage;
	}

	public void setPathImage(String pathImage) {
		this.pathImage = pathImage;
	}

	public Integer getRang() {
		return rang;
	}

	public void setRang(Integer rang) {
		this.rang = rang;
	}

}
