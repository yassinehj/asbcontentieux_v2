package com.fr.adaming.entities;


import javax.persistence.*;
import java.util.Date;

/**
 * 
 * @author hhboumaiza
 *
 */

@Entity
@Table(name = "HISTORIQUE_RECOUVEREMENT")
public class HistoriqueRecouverement {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_HISTORIQUE_RECOUVREMENT", nullable=false)
	private Long idHistoriqueRecouverement;
	@Column(name = "NOM")
	private String nom;

	@Column(name = "CREATION_TIME")
	private Date creationTime;

	@Column(name = "END_TIME")
	private Date endTime;

	@Column(name = "DESCRIPTION_PROLONGATION")
	private String descriptionProlongation;

	@Column(name = "DURATION_PROLONGATION")
	private Long durationProlongation;

	@Column(name = "LETTRE_SEVERE")
	private boolean lettreSevere;

	@Column(name = "LETTRE_COMMERCIAL")
	private boolean lettreCommercial;

	@Column(name = "APPEL")
	private boolean appel;

	@Column(name = "SOMMATION")
	private boolean sommation;

	@Column(name = "COMITE")
	private boolean comite;

	@Column(name = "AGENT")
	private String agent;

	@Column(name = "ID_RAPPORT_HN")
	private String idRapportHn;

	@Column(name = "ID_PROCESS")
	private String idProcess;

	@Column(name = "ID_DOSSIER")
	private Long idDossier;

	@ManyToOne
	@JoinColumn(name = "DOSSIER_PREC_ID", nullable = false)
	private DossierPrec dossierPrec;

	/** L'id. */
	public Long getIdHistoriqueRecouverement() {
		return idHistoriqueRecouverement;
	}

	public void setIdHistoriqueRecouverement(Long idHistoriqueRecouverement) {
		this.idHistoriqueRecouverement = idHistoriqueRecouverement;
	}

	/** Le nom */
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * temps de creation
	 */
	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * heure de fin
	 */
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * la description du prolongation
	 */
	public String getDescriptionProlongation() {
		return descriptionProlongation;
	}

	public void setDescriptionProlongation(String descriptionProlongation) {
		this.descriptionProlongation = descriptionProlongation;
	}

	/**
	 * la durée du prolongation
	 */
	public Long getDurationProlongation() {
		return durationProlongation;
	}

	public void setDurationProlongation(Long durationProlongation) {
		this.durationProlongation = durationProlongation;
	}

	/**
	 * la lettre severe
	 */
	public boolean isLettreSevere() {
		return lettreSevere;
	}

	public void setLettreSevere(boolean lettreSevere) {
		this.lettreSevere = lettreSevere;
	}

	/**
	 * la lettre commercial
	 */

	public boolean isLettreCommercial() {
		return lettreCommercial;
	}

	public void setLettreCommercial(boolean lettreCommercial) {
		this.lettreCommercial = lettreCommercial;
	}

	/**
	 * appel
	 */
	public boolean isAppel() {
		return appel;
	}

	public void setAppel(boolean appel) {
		this.appel = appel;
	}

	/**
	 * la sommation
	 */
	public boolean isSommation() {
		return sommation;
	}

	public void setSommation(boolean sommation) {
		this.sommation = sommation;
	}

	/**
	 * le commite
	 */

	public boolean isComite() {
		return comite;
	}

	public void setComite(boolean comite) {
		this.comite = comite;
	}

	/**
	 * l'agent
	 */
	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	 * L'id rapport
	 * 
	 * @return
	 */
	public String getIdRapportHn() {
		return idRapportHn;
	}

	public void setIdRapportHn(String idRapportHn) {
		this.idRapportHn = idRapportHn;
	}

	/**
	 * L'id du process
	 * 
	 * @return
	 */
	public String getIdProcess() {
		return idProcess;
	}

	public void setIdProcess(String idProcess) {
		this.idProcess = idProcess;
	}

	public Long getIdDossier() {
		return idDossier;
	}

	/**
	 * L'id du dossier
	 * 
	 * @param idDossier
	 */
	public void setIdDossier(Long idDossier) {
		this.idDossier = idDossier;
	}

	/**
	 * un object de type dossier precententieux
	 * 
	 * @return
	 */
	public DossierPrec getDossierPrec() {
		return dossierPrec;
	}

	public void setDossierPrec(DossierPrec dossierPrec) {
		this.dossierPrec = dossierPrec;
	}

	/**
	 * constructeur vide
	 */
	public HistoriqueRecouverement() {
		super();
	}

	/**
	 * 
	 * @param id
	 * @param nom
	 * @param creationTime
	 * @param endTime
	 * @param descriptionProlongation
	 * @param durationProlongation
	 * @param lettreSevere
	 * @param lettreCommercial
	 * @param appel
	 * @param sommation
	 * @param comite
	 * @param agent
	 * @param idRapportHn
	 * @param idProcess
	 * @param idDossier
	 * @param dossierPrec
	 */

	/**
	 * constructeur parametrés
	 */
	public HistoriqueRecouverement(String nom, Date creationTime, Date endTime, String descriptionProlongation, Long durationProlongation, boolean lettreSevere, boolean lettreCommercial, boolean appel, boolean sommation, boolean comite, String agent, String idRapportHn, String idProcess, Long idDossier, DossierPrec dossierPrec) {
		this.nom = nom;
		this.creationTime = creationTime;
		this.endTime = endTime;
		this.descriptionProlongation = descriptionProlongation;
		this.durationProlongation = durationProlongation;
		this.lettreSevere = lettreSevere;
		this.lettreCommercial = lettreCommercial;
		this.appel = appel;
		this.sommation = sommation;
		this.comite = comite;
		this.agent = agent;
		this.idRapportHn = idRapportHn;
		this.idProcess = idProcess;
		this.idDossier = idDossier;
		this.dossierPrec = dossierPrec;
	}

	@Override
	public String toString() {
		return "HistoriqueRecouverement{" +
				"idHistoriqueRecouverement=" + idHistoriqueRecouverement +
				", nom='" + nom + '\'' +
				", creationTime=" + creationTime +
				", endTime=" + endTime +
				", descriptionProlongation='" + descriptionProlongation + '\'' +
				", durationProlongation=" + durationProlongation +
				", lettreSevere=" + lettreSevere +
				", lettreCommercial=" + lettreCommercial +
				", appel=" + appel +
				", sommation=" + sommation +
				", comite=" + comite +
				", agent='" + agent + '\'' +
				", idRapportHn='" + idRapportHn + '\'' +
				", idProcess='" + idProcess + '\'' +
				", idDossier=" + idDossier +
				'}';
	}
}
