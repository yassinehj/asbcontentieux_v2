package com.fr.adaming.entities;

import com.fr.adaming.entities.ProfileID;
import com.fr.adaming.entities.Role;
import com.fr.adaming.entities.Utilisateur;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * <b>Profile est la classe représentant un profil d'un untilisateur de
 * l'application.</b>
 * <p>
 * Un profil est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un identifiant, il est le couple roleId de la table <b> Role</b> et
 * userId de la table <b> Utilisateur </b> .</li>
 * <li>Un nom, unique attribué définitivement.</li>
 * </ul>
 * 
 * @see Role
 * @see Utilisateur
 * @see ProfileID
 * @author mbouhdida
 */
@Entity
@Table(name = "PROFILE")
public class Profile {

	/** L'id. */

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "roleId", column = @Column(name = "ROLE_ID", nullable = false, length = 50)),
			@AttributeOverride(name = "userId", column = @Column(name = "USER_ID", nullable = false, precision = 8, scale = 0)) })
	@Column(name = "ID_PROFILE", unique = true, nullable = false, precision = 22, scale = 0)
	private ProfileID id;

	/** Le nom profile. */

	@Column(name = "NOM_PROFILE", length = 50)
	private String nomProfile;

	/** L'utilisateur. */

	@ManyToOne
	@JoinColumn(name = "USER_ID", nullable = false, insertable = false, updatable = false)
	private Utilisateur user;

	/** Le role. */
	
	@ManyToOne
	@JoinColumn(name = "ROLE_ID", nullable = false, insertable = false, updatable = false)
	private Role role;

	public Profile() {
		super();
	}

	public Profile(ProfileID id, String nomProfile, Utilisateur user, Role role) {
		super();
		this.id = id;
		this.nomProfile = nomProfile;
		this.user = user;
		this.role = role;
	}

	public ProfileID getId() {
		return id;
	}

	public void setId(ProfileID id) {
		this.id = id;
	}

	public String getNomProfile() {
		return nomProfile;
	}

	public void setNomProfile(String nomProfile) {
		this.nomProfile = nomProfile;
	}

	public Utilisateur getUser() {
		return user;
	}

	public void setUser(Utilisateur user) {
		this.user = user;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
