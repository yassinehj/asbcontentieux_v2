package com.fr.adaming.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author hhboumaiza
 *
 */
@Entity
@Table(name="CONFIGURATION")
public class Configuration implements Serializable{
	private static final long serialVersionUID = 1L;
	/** L 'id */
	@Id
	@Column(name = "ID_CONFIGURATION", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idConfiguration;
	/** Le nom du configuration */
	@Column(name="NOM")
    private String nom ;
	/** le nom arabe du configuration */
	@Column(name="NOM_AR")
    private String nom_ar;
	/** Le type */
	@Column(name="TYPE")
	private String type;
	public Long getIdConfiguration() {
		return idConfiguration;
	}
	public void setIdConfiguration(Long idConfiguration) {
		this.idConfiguration = idConfiguration;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getNom_ar() {
		return nom_ar;
	}
	public void setNom_ar(String nom_ar) {
		this.nom_ar = nom_ar;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Configuration(Long idConfiguration, String nom, String nom_ar, String type) {
		super();
		this.idConfiguration = idConfiguration;
		this.nom = nom;
		this.nom_ar = nom_ar;
		this.type = type;
	}
	@Override
	public String toString() {
		return "Configuration [idConfiguration=" + idConfiguration + ", nom=" + nom + ", nom_ar=" + nom_ar + ", type="
				+ type + "]";
	}
	public Configuration() {
		super();
	}
	
    
	
}
