package com.fr.adaming.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author hhboumaiza
 * @since 01-04-2019
 */

@Entity
@Table(name = "AGENCE")
public class Agence {

	/** L'id. */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ID_INSTITUTION_SIEGE")
	private String idInstitutionSiege;

	@Column(name = "ID_INSTITUTION_REGIONALE")
	private Long idInstitutionRegionale;

	@Column(name = "RAISON_SOCIAL")
	private String raisonSocial;

	@Column(name = "ID_REPRESANTANT")
	private Long idRepresantant;

	@Column(name = "CODE_INSTITUTION")
	private String codeInstitution;

	@Column(name = "NATURE")
	private String nature;

	@Column(name = "NUM_TEL_RESPO")
	private String numTelRespo;

	@Column(name = "EMAIL_RESPO")
	private String emailRespo;

	@OneToMany(mappedBy = "agence", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<DossierPrec> dossierPrec = new ArrayList<>();
	@OneToOne(cascade = CascadeType.ALL)
	private Adresse adresse;

	/**
	 * L'id
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * L'id de l'institution du siège
	 * 
	 * @return
	 */
	public String getIdInstitutionSiege() {
		return idInstitutionSiege;
	}

	public void setIdInstitutionSiege(String idInstitutionSiege) {
		this.idInstitutionSiege = idInstitutionSiege;
	}

	/**
	 * L'id de l'institution regionale
	 * 
	 * @return
	 */
	public Long getIdInstitutionRegionale() {
		return idInstitutionRegionale;
	}

	public void setIdInstitutionRegionale(Long idInstitutionRegionale) {
		this.idInstitutionRegionale = idInstitutionRegionale;
	}

	/**
	 * Le raison social
	 * 
	 * @return
	 */

	public String getRaisonSocial() {
		return raisonSocial;
	}

	public void setRaisonSocial(String raisonSocial) {
		this.raisonSocial = raisonSocial;
	}

	/**
	 * L'id du representant
	 * 
	 * @return
	 */
	public Long getIdRepresantant() {
		return idRepresantant;
	}

	public void setIdRepresantant(Long idRepresantant) {
		this.idRepresantant = idRepresantant;
	}

	/**
	 * Le code de l'institution
	 * 
	 * @return
	 */
	public String getCodeInstitution() {
		return codeInstitution;
	}

	public void setCodeInstitution(String codeInstitution) {
		this.codeInstitution = codeInstitution;
	}

	/**
	 * La nature
	 * 
	 * @return
	 */
	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	/**
	 * Le numéro tel reponsable
	 * 
	 * @return
	 */
	public String getNumTelRespo() {
		return numTelRespo;
	}

	public void setNumTelRespo(String numTelRespo) {
		this.numTelRespo = numTelRespo;
	}

	/**
	 * L'email du responsable
	 * 
	 * @return
	 */
	public String getEmailRespo() {
		return emailRespo;
	}

	public void setEmailRespo(String emailRespo) {
		this.emailRespo = emailRespo;
	}

	/**
	 * La liste des dossiers précontentieux
	 * 
	 * @return
	 */
	public List<DossierPrec> getDossierPrec() {
		return dossierPrec;
	}

	public void setDossierPrec(List<DossierPrec> dossierPrec) {
		this.dossierPrec = dossierPrec;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	/**
	 * constructeur vide
	 */
	public Agence() {
		super();
	}

	/**
	 * 
	 * @param id
	 * @param idInstitutionSiege
	 * @param idInstitutionRegionale
	 * @param raisonSocial
	 * @param idRepresantant
	 * @param codeInstitution
	 * @param nature
	 * @param numTelRespo
	 * @param emailRespo
	 * @param dossierPrec
	 */

	/**
	 * constructeur parametrés
	 */
	public Agence(Long id, String idInstitutionSiege, Long idInstitutionRegionale, String raisonSocial,
			Long idRepresantant, String codeInstitution, String nature, String numTelRespo, String emailRespo,
			List<DossierPrec> dossierPrec) {
		super();
		this.id = id;
		this.idInstitutionSiege = idInstitutionSiege;
		this.idInstitutionRegionale = idInstitutionRegionale;
		this.raisonSocial = raisonSocial;
		this.idRepresantant = idRepresantant;
		this.codeInstitution = codeInstitution;
		this.nature = nature;
		this.numTelRespo = numTelRespo;
		this.emailRespo = emailRespo;
		this.dossierPrec = dossierPrec;
	}

	public Agence(String idInstitutionSiege, String codeInstitution) {
		this.idInstitutionSiege = idInstitutionSiege;
		this.codeInstitution = codeInstitution;
	}

	public Agence(String idInstitutionSiege, String codeInstitution, Adresse adresse) {

		this.idInstitutionSiege = idInstitutionSiege;
		this.codeInstitution = codeInstitution;
		this.adresse = adresse;
	}

	@Override
	public String toString() {
		return "Agence{" +
				"id=" + id +
				", idInstitutionSiege='" + idInstitutionSiege + '\'' +
				", idInstitutionRegionale=" + idInstitutionRegionale +
				", raisonSocial='" + raisonSocial + '\'' +
				", idRepresantant=" + idRepresantant +
				", codeInstitution='" + codeInstitution + '\'' +
				", nature='" + nature + '\'' +
				", numTelRespo='" + numTelRespo + '\'' +
				", emailRespo='" + emailRespo + '\'' +
				", adresse=" + adresse +
				'}';
	}
}
