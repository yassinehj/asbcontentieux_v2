package com.fr.adaming.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "REF_JOUR_FERIE")
public class RefJourFerie implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6512526123689622913L;
	
	@Id
	@Column(name = "ID_JOUR_FERIE", unique = true, nullable = false, precision = 22, scale = 0)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idJourFerie;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_JOUR_FERIE", nullable = false, length = 7)
	private Date dateJourFerie;
	
	@Column(name = "LIBELLE_JOUR_FERIE", nullable = false, length = 50)
	private String libelleJourFerie;
	
	public RefJourFerie() {
		
	}

	public RefJourFerie(Long idJourferie, Date dateJourFerie, String libelleJourFerie) {
		super();
		this.idJourFerie = idJourferie;
		this.dateJourFerie = dateJourFerie;
		this.libelleJourFerie = libelleJourFerie;
	}
	
	public RefJourFerie(Date dateJourFerie, String libelleJourFerie) {
		super();
		this.dateJourFerie = dateJourFerie;
		this.libelleJourFerie = libelleJourFerie;
	}

	public Long getIdJourferie() {
		return idJourFerie;
	}

	public void setIdJourferie(Long idJourferie) {
		this.idJourFerie = idJourferie;
	}

	public Date getDateJourFerie() {
		return dateJourFerie;
	}

	public void setDateJourFerie(Date dateJourFerie) {
		this.dateJourFerie = dateJourFerie;
	}

	public String getLibelleJourFerie() {
		return libelleJourFerie;
	}

	public void setLibelleJourFerie(String libelleJourFerie) {
		this.libelleJourFerie = libelleJourFerie;
	}
	
	

}
