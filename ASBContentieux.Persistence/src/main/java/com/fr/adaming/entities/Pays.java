package com.fr.adaming.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author azaaboub,nbelhajyahia
 * @since 03-04-2019
 */
@Entity
@Table(name = "PAYS")
public class Pays implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_PAYS", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idPays;

	@Column(name = "NOM")
	private String nom;

	@JsonIgnore
	@OneToMany(mappedBy="pays",fetch=FetchType.LAZY, cascade = CascadeType.ALL)

	private List<Gouvernorat> gouvernorat = new ArrayList<Gouvernorat>();

	/**
	 *
	 * @param idPays
	 * @param nom
	 * @param gouvernorat
	 */
	public Pays(Long idPays, String nom, List<Gouvernorat> gouvernorat) {
		super();
		this.idPays = idPays;
		this.nom = nom;
		this.gouvernorat = gouvernorat;
	}

	public Pays(String nom) {
		this.nom = nom;
	}

	public Pays() {
		super();
	}

	public Long getIdPays() {
		return idPays;
	}

	public void setIdPays(Long idPays) {
		this.idPays = idPays;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Gouvernorat> getGouvernorat() {
		return gouvernorat;
	}

	public void setGouvernorat(List<Gouvernorat> gouvernorat) {
		this.gouvernorat = gouvernorat;
	}


	@Override
	public String toString() {
		return "Pays [idPays=" + idPays + ", nom=" + nom + ", gouvernorat=" + gouvernorat + "]";
	}

}
