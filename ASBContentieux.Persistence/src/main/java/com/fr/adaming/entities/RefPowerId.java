package com.fr.adaming.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * <b>RefPowerId represente l'identifiant des la classe RefPower .</b>
 * <p>
 * Un RefPowerId est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un idRefMenu.</li>
 * <li>Un idRole creation.</li>
 * </ul>
 * 
 * @author mbouhdida
 */
@Embeddable
public class RefPowerId implements Serializable {

	private static final long serialVersionUID = 1L;

	/** L'id ref menu. */ 
	
	@Column(name = "MENU_ID", nullable = false, precision = 8, scale = 0)
	private Long idRefMenu;

	/** L'id role. */
	
	@Column(name = "ROLE_ID", nullable = false, length = 50)
	private String idRole;

	public RefPowerId() {
		super();
	}

	public RefPowerId(Long idRefMenu, String idRole) {
		super();
		this.idRefMenu = idRefMenu;
		this.idRole = idRole;
	}

	public Long getIdRefMenu() {
		return idRefMenu;
	}

	public void setIdRefMenu(Long idRefMenu) {
		this.idRefMenu = idRefMenu;
	}

	public String getIdRole() {
		return idRole;
	}

	public void setIdRole(String idRole) {
		this.idRole = idRole;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
