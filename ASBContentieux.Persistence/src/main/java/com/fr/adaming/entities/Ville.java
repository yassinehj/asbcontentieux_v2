package com.fr.adaming.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <b>Ville est la classe représentant une ville quelconque.</b>
 * <p>
 * Une ville est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un identifiant, unique attribué définitivement.</li>
 * <li>Un nom</li>
 * <li>Un code postal.</li>
 * <li>Une gouvernorat , une ville appartient à une gouvernorat .</li>
 * <li>Une liste des adresses , une ville comporte plusieurs adresses.</li>
 * </ul>
 * </p>
 * 
 * @author azaaboub
 * @since 02/04/2019
 * @see Adresse
 * @see Gouvernorat
 */
@Entity
@Table(name = "VILLE")
public class Ville implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_VILLE", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idVille;

	@Column(name = "NOM")
	private String nom;

	@Column(name = "CODE_POSTAL")
	private String codePostal;

	@JsonIgnore
    @OneToMany(mappedBy = "ville",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Adresse> adresses= new ArrayList<Adresse>();
    
    @ManyToOne
	@JoinColumn(name="ID_GOUVERNORAT")
    private Gouvernorat gouvernorat;

	public Ville() {
		super();
	}
	/**
	 * Un constructeur permettant d'instancier une ville avec les parametres
	 * suivants:
	 * <p>
	 * nom , codePostal , adresses , gouvernorat
	 * </p>
	 * 
	 * @param nom
	 * @param codePostal
	 * @param adresses
	 * @param gouvernorat
	 */
	public Ville(String nom, String codePostal, List<Adresse> adresses, Gouvernorat gouvernorat) {
		this.nom = nom;
		this.codePostal = codePostal;
		this.adresses = adresses;
		this.gouvernorat = gouvernorat;
	}
	/**
	 * Un constructeur permettant d'instancier une ville avec les parametres
	 * suivants:
	 * <p>
	 * nom , codePostal
	 * </p>
	 *
	 * @param nom
	 * @param codePostal
	 */
	public Ville(String nom, String codePostal) {
		this.nom = nom;
		this.codePostal = codePostal;
	}
	/**
	 * Un constructeur permettant d'instancier une ville avec les parametres
	 * suivants:
	 * <p>
	 * nom , codePostal ,gouvernorat
	 * </p>
	 *
	 * @param nom
	 * @param codePostal
	 * @param gouvernorat
	 */
	public Ville(String nom, String codePostal, Gouvernorat gouvernorat) {
		this.nom = nom;
		this.codePostal = codePostal;
		this.gouvernorat = gouvernorat;
	}

	/**
	 * retourne l'id de la ville
	 * 
	 * @return idVille
	 */
	public Long getIdVille() {
		return idVille;
	}

	/**
	 * saisie de l'id de la ville
	 * 
	 * @param idVille
	 */
	public void setIdVille(Long idVille) {
		this.idVille = idVille;
	}

	/**
	 * retourne le nom de la ville
	 * 
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * saisie du nom de la ville
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * retourne le code postal de la ville
	 * 
	 * @return codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * saisie du code postal de la ville
	 * 
	 * @param codePostal
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * retourne un ensemble d'adresses appartenant à une ville
	 * 
	 * @return adresses
	 */
	public List<Adresse> getAdresses() {
		return adresses;
	}

	/**
	 * saisie d'un ensemble d'adresses appartenant à une ville
	 * 
	 * @param adresses
	 */
	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}

	/**
	 * retourne le gouvernorat de la ville
	 * 
	 * @return gouvernorat
	 */
	public Gouvernorat getGouvernorat() {
		return gouvernorat;
	}

	/**
	 * saisie du gouvernorat de la ville
	 * 
	 * @param gouvernorat
	 */
	public void setGouvernorat(Gouvernorat gouvernorat) {
		this.gouvernorat = gouvernorat;
	}

	@Override
	public String toString() {
		return "Ville{" + "idVille=" + idVille + ", nom='" + nom + '\'' + ", codePostal='" + codePostal + '\''
				+ ", adresses=" + adresses + ", gouvernorat=" + gouvernorat + '}';
	}
}
