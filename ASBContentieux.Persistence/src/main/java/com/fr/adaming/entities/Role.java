package com.fr.adaming.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <b>Role represente un role .</b>
 * <p>
 * Un role est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un name, Iunique attribué définitivement.</li>
 * </ul>
 * 
 * @author mbouhdida
 */
@Entity
@Table(name = "role")
public class Role {

	/** Le nom. */
	@Id
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Role() {
		super();
	}

	public Role(String name) {
		super();
		this.name = name;
	}

}
