package com.fr.adaming.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Tribunal est la classe représentant le lieu où est rendue la justice</b>
 * <p>
 * Un tribunal est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un identifiant, unique attribué définitivement.</li>
 * <li>Un type, comme tribunal civile , administratif ou immobilier.</li>
 * <li>Un numéro de téléphone.</li>
 * <li>Un numéro de fax .</li>
 * <li>Une reférence sur le tribunal suivant.</li>
 * <li>un email.</li>
 * <li>une liste de phases, un tribunal peut attribué dans plusieurs
 * phases.</li>
 * </ul>
 * 
 * @author nbelhajyahia
 * @author mabelkaid
 * @since 01-04-2019
 */
@Entity
@Table(name = "TRIBUNAL")
public class Tribunal implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_TRIBUNAL")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idTribunal;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "NUM_TEL")
	private String numTel;

	@Column(name = "FAX")
	private String fax;

	@Column(name = "TRIBUNAL_SUIVANT")
	private Tribunal tribunalSuivant;


	@OneToMany(mappedBy = "tribunal", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Phase> phases = new ArrayList<>();

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_ADRESSE")
	private Adresse adresse;

	public Tribunal() {
		super();
	}

	/**
	 * Un constructeur permettant d'instancier un tribunal avec les parametres
	 * suivants:
	 * <p>
	 * idTribunal , type , numTel , fax , tribunalSuivant , phases
	 * </p>
	 *
	 * @param idTribunal
	 * @param type
	 * @param numTel
	 * @param fax
	 * @param tribunalSuivant
	 * @param phases
	 */
	public Tribunal(Long idTribunal, String type, String numTel, String fax, Tribunal tribunalSuivant,
			List<Phase> phases) {
		super();
		this.idTribunal = idTribunal;
		this.type = type;
		this.numTel = numTel;
		this.fax = fax;
		this.tribunalSuivant = tribunalSuivant;
		this.phases = phases;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	/**
	 * retourne l'id du tribunal
	 *
	 * @return idTribunal
	 */
	public Long getIdTribunal() {
		return idTribunal;
	}

	/**
	 * saisie de l'id du tribunal
	 * 
	 * @param idTribunal
	 */
	public void setIdTribunal(Long idTribunal) {
		this.idTribunal = idTribunal;
	}

	/**
	 * retourne le type de la tribunal
	 * 
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * saisie du type de la tribunal
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * retourne le numéro de téléphone de la tribunal
	 * 
	 * @return numTel
	 */
	public String getNumTel() {
		return numTel;
	}

	/**
	 * saisie de numéro de téléphone de la tribunal
	 * 
	 * @param numTel
	 */
	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	/**
	 * retourne le numéro de fax de la tribunal
	 * 
	 * @return fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * saisie de numéro de fax de la tribunal
	 * 
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * retourne le tribunal suivant
	 * 
	 * @return tribunalSuivant
	 */
	public Tribunal getTribunalSuivant() {
		return tribunalSuivant;
	}

	/**
	 * saisie du tribunal suivant
	 * 
	 * @param tribunalSuivant
	 */
	public void setTribunalSuivant(Tribunal tribunalSuivant) {
		this.tribunalSuivant = tribunalSuivant;
	}

	/**
	 * retourne la liste des phases
	 * 
	 * @return phases
	 */
	public List<Phase> getPhases() {
		return phases;
	}

	/**
	 * saisie de la liste de phases
	 * 
	 * @param phases
	 */
	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}

	@Override
	public String toString() {
		return "Tribunal [idTribunal=" + idTribunal + ", type=" + type + ", numTel=" + numTel + ", fax=" + fax
				+ ", tribunalSuivant=" + tribunalSuivant + ", phases=" + phases + "]";
	}

}
