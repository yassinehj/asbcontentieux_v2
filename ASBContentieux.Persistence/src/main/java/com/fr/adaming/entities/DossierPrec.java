package com.fr.adaming.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author hhboumaiza
 *
 */
@Entity
@Table(name = "DOSSIER_PREC")
public class DossierPrec {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_DOSSIER_PREC", nullable=false)
	private Long idDossierPrec;

	@Column(name = "ID_DOSSIER")
	private Long idDossier;

	@Column(name = "CREATION_TIME")
	@Temporal(TemporalType.DATE)
	private Date creationTime;

	@Column(name = "DUE_DATE")
	private Date dueDate;
	@Column(name="PROC_INSTANCE_ID")
	private String procInstanceId;

	@Column(name = "ID_TITULAIRE")
	private Long idTitulaire;

	@Column(name = "NOM_TITULAIRE")
	private String nomTitulaire;

	@Column(name = "TEL_TITULAIRE")
	private String telTitulaire;

	@Column(name = "REGULARISE")
	private boolean regularise;

	@Column(name = "ARCHIVER")
	private boolean archiver;

	@ManyToOne
	@JoinColumn(name = "AGENCE_ID", nullable = false)
	private Agence agence;

	@ManyToOne
	@JoinColumn(name = "COMPTE_BANCAIRE_ID", nullable = false)
	private CompteBancaire compteBancaire;

	@OneToMany(mappedBy = "dossierPrec", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<HistoriqueRecouverement> historiqueRecouverement = new ArrayList<>();

	@OneToMany(mappedBy = "dossierPrec", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<InjonctionPayer> injonctionPayer = new ArrayList<>();

	/**
	 * L'id
	 * 
	 * @return
	 */
	public Long getIdDossierPrec() {
		return idDossierPrec;
	}

	public void setIdDossierPrec(Long idDossierPrec) {
		this.idDossierPrec = idDossierPrec;
	}

	/**
	 * L'id du dossier
	 * 
	 * @return
	 */
	public Long getIdDossier() {
		return idDossier;
	}

	public void setIdDossier(Long idDossier) {
		this.idDossier = idDossier;
	}

	/**
	 * le temps de creation
	 * 
	 * @return
	 */
	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * Le due date
	 * 
	 * @return
	 */
	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getProcInstanceId() {
		return procInstanceId;
	}

	public void setProcInstanceId(String procInstanceId) {
		this.procInstanceId = procInstanceId;
	}

	/**
	 * L'id titulaire
	 * 
	 * @return
	 */
	public Long getIdTitulaire() {
		return idTitulaire;
	}

	public void setIdTitulaire(Long idTitulaire) {
		this.idTitulaire = idTitulaire;
	}

	/**
	 * Le nom du titulaire
	 * 
	 * @return
	 */
	public String getNomTitulaire() {
		return nomTitulaire;
	}

	public void setNomTitulaire(String nomTitulaire) {
		this.nomTitulaire = nomTitulaire;
	}

	/**
	 * Le telephone du titulaires
	 * 
	 * @return
	 */
	public String getTelTitulaire() {
		return telTitulaire;
	}

	public void setTelTitulaire(String telTitulaire) {
		this.telTitulaire = telTitulaire;
	}

	/**
	 * dossier regularisé ou non
	 * 
	 * @return
	 */
	public boolean isRegularise() {
		return regularise;
	}

	public void setRegularise(boolean regularise) {
		this.regularise = regularise;
	}

	/**
	 * dossier archiver ou non
	 * 
	 * @return
	 */
	public boolean isArchiver() {
		return archiver;
	}

	public void setArchiver(boolean archiver) {
		this.archiver = archiver;
	}

	/**
	 * un object de type agence
	 * 
	 * @return
	 */
	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	/**
	 * un object de type compte bancaire
	 * 
	 * @return
	 */
	public CompteBancaire getCompteBancaire() {
		return compteBancaire;
	}

	public void setCompteBancaire(CompteBancaire compteBancaire) {
		this.compteBancaire = compteBancaire;
	}

	/**
	 * un object de type historique recouverement
	 * 
	 * @return
	 */
	public List<HistoriqueRecouverement> getHistoriqueRecouverement() {
		return historiqueRecouverement;
	}

	public void setHistoriqueRecouverement(List<HistoriqueRecouverement> historiqueRecouverement) {
		this.historiqueRecouverement = historiqueRecouverement;
	}

	/**
	 * un objet de type injonction payer
	 * 
	 * @return
	 */
	public List<InjonctionPayer> getInjonctionPayer() {
		return injonctionPayer;
	}

	public void setInjonctionPayer(List<InjonctionPayer> injonctionPayer) {
		this.injonctionPayer = injonctionPayer;
	}

	/**
	 * constructeur vide
	 */
	public DossierPrec() {
		super();
	}

	/**
	 * 
	 * @param id
	 * @param idDossier
	 * @param creationTime
	 * @param dueDate
	 * @param idTitulaire
	 * @param nomTitulaire
	 * @param telTitulaire
	 * @param regularise
	 * @param archiver
	 * @param agence
	 * @param compteBancaire
	 * @param historiqueRecouverement
	 * @param injonctionPayer
	 */

	/**
	 * constructeur parametrés
	 */
	public DossierPrec(Long idDossier, Date creationTime, Date dueDate, Long idTitulaire, String nomTitulaire, String telTitulaire, boolean regularise, boolean archiver, Agence agence, CompteBancaire compteBancaire, List<HistoriqueRecouverement> historiqueRecouverement, List<InjonctionPayer> injonctionPayer) {
		this.idDossier = idDossier;
		this.creationTime = creationTime;
		this.dueDate = dueDate;
		this.idTitulaire = idTitulaire;
		this.nomTitulaire = nomTitulaire;
		this.telTitulaire = telTitulaire;
		this.regularise = regularise;
		this.archiver = archiver;
		this.agence = agence;
		this.compteBancaire = compteBancaire;
		this.historiqueRecouverement = historiqueRecouverement;
		this.injonctionPayer = injonctionPayer;
	}

	public DossierPrec(Long idDossier, Date creationTime, Date dueDate, Long idTitulaire, String nomTitulaire, String telTitulaire, boolean regularise, boolean archiver) {
		this.idDossier = idDossier;
		this.creationTime = creationTime;
		this.dueDate = dueDate;
		this.idTitulaire = idTitulaire;
		this.nomTitulaire = nomTitulaire;
		this.telTitulaire = telTitulaire;
		this.regularise = regularise;
		this.archiver = archiver;
	}

	public DossierPrec(Long idDossier, Date creationTime, Date dueDate, String procInstanceId, Long idTitulaire, String nomTitulaire, String telTitulaire, boolean regularise, boolean archiver) {
		this.idDossier = idDossier;
		this.creationTime = creationTime;
		this.dueDate = dueDate;
		this.procInstanceId = procInstanceId;
		this.idTitulaire = idTitulaire;
		this.nomTitulaire = nomTitulaire;
		this.telTitulaire = telTitulaire;
		this.regularise = regularise;
		this.archiver = archiver;
	}

	@Override
	public String toString() {
		return "DossierPrec{" +
				"idDossierPrec=" + idDossierPrec +
				", idDossier=" + idDossier +
				", creationTime=" + creationTime +
				", dueDate=" + dueDate +
				", procInstanceId='" + procInstanceId + '\'' +
				", idTitulaire=" + idTitulaire +
				", nomTitulaire='" + nomTitulaire + '\'' +
				", telTitulaire='" + telTitulaire + '\'' +
				", regularise=" + regularise +
				", archiver=" + archiver +
				", agence=" + agence +
				", compteBancaire=" + compteBancaire +
				", historiqueRecouverement=" + historiqueRecouverement +
				'}';
	}
}
