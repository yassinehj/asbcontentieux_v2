package com.fr.adaming.entities;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * <b>RefPower represente pour chaque role le pouvoir d'ajouter, supprimer ou
 * modifier des données danns un ecran bien précis .</b>
 * <p>
 * Un RefPower est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un identifiant, Il est composé d'un roleId et refMMenuId.</li>
 * <li>Un flag creation, si le flag est egale true donc l'utilisateur a le
 * pouvoir d'ajouter des données</li>
 * <li>Un flag suppression, si le flag est egale true donc l'utilisateur a le
 * pouvoir de supprimer des données</li>
 * <li>Un flag modification, si le flag est egale true donc l'utilisateur a le
 * pouvoir de modifier des données.</li>
 * <li>Un flag consultation, si le flag est egale true donc l'utilisateur a le
 * pouvoir de consulter des données..</li>
 * </ul>
 * 
 * @see RefPowerId
 * @see Role
 * @see Utilisateur
 * @author mbouhdida
 */
@Entity
@Table(name = "REF_POWER")
public class 	RefPower implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Le ref power id. */

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "roleId", column = @Column(name = "ROLE_ID", nullable = false, length = 50)),
			@AttributeOverride(name = "RefMenuId", column = @Column(name = "MENU_ID", nullable = false, precision = 8, scale = 0)) })
	private RefPowerId refPowerId;

	/** Le ref menu. */

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "MENU_ID", insertable = false, updatable = false)
	private RefMenu refMenu;

	/** Le role. */

	@ManyToOne
	@JoinColumn(name = "ROLE_ID", insertable = false, updatable = false)
	private Role role;

	/** Le flag creeation. */

	@Column(name = "FLAG_CREATION", precision = 1, scale = 0)
	private boolean flagCreation;

	/** Le flag modification. */

	@Column(name = "FLAG_MODIFICATION", precision = 1, scale = 0)
	private boolean flagModification;

	/** Le flag suppression. */

	@Column(name = "FLAG_SUPPRESSION", precision = 1, scale = 0)
	private boolean flagSupression;

	/** Le flag consultation. */

	@Column(name = "FLAG_CONSULTATION", precision = 1, scale = 0)
	private boolean flagConsultation;

	public RefPower() {
		super();
	}

	public RefPower(RefPowerId refPowerId, RefMenu refMenu, Role role, boolean flagCreation, boolean flagModification,
                    boolean flagSupression, boolean flagConsultation) {
		super();
		this.refPowerId = refPowerId;
		this.refMenu = refMenu;
		this.role = role;
		this.flagCreation = flagCreation;
		this.flagModification = flagModification;
		this.flagSupression = flagSupression;
		this.flagConsultation = flagConsultation;
	}

	public RefPowerId getRefPowerId() {
		return refPowerId;
	}

	public void setRefPowerId(RefPowerId refPowerId) {
		this.refPowerId = refPowerId;
	}

	public RefMenu getRefMenu() {
		return refMenu;
	}

	public void setRefMenu(RefMenu refMenu) {
		this.refMenu = refMenu;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public boolean isFlagCreation() {
		return flagCreation;
	}

	public void setFlagCreation(boolean flagCreation) {
		this.flagCreation = flagCreation;
	}

	public boolean isFlagModification() {
		return flagModification;
	}

	public void setFlagModification(boolean flagModification) {
		this.flagModification = flagModification;
	}

	public boolean isFlagSupression() {
		return flagSupression;
	}

	public void setFlagSupression(boolean flagSupression) {
		this.flagSupression = flagSupression;
	}

	public boolean isFlagConsultation() {
		return flagConsultation;
	}

	public void setFlagConsultation(boolean flagConsultation) {
		this.flagConsultation = flagConsultation;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
