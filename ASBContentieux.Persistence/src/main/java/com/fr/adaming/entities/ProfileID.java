package com.fr.adaming.entities;

import com.fr.adaming.entities.Role;
import com.fr.adaming.entities.Utilisateur;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * <b>ProfileID est la classe représentant un identifiant de la classe
 * profile.</b>
 *
 * @see Role
 * @see Utilisateur
 * @author mbouhdida
 */
@Embeddable
public class ProfileID implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** L'id role. */
	
	@Column(name = "ROLE_ID", nullable = false, scale = 0)
	private String roleId;

	/** L'id utilisateur. */

	@Column(name = "USER_ID", nullable = false, scale = 0)
	private Long userId;

	public ProfileID() {
		super();
	}

	public ProfileID(String roleId, Long userId) {
		super();
		this.roleId = roleId;
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
