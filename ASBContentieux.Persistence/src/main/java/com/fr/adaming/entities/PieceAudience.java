package com.fr.adaming.entities;

import javax.persistence.*;
import java.io.Serializable;
/**
 * <p>
 * Une Piece Audience est caractérisé par les informations suivantes :
 * <ul>
 * <li>id_piece_audience, texte_jugement, id_alfresco, fileType, fileName, status</li>
 * </ul>
 * @author nbelhajyahia
 * @since 01-04-2019
 */
@Table(name="PIECE_AUDIENCE")
@Entity
public class PieceAudience implements Serializable {

    private static final long serialVersionUID = 1L;
    /** L 'id */
    @Id
    @Column(name = "ID_PIECE_AUDIENCE")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idPieceAudience;
    /** le text de jugement*/
    @Column(name = "TEXT_JUGEMENT")
    private String texteJugement;
    /** l 'id alfresco*/
    @Column(name = "ID_ALFRESCO")
    private String idAlfresco;
    /** file type*/
    @Column(name = "FILETYPE")
    private String fileType;
    /** file name */
    @Column(name = "FILENAME")
    private String fileName;
    /** le status*/
    @Column(name = "STATUS")
    private String status;
    @ManyToOne
    @JoinColumn(name="ID_AUDIENCE")
    private Audience audience;

    public Long getIdPieceAudience() { return idPieceAudience; }

    public void setIdPieceAudience(Long idPieceAudience) { this.idPieceAudience = idPieceAudience; }


    public String getTexteJugement() { return texteJugement; }

    public void setTexteJugement(String texteJugement) { this.texteJugement = texteJugement; }


    public String getIdAlfresco() { return idAlfresco; }

    public void setIdAlfresco(String idAlfresco) { this.idAlfresco = idAlfresco; }


    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Audience getAudience() {
        return audience;
    }

    public void setAudience(Audience audience) {
        this.audience = audience;
    }

    public PieceAudience() {
    }

    public PieceAudience(String texteJugement, String idAlfresco, String fileType, String fileName, String status, Audience audience) {
        this.texteJugement = texteJugement;
        this.idAlfresco = idAlfresco;
        this.fileType = fileType;
        this.fileName = fileName;
        this.status = status;
        this.audience = audience;
    }

    @Override
    public String toString() {
        return "PieceAudience{" +
                "idPieceAudience=" + idPieceAudience +
                ", texteJugement='" + texteJugement + '\'' +
                ", idAlfresco='" + idAlfresco + '\'' +
                ", fileType='" + fileType + '\'' +
                ", fileName='" + fileName + '\'' +
                ", status='" + status + '\'' +
                ", audience=" + audience +
                '}';
    }
}
