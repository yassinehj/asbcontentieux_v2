package com.fr.adaming.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * <p>
 * Une Affaire est caractérisé par les informations suivantes :
 * <ul>
 * <li>idAffaire, reference, dateCreation,dateCloture,archiver,description</li>
 * </ul>
 * 
 * @author azaaboub
 * @since 29/03/2019
 *
 */

@Entity
@Table(name = "AFFAIRE")
public class Affaire implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_AFFAIRE", nullable = false)
	private Integer idAffaire;

	@Column(name = "REFERENCE")
	private String reference;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE_CREATION")
	private Date dateCreation;

	@Column(name = "DATE_CLOTURE")
	private Date dateCloture;

	@Column(name = "ARCHIVER")
	private Boolean archiver;

	@Column(name = "DESCRIPTION")
	private String description;

	@OneToMany(mappedBy = "affaire", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Dossier> dossiers = new ArrayList<>();

	@OneToMany(mappedBy = "affaire", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Honnoraire> honnoraires = new ArrayList<>();

	@OneToMany(mappedBy = "affaire", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Phase> phases = new ArrayList<>();

	/**
	 * Gets the id affaire
	 * 
	 * @return the idAffaire
	 */

	public Integer getIdAffaire() {
		return idAffaire;
	}

	/**
	 * sets the id affaire
	 * 
	 * @param idAffaire
	 */

	public void setIdAffaire(Integer idAffaire) {
		this.idAffaire = idAffaire;
	}

	/**
	 * Gets the references
	 * 
	 * @return the reference
	 */

	public String getReference() {
		return reference;
	}

	/**
	 * sets the reference
	 * 
	 * @param reference
	 */

	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * Gets the date creation
	 * 
	 * @return the date creation
	 */

	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * sets the date creation
	 * 
	 * @param dateCreation
	 */

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	/**
	 * Gets the date cloture
	 * 
	 * @return the date cloture
	 */

	public Date getDateCloture() {
		return dateCloture;
	}

	/**
	 * sets the date cloture
	 * 
	 * @param dateCloture
	 */

	public void setDateCloture(Date dateCloture) {
		this.dateCloture = dateCloture;
	}

	/**
	 * Gets the archiver
	 * 
	 * @return archiver
	 */

	public Boolean getArchiver() {
		return archiver;
	}

	/**
	 * sets the archiver
	 * 
	 * @param archiver
	 */

	public void setArchiver(Boolean archiver) {
		this.archiver = archiver;
	}

	/**
	 * Gets the description
	 * 
	 * @return Description
	 */

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Dossier> getDossiers() {
		return dossiers;
	}

	public void setDossiers(List<Dossier> dossiers) {
		this.dossiers = dossiers;
	}

	public List<Phase> getPhases() {
		return phases;
	}

	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}

	public List<Honnoraire> getHonnoraires() {
		return honnoraires;
	}

	public void setHonnoraires(List<Honnoraire> honnoraires) {
		this.honnoraires = honnoraires;
	}

	public Affaire() {
		super();
	}

	/**
	 * 
	 * @param idAffaire
	 * @param reference
	 * @param dateCreation
	 * @param dateCloture
	 * @param archiver
	 * @param description
	 * @param dossiers
	 * @param honnoraires
	 * @param phases
	 */
	public Affaire(Integer idAffaire, String reference, Date dateCreation, Date dateCloture, Boolean archiver,
			String description, List<Dossier> dossiers, List<Honnoraire> honnoraires, List<Phase> phases) {
		super();
		this.idAffaire = idAffaire;
		this.reference = reference;
		this.dateCreation = dateCreation;
		this.dateCloture = dateCloture;
		this.archiver = archiver;
		this.description = description;
		this.dossiers = dossiers;
		this.honnoraires = honnoraires;
		this.phases = phases;
	}

	@Override
	public String toString() {
		return "Affaire [idAffaire=" + idAffaire + ", reference=" + reference + ", dateCreation=" + dateCreation
				+ ", dateCloture=" + dateCloture + ", archiver=" + archiver + ", description=" + description
				+ ", dossiers=" + dossiers + ", honnoraires=" + honnoraires + ", phases=" + phases + "]";
	}

}
