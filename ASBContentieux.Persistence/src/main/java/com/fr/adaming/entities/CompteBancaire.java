package com.fr.adaming.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author hhboumaiza
 * @since 01-04-2019
 */
@Entity
@Table(name = "COMPTE_BANCAIRE")
public class CompteBancaire implements Serializable {


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "RIB")
	private String rib;

	@Column(name = "SOLDE")
	private double solde;

	@OneToMany(mappedBy = "compteBancaire")
	@JsonIgnore
	private List<DossierPrec> dossierPrec = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "DEBITEUR_ID", nullable = false)
	private Debiteur debiteur ; 

	/** L'id. */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	/** Le RIB . */
	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}


	/** Le solde du compte. */
	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	/**
	 * La liste des dossiers precontentieux
	 * 
	 * @return
	 */
	public List<DossierPrec> getDossierPrec() {
		return dossierPrec;
	}

	public void setDossierPrec(List<DossierPrec> dossierPrec) {
		this.dossierPrec = dossierPrec;
	}

	/**
	 * un object de type debiteur
	 * @return
	 */

	public Debiteur getDebiteur() {
		return debiteur;
	}

	public void setDebiteur(Debiteur debiteur) {
		this.debiteur = debiteur;
	}

	/**
	 * constructeur vide
	 */
	public CompteBancaire() {
		super();

	}


	/**
	 * 
	 * @param id
	 * @param rib
	 * @param solde
	 * @param dossierPrec
	 * @param debiteur
	 */

	/**
	 * constructeur parametrés
	 */
	public CompteBancaire(Long id, String rib, double solde, List<DossierPrec> dossierPrec, Debiteur debiteur) {
		super();
		this.id = id;
		this.rib = rib;
		this.solde = solde;
		this.dossierPrec = dossierPrec;
		this.debiteur = debiteur;
	}
	/**
	 *
	 * @param rib
	 * @param solde
	 * @param debiteur
	 */
	public CompteBancaire(String rib, double solde, Debiteur debiteur) {
		super();
		this.rib = rib;
		this.solde = solde;
		this.debiteur = debiteur;
	}
	/**
	 *
	 * @param rib
	 * @param solde
	 */
	public CompteBancaire(String rib, double solde) {
		this.rib = rib;
		this.solde = solde;
	}
	/**
	 *
	 * @param rib
	 */
	public CompteBancaire(String rib) {
		this.rib = rib;
	}

	@Override
	public String toString() {
		return "CompteBancaire{" +
				"id=" + id +
				", rib='" + rib + '\'' +
				", solde=" + solde +
				", debiteur=" + debiteur +
				'}';
	}
}
