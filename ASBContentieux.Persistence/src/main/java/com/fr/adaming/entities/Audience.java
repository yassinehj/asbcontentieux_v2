package com.fr.adaming.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * Une Audience est caractérisé par les informations suivantes :
 * <ul>
 * <li>id_audience, date_creation, descreption, date_audience, etat, date_fin,
 * duree_notif, notif</li>
 * </ul>
 * 
 * @author nbelhajyahia
 * @since 01-04-2019
 */

@Table(name = "AUDIENCE")
@Entity
public class Audience implements Serializable {
	private static final long serialVersionUID = 1L;

	/** L 'id */
	@Id
	@Column(name = "ID_AUDIENCE")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idAudience;
	/** La date de l'audience */
	@Column(name = "DATE_CREATION")
	private Date dateCreation;
	/** La description. */
	@Column(name = "DESCRIPTION")
	private String description;
	/** La date d'audience. */
	@Column(name = "DATE_AUDIENCE")
	private Date dateAudience;
	/** L'etat. */
	@Column(name = "ETAT")
	private String etat;
	/** La date fin. */
	@Column(name = "DATE_FIN")
	private Date dateFin;
	/** La duree de notif. */
	@Column(name = "DUREE_NOTIF")
	private float dureeNotif;
	/** La notification */
	@Column(name = "NOTIF")
	private String notif;

	@OneToMany(mappedBy = "audience", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PieceAudience> piecesAudiences = new ArrayList<>();

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "AUDIENCE_UTILISATEUR", joinColumns = {
			@JoinColumn(name = "ID_AUDIENCE") }, inverseJoinColumns = { @JoinColumn(name = "ID_UTILISATEUR") })
	private List<Utilisateur> utilisateurs = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "ID_PHASE")
	private Phase phase;

	public Audience() {
		super();
	}

	/**
	 * 
	 * @param idAudience
	 * @param dateCreation
	 * @param description
	 * @param dateAudience
	 * @param etat
	 * @param dateFin
	 * @param dureeNotif
	 * @param notif
	 * @param piecesAudiences
	 * @param utilisateurs
	 * @param phase
	 */
	public Audience(Long idAudience, Date dateCreation, String description, Date dateAudience, String etat,
			Date dateFin, float dureeNotif, String notif, List<PieceAudience> piecesAudiences,
			List<Utilisateur> utilisateurs, Phase phase) {
		super();
		this.idAudience = idAudience;
		this.dateCreation = dateCreation;
		this.description = description;
		this.dateAudience = dateAudience;
		this.etat = etat;
		this.dateFin = dateFin;
		this.dureeNotif = dureeNotif;
		this.notif = notif;
		this.piecesAudiences = piecesAudiences;
		this.utilisateurs = utilisateurs;
		this.phase = phase;
	}

	public Long getIdAudience() {
		return idAudience;
	}

	public void setIdAudience(Long idAudience) {
		this.idAudience = idAudience;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateAudience() {
		return dateAudience;
	}

	public void setDateAudience(Date dateAudience) {
		this.dateAudience = dateAudience;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public float getDureeNotif() {
		return dureeNotif;
	}

	public void setDureeNotif(float dureeNotif) {
		this.dureeNotif = dureeNotif;
	}

	public String getNotif() {
		return notif;
	}

	public void setNotif(String notif) {
		this.notif = notif;
	}

	public List<PieceAudience> getPiecesAudiences() {
		return piecesAudiences;
	}

	public void setPiecesAudiences(List<PieceAudience> piecesAudiences) {
		this.piecesAudiences = piecesAudiences;
	}

	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	public Phase getPhase() {
		return phase;
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	@Override
	public String toString() {
		return "Audience [idAudience=" + idAudience + ", dateCreation=" + dateCreation + ", description=" + description
				+ ", dateAudience=" + dateAudience + ", etat=" + etat + ", dateFin=" + dateFin + ", dureeNotif="
				+ dureeNotif + ", notif=" + notif + ", piecesAudiences=" + piecesAudiences + ", utilisateurs="
				+ utilisateurs + ", phase=" + phase + "]";
	}
	
	

}
