package com.fr.adaming.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  <p>
 * Une PieceAffaire est caractérisé par les informations suivantes :
 * <ul>
 * <li>idPieceAffaire, reference, dateCreation,idAlfresco,fileType,fileName,size</li>
 * </ul>
 * @author azaaboub
 * @since 29/03/2019
 *
 */
@Table(name="PIECE_AFFAIRE")
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PieceAffaire implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_PIECE_AFFAIRE", nullable=false)
    private Long idPieceAffaire;
	
	
	
	@Column(name="NOM")
    private String nom;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="DATE_CREATION")
	@JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateCreation;
	
	@Column(name="ID_ALFRESCO")
    private String idAlfresco;
	
	@Column(name="FILE_TYPE")
    private String fileType;
	
    @Column(name="FILE_NAME")
	private String fileName;

	@Column(name="SIZE_PIECE")
    private Long size;
    
	@ManyToOne
	@JoinColumn(name="ID_DOSSIER")
	private Dossier dossier;

	/**
	 * Gets idPieceAffaire
	 * @return idPieceAffaire
	 */
	
	public Long getIdPieceAffaire() {
		return idPieceAffaire;
	}
	
	/**
	 * Sets idPieceAffaire
	 * @param idPieceAffaire
	 */

	public void setIdPieceAffaire(Long idPieceAffaire) {
		this.idPieceAffaire = idPieceAffaire;
	}
	
	/**
	 * Gets nom
	 * @return nom
	 */

	public String getNom() {
		return nom;
	}
	
	/**
	 * Sets nom
	 * @param nom
	 */

	public void setNom(String nom) {
		this.nom = nom;
	}

	
	/**
	 * Gets Description
	 * @return Description
	 */
	public String getDescription() {
		return description;
	}

	
	/**
	 * Sets Description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**Gets date creation
	 * 
	 * @return Date Creation
	 */

	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * Sets date creation
	 * @param dateCreation
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	/**
	 * Gets idAlfresco
	 * @return idAlfresco
	 */
	public String getIdAlfresco() {
		return idAlfresco;
	}

	/**
	 * Sets idAlfresco
	 * @param idAlfresco
	 */
	public void setIdAlfresco(String idAlfresco) {
		this.idAlfresco = idAlfresco;
	}

	
	/**
	 * Gets fileType
	 * @return fileType
	 */
	public String getFileType() {
		return fileType;
	}
	
	/**
	 * Sets fileType
	 * @param fileType
	 */

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	
	/**
	 * Gets fileName
	 * @return fileName
	 */

	public String getFileName() {
		return fileName;
	}

	
	/**
	 * Sets fileName
	 * @param fileName
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	/**
	 * Gets Dossier
	 * @return Dossier
	 */

	public Dossier getDossier() {
		return dossier;
	}
	
	/**
	 * Sets Dossier
	 * @param dossier
 */

	public void setDossier(Dossier dossier) {
		this.dossier = dossier;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public PieceAffaire(Long idPieceAffaire, String nom, String description, Date dateCreation, String idAlfresco,
			String fileType, String fileName, Long size, Dossier dossier) {
		super();
		this.idPieceAffaire = idPieceAffaire;
		this.nom = nom;
		this.description = description;
		this.dateCreation = dateCreation;
		this.idAlfresco = idAlfresco;
		this.fileType = fileType;
		this.fileName = fileName;
//		this.size = size;
//		this.dossier = dossier;
	}

	public PieceAffaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "PieceAffaire [idPieceAffaire=" + idPieceAffaire + ", nom=" + nom + ", description=" + description
				+ ", dateCreation=" + dateCreation + ", idAlfresco=" + idAlfresco + ", fileType=" + fileType
				+ ", fileName=" + fileName + ", size=" + size + ", dossier=" + dossier + "]";
	}

	

	






}
