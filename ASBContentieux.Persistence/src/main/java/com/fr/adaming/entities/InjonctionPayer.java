package com.fr.adaming.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author hhboumaiza
 *
 */
@Entity
@Table(name = "INJONCTION_PAYER")
public class InjonctionPayer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_INJONCTION_PAYER", nullable=false)
	private Long idInjonctionPayer;

	@Column(name = "ID_ALFRESCO")
	private String idAlfresco;

	@Column(name = "FILE_TYPE")
	private String fileType;

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "DATE_ORIGINE")
	private Date dateOrigine;

	@Column(name = "DUREE_PASSAGE_CONT")
	private int dureePassageCont;

	@Column(name = "TYPE")
	private String type;

	@ManyToOne
	@JoinColumn(name = "DOSSIER_PREC_ID", nullable = false, insertable = false, updatable = false)
	private DossierPrec dossierPrec;

	/**
	 * L'id
	 * 
	 * @return
	 */
	public Long getIdInjonctionPayer() {
		return idInjonctionPayer;
	}

	public void setIdInjonctionPayer(Long idInjonctionPayer) {
		this.idInjonctionPayer = idInjonctionPayer;
	}

	/**
	 * L'id d'alfresco
	 * 
	 * @return
	 */
	public String getIdAlfresco() {
		return idAlfresco;
	}

	public void setIdAlfresco(String idAlfresco) {
		this.idAlfresco = idAlfresco;
	}

	/**
	 * Le type du fichier
	 * 
	 * @return
	 */
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * Le nom du fichier
	 * 
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * La date d'origine
	 * 
	 * @return
	 */
	public Date getDateOrigine() {
		return dateOrigine;
	}

	public void setDateOrigine(Date dateOrigine) {
		this.dateOrigine = dateOrigine;
	}

	/**
	 * durée du passage contentieuxs
	 * 
	 * @return
	 */
	public int getDureePassageCont() {
		return dureePassageCont;
	}

	public void setDureePassageCont(int dureePassageCont) {
		this.dureePassageCont = dureePassageCont;
	}

	/**
	 * Le type de l'injonction
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * un object de type dossier precententieux
	 * 
	 * @return
	 */
	public DossierPrec getDossierPrec() {
		return dossierPrec;
	}

	public void setDossierPrec(DossierPrec dossierPrec) {
		this.dossierPrec = dossierPrec;
	}

	/**
	 * constructeur vide
	 */
	public InjonctionPayer() {
		super();
	}

	/**
	 * 
	 * @param id
	 * @param idAlfresco
	 * @param fileType
	 * @param fileName
	 * @param dateOrigine
	 * @param dureePassageCont
	 * @param type
	 * @param dossierPrec
	 */

	/**
	 * constructeur parametrés
	 */
	public InjonctionPayer(String idAlfresco, String fileType, String fileName, Date dateOrigine, int dureePassageCont, String type, DossierPrec dossierPrec) {
		this.idAlfresco = idAlfresco;
		this.fileType = fileType;
		this.fileName = fileName;
		this.dateOrigine = dateOrigine;
		this.dureePassageCont = dureePassageCont;
		this.type = type;
		this.dossierPrec = dossierPrec;
	}

	@Override
	public String toString() {
		return "InjonctionPayer{" +
				"idInjonctionPayer=" + idInjonctionPayer +
				", idAlfresco='" + idAlfresco + '\'' +
				", fileType='" + fileType + '\'' +
				", fileName='" + fileName + '\'' +
				", dateOrigine=" + dateOrigine +
				", dureePassageCont=" + dureePassageCont +
				", type='" + type + '\'' +
				", dossierPrec=" + dossierPrec +
				'}';
	}
}
