package com.fr.adaming.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 *<p>
 * Un Dossier est caractérisé par les informations suivantes :
 * <ul>
 * <li>idDossier, nom, description,dateCreatione</li>
 * </ul>
 * @author azaaboub
 * @since 29/03/2019
 */

@Entity
@Table(name="DOSSIER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Dossier implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_DOSSIER", nullable=false)
	private Long idDossier;
	
	
	
	@Column(name="NOM")
	private String nom;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_CREATION")
	private Date dateCreation;
	
	@ManyToOne
	@JoinColumn(name="ID_AFFAIRE")
	private Affaire affaire;
	
	@OneToMany(mappedBy="dossier" ,cascade = CascadeType.ALL)
	private List<PieceAffaire> pieceAffaires = new ArrayList<>();

	
	/** Gets the iddossier
	 * 
	 * @return idDossier
	 */
	public Long getIdDossier() {
		return idDossier;
	}

	
	/**
	 * sets the idDossier
	 * @param idDossier
	 */
	public void setIdDossier(Long idDossier) {
		this.idDossier = idDossier;
	}

	/**
	 * Gets the nom
	 * @return nom 
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * sets the nom
	 * @param nom
	 */

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * Gets the description
	 * @return description
	 */

	public String getDescription() {
		return description;
	}
	/**
	 * sets the description
	 * @param description
	 */

	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets date creation
	 * @return date creation
	 */

	public Date getDateCreation() {
		return dateCreation;
	}
	
	/**
	 * sets date creation
	 * @param dateCreation
	 */

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	/**
	 * Gets the affaire
	 * @return affaire
	 */

	public Affaire getAffaire() {
		return affaire;
	}
	
	/**
	 * Sets the affaire
	 * @param affaire
	 */

	public void setAffaire(Affaire affaire) {
		this.affaire = affaire;
	}

//	public List<PieceAffaire> getPieceAffaires() {
//		return pieceAffaires;
//	}
//
//	public void setPieceAffaires(List<PieceAffaire> pieceAffaires) {
//		this.pieceAffaires = pieceAffaires;
//	}

	public Dossier(Long idDossier, String nom, String description, Date dateCreation, Affaire affaire,
			List<PieceAffaire> pieceAffaires) {
		super();
		this.idDossier = idDossier;
		this.nom = nom;
		this.description = description;
		this.dateCreation = dateCreation;
		this.affaire = affaire;
//		this.pieceAffaires = pieceAffaires;
	}

	public Dossier() {
		super();
		// TODO Auto-generated constructor stub
	}

//	@Override
//	public String toString() {
//		return "Dossier [idDossier=" + idDossier + ", nom=" + nom + ", description=" + description + ", dateCreation="
//				+ dateCreation + ", affaire=" + affaire + ", pieceAffaires=" + pieceAffaires + "]";
//	}
	
	
	

}
