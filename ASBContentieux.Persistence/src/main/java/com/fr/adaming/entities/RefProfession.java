package com.fr.adaming.entities;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * <b>RefProfession est une classe representant la profession de chaque
 * utilisateur.</b>
 * <p>
 * RefProfession est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un identifiant, unique attribué définitivement.</li>
 * <li>Un code profession.</li>
 * <li>Une libelle profession.</li>
 * </ul>
 * 
 * @author mbouhdida
 */
@Entity
public class RefProfession {

	/** L'id ref profession. */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idRefProfession;

	/** Le code profession. */

	@Column(name = "COD_PRO")
	private String codProfession;

	/** La libelle profession. */

	private String libelleProfession;

	public Long getIdRefProfession() {
		return idRefProfession;
	}

	public void setIdRefProfession(Long idRefProfession) {
		this.idRefProfession = idRefProfession;
	}

	public String getCodProfession() {
		return codProfession;
	}

	public void setCodProfession(String codProfession) {
		this.codProfession = codProfession;
	}

	public String getLibelleProfession() {
		return libelleProfession;
	}

	public void setLibelleProfession(String libelleProfession) {
		this.libelleProfession = libelleProfession;
	}

	public RefProfession() {
		super();
	}

	public RefProfession(Long idRefProfession, String codProfession, String libelleProfession) {
		super();
		this.idRefProfession = idRefProfession;
		this.codProfession = codProfession;
		this.libelleProfession = libelleProfession;
	}

	public RefProfession(Long idRefProfession, String codProfession, String libelleProfession,
			List<Utilisateur> users) {
		super();
		this.idRefProfession = idRefProfession;
		this.codProfession = codProfession;
		this.libelleProfession = libelleProfession;
	}
}
