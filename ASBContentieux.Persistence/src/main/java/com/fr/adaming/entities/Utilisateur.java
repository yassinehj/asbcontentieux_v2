package com.fr.adaming.entities;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * <b>Utilisateur est la classe représentant un membre l'application.</b>
 * <p>
 * Un utilisateur est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un identifiant, unique attribué définitivement.</li>
 * <li>Un login, unique attribué définitivement.</li>
 * <li>Un password, ce module sera affiché dans un ordre bien précis.</li>
 * <li>Un nom prenom .</li>
 * <li>Un ref institution, chaque utilisateur est associé a une agence.</li>
 * <li>un email.</li>
 * <li>un refProfession, chaque utilisateur a une profession.</li>
 * <li>un activated, c'est un flag si l'utilisateur est désactivé, il ne peut
 * pas accéder à l'application.</li>
 * </ul>
 * 
 * @see RefInstitution
 * @see RefProfession
 * @author mbouhdida
 */
@Entity
@Table(name = "utilisateur")
public class Utilisateur {

	/** L'id. */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_UTILISATEUR", nullable=false)
	private Long id;

	/** Le login. */

	private String login;

	/** Le password. */

	private String password;

	/** Le first name. */

	private String firstName;

	/** Le last name. */

	private String lastName;

	/** L'email. */

	private String email;

	/** activated. */

	private boolean activated = false;

	/** Le lang key. */

	private String langKey;

	/** Le reset key. */

	private String resetKey;

	/** Le reset date. */

	private Date resetDate = null;

	/** La date creation. */

	private Date dateCreation;

	/** La date modification. */

	private Date dateModification;

	/** La date last login. */

	@Column(name = "DATE_DER_LOGIN")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateLastLogIn;

	/** modifier par. */

	private String modifierPar;

	/** Le ref institution. */

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_Institution")
	private RefInstitution refInstitution;

	/** Le ref profession. */

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "COD_PRO", nullable = true)
	private RefProfession refProfession;

	@OneToMany(mappedBy="utilisateur",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Honnoraire> honnoraires = new ArrayList<>();


	@OneToMany(mappedBy="utilisateur",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Tache> taches = new ArrayList<>();


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}


	public String getLangKey() {
		return langKey;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}


	public String getResetKey() {
		return resetKey;
	}

	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}


	public Date getResetDate() {
		return resetDate;
	}

	public void setResetDate(Date resetDate) {
		this.resetDate = resetDate;
	}


	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}


	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}


	public Date getDateLastLogIn() {
		return dateLastLogIn;
	}

	public void setDateLastLogIn(Date dateLastLogIn) {
		this.dateLastLogIn = dateLastLogIn;
	}


	public String getModifierPar() {
		return modifierPar;
	}

	public void setModifierPar(String modifierPar) {
		this.modifierPar = modifierPar;
	}


	public RefInstitution getRefInstitution() {
		return refInstitution;
	}

	public void setRefInstitution(RefInstitution refInstitution) {
		this.refInstitution = refInstitution;
	}


	public RefProfession getRefProfession() {
		return refProfession;
	}

	public void setRefProfession(RefProfession refProfession) {
		this.refProfession = refProfession;
	}


	public List<Honnoraire> getHonnoraires() {
		return honnoraires;
	}

	public void setHonnoraires(List<Honnoraire> honnoraires) {
		this.honnoraires = honnoraires;
	}


	public Utilisateur() {
		super();
	}

	public Utilisateur(Long id, String login, String password, String firstName, String lastName, Long defaultAgence,
			String email, boolean activated, String langKey, String resetKey, Date resetDate, Date dateCreation,
			Date dateModification, RefProfession refProfession) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.activated = activated;
		this.langKey = langKey;

		this.resetKey = resetKey;
		this.resetDate = resetDate;
		this.dateCreation = dateCreation;
		this.dateModification = dateModification;
		// this.actions = actions;
		this.refProfession = refProfession;
	}

	public Utilisateur(Long id, String login, String password, String firstName, String lastName, String email,
			boolean activated, String langKey, String resetKey, Date resetDate, Date dateCreation,
			Date dateModification, Date dateLastLogIn, String modifierPar, RefInstitution refInstitution,
			RefProfession refProfession) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.activated = activated;
		this.langKey = langKey;
		this.resetKey = resetKey;
		this.resetDate = resetDate;
		this.dateCreation = dateCreation;
		this.dateModification = dateModification;
		this.dateLastLogIn = dateLastLogIn;
		this.modifierPar = modifierPar;
		this.refInstitution = refInstitution;
		this.refProfession = refProfession;
	}

	
	

}
