package com.fr.adaming.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * <p>
 * Une Adresse est caractérisé par les informations suivantes :
 * <ul>
 * <li>idAdresse, adresse</li>
 * 
 * @author azaaboub
 * @since 02/04/2019
 *
 */

@Entity
@Table(name = "ADRESSE")
public class Adresse implements Serializable {

	private static final long serialVersionUID = 1L;
	/** L 'id */
	@Id
	@Column(name = "ID_ADRESSE", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idAdresse;

	@Column(name = "DESCRIPTION")
	private String description;

	@ManyToOne
	@JoinColumn(name = "ID_VILLE")
	private Ville ville;

	@OneToMany(mappedBy = "adresse" ,fetch = FetchType.LAZY )
	@JsonIgnore
	private List<Tribunal> tribunals= new ArrayList<>();

	@OneToMany(mappedBy = "adresse",fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Debiteur> debiteurs= new ArrayList<>();

	public Adresse() {
	}


	public Adresse(String description, Ville ville, Tribunal tribunal) {
		this.description = description;
		this.ville = ville;

	}

	public Adresse(String description, Ville ville) {
		this.description = description;
		this.ville = ville;
	}

	public Adresse(String description) {
		this.description	 = description;
	}

	public Long getIdAdresse() {
		return idAdresse;
	}

	public void setIdAdresse(Long idAdresse) {
		this.idAdresse = idAdresse;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public List<Tribunal> getTribunals() {
		return tribunals;
	}

	public void setTribunals(List<Tribunal> tribunals) {
		this.tribunals = tribunals;
	}

	public List<Debiteur> getDebiteurs() {
		return debiteurs;
	}

	public void setDebiteurs(List<Debiteur> debiteurs) {
		this.debiteurs = debiteurs;
	}

	@Override
	public String toString() {
		return "Adresse{" +
				"idAdresse=" + idAdresse +
				", description='" + description + '\'' +
				", ville=" + ville +
				'}';
	}
}
