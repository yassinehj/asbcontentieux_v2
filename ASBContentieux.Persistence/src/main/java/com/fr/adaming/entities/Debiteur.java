package com.fr.adaming.entities;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author hhboumaiza
 *
 */
@Entity
@Table(name = "DEBITEUR")
public class Debiteur implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idDebiteur;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "TEL")
	private String tel;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "CIN")
	private String cin;

	@Column(name = "PASSPORT")
	private String passport;

	@Column(name = "DATE_CIN")
	private Date dateCin;

	@Column(name = "LIEU_CIN")
	private String lieuCin;

	@Column(name = "NATURE")
	private String nature;

	@Column(name = "PROFESSION")
	private String profession;

	@Column(name = "REGISTRE_DE_COMMERCE")
	private String registreCommerce;

	@Column(name = "RAISON_SOCIAL")
	private String raisonSocial;

	
	@OneToMany(mappedBy = "debiteur", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<CompteBancaire>  compteBancaire = new ArrayList<>()  ;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_ADRESSE")
	private Adresse adresse;

	/** L'id. */
	public Long getIdDebiteur() {
		return idDebiteur;
	}

	public void setIdDebiteur(Long idDebiteur) {
		this.idDebiteur = idDebiteur;
	}

	/** Le first name. */
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/** Le last name. */
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/** Le numéro du téléphone. */
	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}


	/** L'email. */
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	/** Le numero du carte d'identité. */
	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}


	/** Le numero du passport. */
	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}


	/** La date du carte d'identité. */
	public Date getDateCin() {
		return dateCin;
	}

	public void setDateCin(Date dateCin) {
		this.dateCin = dateCin;
	}


	/** Le lieu du carte d'identité. */
	public String getLieuCin() {
		return lieuCin;
	}

	public void setLieuCin(String lieuCin) {
		this.lieuCin = lieuCin;
	}


	/** La nature. */
	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}


	/** La profession. */
	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}


	/** Le registre du Commerce. */
	public String getRegistreCommerce() {
		return registreCommerce;
	}

	public void setRegistreCommerce(String registreCommerce) {
		this.registreCommerce = registreCommerce;
	}


	/** La raison Social. */
	public String getRaisonSocial() {
		return raisonSocial;
	}

	public void setRaisonSocial(String raisonSocial) {
		this.raisonSocial = raisonSocial;
	}

	/**
	 * Liste des comptes bancaires
	 * @return
	 */
	public List<CompteBancaire> getCompteBancaire() {
		return compteBancaire;
	}

	public void setCompteBancaire(List<CompteBancaire> compteBancaire) {
		this.compteBancaire = compteBancaire;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	/**
	 * constructeur vide
	 */
	public Debiteur() {
		super();
	}


	/**
	 * 
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param tel
	 * @param email
	 * @param cin
	 * @param passport
	 * @param dateCin
	 * @param lieuCin
	 * @param nature
	 * @param profession
	 * @param registreCommerce
	 * @param raisonSocial
	 * @param compteBancaire
	 */

	/**
	 * constructeur parametrés
	 */
	public Debiteur(String firstName, String lastName, String tel, String email, String cin, String passport, Date dateCin, String lieuCin, String nature, String profession, String registreCommerce, String raisonSocial, List<CompteBancaire> compteBancaire) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.tel = tel;
		this.email = email;
		this.cin = cin;
		this.passport = passport;
		this.dateCin = dateCin;
		this.lieuCin = lieuCin;
		this.nature = nature;
		this.profession = profession;
		this.registreCommerce = registreCommerce;
		this.raisonSocial = raisonSocial;
		this.compteBancaire = compteBancaire;
	}
	/**
	 *
	 * @param firstName
	 * @param lastName
	 * @param tel
	 * @param email
	 * @param cin
	 * @param nature
	 * @param adresse
	 */
	public Debiteur(String firstName, String lastName, String tel, String email, String cin, String nature,Adresse adresse) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.tel = tel;
		this.email = email;
		this.cin = cin;
		this.nature = nature;
		this.adresse = adresse;
	}
	/**
	 *
	 * @param firstName
	 * @param lastName
	 * @param tel
	 * @param email
	 * @param cin
	 * @param nature
	 */
	public Debiteur(String firstName, String lastName, String tel, String email, String cin, String nature) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.tel = tel;
		this.email = email;
		this.cin = cin;
		this.nature = nature;
	}

	@Override
	public String toString() {
		return "Debiteur{" +
				"idDebiteur=" + idDebiteur +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", tel='" + tel + '\'' +
				", email='" + email + '\'' +
				", cin='" + cin + '\'' +
				", passport='" + passport + '\'' +
				", dateCin=" + dateCin +
				", lieuCin='" + lieuCin + '\'' +
				", nature='" + nature + '\'' +
				", profession='" + profession + '\'' +
				", registreCommerce='" + registreCommerce + '\'' +
				", raisonSocial='" + raisonSocial + '\'' +
				", compteBancaire=" + compteBancaire +
				'}';
	}
}
