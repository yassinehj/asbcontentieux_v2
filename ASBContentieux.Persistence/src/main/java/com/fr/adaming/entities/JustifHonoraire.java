package com.fr.adaming.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p>
 * Une JustifHonoraire est caractérisé par les informations suivantes :
 * <ul>
 * <li>idJustifHonoraire,fileType, fileName,dateCreation</li>
 * </ul>
 * 
 * @author azaaboub
 * @since 29/03/2019
 *
 */

@Entity
@Table(name = "JUTIF_HONORAIRE")
public class JustifHonoraire implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_JUSTIF_HONORAIRE", nullable = false)
	private Long idJustifHonoraire;

	@Column(name = "ID_ALFRESCO")
	private String idAlfresco;

	@Column(name = "FILETYPE")
	private String fileType;

	@Column(name = "FILENAME")
	private String fileName;

	@Column(name = "DATE_CREATION")
	private Date dateCreation;

	@OneToMany(mappedBy = "justifHonoraire", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Honnoraire> honoraires = new ArrayList<>();

	/**
	 * Gets idJustifHonoraire
	 * 
	 * @return idJustifHonoraire
	 */
	public Long getIdJustifHonoraire() {
		return idJustifHonoraire;
	}

	/**
	 * Sets idJustifHonoraire
	 * 
	 * @param idJustifHonoraire
	 */

	public void setIdJustifHonoraire(Long idJustifHonoraire) {
		this.idJustifHonoraire = idJustifHonoraire;
	}

	/**
	 * Gets id_Alfresco
	 * 
	 * @return id_Alfresco
	 */

	public String getIdAlfresco() {
		return idAlfresco;
	}

	/**
	 * Sets idAlfresco
	 * 
	 * @param id_Alfresco
	 */

	public void setIdAlfresco(String idAlfresco) {
		this.idAlfresco = idAlfresco;
	}

	/**
	 * Gets file type
	 * 
	 * @return fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * Sets file type
	 * 
	 * @param fileType
	 */

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * Gets fileName
	 * 
	 * @return FileName
	 */

	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets fileName
	 * 
	 * @param fileName
	 */

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets date cration
	 * 
	 * @return dateCreation
	 */

	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * Sets date creation
	 * 
	 * @param dateCreation
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<Honnoraire> getHonoraires() {
		return honoraires;
	}

	public void setHonoraires(List<Honnoraire> honoraires) {
		this.honoraires = honoraires;
	}

	/**
	 * 
	 * @param idAlfresco
	 * @param fileType
	 * @param fileName
	 * @param dateCreation
	 * @param honoraires
	 */
	public JustifHonoraire(String idAlfresco, String fileType, String fileName, Date dateCreation,
			List<Honnoraire> honoraires) {
		super();
		this.idAlfresco = idAlfresco;
		this.fileType = fileType;
		this.fileName = fileName;
		this.dateCreation = dateCreation;
		this.honoraires = honoraires;
	}

	public JustifHonoraire() {
		super();
	}

	@Override
	public String toString() {
		return "JustifHonoraire [idJustifHonoraire=" + idJustifHonoraire + ", idAlfresco=" + idAlfresco + ", fileType="
				+ fileType + ", fileName=" + fileName + ", dateCreation=" + dateCreation + ", honoraires=" + honoraires
				+ "]";
	}

}
