package com.fr.adaming.dto;

import com.fr.adaming.entities.Pays;
import com.fr.adaming.entities.Gouvernorat;
import com.fr.adaming.entities.Ville;
import com.fr.adaming.entities.Tribunal;

public class TribunalDto {

    private Tribunal tribunal;
    private Pays pays;
    private Gouvernorat gouvernorat;
    private Ville ville;
    private String desciption;


    public Tribunal getTribunal() {
        return tribunal;
    }

    public TribunalDto(Tribunal tribunal, Pays pays, Gouvernorat gouvernorat, Ville ville, String desciption) {
        this.tribunal = tribunal;
        this.pays = pays;
        this.gouvernorat = gouvernorat;
        this.ville = ville;
        this.desciption = desciption;
    }

    public TribunalDto() {
    }

    public void setTribunal(Tribunal tribunal) {
        this.tribunal = tribunal;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public Gouvernorat getGouvernorat() {
        return gouvernorat;
    }

    public void setGouvernorat(Gouvernorat gouvernorat) {
        this.gouvernorat = gouvernorat;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }
}
