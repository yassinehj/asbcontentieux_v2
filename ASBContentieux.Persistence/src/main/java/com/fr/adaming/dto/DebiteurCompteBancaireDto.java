package com.fr.adaming.dto;

import com.fr.adaming.entities.CompteBancaire;
import com.fr.adaming.entities.Debiteur;

public class DebiteurCompteBancaireDto {
    private Debiteur debiteur;
    private CompteBancaire compteBancaire;

    public DebiteurCompteBancaireDto() {
    }

    public DebiteurCompteBancaireDto(Debiteur debiteur, CompteBancaire compteBancaire) {
        this.debiteur = debiteur;
        this.compteBancaire = compteBancaire;
    }

    public Debiteur getDebiteur() {
        return debiteur;
    }

    public void setDebiteur(Debiteur debiteur) {
        this.debiteur = debiteur;
    }

    public CompteBancaire getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(CompteBancaire compteBancaire) {
        this.compteBancaire = compteBancaire;
    }
}
