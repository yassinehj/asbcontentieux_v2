package com.fr.adaming.dto;

public class ProfilDto {

	private Long idUser;

	private String firstName;

	private String lastName;

	private String nomRole;

	private Long idInstitution;

	private String raisonSociale;

	private String codeInstitution;

	private String nomProfile;

	private String nomModule;

	public ProfilDto() {
		super();
	}

	public ProfilDto(Long idUser, String firstName, String lastName, String nomRole, Long idInstitution,
			String raisonSociale, String codeInstitution, String nomProfile, String nomModule) {
		super();
		this.idUser = idUser;
		this.firstName = firstName;
		this.lastName = lastName;
		this.nomRole = nomRole;
		this.idInstitution = idInstitution;
		this.raisonSociale = raisonSociale;
		this.codeInstitution = codeInstitution;
		this.nomProfile = nomProfile;
		this.nomModule = nomModule;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNomRole() {
		return nomRole;
	}

	public void setNomRole(String nomRole) {
		this.nomRole = nomRole;
	}

	public Long getIdInstitution() {
		return idInstitution;
	}

	public void setIdInstitution(Long idInstitution) {
		this.idInstitution = idInstitution;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getCodeInstitution() {
		return codeInstitution;
	}

	public void setCodeInstitution(String codeInstitution) {
		this.codeInstitution = codeInstitution;
	}

	public String getNomProfile() {
		return nomProfile;
	}

	public void setNomProfile(String nomProfile) {
		this.nomProfile = nomProfile;
	}

	public String getNomModule() {
		return nomModule;
	}

	public void setNomModule(String nomModule) {
		this.nomModule = nomModule;
	}

}
