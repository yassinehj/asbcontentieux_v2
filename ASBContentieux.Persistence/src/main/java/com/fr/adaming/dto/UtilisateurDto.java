package com.fr.adaming.dto;

import com.fr.adaming.entities.Utilisateur;

import java.util.Date;


public class UtilisateurDto {
	private Long id;

	private String login;

	private String password;

	private String firstName;

	private String lastName;

	private Long defaultAgence;

	private String email;

	private boolean activated = false;

	private String langKey;

	private String activationKey;

	private String resetKey;

	private Date resetDate = null;

	private String rib;

	private float honoraires;

	private Date dateCreation;

	private Date dateModification;

	private Long idProfession;

	public UtilisateurDto() {
		super();
	}

	public UtilisateurDto(Utilisateur user) {
		super();
		this.id = user.getId();
		this.login = user.getLogin();
		this.password = user.getPassword();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.activated = user.isActivated();
		this.langKey = user.getLangKey();
		this.resetKey = user.getResetKey();
		this.resetDate = user.getResetDate();
		this.dateCreation = user.getDateCreation();
		this.dateModification = user.getDateModification();
		if (user.getRefProfession() != null) {
			this.idProfession = user.getRefProfession().getIdRefProfession();
		}
	}

	public UtilisateurDto(Long id, String login, String password, String firstName, String lastName, Long defaultAgence,
			String email, boolean activated, String langKey, String activationKey, String resetKey, Date resetDate,
			String rib, float honoraires, Date dateCreation, Date dateModification, Long idProfession) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.defaultAgence = defaultAgence;
		this.email = email;
		this.activated = activated;
		this.langKey = langKey;
		this.activationKey = activationKey;
		this.resetKey = resetKey;
		this.resetDate = resetDate;
		this.rib = rib;
		this.honoraires = honoraires;
		this.dateCreation = dateCreation;
		this.dateModification = dateModification;
		this.idProfession = idProfession;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getDefaultAgence() {
		return defaultAgence;
	}

	public void setDefaultAgence(Long defaultAgence) {
		this.defaultAgence = defaultAgence;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public String getLangKey() {
		return langKey;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	public String getActivationKey() {
		return activationKey;
	}

	public void setActivationKey(String activationKey) {
		this.activationKey = activationKey;
	}

	public String getResetKey() {
		return resetKey;
	}

	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}

	public Date getResetDate() {
		return resetDate;
	}

	public void setResetDate(Date resetDate) {
		this.resetDate = resetDate;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}

	public float getHonoraires() {
		return honoraires;
	}

	public void setHonoraires(float honoraires) {
		this.honoraires = honoraires;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public Long getIdProfession() {
		return idProfession;
	}

	public void setIdProfession(Long idProfession) {
		this.idProfession = idProfession;
	}

}
