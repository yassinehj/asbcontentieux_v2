package com.fr.adaming.dto;

import com.fr.adaming.entities.RefMenu;

import java.util.List;


public class MenuDto {

	private String ecranPereFr;
	private String ecranPereEn;
	private String ecranPereAr;
	private String iconPere;
	private List<RefMenu> ecransfils;

	public MenuDto() {
		super();
	}

	public MenuDto(String ecranPereFr, String ecranPereEn, String ecranPereAr, String iconPere,
			List<RefMenu> ecransfils) {
		super();
		this.ecranPereFr = ecranPereFr;
		this.ecranPereEn = ecranPereEn;
		this.ecranPereAr = ecranPereAr;
		this.iconPere = iconPere;
		this.ecransfils = ecransfils;
	}

	public String getEcranPereFr() {
		return ecranPereFr;
	}

	public void setEcranPereFr(String ecranPereFr) {
		this.ecranPereFr = ecranPereFr;
	}

	public String getEcranPereEn() {
		return ecranPereEn;
	}

	public void setEcranPereEn(String ecranPereEn) {
		this.ecranPereEn = ecranPereEn;
	}

	public String getEcranPereAr() {
		return ecranPereAr;
	}

	public void setEcranPereAr(String ecranPereAr) {
		this.ecranPereAr = ecranPereAr;
	}

	public String getIconPere() {
		return iconPere;
	}

	public void setIconPere(String iconPere) {
		this.iconPere = iconPere;
	}

	public List<RefMenu> getEcransfils() {
		return ecransfils;
	}

	public void setEcransfils(List<RefMenu> ecransfils) {
		this.ecransfils = ecransfils;
	}

}
