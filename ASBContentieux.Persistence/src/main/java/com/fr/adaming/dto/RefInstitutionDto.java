package com.fr.adaming.dto;

import com.fr.adaming.entities.RefInstitution;

import java.util.List;


public class RefInstitutionDto {

	private Long idInstitutionRegionale;
	private List<RefInstitution> institutions;

	public RefInstitutionDto() {
		super();
	}

	public RefInstitutionDto(Long idInstitutionRegionale, List<RefInstitution> institutions) {
		super();
		this.idInstitutionRegionale = idInstitutionRegionale;
		this.institutions = institutions;
	}

	public Long getIdInstitutionRegionale() {
		return idInstitutionRegionale;
	}

	public void setIdInstitutionRegionale(Long idInstitutionRegionale) {
		this.idInstitutionRegionale = idInstitutionRegionale;
	}

	public List<RefInstitution> getInstitutions() {
		return institutions;
	}

	public void setInstitutions(List<RefInstitution> institutions) {
		this.institutions = institutions;
	}

}
