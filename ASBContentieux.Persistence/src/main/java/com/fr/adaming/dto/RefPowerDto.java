package com.fr.adaming.dto;

public class RefPowerDto {

	private String nomRole;
	private Long idMenu;
	private String nomMenu;
	private String nomMenuEn;
	private String nomModule;
	private boolean flagCreation;
	private boolean flagConsultation;
	private boolean flagModification;
	private boolean flagSuppression;

	public RefPowerDto() {
		super();
	}

	public RefPowerDto(String nomRole, Long idMenu, String nomMenu, String nomMenuEn, String nomModule,
			boolean flagCreation, boolean flagConsultation, boolean flagModification, boolean flagSuppression) {
		super();
		this.nomRole = nomRole;
		this.idMenu = idMenu;
		this.nomMenu = nomMenu;
		this.nomMenuEn = nomMenuEn;
		this.nomModule = nomModule;
		this.flagCreation = flagCreation;
		this.flagConsultation = flagConsultation;
		this.flagModification = flagModification;
		this.flagSuppression = flagSuppression;
	}

	public RefPowerDto(Long idMenu, String nomMenu, String nomMenuEn) {
		super();
		this.idMenu = idMenu;
		this.nomMenu = nomMenu;
		this.nomMenuEn = nomMenuEn;
	}

	public String getNomRole() {
		return nomRole;
	}

	public void setNomRole(String nomRole) {
		this.nomRole = nomRole;
	}

	public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}

	public String getNomMenu() {
		return nomMenu;
	}

	public void setNomMenu(String nomMenu) {
		this.nomMenu = nomMenu;
	}

	public String getNomMenuEn() {
		return nomMenuEn;
	}

	public void setNomMenuEn(String nomMenuEn) {
		this.nomMenuEn = nomMenuEn;
	}

	public String getNomModule() {
		return nomModule;
	}

	public void setNomModule(String nomModule) {
		this.nomModule = nomModule;
	}

	public boolean isFlagCreation() {
		return flagCreation;
	}

	public void setFlagCreation(boolean flagCreation) {
		this.flagCreation = flagCreation;
	}

	public boolean isFlagConsultation() {
		return flagConsultation;
	}

	public void setFlagConsultation(boolean flagConsultation) {
		this.flagConsultation = flagConsultation;
	}

	public boolean isFlagModification() {
		return flagModification;
	}

	public void setFlagModification(boolean flagModification) {
		this.flagModification = flagModification;
	}

	public boolean isFlagSuppression() {
		return flagSuppression;
	}

	public void setFlagSuppression(boolean flagSuppression) {
		this.flagSuppression = flagSuppression;
	}

}
