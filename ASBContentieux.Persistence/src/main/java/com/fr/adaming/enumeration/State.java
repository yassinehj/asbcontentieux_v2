package com.fr.adaming.enumeration;

public enum State {
    EN_COURS,
    RAPPORTE,
    CLOTURE,
    ANNULE
}

