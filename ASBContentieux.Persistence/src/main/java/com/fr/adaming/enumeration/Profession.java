package com.fr.adaming.enumeration;

public enum Profession {
    ADMINISTRATEUR("ADMINISTRATEUR"),
    RESPONSABLE_SERVICE_JURIDIQUE("RESPONSABLE_SERVICE_JURIDIQUE"),
    AGENT_SERVICE_JURIDIQUE("AGENT_SERVICE_JURIDIQUE"),
    AVOCAT("AVOCAT"),
    HUISSIER_JUSTICE("HUISSIER_JUSTICE");

    public String libelle;

    Profession(String libelle) {
        this.libelle = libelle;
    }

    public String getLibelle() {
        return this.libelle;
    }
}
