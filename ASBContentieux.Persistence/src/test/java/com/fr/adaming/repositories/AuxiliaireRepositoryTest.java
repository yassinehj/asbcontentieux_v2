package com.fr.adaming.repositories;


import com.fr.adaming.entities.Auxiliaire;
import com.fr.adaming.utils.LoggerUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
        *
        * @author nbelhajyahia: Cette classe permet d'effectuer les test unitaires
        *         nécessaires pour la couche persistence de l'auxiliaire. Pour créer
        *         un nouveau test , il faut ajouter l'annotation @Test au dessus de la
        *         méthode implémentée.On peut utiliser les méthodes fournies par JPA
        *         Repository ou bien directement entityManager.
        *
        */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AuxiliaireRepositoryTest {


    @Autowired
    @Qualifier("IAuxiliaireRepository")
    private IAuxiliaireRepository auxiliaireRepository;

    @Autowired
    private TestEntityManager entityManager;
    @Test
    public void ajouterAuxiliaire(){
        LoggerUtil.log("--------------- Constructing User ---------------");
        Auxiliaire auxiliaire= getAuxiliaire();
        Auxiliaire addAuxiliere = entityManager.persist(auxiliaire);
        LoggerUtil.log("--------------- Auxiliaire saved ---------------");
        Auxiliaire getAuxiliareFromDb = auxiliaireRepository.findById(addAuxiliere.getIdAuxiliaire()).get();


        /**
         * Modifier l'id de l'auxiliaire
         */

        entityManager.persist(getAuxiliareFromDb); /* Ou bien audienceRepository.save(getAudienceFromDb) */
        LoggerUtil.log("--------------- Auxiliere Updated ---------------");

        /**
        * Vérifier si l'email de l'auxiliaire à été modifié
        */
        assertThat(getAuxiliareFromDb.getEmail()).isEqualTo("nbelhadjyahia@gmail.com");

    }

    private Auxiliaire getAuxiliaire() {
        Auxiliaire auxiliaire = new Auxiliaire();
        auxiliaire.setNom("bhy");
        auxiliaire.setPrenom("Nidhal");
        auxiliaire.setNumTel("22021548");
        auxiliaire.setSpecialite("aaaaa");
        auxiliaire.setEmail("nbelhadjyahia@gmail.com");
        auxiliaire.setType("azerty");
        auxiliaire.setNumTel2("22587987");
        return auxiliaire;
    }

}
