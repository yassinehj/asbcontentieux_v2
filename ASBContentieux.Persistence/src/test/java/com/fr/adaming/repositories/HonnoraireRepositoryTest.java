package com.fr.adaming.repositories;

import com.fr.adaming.entities.Honnoraire;
import com.fr.adaming.utils.LoggerUtil;
import org.apache.cxf.jaxws.interceptors.HolderOutInterceptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author azaaboub: Cette classe permet d'effectuer les test unitaires
 *           nécessaires pour la couche persistence de l'honnoraire.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class HonnoraireRepositoryTest {

    @Autowired
    @Qualifier("IHonnoraireRepository")
    private IHonnoraireRepository honnoraireRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void testAddHonnoraire(){


        LoggerUtil.log("--------------- Constructing Honnoraire ---------------");
        Honnoraire honnoraire = getHonnoraires();
        LoggerUtil.log("-----------------------Testing add Honnoraire Method----------");
        Honnoraire addHonnoraire = entityManager.persist(honnoraire);
        LoggerUtil.log("---------Honnoraire saved--------");

        /**
         * Vérification de le l'existence de l'honnoraire ajouté dans la BD
         */

            Honnoraire getHonnoraireFromDB = honnoraireRepository.findById(addHonnoraire.getIdHonnoraire()).get();

             assertThat(getHonnoraireFromDB).isEqualTo(addHonnoraire);



    }


    /**
     * Tester la méthode findAll de la couche repository
     */
    @Test
    public void testGetAllHonnoraire(){

        LoggerUtil.log("--------------- Constructing Honnoraire ---------------");

        Honnoraire honnoraire1 = new Honnoraire();
        honnoraire1.setDateHonnoraire(new Date());
        honnoraire1.setMontant(3500);
        honnoraire1.setTypePaiement("carte");
        honnoraire1.setTypeOperation("cheque");
        honnoraire1.setPaye(true);

        Honnoraire honnoraire2 = new Honnoraire();
        honnoraire2.setDateHonnoraire(new Date());
        honnoraire2.setMontant(6000);
        honnoraire2.setTypePaiement("cheque");
        honnoraire2.setTypeOperation("virement");
        honnoraire2.setPaye(true);

        honnoraireRepository.save(honnoraire1);
        honnoraireRepository.save(honnoraire2);

        LoggerUtil.log("-----------honnoraire saved---------");
        Iterable<Honnoraire> allHonnoraireFromDB = honnoraireRepository.findAll();
        List<Honnoraire> honnoraireList = new ArrayList<>();

        for (Honnoraire honnaire: allHonnoraireFromDB){
            honnoraireList.add(honnaire);


        }
        assertThat(honnoraireList.size()).isEqualTo(2);


    }







    private Honnoraire getHonnoraires(){

        Honnoraire honnoraire = new Honnoraire();

        honnoraire.setDateHonnoraire(new Date());
        honnoraire.setMontant(2500);
        honnoraire.setTypePaiement("cheque");
        honnoraire.setTypeOperation("virement");
        honnoraire.setPaye(true);
        honnoraire.setTitre("reglement");


        return  honnoraire;

    }




}
