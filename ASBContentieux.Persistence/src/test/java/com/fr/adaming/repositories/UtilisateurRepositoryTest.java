package com.fr.adaming.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.fr.adaming.entities.Utilisateur;
import com.fr.adaming.utils.LoggerUtil;

/**
 *
 * @author mabelkaid: Cette classe permet d'effectuer les test unitaires
 *         nécessaires pour la couche persistence de l'utilisateur. Pour créer
 *         un nouveau test , il faut ajouter l'annotation @Test au dessus de la
 *         méthode implémentée.On peut utiliser les méthodes fournies par JPA
 *         Repository ou bien directement entityManager.
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UtilisateurRepositoryTest {


	@Autowired
	@Qualifier("IUtilisateurRepository")
	private IUtilisateurRepository utilisateurRepository;

	@Autowired
	private TestEntityManager entityManager;

	/**
	 * Tester la méthode ajouter utilisateur de la couche repository
	 */
	@Test
	public void testAddUser() {
		LoggerUtil.log("--------------- Constructing User ---------------");
		Utilisateur utilisateur = getUser();
		LoggerUtil.log("--------------- Testing add User Method ---------------");
		Utilisateur addedUser = entityManager
				.persist(utilisateur); /* Ou bien utilisateurRepository.save(utilisateur) */
		LoggerUtil.log("--------------- User saved ---------------");

		/**
		 * Vérification de le l'existence de l'utilisateur ajouté dans la BD
		 */
		Utilisateur getUserFromDb = utilisateurRepository.findById(addedUser.getId()).get();
		assertThat(getUserFromDb).isEqualTo(addedUser);
	}

	/**
	 * Tester la méthode ajouter utilisateur de la couche repository avec les
	 * propriétés firstName et lastName
	 */
	@Test
	public void testAddUserWithFieldOrProperty() {
		LoggerUtil.log("--------------- Constructing User ---------------");
		Utilisateur utilisateur = getUser();
		LoggerUtil.log("--------------- Testing add User Method ---------------");
		Utilisateur addedUser = entityManager
				.persist(utilisateur); /* Ou bien utilisateurRepository.save(utilisateur) */
		LoggerUtil.log("--------------- User saved ---------------");

		/**
		 * Vérification de l'ajout d'un utilisateur à travers son nom et son prénom
		 */
		assertThat(addedUser).hasFieldOrPropertyWithValue("firstName", "Amine");
		assertThat(addedUser).hasFieldOrPropertyWithValue("lastName", "Belkaied");

	}

	/**
	 * Tester la méthode findById de la couche repository
	 */
	@Test
	public void testGetUserById() {
		LoggerUtil.log("--------------- Constructing User ---------------");
		Utilisateur utilisateur = getUser();
		Utilisateur addedUser = entityManager
				.persist(utilisateur); /* Ou bien utilisateurRepository.save(utilisateur) */
		LoggerUtil.log("--------------- User saved ---------------");
		/**
		 * Récupérer l'objet utilisateur de la BD a travers la méthode findById
		 */
		Utilisateur userFromInDb = utilisateurRepository.findById(addedUser.getId()).get();
		/**
		 * Comparer l'objet retourné par la méthode findById avec l'objet persisté
		 */
		assertThat(userFromInDb).isEqualTo(addedUser);

	}

	/**
	 * Tester la méthode findAll de la couche repository
	 */
	@Test
	public void testGetAllUsers() {
		LoggerUtil.log("--------------- Constructing Users ---------------");
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setLogin("aminebk");
		utilisateur1.setPassword("1234");
		utilisateur1.setFirstName("Amine");
		utilisateur1.setLastName("Belkaied");
		utilisateur1.setEmail("aminebelkaied@gmail.com");
		utilisateur1.setActivated(true);

		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setLogin("yassinehj");
		utilisateur2.setPassword("1234");
		utilisateur2.setFirstName("Yassine");
		utilisateur2.setLastName("Hajji");
		utilisateur2.setEmail("yassinehajji@gmail.com");
		utilisateur2.setActivated(true);

		entityManager.persist(utilisateur1); /* Ou bien utilisateurRepository.save(utilisateur1) */
		entityManager.persist(utilisateur2); /* Ou bien utilisateurRepository.save(utilisateur2) */
		LoggerUtil.log("--------------- Users saved ---------------");

		Iterable<Utilisateur> allUsersFromDb = utilisateurRepository.findAll();
		List<Utilisateur> usersList = new ArrayList<>();

		for (Utilisateur user : allUsersFromDb) {
			usersList.add(user);
		}
		/**
		 * Vérifier si la méthode findAll() retourne deux utilisateurs
		 */
		assertThat(usersList.size()).isEqualTo(2);
	}

	/**
	 * Tester la méthode findByEmail de la couche repository
	 */
	@Test
	public void testFindUserByEmail() {
		LoggerUtil.log("--------------- Constructing User ---------------");
		Utilisateur utilisateur = getUser();

		entityManager.persist(utilisateur); /* Ou bien utilisateurRepository.save(utilisateur) */
		LoggerUtil.log("--------------- User saved ---------------");

		/**
		 * Vérifier si le nom de l'utilisateur persisté est égal à celui retourné par la
		 * méthode findByEmail
		 */
		Utilisateur getUserFromDb = utilisateurRepository.findByEmail("aminebelkaied@gmail.com");
		assertThat(getUserFromDb.getFirstName()).isEqualTo("Amine");
	}

	/**
	 * Tester la méthode delete de la couche repository
	 */
	@Test
	public void testDeleteUserById() {
		LoggerUtil.log("--------------- Constructing Users ---------------");
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setLogin("aminebk");
		utilisateur1.setPassword("1234");
		utilisateur1.setFirstName("Amine");
		utilisateur1.setLastName("Belkaied");
		utilisateur1.setEmail("aminebelkaied@gmail.com");
		utilisateur1.setActivated(true);

		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setLogin("yassinehj");
		utilisateur2.setPassword("1234");
		utilisateur2.setFirstName("Yassine");
		utilisateur2.setLastName("Hajji");
		utilisateur2.setEmail("yassinehajji@gmail.com");
		utilisateur2.setActivated(true);

		Utilisateur savedUser = entityManager
				.persist(utilisateur1); /* Ou bien utilisateurRepository.save(utilisateur1) */
		entityManager.persist(utilisateur2); /* Ou bien utilisateurRepository.save(utilisateur2) */

		LoggerUtil.log("--------------- Users saved ---------------");

		/**
		 * Supprimer un utilisateur de la DB
		 */
		entityManager.remove(savedUser); /* Ou bien utilisateurRepository.delete(savedUser) */

		Iterable<Utilisateur> allUsersFromDb = utilisateurRepository.findAll();
		List<Utilisateur> usersList = new ArrayList<>();

		for (Utilisateur user : allUsersFromDb) {
			usersList.add(user);
		}
		/**
		 * Vérifier si l'utilisateur est supprimé
		 */
		assertThat(usersList.size()).isEqualTo(1);
	}

	/**
	 * Tester la méthode update de la couche repository
	 */
	@Test
	public void testUpdateUser() {
		LoggerUtil.log("--------------- Constructing User ---------------");
		Utilisateur utilisateur = getUser();
		entityManager.persist(utilisateur); /* Ou bien utilisateurRepository.save(utilisateur) */
		LoggerUtil.log("--------------- User saved ---------------");
		Utilisateur getUserFromDb = utilisateurRepository.findByEmail("aminebelkaied@gmail.com");

		LoggerUtil.log("--------------- Testing Update User Method ---------------");
		/**
		 * Modifier l'email de l'utilisateur
		 */
		getUserFromDb.setEmail("yassinehajji@gmail.com");
		entityManager.persist(getUserFromDb); /* Ou bien utilisateurRepository.save(getUserFromDb) */
		LoggerUtil.log("--------------- User Updated ---------------");
		/**
		 * Vérifier si l'email de l'utilisateur à été modifié
		 */
		assertThat(getUserFromDb.getEmail()).isEqualTo("yassinehajji@gmail.com");
	}

	private Utilisateur getUser() {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setLogin("aminebk");
		utilisateur.setPassword("1234");
		utilisateur.setFirstName("Amine");
		utilisateur.setLastName("Belkaied");
		utilisateur.setEmail("aminebelkaied@gmail.com");
		utilisateur.setActivated(true);
		return utilisateur;
	}

}
