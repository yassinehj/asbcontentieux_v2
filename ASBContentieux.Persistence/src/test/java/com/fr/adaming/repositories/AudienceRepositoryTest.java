package com.fr.adaming.repositories;

import com.fr.adaming.entities.Audience;

import com.fr.adaming.entities.Honnoraire;
import com.fr.adaming.utils.LoggerUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * @author nbelhajyahia: Cette classe permet d'effectuer les test unitaires
 *         nécessaires pour la couche persistence de l'audience. Pour créer
 *         un nouveau test , il faut ajouter l'annotation @Test au dessus de la
 *         méthode implémentée.On peut utiliser les méthodes fournies par JPA
 *         Repository ou bien directement entityManager.
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AudienceRepositoryTest {

    @Autowired
    @Qualifier("IAudienceRepository")
    private IAudienceRepository audienceRepository;

    @Autowired
    private TestEntityManager entityManager;

    /**
     * Tester la méthode ajouter Audience de la couche repository
     */
    @Test
    public void testAddAudience(){


        LoggerUtil.log("--------------- Constructing Audience ---------------");
        Audience audience = getAudience();
        LoggerUtil.log("-----------------------Testing add Audience Method----------");
        Audience addedAudience= entityManager.persist(audience);
        LoggerUtil.log("---------Audience saved--------");

        /**
         * Vérification de le l'existence de l'audience ajouté dans la BD
         */

        Audience getAudienceFromDB = audienceRepository.findById(addedAudience.getIdAudience()).get();

        assertThat(getAudienceFromDB).isEqualTo(addedAudience);



    }

    private Audience getAudience() {
        Audience audience = new Audience();
        audience.setDateCreation(new Date());
        audience.setDescription("aaa");
        audience.setDateAudience(new Date());
        audience.setEtat("aaaaa");
        audience.setDateFin(new Date());
        audience.setDureeNotif(2f);
        audience.setNotif("aaaaaa");
        return audience;
    }
}
